#
# WOMBAT2 Makefile
#
# You should not need to edit this file !
#
# The architecture is either set by shell variable ARCH, or, if omitted, by hostname
# ./arch/$(ARCH) defines machine specific compilation variables
# For debugging call "make debug"

SHELL = /bin/bash

ifndef ARCH
ARCH = ${shell hostname}
endif

SRCDIR   = src/

# determine the problem file we will be using
$(shell grep '^PROBLEM' Config  | tail -1 > ./Problem)
include ./Problem

# these 2 dont belong here !
AIODIR   = $(LIBDIR)/aio10
E3DDIR    = $(LIBDIR)/e3d/e3d_v31s6

ifndef EXE
EXE      = wombat
endif

LIBDIR   = ${shell pwd}/lib/

include arch/$(ARCH) # these files define the machine flags

ifeq ($(MAKECMDGOALS),debug)
CFLAGS = -DDEBUG $(DEBUG_CFLAGS)
FFLAGS = -DDEBUG $(DEBUG_FFLAGS)
endif

LDFLAGS	+= -L$(AIODIR)/lib -laio -lz # always needed !

.SUFFIXES:
.SUFFIXES: .o .f90 .c .h

FORTRAN_FILES= globals.f90 \
	       error.f90 \
	       rank_location.f90 \
	       simulation_units.f90 \
	       patch.f90 \
	       datapool.f90 \
	       mhdpool.f90 \
	       passivepool.f90 \
	       mhdtvd.f90 \
	       passive.f90 \
	       decomposition.f90 \
	       problem_base.f90 \
	       problems/$(PROBLEM) \
	       domain.f90 \
	       domainsolver.f90 \
	       io.f90 \
	       wombat.f90

C_FILES = c_interface.c
H_FILES = config.h

C_SOURCES = $(addprefix $(SRCDIR),$(C_FILES))
F_SOURCES = $(addprefix $(SRCDIR),$(FORTRAN_FILES))
H_SOURCES = $(addprefix $(SRCDIR),$(H_FILES))

OBJECTS = $(C_SOURCES:.c=.c.o) $(F_SOURCES:.f90=.f90.o)

default debug: aio Makefile $(H_SOURCES) $(OBJECTS) 
	$(FC) $(FFLAGS) $(OBJECTS) $(LDFLAGS)  -o $(EXE)
	@ctags -w $(F_SOURCES) $(C_SOURCES)

clean:
	$(RM) $(SRCDIR)/*.mod $(OBJECTS) $(SRCDIR)/config.h $(SRCDIR)/problems/*.mod $(SRCDIR)/problems/*.o tags
	cd $(AIODIR)/src && make clean
	cd $(E3DDIR) && make clean

$(SRCDIR)config.h: Config 
	@sed '/^PROBLEM/d; /^#/d; /^$$/d; s/^/#define /g' Config > $(SRCDIR)config.h
	@echo '#define ASSERT(X,Y) CALL Assert_Info(X,Y,  __FILE__, __LINE__)'>> $(SRCDIR)config.h
	@echo '#define RASSERT(X,Y) CALL Assert_Info_Root(X,Y,  __FILE__, __LINE__)'>> $(SRCDIR)config.h
	@echo '#define MSG(X) CALL Msg_Info(X,  __FILE__, __LINE__)'>> $(SRCDIR)config.h
	@echo '#define RMSG(X) CALL Msg_Info_Root(X,  __FILE__, __LINE__)'>> $(SRCDIR)config.h
	@echo '#define DEBUGMSG(X) CALL Debugmsg_Info(X,  __FILE__, __LINE__)'>> $(SRCDIR)config.h

# these don't belong here !
aio:
	cd $(AIODIR)/src && make FC=$(FC) CC=$(CC) CFLAGS="$(AIOCFLAGS)" FFLAGS="$(AIOFFLAGS)"	

e3d: $(E3DDIR)/e3d.f
	cd $(E3DDIR) && make MYFC=$(E3DFC) MYCC=$(E3DCC) FLAGS="$(E3DFLAGS)" MYFPP=$(E3DFPP)

# helper macros
%.f90.o: %.f90 $(H_SOURCES)
	$(FC) $(FFLAGS) -c $< -o $@

%.c.o: %.c $(H_SOURCES)
	$(CC) $(CFLAGS) -c $< -o $@


