#include "config.h"

MODULE Mod_Decomposition

!######################################################################
!#
!# FILENAME: mod_decomposition.f90
!#
!# DESCRIPTION: This module provides a class for packaging and managing
!#  communications
!#
!# DEPENDENCIES: Mod_Globals for defining data types
!#
!# HISTORY:
!#    10/22/2013  - Peter Mendygral
!#
!######################################################################

!### require the needed externals ###
USE OMP_LIB
USE MPI
USE ISO_C_BINDING

!### require the needed modules ###
USE Mod_Globals
USE Mod_Error
USE Mod_Patch

IMPLICIT NONE

!### default private but make the data type available ###
PRIVATE
PUBLIC :: Decomposition

!### create a mailboxing object for handling messages from our neighbors ###
TYPE :: PatchMailbox

    INTEGER(WIS) :: nbounds
    INTEGER(MPI_ADDRESS_KIND), ALLOCATABLE :: b_start(:,:), b_end(:,:), b_signal(:,:), b_fsignal(:,:)
    LOGICAL(WIS), ALLOCATABLE :: in_use(:,:), in_flight(:,:), is_empty(:,:)
    INTEGER(WOMP_LOCK_SIZE), ALLOCATABLE :: tlock(:)

END TYPE PatchMailbox

!### create a type for managing patch mailboxes for all ranks in our neighborhood ###
TYPE :: CommNeighbor

    INTEGER(WIS) :: rank, rr_cn_slot, my_cn_slot, nmboxes, loc(3), myloc(3), n_total_exchanges
    INTEGER(WOMP_LOCK_SIZE) :: tlock
    LOGICAL(WIS) :: in_use, locked
    TYPE(PatchMailbox), ALLOCATABLE :: mbox(:)

END TYPE CommNeighbor

!### define a data type for this module ###
TYPE :: Decomposition

    !### default everything as private ###
    PRIVATE

    !### communicators and windows followed by grid layout and location ###
    INTEGER(WIS) :: world_comm, noaio_comm, grid_comm, grid_window
    INTEGER(WIS), ALLOCATABLE :: parapoint_window(:)
    INTEGER(WIS), PUBLIC :: nthreads, ndims, world_nranks, world_myrank, &
                            nranks, myrank, nranks_x, nranks_y, nranks_z, my_coords(3), naio, my_io_rank
    LOGICAL(WIS), PUBLIC :: is_aiosrv, lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound, window_open, mylock_open, ioLead
    CHARACTER(WMAX_NAMELIST_LEN), PUBLIC :: namelist

    INTEGER(WIS), ALLOCATABLE :: tcur_inflight(:)
    INTEGER(WIS) :: max_inflightcomms, nspins

    TYPE(c_ptr) :: rma_cbuf
    REAL(WRD), POINTER :: rma_buf(:)

    !### structures and management for neighborhood ###
    TYPE(CommNeighbor), ALLOCATABLE :: neighborhood(:)
    INTEGER(WOMP_LOCK_SIZE) :: rmalock, myrmalock  ! needed for MPI libs that do not have a thread safe MPI implementation
    INTEGER(WIS) :: patch_mailboxes_pslot, neighborhood_size, neighborhood_Xlength, neighborhood_Ylength, &
         neighborhood_Zlength, domains_Xmin, domains_Xmax, domains_Ymin, domains_Ymax, domains_Zmin, domains_Zmax, &
         neighbors_notme, bufcount_ppatch
    INTEGER(WIS), PUBLIC, ALLOCATABLE :: domain_neighbors1d(:,:), domain_neighbors2d(:,:,:), domain_neighbors3d(:,:,:,:)

    !### RMA bookeeping ###
    INTEGER(WIS) :: bb
    REAL(WRD) :: signal_noget, signal_init, sigtol
    
    !### Timers ###
    REAL(WRD), PUBLIC, ALLOCATABLE :: get_time(:), put_time(:), sync_time(:)
    REAL(WRD), PUBLIC, ALLOCATABLE :: flush_time(:), flush_all_time(:), flush_local_all_time(:)
    REAL(WRD), PUBLIC, ALLOCATABLE :: barrier_time(:)

    CONTAINS

    !### default everything as private ###
    PRIVATE

    PROCEDURE, PUBLIC :: destroy
    PROCEDURE :: initdecomposition
    PROCEDURE :: start_aio_framework
    PROCEDURE :: read_namelist
    PROCEDURE :: init_neighbors
    PROCEDURE :: register_threads
    PROCEDURE :: build_mailbox
    PROCEDURE :: free_mailbox
    PROCEDURE :: handle_mpi_error
    PROCEDURE :: progress_aio
    PROCEDURE, PUBLIC :: initRMA
    PROCEDURE, PUBLIC :: initRMA_pipeline
    PROCEDURE, PUBLIC :: rma_lock_neighbors
    PROCEDURE, PUBLIC :: rma_reset_neighbors
    PROCEDURE, PUBLIC :: rma_unlock_neighbors
    PROCEDURE, PUBLIC :: pack_patch_bound
    PROCEDURE, PUBLIC :: unpack_mailboxes
    PROCEDURE, PUBLIC :: signal_empty_mailboxes
    PROCEDURE, PUBLIC :: flush_all_neighbors
    PROCEDURE, PUBLIC :: flush_neighbor
    PROCEDURE, PUBLIC :: issue_signalled_gets
    PROCEDURE, PUBLIC :: check_completed_gets
    PROCEDURE, PUBLIC :: start_allreduce_min
    PROCEDURE, PUBLIC :: start_allreduce_max
    PROCEDURE, PUBLIC :: end_allreduce
    PROCEDURE, PUBLIC :: global_time

END TYPE Decomposition

!### create an interface to the constructor ###
INTERFACE Decomposition

    MODULE PROCEDURE constructor

END INTERFACE Decomposition

!### object methods ###
CONTAINS

!### constructor for the class ###
!###   arguments are: the full path (in AIO format) to the namelist containing the decomposition parameters ###
FUNCTION constructor(namefile)

    TYPE(Decomposition) :: constructor
    CHARACTER(WNAMELIST_PATHLEN), INTENT(IN) :: namefile

    CALL constructor%initdecomposition(namefile)
    RETURN

END FUNCTION constructor

!### this routine will build the communication grid using parameters in the given namelist file ###
!### this module assumes that the required info is in a namelist section titled 'RankDecompsition' ###
SUBROUTINE initdecomposition(self, namefile)

    CLASS(Decomposition) :: self
    CHARACTER(WNAMELIST_PATHLEN), INTENT(IN) :: namefile
    INTEGER(WIS) :: ierr, thread_support, tid
    INTEGER(WIS) :: dims(3)
    LOGICAL(WIS) :: periodic(3)

    !### we do FP signalling, set a tolerance around those signals ###
    self%sigtol = WSIGNAL_TOLERANCE

    !### first initialize MPI with thread support (required) ###
    self%naio = 0
    CALL MPI_INIT_THREAD(MPI_THREAD_MULTIPLE, thread_support, ierr)
    IF (ierr .NE. MPI_SUCCESS) CALL self%handle_mpi_error(ierr, .TRUE., .TRUE.)

    RASSERT(thread_support .EQ. MPI_THREAD_MULTIPLE,'MPI_THREAD_MULTIPLE is NOT available but is required to run!')

    WRANKSTR = 'AllRanks'

    DEBUGMSG('MPI communicator created')

    !### get the world communicator's information ###
    CALL MPI_COMM_DUP(MPI_COMM_WORLD, self%world_comm, ierr)
    CALL MPI_COMM_SIZE(self%world_comm, self%world_nranks, ierr)
    CALL MPI_COMM_RANK(self%world_comm, self%world_myrank, ierr)

    !### we'll be responsible for setting the global rank and rank string ###
    WRANK = self%world_myrank
    WRITE(WRANKSTR, '(I8)') WRANK

    !### read in the specified input file ###

    DEBUGMSG('Reading input namelist file : '//TRIM(namefile))

    CALL self%read_namelist(namefile)

    !### startup AIO as needed based on the settings in the namelist ###

    DEBUGMSG('Starting AIO framework')

    CALL self%start_aio_framework()

    !### if we're an AIO server that gets here (on finalize) just exit this routine ###
    !### the caller should check the AIO server flag and do nothing more with MPI after calling this routine for an AIO server ###
    IF (self%is_aiosrv) THEN

        STOP 'AIO Server Shutdown'

    END IF

    !### do thread registration (threads are not yet started - started outside of this module, but we are preparing arrays for managing comm lists) ###

    DEBUGMSG('Registering threads')

    CALL self%register_threads()

    !### now create the cartesian communicator from the comm excluding any AIO.  we always default to periodic ###
    DEBUGMSG('Creating Cartesian communicator for world grid')
    
    periodic = .TRUE.
    dims     = 1
    dims(1)  = MAX(1, self%nranks_x)
    IF (self%ndims .GT. 1) dims(2) = MAX(1, self%nranks_y)
    IF (self%ndims .GT. 2) dims(3) = MAX(1, self%nranks_z)

    CALL MPI_CART_CREATE(self%noaio_comm, self%ndims, dims(1:self%ndims), periodic(1:self%ndims), .TRUE., self%grid_comm, ierr)

    ASSERT(ierr .EQ. MPI_SUCCESS, 'Failed to create the Cartesian grid communicator!')

    !### gather up more information about the new communicator ###
    self%my_coords = 0

    !### now we can query what our local grid location is and save off properties of the grid ###
    CALL MPI_COMM_SIZE(self%grid_comm, self%nranks, ierr)
    CALL MPI_COMM_RANK(self%grid_comm, self%myrank, ierr)
    CALL MPI_CART_GET(self%grid_comm, self%ndims, dims, periodic, self%my_coords, ierr)

    self%nranks_x = dims(1)
    self%nranks_y = 1
    self%nranks_z = 1
    IF (self%ndims .GT. 1) self%nranks_y = dims(2)
    IF (self%ndims .GT. 2) self%nranks_z = dims(3)

    !### set the appropriate flags if our domain is at world boundaries ###
    IF (self%my_coords(1) .EQ. 0              ) self%lx_bound = .TRUE.
    IF (self%my_coords(1) .EQ. self%nranks_x-1) self%hx_bound = .TRUE.
    IF (self%my_coords(2) .EQ. 0              ) self%ly_bound = .TRUE.
    IF (self%my_coords(2) .EQ. self%nranks_y-1) self%hy_bound = .TRUE.
    IF (self%my_coords(3) .EQ. 0              ) self%lz_bound = .TRUE.
    IF (self%my_coords(3) .EQ. self%nranks_z-1) self%hz_bound = .TRUE.
    
    !### now determine who our neighbors are ###
    DEBUGMSG('Determining neighbors')

    CALL self%init_neighbors()
    
    !### allocate the timers ###
    ALLOCATE(self%get_time(self%nthreads))
    ALLOCATE(self%put_time(self%nthreads))
    ALLOCATE(self%sync_time(self%nthreads))
    ALLOCATE(self%flush_time(self%nthreads))
    ALLOCATE(self%flush_all_time(self%nthreads))
    ALLOCATE(self%flush_local_all_time(self%nthreads))
    ALLOCATE(self%barrier_time(self%nthreads))
    
    !### initialize the timers ###
    DO tid = 1, self%nthreads
        self%get_time(tid) = 0
        self%put_time(tid) = 0
        self%sync_time(tid) = 0
        self%flush_time(tid) = 0
        self%flush_all_time(tid) = 0
        self%flush_local_all_time(tid) = 0
        self%barrier_time(tid) = 0
    END DO

    DEBUGMSG('Decomposition is initialized')

    self%window_open = .FALSE.

END SUBROUTINE initdecomposition

!### initialize the RMA window by allocating the single MPI buffer and starting the window ###
!### we need to know the maximum patch size so that the correct buffer size can be computed for any patch boundaries being communicated ###
!### this routine IS NOT thread safe ###
SUBROUTINE initRMA(self, patch_nx, patch_ny, patch_nz, patch_nb, nvars)

    CLASS(Decomposition) :: self
    INTEGER(WIS), INTENT(IN) :: patch_nx, patch_ny, patch_nz, patch_nb, nvars
    INTEGER(MPI_ADDRESS_KIND) :: length, offset
    INTEGER(WIS) :: ierr, m, n, b, blength(26), vci_info
    REAL(WRD) :: t

    DEBUGMSG('Allocating MPI-RMA buffer and logically dividing into mailboxes')

    !### set our signalling.  We init to -1.  if we add 1 to that it say we've signalled, but there is no data to get.  anything > 0 means how many bytes to transfer ###
    self%signal_init  = -1.0_WRD
    self%signal_noget = 0.0_WRD

    !### get the storage size (in bytes) for a WRD variable ###
    self%bb = STORAGE_SIZE(t) / 8
    
    !### compute the full length of the buffer (note the factor of two accounting for a "send" and "recv" buffer) ###
    IF (self%ndims .EQ. 1) THEN

        self%bufcount_ppatch = 2 * (2 * patch_nb) * nvars + 4 * (WLB_HEADER_SIZE + 2)
        length = INT(self%patch_mailboxes_pslot,MPI_ADDRESS_KIND) * INT(self%neighborhood_size,MPI_ADDRESS_KIND) * INT(self%bufcount_ppatch,MPI_ADDRESS_KIND)

    ELSE IF (self%ndims .EQ. 2) THEN

        self%bufcount_ppatch = 2 * (2 * patch_nx + 2 * patch_ny + 4 * patch_nb) * patch_nb * nvars + 16 * (WLB_HEADER_SIZE + 2)
        length = INT(self%patch_mailboxes_pslot,MPI_ADDRESS_KIND) * INT(self%neighborhood_size,MPI_ADDRESS_KIND) * INT(self%bufcount_ppatch,MPI_ADDRESS_KIND)

    ELSE IF (self%ndims .EQ. 3) THEN

        self%bufcount_ppatch = 2 * ((2 * patch_nx * patch_ny) + (2 * patch_ny * patch_nz) + (2 * patch_nx * patch_nz) + (8 * patch_nb * patch_nb) + &
             (4 * patch_nx * patch_nb) + (4 * patch_ny * patch_nb) + (4 * patch_nz * patch_nb)) * patch_nb * nvars + 52 * (WLB_HEADER_SIZE + 2)
        length = INT(self%patch_mailboxes_pslot,MPI_ADDRESS_KIND) * INT(self%neighborhood_size,MPI_ADDRESS_KIND) * INT(self%bufcount_ppatch,MPI_ADDRESS_KIND)

    END IF

    !### allocate the single buffer and RMA window ###
    CALL MPI_WIN_ALLOCATE(length * INT(self%bb,MPI_ADDRESS_KIND), self%bb, MPI_INFO_NULL, self%grid_comm, self%rma_cbuf, self%grid_window, ierr) 

    ASSERT(ierr .EQ. MPI_SUCCESS,'Failed to allocate RMA window buffer!'  )

    CALL C_F_POINTER(self%rma_cbuf, self%rma_buf, (/length/))
    DO n = 1, length
        self%rma_buf(n) = self%signal_noget
    END DO

    !### create as many windows as there are threads to express parallelism
    ALLOCATE(self%parapoint_window(self%nthreads))
    
    CALL MPI_INFO_CREATE(vci_info, ierr)
    CALL MPI_INFO_SET(vci_info, "mpi_assert_new_vci", "true", ierr)
    DO n = 1, self%nthreads
        CALL MPI_WIN_CREATE(self%rma_buf, length * INT(self%bb,MPI_ADDRESS_KIND), self%bb, vci_info, self%grid_comm, self%parapoint_window(n), ierr)
    END DO 
    CALL MPI_INFO_FREE(vci_info, ierr)

    !### now that it is allocated we can divide it up into the individual pieces for comms ###
    IF (self%ndims .EQ. 1) THEN

        blength(1:2) = patch_nb * nvars + WLB_HEADER_SIZE
        offset       = 1 
        DO n = 1, self%neighborhood_size

            IF (self%neighborhood(n)%rank .NE. self%myrank) THEN

                DO m = 1, self%neighborhood(n)%nmboxes
                    
                    DO b = 1, 2
                        
                        ! RECVs
                        self%neighborhood(n)%mbox(m)%b_signal(b,1)  = offset
                        offset = offset + 1
                        self%neighborhood(n)%mbox(m)%b_fsignal(b,1) = offset
                        offset = offset + 1
                        self%neighborhood(n)%mbox(m)%b_start(b,1)   = offset
                        self%neighborhood(n)%mbox(m)%b_end(b,1)     = offset + blength(b) - 1
                        offset = offset + blength(b)
                        
                        ! SENDs
                        self%neighborhood(n)%mbox(m)%b_signal(b,2)  = offset
                        offset = offset + 1
                        self%neighborhood(n)%mbox(m)%b_fsignal(b,2) = offset
                        offset = offset + 1
                        self%neighborhood(n)%mbox(m)%b_start(b,2)   = offset
                        self%neighborhood(n)%mbox(m)%b_end(b,2)     = offset + blength(b) - 1
                        offset = offset + blength(b)
                        
                    END DO
                    
                END DO

            END IF

        END DO

    ELSE IF (self%ndims .EQ. 2) THEN

        blength(1:2) = patch_ny * patch_nb * nvars + WLB_HEADER_SIZE
        blength(3:4) = patch_nx * patch_nb * nvars + WLB_HEADER_SIZE
        blength(5:8) = patch_nb * patch_nb * nvars + WLB_HEADER_SIZE
        offset = 1
        DO n = 1, self%neighborhood_size

            IF (self%neighborhood(n)%rank .NE. self%myrank) THEN

                DO m = 1, self%neighborhood(n)%nmboxes

                    DO b = 1, 8

                        ! RECVs
                        self%neighborhood(n)%mbox(m)%b_signal(b,1)  = offset
                        offset = offset + 1
                        self%neighborhood(n)%mbox(m)%b_fsignal(b,1) = offset
                        offset = offset + 1
                        self%neighborhood(n)%mbox(m)%b_start(b,1)   = offset
                        self%neighborhood(n)%mbox(m)%b_end(b,1)     = offset + blength(b) - 1
                        offset = offset + blength(b)
                        
                        ! SENDs
                        self%neighborhood(n)%mbox(m)%b_signal(b,2)  = offset
                        offset = offset + 1
                        self%neighborhood(n)%mbox(m)%b_fsignal(b,2) = offset
                        offset = offset + 1
                        self%neighborhood(n)%mbox(m)%b_start(b,2)   = offset
                        self%neighborhood(n)%mbox(m)%b_end(b,2)     = offset + blength(b) - 1
                        offset = offset + blength(b)
                        
                    END DO
                    
                END DO

            END IF

        END DO

    ELSE IF (self%ndims .EQ. 3) THEN

        blength(1:2)   = patch_ny * patch_nz * patch_nb * nvars + WLB_HEADER_SIZE  ! slabs for X
        blength(3:4)   = patch_nx * patch_nz * patch_nb * nvars + WLB_HEADER_SIZE  ! slabs for Y
        blength(5:6)   = patch_nx * patch_ny * patch_nb * nvars + WLB_HEADER_SIZE  ! slabs for Z
        blength(7:14)  = patch_nb**3 * nvars + WLB_HEADER_SIZE                     ! corners
        blength(15:18) = patch_nx * patch_nb**2 * nvars + WLB_HEADER_SIZE          ! X edges
        blength(19:22) = patch_ny * patch_nb**2 * nvars + WLB_HEADER_SIZE          ! Y edges
        blength(23:26) = patch_nz * patch_nb**2 * nvars + WLB_HEADER_SIZE          ! Z edges
        offset = 1_MPI_ADDRESS_KIND
        DO n = 1, self%neighborhood_size

            IF (self%neighborhood(n)%rank .NE. self%myrank) THEN

                DO m = 1, self%neighborhood(n)%nmboxes
                    
                    DO b = 1, 26
                        
                        ! RECVs
                        self%neighborhood(n)%mbox(m)%b_signal(b,1)  = offset
                        offset = offset + 1_MPI_ADDRESS_KIND
                        self%neighborhood(n)%mbox(m)%b_fsignal(b,1) = offset
                        offset = offset + 1_MPI_ADDRESS_KIND
                        self%neighborhood(n)%mbox(m)%b_start(b,1)   = offset
                        self%neighborhood(n)%mbox(m)%b_end(b,1)     = offset + INT(blength(b),MPI_ADDRESS_KIND) - 1_MPI_ADDRESS_KIND
                        offset = offset + INT(blength(b),MPI_ADDRESS_KIND)
                        
                        ! SENDs
                        self%neighborhood(n)%mbox(m)%b_signal(b,2)  = offset
                        offset = offset + 1_MPI_ADDRESS_KIND
                        self%neighborhood(n)%mbox(m)%b_fsignal(b,2) = offset
                        offset = offset + 1_MPI_ADDRESS_KIND
                        self%neighborhood(n)%mbox(m)%b_start(b,2)   = offset
                        self%neighborhood(n)%mbox(m)%b_end(b,2)     = offset + INT(blength(b),MPI_ADDRESS_KIND) - 1_MPI_ADDRESS_KIND
                        offset = offset + INT(blength(b),MPI_ADDRESS_KIND)
                        
                    END DO
                    
                END DO
                
            END IF

        END DO

    END IF

    CALL MPI_BARRIER(self%grid_comm, ierr)
    self%window_open = .TRUE.
    self%mylock_open = .FALSE.
   
#ifdef WMBT_NO_RMA_THREAD_SAFE
    !### initialize a global lock for RMA if needed ###
    CALL OMP_INIT_LOCK(self%rmalock)
#endif
#ifdef WMBT_NO_RMA_TRULY_PASSIVE
    CALL OMP_INIT_LOCK(self%myrmalock)
#endif

END SUBROUTINE initRMA

!### destroy will clean up any allocated memory and close down MPI ###
SUBROUTINE destroy(self)

    CLASS(Decomposition) :: self
    INTEGER(WIS) :: ierr, n

    CALL MPI_BARRIER(self%grid_comm, ierr)

    !### clean up the thread register ###
    IF (ALLOCATED(self%tcur_inflight)) THEN
        
        DEALLOCATE(self%tcur_inflight)

    END IF

    !### note the shutdown is happeneing ###
    IF (self%world_myrank .EQ. 0) &   
        MSG('MPI is shutting down')

    !### clean up the neighborhood structures ###
    IF (ALLOCATED(self%neighborhood)) THEN

        DO n = 1, self%neighborhood_size

            CALL self%free_mailbox(n)
            CALL OMP_DESTROY_LOCK(self%neighborhood(n)%tlock)

        END DO
        DEALLOCATE(self%neighborhood)

    END IF
#ifdef WMBT_NO_RMA_THREAD_SAFE
    CALL OMP_DESTROY_LOCK(self%rmalock)
#endif
#ifdef WMBT_NO_RMA_TRULY_PASSIVE
    CALL OMP_DESTROY_LOCK(self%myrmalock)
#endif

    !### cleanup of domain neighbors structures ###
    IF (ALLOCATED(self%domain_neighbors1d)) DEALLOCATE(self%domain_neighbors1d)
    IF (ALLOCATED(self%domain_neighbors2d)) DEALLOCATE(self%domain_neighbors2d)
    IF (ALLOCATED(self%domain_neighbors3d)) DEALLOCATE(self%domain_neighbors3d)

    !### close the RMA windows and free the RMA buffer if open ###
    IF (self%window_open) THEN

        NULLIFY(self%rma_buf)
        CALL MPI_WIN_FREE(self%grid_window, ierr)
        DO n = 1, self%nthreads
            CALL MPI_WIN_FREE(self%parapoint_window(n), ierr)
        END DO
        DEALLOCATE(self%parapoint_window)

    END IF

    !### if there are AIO servers let's bring them down ###
    IF (self%naio .GT. 0) THEN

        IF (self%world_myrank .EQ. 0) MSG("AIO shutdown sequence has been initiated...")
        CALL AIO_FINALIZE()

    END IF

    CALL MPI_BARRIER(MPI_COMM_WORLD, ierr)
    CALL MPI_FINALIZE(ierr)

    IF (self%world_myrank .EQ. 0) &
        MSG("WOMBAT has completed")

END SUBROUTINE destroy

!### global_time just returns MPIWtime ###
REAL(WRD) FUNCTION global_time(self)

    CLASS(Decomposition) :: self
    global_time = MPI_WTIME()

    RETURN

END FUNCTION global_time

!### this routine will setup the AIO framework ###
SUBROUTINE start_aio_framework(self)

    CLASS(Decomposition) :: self
    INTEGER(WIS) :: maxbuf, ierr, rank, aio_rank, aio_stride, aio_color, aio_key, aios_before, aio, waio, &
         aio_servers(self%naio), aio_workers(self%world_nranks-self%naio)

    self%is_aiosrv  = .FALSE.
    self%ioLead     = .TRUE.

    ASSERT((self%world_nranks-self%naio) .GT. 0, 'There are not enough ranks to support the number of AIO servers requested')

    !### determine which ranks will be AIO servers ###
    !### we fan them out evenly across all ranks since the operations are many to one, and we should preserve some locality ###
    IF (self%naio .GT. 0) THEN

        aio_stride  = INT(REAL(self%world_nranks,WIS) / REAL(self%naio,WIS))
        aio_rank    = aio_stride - 1
        aios_before = 0
        aio         = 1
        DO rank = 1, self%naio

            IF (self%world_myrank .EQ. aio_rank) self%is_aiosrv = .TRUE.
            IF (self%world_myrank .GT. aio_rank) aios_before    = aios_before + 1
            aio_servers(rank)                                   = aio_rank
            aio_rank                                            = aio_rank + aio_stride

        END DO

        !### if we're a server we need to build our list of workers ###
        waio = 1
        IF (self%is_aiosrv) THEN

            !### loop through all ranks and pluck out those that are greater than the AIO server before us but less than us ###
            aio  = 1
            waio = 1
            DO rank = 0, self%world_nranks-1

                IF (rank .LT. self%world_myrank .AND. aio-1 .EQ. aios_before) THEN

                    aio_workers(waio) = rank
                    waio              = waio + 1

                ELSE IF (rank .EQ. aio_servers(aio)) THEN
                    
                    aio = aio + 1

                END IF

            END DO
            waio = waio - 1

        !### otherwise we need to know who our IO server is and if we are the lead for the server ###
        ELSE

            self%my_io_rank = -1
            self%ioLead     = .FALSE.
            DO rank = 1, self%naio

                IF (self%world_myrank .LT. aio_servers(rank) .AND. self%my_io_rank .EQ. -1) self%my_io_rank = aio_servers(rank)
                IF (self%world_myrank .EQ. 0                                              ) self%ioLead = .TRUE.

            END DO

        END IF

        !### set AIO verbosity ###
#ifdef DEBUG
        CALL AIO_SET_VERBOSITY(10)
#else
        CALL AIO_SET_VERBOSITY(2)
#endif

        !### set how big the server buffers need to be ###
        maxbuf = WMPI_MAX_MESSAGE
        maxbuf = maxbuf * 1024**2
        CALL AIO_SET_MAX_DATA_SIZE(maxbuf)

        !### set the output file behavior ###
        CALL AIO_SET_DATA_EXTENTION_RANGE("-000", "-999")

        !### initialize AIO with worker and server ranks ###
        CALL AIO_SET_EXPLICIT_CLIENTS(aio_servers, self%naio, aio_workers(1:waio), waio, self%world_comm)

        !### create a new comm group that excludes the AIO ranks that can be used to construct the Cartesian communicator ###
        aio_color = 1
        IF (self%is_aiosrv) aio_color = 0
        aio_key   = self%world_myrank - aios_before
        CALL MPI_COMM_SPLIT(self%world_comm, aio_color, aio_key, self%noaio_comm, ierr)

        ASSERT(ierr .EQ. MPI_SUCCESS,'Failed to create a new comminucator excluding AIO ranks!')

        !### startup the AIO system ###
        CALL AIO_START_SERVERS()

    ELSE

        !### just duplicate comm_wortld into the "non-aio" communicator ###
        CALL MPI_COMM_DUP(self%world_comm, self%noaio_comm, ierr)

    END IF

END SUBROUTINE start_aio_framework

!### read in a namelist file either with AIO or have rank 0 do it and then broadcast ###
SUBROUTINE read_namelist(self, namefile)

    CLASS(Decomposition) :: self
    CHARACTER(WNAMELIST_PATHLEN) :: namefile
    INTEGER(WIS) :: nmllen, nranks_x, nranks_y, nranks_z, naio, ndims, nthreads, max_inflightcomms, neighborhood_Xlength, &
         neighborhood_Ylength, neighborhood_Zlength, patch_mailboxes_pslot, nspins, n, nn
    LOGICAL(WIS) :: incomment, inquote, start_section
    CHARACTER(WMAX_NAMELIST_LEN) :: packednm

    NAMELIST/RankDecomposition/ nranks_x, nranks_y, nranks_z, naio, ndims, nthreads, max_inflightcomms, neighborhood_Xlength, &
         neighborhood_Ylength, neighborhood_Zlength, patch_mailboxes_pslot, nspins
    
    !### blank out the namelist ###
    DO n = 1, WMAX_NAMELIST_LEN

        self%namelist(n:n) = CHAR(0) 
        
    END DO

    !### call all_read prior to AIO server starts (if any).  a single rank reads and BCASTS out to COMM_WORLD ###
    CALL AIO_ALL_READ(namefile, WMAX_NAMELIST_LEN, nmllen, self%namelist)

    !### strip out any comment lines in the file (must start with !) as some compilers expect the string to be comment free for namelist parsing ###
    incomment     = .FALSE.
    inquote       = .FALSE.
    start_section = .FALSE.
    DO n = 1, WMAX_NAMELIST_LEN

        IF (self%namelist(n:n) .EQ. ACHAR(33)) incomment = .TRUE.
        IF (self%namelist(n:n) .EQ. ACHAR(10) .OR. self%namelist(n:n) .EQ. ACHAR(13)) incomment = .FALSE.
        IF (incomment) self%namelist(n:n) = CHAR(0) 

        IF ((self%namelist(n:n) .EQ. ACHAR(34) .OR. self%namelist(n:n) .EQ. ACHAR(39)) .AND. .NOT. inquote) THEN
            inquote = .TRUE.
            CYCLE
        END IF
        IF ((self%namelist(n:n) .EQ. ACHAR(32) .OR. self%namelist(n:n) .EQ. ACHAR(9)) .AND. .NOT. inquote) self%namelist(n:n) = CHAR(0)
        IF ((self%namelist(n:n) .EQ. ACHAR(34) .OR. self%namelist(n:n) .EQ. ACHAR(39)) .AND. inquote) THEN
            inquote = .FALSE.
            CYCLE
        END IF

        IF (n .LT. WMAX_NAMELIST_LEN) THEN
            IF (self%namelist(n:n) .EQ. ACHAR(38) .AND. (self%namelist(n+1:n+1) .NE. ACHAR(32) .AND. &
                                                         self%namelist(n+1:n+1) .NE. ACHAR(10) .AND. &
                                                         self%namelist(n+1:n+1) .NE. ACHAR(13) .AND. &
                                                         self%namelist(n+1:n+1) .NE. ACHAR(11) .AND. &
                                                         self%namelist(n+1:n+1) .NE. ACHAR(12))) start_section = .TRUE.
        END IF
        IF (self%namelist(n:n) .EQ. ACHAR(10) .OR. self%namelist(n:n) .EQ. ACHAR(13) .OR. self%namelist(n:n) .EQ. ACHAR(11) .OR. self%namelist(n:n) .EQ. ACHAR(12)) THEN
            IF (start_section) THEN
                self%namelist(n:n) = CHAR(9)
                start_section      = .FALSE.
            ELSE
                self%namelist(n:n) = CHAR(0) 
            END IF
        END IF
        
    END DO

    nn = 1
    DO n = 1, WMAX_NAMELIST_LEN

        IF (self%namelist(n:n) .NE. CHAR(0)) THEN
            packednm(nn:nn) = self%namelist(n:n)
            nn = nn + 1
        END IF
        self%namelist(n:n) = CHAR(0)

    END DO
    DO n = 1, nn-1 

        self%namelist(n:n) = packednm(n:n)

    END DO 

    ASSERT(nmllen .LT. WMAX_NAMELIST_LEN, 'The specified namelist file is at least the maximum supported length.  WMAX_NAMELIST_LEN must be increased to be certain we have enough parameter data.')

    ASSERT(nmllen .GT. 0, 'The specified namelist file was not successfully read. Does it exist and has correct format?')

    !### now pull in the namelist contents ###
    READ(self%namelist, NML=RankDecomposition)

    !### store the read-in values into the object's handles ###
    IF (naio .LT. 0                ) naio = 0
    IF (nthreads .LT. 1            ) nthreads = 1
    IF (max_inflightcomms .LT. 0   ) max_inflightcomms = 0
    IF (nspins .LT. 1              ) nspins = 1

    IF (nranks_x .LT. 1) nranks_x = 1
    neighborhood_Xlength = MAX(neighborhood_Xlength, 1)
    IF (neighborhood_Xlength .LT. 2 .AND. nranks_x .GT. 1) neighborhood_Xlength = 3
    IF (neighborhood_Xlength .GT. nranks_x               ) neighborhood_Xlength = nranks_x
    IF (MOD(neighborhood_Xlength,2) .EQ. 0               ) neighborhood_Xlength = neighborhood_Xlength + 1  ! and we must have an odd number for the size of the neighborhood along a dimension

    IF (ndims .GT. 1) THEN
        IF (nranks_y .LT. 1) nranks_y = 1
        neighborhood_Ylength = MAX(neighborhood_Ylength, 1)
        IF (neighborhood_Ylength .LT. 2 .AND. nranks_y .GT. 1) neighborhood_Ylength = 3
        IF (neighborhood_Ylength .GT. nranks_y               ) neighborhood_Ylength = nranks_y
        IF (MOD(neighborhood_Ylength,2) .EQ. 0               ) neighborhood_Ylength = neighborhood_Ylength + 1  ! and we must have an odd number for the size of the neighborhood along a dimension
    ELSE
        nranks_y             = 1
        neighborhood_Ylength = 1
    END IF

    IF (ndims .GT. 2) THEN
        IF (nranks_z .LT. 1) nranks_z = 1
        neighborhood_Zlength = MAX(neighborhood_Zlength, 1)
        IF (neighborhood_Zlength .LT. 2 .AND. nranks_z .GT. 1) neighborhood_Zlength = 3
        IF (neighborhood_Zlength .GT. nranks_z               ) neighborhood_Zlength = nranks_z
        IF (MOD(neighborhood_Zlength,2) .EQ. 0               ) neighborhood_Zlength = neighborhood_Zlength + 1  ! and we must have an odd number for the size of the neighborhood along a dimension
    ELSE
        nranks_z             = 1
        neighborhood_Zlength = 1
    END IF

    IF (patch_mailboxes_pslot .LT. 1) patch_mailboxes_pslot = 1

    self%ndims                 = ndims
    self%nranks_x              = nranks_x
    self%nranks_y              = nranks_y
    self%nranks_z              = nranks_z
    self%naio                  = naio
    self%nthreads              = nthreads
    self%max_inflightcomms     = max_inflightcomms
    self%nspins                = nspins
    self%neighborhood_Xlength  = neighborhood_Xlength
    self%neighborhood_Ylength  = neighborhood_Ylength
    self%neighborhood_Zlength  = neighborhood_Zlength
    self%patch_mailboxes_pslot = patch_mailboxes_pslot

    ASSERT(self%ndims .GT. 0 .AND. self%ndims .LE. 3,'The specified number of dimensions is outside of the supported range of 1<=ndims<=3.')    
    
    ASSERT(self%naio .LE. self%nranks_x*self%nranks_y*self%nranks_z,'The specified number of AIO servers exceeds the number of crew ranks.')

END SUBROUTINE read_namelist

!### determine who all of our neighbors are on the grid ###
SUBROUTINE init_neighbors(self)

    CLASS(Decomposition) :: self
    INTEGER(WIS) :: ierr, loc(3), offset(3), offsets(3,26), n, nbounds, i, j, k, ns, nsX, nsY, nsZ, m, b, nneighbors, &
         di, dj, dk, dcoords(3), rcoords(3), neighbor, nn, mn

    !### allocate space to our neighborhood structure ###
    self%neighbors_notme = 0
    IF (self%ndims .EQ. 1) THEN

        nsX                    = (self%neighborhood_Xlength-1)/2
        self%neighborhood_size = self%neighborhood_Xlength
        nbounds                = 2

        !### if there are no neighbors to work with then we don't need mailboxes etc. ###
        IF (self%neighborhood_size .GT. 0) THEN

            ALLOCATE(self%neighborhood(self%neighborhood_size))

            !### loop over the neighborhood setting up who is in it ###
            n = 0
            DO i = -nsX, nsX
            
                n = n + 1
            
                offset = (/i, 0, 0/)
                offset = offset + self%my_coords
                CALL MPI_CART_RANK(self%grid_comm, offset(1:self%ndims), self%neighborhood(n)%rank, ierr)

                self%neighborhood(n)%in_use     = .FALSE.
                self%neighborhood(n)%locked     = .FALSE.
                self%neighborhood(n)%rr_cn_slot = n
                self%neighborhood(n)%loc        = offset - self%my_coords
                CALL OMP_INIT_LOCK(self%neighborhood(n)%tlock)

                !### if the rank is not us we do need to proceed with setting up the mailboxes ###
                IF (self%neighborhood(n)%rank .NE. self%myrank) THEN
                    
                    self%neighbors_notme = self%neighbors_notme + 1
                    CALL self%build_mailbox(n, nbounds)

                END IF
            
            END DO

            !### now we must go back through our neighbors and determine which of their slots we occupy ###
            DO n = 1, self%neighborhood_size

                !### quick look to see if another earlier slots points to this rank.  we need to track that so WIN_LOCKS are opened only once ###
                IF (n .GT. 1) THEN

                    DO nn = 1, n-1

                        IF (self%neighborhood(n)%rank .EQ. self%neighborhood(nn)%rank) THEN
                
                            self%neighborhood(n)%rr_cn_slot = nn
                            EXIT
                
                        END IF

                    END DO

                END IF

                CALL MPI_CART_COORDS(self%grid_comm, self%neighborhood(n)%rank, 3, rcoords, ierr)
                mn = 0
                DO i = -nsX, nsX

                    mn     = mn + 1
                    offset = (/i, 0, 0/)
                    offset = offset + rcoords
                    CALL MPI_CART_RANK(self%grid_comm, offset, neighbor, ierr)
                    IF (self%myrank .EQ. neighbor) THEN

                        !### given that we're periodic we can show up multiple times.  make certain the offset to us from this rank matches their offset from us ###
                        offset = offset - rcoords
                        IF (-offset(1) .EQ. self%neighborhood(n)%loc(1)) THEN

                            self%neighborhood(n)%my_cn_slot = mn
                            self%neighborhood(n)%myloc      = offset

                        END IF

                    END IF

                END DO

            END DO

        END IF

        !### determine how many domains we might manage based on the neighborhood size ###
        !### we know that we can grow to be as big as the nighborhood, but we cannot accept patches that may communicate outside of our neighborhood ###
        self%domains_Xmin = FLOOR(0.5_WRS * REAL(-nsX,WRS))
        self%domains_Xmax = FLOOR(0.5_WRS * REAL(nsX,WRS))
        self%domains_Ymin = 0
        self%domains_Ymax = 0
        self%domains_Zmin = 0
        self%domains_Zmax = 0

    ELSE IF (self%ndims .EQ. 2) THEN

        nsX                    = (self%neighborhood_Xlength-1)/2
        nsY                    = (self%neighborhood_Ylength-1)/2
        self%neighborhood_size = self%neighborhood_Xlength * self%neighborhood_Ylength
        nbounds                = 8

        !### if there are no neighbors to work with then we don't need mailboxes etc. ###
        IF (self%neighborhood_size .GT. 0) THEN

            ALLOCATE(self%neighborhood(self%neighborhood_size))

            !### loop over the neighborhood setting up who is in it ###
            n = 0
            DO j = -nsY, nsY
                
                DO i = -nsX, nsX
                    
                    n = n + 1
            
                    offset = (/i, j, 0/)
                    offset = offset + self%my_coords
                    CALL MPI_CART_RANK(self%grid_comm, offset(1:self%ndims), self%neighborhood(n)%rank, ierr)
                    
                    self%neighborhood(n)%in_use     = .FALSE.
                    self%neighborhood(n)%locked     = .FALSE.
                    self%neighborhood(n)%rr_cn_slot = n
                    self%neighborhood(n)%loc        = offset - self%my_coords
                    CALL OMP_INIT_LOCK(self%neighborhood(n)%tlock)

                    !### if the rank is not us we do need to proceed with setting up the mailboxes ###
                    IF (self%neighborhood(n)%rank .NE. self%myrank) THEN
                        
                        self%neighbors_notme = self%neighbors_notme + 1
                        CALL self%build_mailbox(n, nbounds)
                        
                    END IF

                END DO

            END DO

            !### now we must go back through our neighbors and determine which of their slots we occupy ###
            DO n = 1, self%neighborhood_size

                !### quick look to see if another earlier slots points to this rank.  we need to track that so WIN_LOCKS are opened only once ###
                IF (n .GT. 1) THEN

                    DO nn = 1, n-1

                        IF (self%neighborhood(n)%rank .EQ. self%neighborhood(nn)%rank) THEN
                
                            self%neighborhood(n)%rr_cn_slot = nn
                            EXIT
                
                        END IF

                    END DO

                END IF

                CALL MPI_CART_COORDS(self%grid_comm, self%neighborhood(n)%rank, 3, rcoords, ierr)
                mn = 0
                DO j = -nsY, nsY

                    DO i = -nsX, nsX

                        mn     = mn + 1
                        offset = (/i, j, 0/)
                        offset = offset + rcoords
                        CALL MPI_CART_RANK(self%grid_comm, offset, neighbor, ierr)
                        IF (self%myrank .EQ. neighbor) THEN
                            
                            !### given that we're periodic we can show up multiple times.  make certain the offset to us from this rank matches their offset from us ###
                            offset = offset - rcoords

                            IF (-offset(1) .EQ. self%neighborhood(n)%loc(1) .AND. -offset(2) .EQ. self%neighborhood(n)%loc(2)) THEN
                                
                                self%neighborhood(n)%my_cn_slot = mn
                                self%neighborhood(n)%myloc      = offset

                            END IF

                        END IF

                    END DO

                END DO

            END DO

        END IF

        !### determine how many domains we might manage based on the neighborhood size ###
        !### we know that we can grow to be as big as the nighborhood, but we cannot accept patches that may communicate outside of our neighborhood ###
        self%domains_Xmin = FLOOR(0.5_WRS * REAL(-nsX,WRS))
        self%domains_Xmax = FLOOR(0.5_WRS * REAL(nsX,WRS))
        self%domains_Ymin = FLOOR(0.5_WRS * REAL(-nsY,WRS))
        self%domains_Ymax = FLOOR(0.5_WRS * REAL(nsY,WRS))
        self%domains_Zmin = 0
        self%domains_Zmax = 0

    ELSE IF (self%ndims .EQ. 3) THEN

        nsX                    = (self%neighborhood_Xlength-1)/2
        nsY                    = (self%neighborhood_Ylength-1)/2
        nsZ                    = (self%neighborhood_Zlength-1)/2
        self%neighborhood_size = self%neighborhood_Xlength * self%neighborhood_Ylength * self%neighborhood_Zlength
        nbounds                = 26

        !### if there are no neighbors to work with then we don't need mailboxes etc. ###
        IF (self%neighborhood_size .GT. 0) THEN

            ALLOCATE(self%neighborhood(self%neighborhood_size))

            !### loop over the neighborhood setting up who is in it ###
            n = 0
            DO k = -nsZ, nsZ
                
                DO j = -nsY, nsY

                    DO i = -nsX, nsX
                    
                        n = n + 1
            
                        offset = (/i, j, k/)
                        offset = offset + self%my_coords
                        CALL MPI_CART_RANK(self%grid_comm, offset(1:self%ndims), self%neighborhood(n)%rank, ierr)
                        
                        self%neighborhood(n)%in_use     = .FALSE.
                        self%neighborhood(n)%locked     = .FALSE.
                        self%neighborhood(n)%rr_cn_slot = n
                        self%neighborhood(n)%loc        = offset - self%my_coords
                        CALL OMP_INIT_LOCK(self%neighborhood(n)%tlock)
                        
                        !### if the rank is not us we do need to proceed with setting up the mailboxes ###
                        IF (self%neighborhood(n)%rank .NE. self%myrank) THEN
                            
                            self%neighbors_notme = self%neighbors_notme + 1
                            CALL self%build_mailbox(n, nbounds)
                            
                        END IF
                        
                    END DO
                    
                END DO
            
            END DO

            !### now we must go back through our neighbors and determine which of their slots we occupy ###
            DO n = 1, self%neighborhood_size

                !### quick look to see if another earlier slots points to this rank.  we need to track that so WIN_LOCKS are opened only once ###
                IF (n .GT. 1) THEN

                    DO nn = 1, n-1

                        IF (self%neighborhood(n)%rank .EQ. self%neighborhood(nn)%rank) THEN
                
                            self%neighborhood(n)%rr_cn_slot = nn
                            EXIT
                
                        END IF

                    END DO

                END IF

                CALL MPI_CART_COORDS(self%grid_comm, self%neighborhood(n)%rank, 3, rcoords, ierr)
                mn = 0
                DO k = -nsZ, nsZ

                    DO j = -nsY, nsY

                        DO i = -nsX, nsX
                        
                            mn     = mn + 1
                            offset = (/i, j, k/)
                            offset = offset + rcoords
                            CALL MPI_CART_RANK(self%grid_comm, offset, neighbor, ierr)
                            IF (self%myrank .EQ. neighbor) THEN
                                
                                !### given that we're periodic we can show up multiple times.  make certain the offset to us from this rank matches their offset from us ###
                                offset = offset - rcoords
                                IF (-offset(1) .EQ. self%neighborhood(n)%loc(1) .AND. -offset(2) .EQ. self%neighborhood(n)%loc(2) .AND. -offset(3) .EQ. self%neighborhood(n)%loc(3)) THEN
                                    
                                    self%neighborhood(n)%my_cn_slot = mn
                                    self%neighborhood(n)%myloc      = offset
                                    
                                END IF
                                
                            END IF
                            
                        END DO

                    END DO

                END DO

            END DO

        END IF

        !### determine how many domains we might manage based on the neighborhood size ###
        !### we know that we can grow to be as big as the nighborhood, but we cannot accept patches that may communicate outside of our neighborhood ###
        self%domains_Xmin = FLOOR(0.5_WRS * REAL(-nsX,WRS))
        self%domains_Xmax = FLOOR(0.5_WRS * REAL(nsX,WRS))
        self%domains_Ymin = FLOOR(0.5_WRS * REAL(-nsY,WRS))
        self%domains_Ymax = FLOOR(0.5_WRS * REAL(nsY,WRS))
        self%domains_Zmin = FLOOR(0.5_WRS * REAL(-nsZ,WRS))
        self%domains_Zmax = FLOOR(0.5_WRS * REAL(nsZ,WRS))

    END IF

    !### now move onto to setting up domain neighbors ###

    !### for each domain that is allowed, define what ranks are its immediate neighbors for comm initialization purposes ###
    IF (self%ndims .EQ. 1) THEN

        nneighbors = 2
        ALLOCATE(self%domain_neighbors1d(self%domains_Xmin:self%domains_Xmax, nneighbors))

        offsets(:,1) = (/-1, 0, 0/)  ! left in X
        offsets(:,2) = (/1, 0, 0/)   ! right in X

        !### loop over each domain and get its neighbors ###
        DO di = self%domains_Xmin, self%domains_Xmax

            dcoords = (/di, 0, 0/)

            !### loop through and query for our neighbors ###
            DO n = 1, nneighbors

                offset(:) = offsets(:,n) + self%my_coords + dcoords
                CALL MPI_CART_RANK(self%grid_comm, offset(1:self%ndims), self%domain_neighbors1d(di,n), ierr)

            END DO

        END DO

    ELSE IF (self%ndims .EQ. 2 ) THEN

        nneighbors = 8
        ALLOCATE(self%domain_neighbors2d(self%domains_Xmin:self%domains_Xmax, self%domains_Ymin:self%domains_Ymax, nneighbors))
        offsets(:,1) = (/-1, 0, 0/)  ! left in X
        offsets(:,2) = (/1, 0, 0/)   ! right in X
        offsets(:,3) = (/0, -1, 0/)  ! left in Y
        offsets(:,4) = (/0, 1, 0/)   ! right in Y
        offsets(:,5) = (/-1, -1, 0/) ! left along -90 degree
        offsets(:,6) = (/1, 1, 0/)   ! right along -90 degree
        offsets(:,7) = (/-1, 1, 0/)  ! left along +90 degree
        offsets(:,8) = (/1, -1, 0/)  ! right along +90 degree

        !### loop over each domain and get its neighbors ###
        DO dj = self%domains_Ymin, self%domains_Ymax

            DO di = self%domains_Xmin, self%domains_Xmax

                dcoords = (/di, dj, 0/)

                !### loop through and query for our neighbors ###
                DO n = 1, nneighbors

                    offset(:) = offsets(:,n) + self%my_coords + dcoords
                    CALL MPI_CART_RANK(self%grid_comm, offset(1:self%ndims), self%domain_neighbors2d(di,dj,n), ierr)

                END DO

            END DO

        END DO

    ELSE IF (self%ndims .EQ. 3) THEN

        !### note that the view description below are fron a right-handed coordinate system with X increasing left, Y increasing back, and Z increasing up ###
        nneighbors = 26
        ALLOCATE(self%domain_neighbors3d(self%domains_Xmin:self%domains_Xmax, self%domains_Ymin:self%domains_Ymax, self%domains_Zmin:self%domains_Zmax, nneighbors))
        offsets(:,1)  = (/-1, 0, 0/)   ! left in X
        offsets(:,2)  = (/1, 0, 0/)    ! right in X
        offsets(:,3)  = (/0, -1, 0/)   ! left in Y
        offsets(:,4)  = (/0, 1, 0/)    ! right in Y
        offsets(:,5)  = (/0, 0, -1/)   ! left in Z
        offsets(:,6)  = (/0, 0, 1/)    ! right in Z

        !### corners ###
        offsets(:,7)  = (/-1, 1, -1/)  ! back bottom left
        offsets(:,8)  = (/1, -1, 1/)   ! front top right
        offsets(:,9)  = (/1, 1, -1/)   ! back bottom right
        offsets(:,10) = (/-1, -1, 1/)  ! front top left
        offsets(:,11) = (/-1, 1, 1/)   ! back top left
        offsets(:,12) = (/1, -1, -1/)  ! front bottom right
        offsets(:,13) = (/1, 1, 1/)    ! back top right
        offsets(:,14) = (/-1, -1, -1/) ! front bottom left

        !### edges ###
        offsets(:,15) = (/0, 1, -1/)   ! back bottom
        offsets(:,16) = (/0, -1, 1/)   ! front top
        offsets(:,17) = (/0, 1, 1/)    ! back top
        offsets(:,18) = (/0, -1, -1/)  ! front bottom
        offsets(:,19) = (/-1, 0, -1/)  ! left bottom
        offsets(:,20) = (/1, 0, 1/)    ! right top
        offsets(:,21) = (/-1, 0, 1/)   ! left top
        offsets(:,22) = (/1, 0, -1/)   ! right bottom
        offsets(:,23) = (/-1, 1, 0/)   ! back left
        offsets(:,24) = (/1, -1, 0/)   ! front right
        offsets(:,25) = (/1, 1, 0/)    ! back right
        offsets(:,26) = (/-1, -1, 0/)  ! front left

        !### loop over each domain and get its neighbors ###
        DO dk = self%domains_Zmin, self%domains_Zmax

            DO dj = self%domains_Ymin, self%domains_Ymax

                DO di = self%domains_Xmin, self%domains_Xmax

                    dcoords = (/di, dj, dk/)

                    !### loop through and query for our neighbors ###
                    DO n = 1, nneighbors

                        offset(:) = offsets(:,n) + self%my_coords + dcoords
                        CALL MPI_CART_RANK(self%grid_comm, offset(1:self%ndims), self%domain_neighbors3d(di,dj,dk,n), ierr)

                    END DO

                END DO

            END DO

        END DO

    END IF
        
END SUBROUTINE init_neighbors

!### setup the mailboxes for a neighbor comm ###
SUBROUTINE build_mailbox(self, n, nbounds)

    CLASS(Decomposition) :: self
    INTEGER(WIS), INTENT(IN) :: n, nbounds
    INTEGER(WIS) :: m, b

    !### allocate however many Patch slots we need ###
    ALLOCATE(self%neighborhood(n)%mbox(self%patch_mailboxes_pslot))
    self%neighborhood(n)%nmboxes = self%patch_mailboxes_pslot

    !### loop over each of those setting up the Patch mailbox ###
    DO m = 1, self%neighborhood(n)%nmboxes
        
        !### allocate for however many boundaries a Patch may have ###
        ALLOCATE(self%neighborhood(n)%mbox(m)%b_start(nbounds,2), self%neighborhood(n)%mbox(m)%b_end(nbounds,2), &
             self%neighborhood(n)%mbox(m)%b_signal(nbounds,2), self%neighborhood(n)%mbox(m)%b_fsignal(nbounds,2), &
             self%neighborhood(n)%mbox(m)%in_use(nbounds,2), self%neighborhood(n)%mbox(m)%in_flight(nbounds,2), &
             self%neighborhood(n)%mbox(m)%is_empty(nbounds,2), self%neighborhood(n)%mbox(m)%tlock(nbounds))
        self%neighborhood(n)%mbox(m)%nbounds = nbounds
        
        !### init each boundary as not in use ###
        DO b = 1, nbounds
            
            self%neighborhood(n)%mbox(m)%in_use(b,:)    = .FALSE.
            self%neighborhood(n)%mbox(m)%in_flight(b,:) = .FALSE.
            self%neighborhood(n)%mbox(m)%is_empty(b,:)  = .TRUE.

            CALL OMP_INIT_LOCK(self%neighborhood(n)%mbox(m)%tlock(b))
            
        END DO
        
    END DO
    
END SUBROUTINE build_mailbox

!### free mailbox cleans up the memory for a mailbox ###
SUBROUTINE free_mailbox(self, n)

    CLASS(Decomposition) :: self
    INTEGER(WIS), INTENT(IN) :: n
    INTEGER(WIS) :: m, b

    IF (ALLOCATED(self%neighborhood(n)%mbox)) THEN

        !### loop over each Patch mailbox ###
        DO m = 1, self%neighborhood(n)%nmboxes

            IF (ALLOCATED(self%neighborhood(n)%mbox(m)%b_start)) THEN
                
                DEALLOCATE(self%neighborhood(n)%mbox(m)%b_start)
                DEALLOCATE(self%neighborhood(n)%mbox(m)%b_end)
                DEALLOCATE(self%neighborhood(n)%mbox(m)%b_signal)
                DEALLOCATE(self%neighborhood(n)%mbox(m)%b_fsignal)
                DEALLOCATE(self%neighborhood(n)%mbox(m)%in_use)
                DEALLOCATE(self%neighborhood(n)%mbox(m)%in_flight)
                DEALLOCATE(self%neighborhood(n)%mbox(m)%is_empty)

                DO b = 1, SIZE(self%neighborhood(n)%mbox(m)%tlock)

                    CALL OMP_DESTROY_LOCK(self%neighborhood(n)%mbox(m)%tlock(b))

                END DO
                DEALLOCATE(self%neighborhood(n)%mbox(m)%tlock)
                
            END IF
            
        END DO
        DEALLOCATE(self%neighborhood(n)%mbox)

    END IF

END SUBROUTINE free_mailbox

!### this routine will register the number and handling of threads with the object ###
SUBROUTINE register_threads(self)

    CLASS(Decomposition) :: self

    !### if the maximum number of messages in flight per thread is greater than 0 we'll be doing fully non-blocking ###
    !### communication.  if not, isends will be turned into blocking sends.  we need to keep track of the ###
    !### in-flight messages (sends) so that when we reach the given limit we wait for one to finish before adding another.  The reason ###
    !### for this is to not totally swamp MPI and/or network resources given that AMR can and will be lots of comms ###
    IF (self%max_inflightcomms .GT. 0) THEN
        
        ALLOCATE(self%tcur_inflight(self%nthreads))
        self%tcur_inflight = 0

    END IF

END SUBROUTINE register_threads

!### make some AIO progress ###
SUBROUTINE progress_aio(self)

     CLASS(Decomposition) :: self

     !### have a thread kick AIO ###
     IF (self%naio .GT. 0) THEN

         !$OMP MASTER
         CALL AIO_PROGRESS()
         !$OMP END MASTER

     END IF

END SUBROUTINE progress_aio

!### PUBLIC COMM ROUTINES ###

!### global allreduction on the world grid ###
SUBROUTINE start_allreduce_min(self, inval, outval, id)

    CLASS(Decomposition) :: self
    REAL(WRD), INTENT(INOUT) :: inval, outval
    INTEGER(WIS), INTENT(OUT) :: id
    INTEGER(WIS) :: ierr

    CALL MPI_IALLREDUCE(inval, outval, 1, MPI_REAL8, MPI_MIN, self%grid_comm, id, ierr)
    IF (ierr .NE. MPI_SUCCESS) THEN

        CALL self%handle_mpi_error(ierr, .FALSE., .FALSE.)

    END IF

END SUBROUTINE start_allreduce_min

!### global allreduction on the world grid ###
SUBROUTINE start_allreduce_max(self, inval, outval, id)

    CLASS(Decomposition) :: self
    REAL(WRD), INTENT(INOUT) :: inval, outval
    INTEGER(WIS), INTENT(OUT) :: id
    INTEGER(WIS) :: ierr

    CALL MPI_IALLREDUCE(inval, outval, 1, MPI_REAL8, MPI_MAX, self%grid_comm, id, ierr)
    IF (ierr .NE. MPI_SUCCESS) THEN

        CALL self%handle_mpi_error(ierr, .FALSE., .FALSE.)

    END IF

END SUBROUTINE start_allreduce_max

SUBROUTINE start_allreduce_sum(self, inval, outval, id)

    CLASS(Decomposition) :: self
    REAL(WRD), INTENT(INOUT) :: inval, outval
    INTEGER(WIS), INTENT(OUT) :: id
    INTEGER(WIS) :: ierr

    CALL MPI_IALLREDUCE(inval, outval, 1, MPI_REAL8, MPI_SUM, self%grid_comm, id, ierr)
    IF (ierr .NE. MPI_SUCCESS) THEN

        CALL self%handle_mpi_error(ierr, .FALSE., .FALSE.)

    END IF


END SUBROUTINE start_allreduce_sum

!### completion of global reduction ###
SUBROUTINE end_allreduce(self, id)


    CLASS(Decomposition) :: self
    INTEGER(WIS), INTENT(INOUT) :: id
    INTEGER(WIS) :: status(MPI_STATUS_SIZE), ierr

    CALL MPI_WAIT(id, status, ierr)
    IF (ierr .NE. MPI_SUCCESS) THEN

        CALL self%handle_mpi_error(ierr, .FALSE., .FALSE.)

    END IF

END SUBROUTINE end_allreduce

!### initialize the RMA pipeline ###
SUBROUTINE initRMA_pipeline(self)

    CLASS(Decomposition) :: self
    INTEGER(WIS) :: n, rn, ierr, m, b

    !$OMP DO SCHEDULE(DYNAMIC)
    DO n = 1, self%neighborhood_size

        IF (self%neighborhood(n)%rank .NE. self%myrank) THEN

            !### we need to get at the single slot for this rank so that we don't open a lock twice ###
            rn = self%neighborhood(n)%rr_cn_slot

            CALL OMP_SET_LOCK(self%neighborhood(rn)%tlock)
            
            !### init the number of exchanges we need to do with this rank.  we'll tally up the total later ###
            self%neighborhood(rn)%n_total_exchanges = 0
            
            CALL OMP_UNSET_LOCK(self%neighborhood(rn)%tlock)
            self%neighborhood(n)%n_total_exchanges  = 0
            
            !### initialize all signals in the slot and setting all mailboxes as not in use ###
            DO m = 1, self%neighborhood(n)%nmboxes
                
                DO b = 1, self%neighborhood(n)%mbox(m)%nbounds
                    
                    self%rma_buf(self%neighborhood(n)%mbox(m)%b_signal(b,1)) = self%signal_init
                    self%rma_buf(self%neighborhood(n)%mbox(m)%b_signal(b,2)) = self%signal_init
                    
                END DO
                self%neighborhood(n)%mbox(m)%in_use    = .FALSE.
                self%neighborhood(n)%mbox(m)%in_flight = .FALSE.
                self%neighborhood(n)%mbox(m)%is_empty  = .TRUE.
                
            END DO
            self%neighborhood(n)%in_use = .FALSE.
        
        END IF

    END DO
    !$OMP END DO

    !$OMP SINGLE
    CALL MPI_BARRIER(self%grid_comm, ierr)
    !$OMP END SINGLE

END SUBROUTINE initRMA_pipeline

!### open up shared locks on all neighbors for use during a comm epoch ###
SUBROUTINE rma_lock_neighbors(self)

    CLASS(Decomposition) :: self
    INTEGER(WIS) :: n, rn, ierr, m, tid

    !$OMP DO SCHEDULE(DYNAMIC)
    DO n = 1, self%neighborhood_size

        IF (self%neighborhood(n)%rank .NE. self%myrank) THEN

            !### we need to get at the single slot for this rank so that we don't open a lock twice ###
            rn = self%neighborhood(n)%rr_cn_slot
            
            CALL OMP_SET_LOCK(self%neighborhood(rn)%tlock)
            
            IF (.NOT. self%neighborhood(rn)%locked) THEN
                
#ifdef WMBT_NO_RMA_THREAD_SAFE
                !### obtain the RMA global lock if the lib isn't thread safe ###
                CALL OMP_SET_LOCK(self%rmalock)
#endif
                CALL MPI_WIN_LOCK(MPI_LOCK_SHARED, self%neighborhood(rn)%rank, MPI_MODE_NOCHECK, self%grid_window, ierr)
                DO tid = 1, self%nthreads
                    CALL MPI_WIN_LOCK(MPI_LOCK_SHARED, self%neighborhood(rn)%rank, MPI_MODE_NOCHECK, self%parapoint_window(tid), ierr)
                END DO
#ifdef WMBT_NO_RMA_THREAD_SAFE
                CALL OMP_UNSET_LOCK(self%rmalock)
#endif
                self%neighborhood(rn)%locked = .TRUE.
                
            END IF
            
            !### init the number of exchanges we need to do with this rank.  we'll tally up the total later ###
            self%neighborhood(rn)%n_total_exchanges = 0
            CALL OMP_UNSET_LOCK(self%neighborhood(rn)%tlock)
            
        END IF
    
    END DO
    !$OMP END DO

END SUBROUTINE rma_lock_neighbors

!### close locks on all neighbors from a comm epoch ###
SUBROUTINE rma_unlock_neighbors(self)

    CLASS(Decomposition) :: self
    INTEGER(WIS) :: n, rn, ierr, tid

    !$OMP DO SCHEDULE(DYNAMIC)
    DO n = 1, self%neighborhood_size

        IF (self%neighborhood(n)%rank .NE. self%myrank) THEN

            !### we need to get at the single slot for this rank so that we don't open a lock twice ###
            rn = self%neighborhood(n)%rr_cn_slot
            
            CALL OMP_SET_LOCK(self%neighborhood(rn)%tlock)
            IF (self%neighborhood(rn)%locked) THEN
                
#ifdef WMBT_NO_RMA_THREAD_SAFE
                !### obtain the RMA global lock if the lib isn't thread safe ###
                CALL OMP_SET_LOCK(self%rmalock)
#endif
                CALL MPI_WIN_UNLOCK(self%neighborhood(rn)%rank, self%grid_window, ierr)
                DO tid = 1, self%nthreads
                    CALL MPI_WIN_UNLOCK(self%neighborhood(rn)%rank, self%parapoint_window(tid), ierr)
                END DO
#ifdef WMBT_NO_RMA_THREAD_SAFE
                CALL OMP_UNSET_LOCK(self%rmalock)
#endif
                self%neighborhood(rn)%locked = .FALSE.
                
            END IF
            CALL OMP_UNSET_LOCK(self%neighborhood(rn)%tlock)
        
        END IF

    END DO
    !$OMP END DO

END SUBROUTINE rma_unlock_neighbors

!### open up shared locks on all neighbors for use during a comm epoch ###
SUBROUTINE rma_reset_neighbors(self)

    CLASS(Decomposition) :: self
    INTEGER(WIS) :: n, rn, ierr, m

    !$OMP DO SCHEDULE(DYNAMIC)
    DO n = 1, self%neighborhood_size

        IF (self%neighborhood(n)%rank .NE. self%myrank) THEN

            self%neighborhood(n)%n_total_exchanges = 0

            !### initialize all mailboxes as not in use ###
            DO m = 1, self%neighborhood(n)%nmboxes

                self%neighborhood(n)%mbox(m)%in_use    = .FALSE.
                self%neighborhood(n)%mbox(m)%in_flight = .FALSE.
                self%neighborhood(n)%mbox(m)%is_empty  = .TRUE.

            END DO
            self%neighborhood(n)%in_use = .FALSE.

        END IF

    END DO
    !$OMP END DO

    CALL self%progress_aio()

END SUBROUTINE rma_reset_neighbors

!### passing in a Patch, pack the given boundary ID into an appropriate slot and mailbox if one is free ###
!### this routine is not thread safe as it should be called on a single Patch ###
SUBROUTINE pack_patch_bound(self, workp, src_bound_id, dest_bound_id, dest_rank, flavor, dest_pi, dest_pj, dest_pk, count_exchanges, packed)

    CLASS(Decomposition) :: self
    TYPE(Patch) :: workp
    INTEGER(WIS), INTENT(IN) :: src_bound_id, dest_bound_id, dest_rank, flavor, dest_pi, dest_pj, dest_pk
    LOGICAL(WIS), INTENT(IN) :: count_exchanges
    LOGICAL(WIS), INTENT(OUT) :: packed
    LOGICAL(WIS) :: was_packed, was_counted
    INTEGER(WIS) :: rn, n, m, ierr, msize, mn, tid
    INTEGER(WADDRESS_KIND) :: start, end
    REAL(WRD) :: msg_header(WLB_HEADER_SIZE)
    REAL(WRD) :: t_start

    tid = OMP_GET_THREAD_NUM() + 1

    !### default to assuming we could not pack the boundary ###
    packed      = .FALSE.
    was_counted = .FALSE.

    !### loop across neighbor slots ###
    DO n = 1, self%neighborhood_size

        !### if the slot is for this rank loop over the mailboxes (thread lock the slot while we do this) ###
        IF (.NOT. packed .AND. self%neighborhood(n)%rank .EQ. dest_rank) THEN

            !### determine the slot we occupy on that rank ###
            rn = self%neighborhood(n)%rr_cn_slot
            mn = self%neighborhood(n)%my_cn_slot

            !### if we are counting exchanges, determine the real slot this rank has and add this to the count ###
            IF (.NOT. was_counted .AND. count_exchanges) THEN

                CALL OMP_SET_LOCK(self%neighborhood(rn)%tlock)
                self%neighborhood(rn)%n_total_exchanges = self%neighborhood(rn)%n_total_exchanges + 2
                CALL OMP_UNSET_LOCK(self%neighborhood(rn)%tlock)
                was_counted = .TRUE.

            END IF

            DO m = 1, self%neighborhood(n)%nmboxes

                !### no need to work further on this if we already packed ###
                IF (packed) EXIT

                !### try to acquire a lock on the mailbox ###
                IF (OMP_TEST_LOCK(self%neighborhood(n)%mbox(m)%tlock(dest_bound_id))) THEN

                    !### check if the mailbox has this boundary open ###
                    IF (.NOT. self%neighborhood(n)%mbox(m)%in_use(dest_bound_id,2)) THEN

                        !### go ahead and reserve this piece of the mailbox ###
                        self%neighborhood(n)%mbox(m)%in_use(dest_bound_id,2)   = .TRUE.
                        self%neighborhood(n)%mbox(m)%is_empty(dest_bound_id,2) = .FALSE.

                        !### release the lock since we already have it reserved ###
                        CALL OMP_UNSET_LOCK(self%neighborhood(n)%mbox(m)%tlock(dest_bound_id))

                        !### construct the message header encoding the PatchID and flavor ###
                        msg_header(1) = REAL(dest_pi,WRD)
                        msg_header(2) = REAL(dest_pj,WRD)
                        msg_header(3) = REAL(dest_pk,WRD)
                        msg_header(4) = REAL(flavor,WRD)
                        
                        !### add the header to the buffer ###
                        start = self%neighborhood(n)%mbox(m)%b_start(dest_bound_id,2)
                        end   = self%neighborhood(n)%mbox(m)%b_start(dest_bound_id,2) + WLB_HEADER_SIZE - 1
                        self%rma_buf(start:end) = msg_header(1:WLB_HEADER_SIZE)
                        start = end + 1
                        end   = self%neighborhood(n)%mbox(m)%b_end(dest_bound_id,2)

                        !### direct pack this patch boundary into it ###
                        CALL workp%bounds_pup(self%rma_buf, start, end, src_bound_id, flavor, .TRUE., was_packed, msize)

                        ASSERT(was_packed,'Patch boundary packing failed due to insufficient size of RMA buffer segment.')

                        packed = .TRUE.
                        
                        !### after packing we need public/private windows sync before any GETs are issued on the remote side ###
#ifdef WMBT_NO_RMA_THREAD_SAFE
                        CALL OMP_SET_LOCK(self%rmalock)
#endif
                        t_start = self%global_time()
                        CALL MPI_WIN_SYNC(self%parapoint_window(tid), ierr)
                        self%sync_time(tid) = self%sync_time(tid) + (self%global_time() - t_start)
#ifdef WMBT_NO_RMA_THREAD_SAFE
                        CALL OMP_UNSET_LOCK(self%rmalock)
#endif
                        
                        !### send a signal that this boundary region has data of the given length and is ready to go ###
                        self%rma_buf(self%neighborhood(n)%mbox(m)%b_fsignal(dest_bound_id,2)) = REAL(msize + WLB_HEADER_SIZE, 8)
#ifdef WMBT_NO_RMA_THREAD_SAFE
                        CALL OMP_SET_LOCK(self%rmalock)
#endif
                        t_start = self%global_time()
                        CALL MPI_PUT(self%rma_buf(self%neighborhood(n)%mbox(m)%b_fsignal(dest_bound_id,2)), 1, MPI_REAL8, dest_rank, &
                             self%neighborhood(mn)%mbox(m)%b_signal(dest_bound_id,2)-1_8, 1, MPI_REAL8, self%parapoint_window(tid), ierr)
                        self%put_time(tid) = self%put_time(tid) + (self%global_time() - t_start)
#ifdef WMBT_NO_RMA_THREAD_SAFE
                        CALL OMP_UNSET_LOCK(self%rmalock)
#endif
                        IF (ierr .NE. MPI_SUCCESS) THEN
                            
                            CALL self%handle_mpi_error(ierr, .FALSE., .FALSE.)
                            
                        END IF

                    ELSE

                        !### release the lock as we don't need it ###
                        CALL OMP_UNSET_LOCK(self%neighborhood(n)%mbox(m)%tlock(dest_bound_id))

                    END IF
                        
                END IF

            END DO

            !### make sure this slot is marked in use if we packed here ###
            IF (packed) THEN

                CALL OMP_SET_LOCK(self%neighborhood(n)%tlock)
                self%neighborhood(n)%in_use = .TRUE.
                CALL OMP_UNSET_LOCK(self%neighborhood(n)%tlock)

            END IF

        END IF

    END DO

END SUBROUTINE pack_patch_bound

!### go through all mailboxes, flushing outstanding GETs, unpacking messages into the appropriate Patch bound ###
SUBROUTINE unpack_mailboxes(self, patch_array, npx, npy, npz, n_unpacked)

    CLASS(Decomposition) :: self
    INTEGER(WIS), INTENT(IN) :: npx, npy, npz
    TYPE(Patch) :: patch_array(npx,npy,npz)
    INTEGER(WIS), INTENT(INOUT) :: n_unpacked
    INTEGER(WIS) :: rn, n, m, b, pi, pj, pk, flavor, ierr, msize, mn, lnm, last_neighbor, tid
    INTEGER(WADDRESS_KIND) :: start, end
    LOGICAL(WIS) :: was_unpacked
    REAL(WRD) :: t_start

    tid = OMP_GET_THREAD_NUM() + 1

    !### flush all the gets on the threads here ###
    !### This may be overkill in some corner cases ###
    CALL self%flush_all_neighbors()

    last_neighbor = 0

#ifndef WMBT_THRMA
    CALL self%flush_all_neighbors()
#endif

    !$OMP DO SCHEDULE(DYNAMIC)
#ifdef WMBT_THRMA
    DO lnm = 1, self%neighborhood_size*self%neighborhood(1)%nmboxes

        CALL collapse2d(lnm, self%neighborhood(1)%nmboxes, self%neighborhood_size, m, n)
#else
    DO n = 1, self%neighborhood_size
#endif

        IF (self%neighborhood(n)%rank .NE. self%myrank) THEN

            !### determine the slot we occupy on that rank ###
            rn = self%neighborhood(n)%rr_cn_slot
            mn = self%neighborhood(n)%my_cn_slot
            
            !### nothing to do if we have no comms with them ###
            IF (self%neighborhood(rn)%n_total_exchanges .GT. 0) THEN

#ifdef WMBT_THRMA
                !### flush any operation with that neighbor ###
                !IF (last_neighbor .NE. n) CALL self%flush_neighbor(n)
                !last_neighbor = n
                !### Flushes for all the puts will occur after all the puts

                !### loop over all bounadries for this neighbor ###
                DO b = 1, self%neighborhood(n)%mbox(m)%nbounds
#else
                !### loop over all mailboxes for this neighbor ###
                DO m = 1, self%neighborhood(n)%nmboxes

                    DO b = 1, self%neighborhood(n)%mbox(m)%nbounds
#endif                  
                        !### if the mailbox is in use we have data to unpack ###
                        IF (self%neighborhood(n)%mbox(m)%in_use(b,1)) THEN

                            IF (self%neighborhood(n)%mbox(m)%in_flight(b,1)) THEN

                                !### read the header data to determine which Patch this goes to ###
                                start  = self%neighborhood(n)%mbox(m)%b_start(b,1)
                                end    = self%neighborhood(n)%mbox(m)%b_end(b,1)
                                pi     = INT(self%rma_buf(start),WIS)
                                pj     = INT(self%rma_buf(start+1),WIS)
                                pk     = INT(self%rma_buf(start+2),WIS)
                                flavor = INT(self%rma_buf(start+3),WIS)
                                start  = start + WLB_HEADER_SIZE

                                !### have the domain object do the unpacking ###
                                IF (pi .LT. 1 .OR. pi .GT. npx .OR. pj .LT. 1. .OR. pj .GT. npy .OR. pk .LT. 1 .OR. pk .GT. npz) THEN

                                    PRINT '("ERROR: (rank = ", I4, ") Incoming boundary (b = ", I3, ") data has malformed header! (PI,PJ,PK,FLAVOR) :: ", 4(I4))', self%myrank, b, pi, pj, pk, flavor
                                    ASSERT(.FALSE.,'Unrecoverable boundary exchange error.')
                                    
                                END IF

                                !### technically I wanted the Patch object to not be thread safe, but since any thread may unpack any Patch at any time here, ###
                                !### there is a lock protecting the resolved flags within the pup calls of the Patch ###
                                !### this is the best way to keep concurrency of the call maximized ###
                                CALL patch_array(pi,pj,pk)%bounds_pup(self%rma_buf, start, end, b, flavor, .FALSE., was_unpacked, msize)
                                IF (was_unpacked) THEN
                                  
                                    !$OMP CRITICAL 
                                    n_unpacked = n_unpacked + 1
                                    !$OMP END CRITICAL
                                    
                                ELSE
                                    
                                    PRINT '("ERROR: (rank = ", I4, ") Incoming boundary (b = ", I3, ") data could not be unpacked! (PI,PJ,PK,FLAVOR,MSIZE) :: ", 5(I4))', self%myrank, b, pi, pj, pk, flavor, msize
                                    ASSERT(.FALSE.,'Unrecoverable boundary exchange error.')
                                    
                                END IF
                                
                                !### clean up the mailbox as not in flight ###
                                self%neighborhood(n)%mbox(m)%in_flight(b,1) = .FALSE.
                                
                            END IF
                            
                            !### issue a signal back that we are done with the remote buffer or just done because there was no GET issued ###
                            self%rma_buf(self%neighborhood(n)%mbox(m)%b_fsignal(b,2)) = self%signal_noget
#ifdef WMBT_NO_RMA_THREAD_SAFE
                            CALL OMP_SET_LOCK(self%rmalock)
#endif
                            t_start = self%global_time()
                            CALL MPI_PUT(self%rma_buf(self%neighborhood(n)%mbox(m)%b_fsignal(b,2)), 1, MPI_REAL8, self%neighborhood(n)%rank, &
                                 self%neighborhood(mn)%mbox(m)%b_signal(b,1)-1_8, 1, MPI_REAL8, self%parapoint_window(tid), ierr)
                             self%put_time(tid) = self%put_time(tid) + (self%global_time() - t_start)
#ifdef WMBT_NO_RMA_THREAD_SAFE
                            CALL OMP_UNSET_LOCK(self%rmalock)
#endif

                            IF (ierr .NE. MPI_SUCCESS) THEN
                                
                                CALL self%handle_mpi_error(ierr, .FALSE., .FALSE.)
                                
                            END IF

                        END IF
#ifndef WMBT_THRMA                 
                    END DO
#endif

                END DO
        
            END IF

        END IF

    END DO
    !$OMP END DO NOWAIT

    CALL self%progress_aio()

    !### flush the posted RMA operations to our neighbors ###
    CALL self%flush_all_neighbors()

END SUBROUTINE unpack_mailboxes

!### we need to at least acknowledge that we are good to go if we have any mailboxes with no data to get.  issue those signals ###
SUBROUTINE signal_empty_mailboxes(self)

    CLASS(Decomposition) :: self
    INTEGER(WIS) :: rn, n, m, b, dir, ierr, mn, tid
    REAL(WRD) :: t_start

    tid = OMP_GET_THREAD_NUM() + 1 

    !### we signal that the mailbox segment is empty only along sends ###
    dir = 2

    !$OMP DO SCHEDULE(DYNAMIC)
    DO n = 1, self%neighborhood_size

        IF (self%neighborhood(n)%rank .NE. self%myrank) THEN

            !### determine the slot we occupy on that rank ###
            rn = self%neighborhood(n)%rr_cn_slot
            mn = self%neighborhood(n)%my_cn_slot
            
            !### only bother signalling empty mailboes to ranks we will exchange messages with ###
            IF (self%neighborhood(rn)%n_total_exchanges .GT. 0) THEN
                
                DO m = 1, self%neighborhood(n)%nmboxes

                    DO b = 1, self%neighborhood(n)%mbox(m)%nbounds
                        
                        IF (.NOT. self%neighborhood(n)%mbox(m)%in_use(b,dir)) THEN
                            
                            self%rma_buf(self%neighborhood(n)%mbox(m)%b_fsignal(b,2)) = self%signal_noget
                            
#ifdef WMBT_NO_RMA_THREAD_SAFE
                            CALL OMP_SET_LOCK(self%rmalock)
#endif
                            t_start = self%global_time()
                            CALL MPI_PUT(self%rma_buf(self%neighborhood(n)%mbox(m)%b_fsignal(b,2)), 1, MPI_REAL8, self%neighborhood(n)%rank, &
                                 self%neighborhood(mn)%mbox(m)%b_signal(b,dir)-1_8, 1, MPI_REAL8, self%parapoint_window(tid), ierr)
                             self%put_time(tid) = self%put_time(tid) + (self%global_time() - t_start)
#ifdef WMBT_NO_RMA_THREAD_SAFE
                            CALL OMP_UNSET_LOCK(self%rmalock)
#endif
                            IF (ierr .NE. MPI_SUCCESS) THEN
                                
                                CALL self%handle_mpi_error(ierr, .FALSE., .FALSE.)
                                
                            END IF

                            !### mark it as in use now ###
                            self%neighborhood(n)%mbox(m)%in_use(b,dir) = .TRUE.
                            
                        END IF
                        
                    END DO
 
                END DO

            END IF

        END IF

    END DO
    !$OMP END DO NOWAIT

    CALL self%progress_aio()

    !### flush RMA operations to our neighbors ###
    CALL self%flush_all_neighbors()

END SUBROUTINE signal_empty_mailboxes

!### flush any outstanding operations to all neighbors ###
SUBROUTINE flush_all_neighbors(self)

    CLASS(Decomposition) :: self
    INTEGER(WIS) :: n, ierr, status(MPI_STATUS_SIZE), tid
    LOGICAL(WIS) :: doflush
    REAL(WRD) :: t_start

    tid = OMP_GET_THREAD_NUM() + 1

#ifndef WMBT_THRMA
    !$OMP SINGLE
    doflush = .FALSE.
    DO n = 1, self%neighborhood_size

        IF (self%neighborhood(n)%locked) doflush = .TRUE.

    END DO
    IF (doflush) THEN
#endif
#ifdef WMBT_NO_RMA_THREAD_SAFE
        CALL OMP_SET_LOCK(self%rmalock)
#endif
        t_start = self%global_time() 
        CALL MPI_WIN_FLUSH_ALL(self%parapoint_window(tid), ierr)
        self%flush_all_time(tid) = self%flush_all_time(tid) + (self%global_time() - t_start)
#ifdef WMBT_NO_RMA_THREAD_SAFE
        CALL OMP_UNSET_LOCK(self%rmalock)
#endif

#ifdef WMBT_THRMA
    t_start = self%global_time()
    !$OMP BARRIER
    self%barrier_time(tid) = self%barrier_time(tid) + (self%global_time() - t_start)
#else
    END IF
    !$OMP END SINGLE
#endif

END SUBROUTINE flush_all_neighbors

!### flush any outstanding operations to a specific neighbor slot ###
SUBROUTINE flush_neighbor(self, slot)
    
    CLASS(Decomposition) :: self
    INTEGER(WIS), INTENT(IN) :: slot
    INTEGER(WIS) :: ierr, status(MPI_STATUS_SIZE), tid
    REAL(WRD) :: t_start

    tid = OMP_GET_THREAD_NUM() + 1

    !### nothing to do if the slot is us ###
    IF (self%neighborhood(slot)%rank .EQ. self%myrank) RETURN
    
#ifdef WMBT_NO_RMA_THREAD_SAFE
    !### obtain the RMA global lock if the lib isn't thread safe ###
    CALL OMP_SET_LOCK(self%rmalock)
#endif
    t_start = self%global_time()
    CALL MPI_WIN_FLUSH(self%neighborhood(slot)%rank, self%grid_window, ierr)
    self%flush_time(tid) = self%flush_time(tid) + (self%global_time() - t_start)
#ifdef WMBT_NO_RMA_THREAD_SAFE
    CALL OMP_UNSET_LOCK(self%rmalock)
#endif

END SUBROUTINE flush_neighbor

!### loop through all neighbors checking to see if we have received a signal.  post a GET if the signal indicates we need to ###
SUBROUTINE issue_signalled_gets(self, all_signals_recv)

    CLASS(Decomposition) :: self
    LOGICAL(WIS), INTENT(INOUT) :: all_signals_recv
    INTEGER(WADDRESS_KIND) :: start
    INTEGER(WIS) :: rn, n, m, b, ierr, bsize, mn, msize, s, tid, f
    REAL(WRD) :: sigval 
    REAL(WRD) :: t_start 
    LOGICAL(WIS) :: myall_signals_recv

    tid = OMP_GET_THREAD_NUM() + 1

    IF (self%max_inflightcomms .GT. 0) self%tcur_inflight(tid) = 0

    !### spin on this however many times the user requests ###
    DO s = 1, self%nspins

        myall_signals_recv = .TRUE.

        !$OMP DO SCHEDULE(DYNAMIC)
        DO n = 1, self%neighborhood_size

            !### determine the slot we occupy on that rank ###
            rn = self%neighborhood(n)%rr_cn_slot
            mn = self%neighborhood(n)%my_cn_slot
            
            !### we only monitor for signals from ranks other than ourself that we will actually exchange messages with ###
            IF (self%neighborhood(n)%rank .NE. self%myrank .AND. self%neighborhood(rn)%n_total_exchanges .GT. 0) THEN
                
                !### if the MPI library is not truly passive we need to lock ourself to sync any signals ###
#ifdef WMBT_NO_RMA_THREAD_SAFE
                CALL OMP_SET_LOCK(self%rmalock)
#endif
                t_start = self%global_time()
                CALL MPI_WIN_SYNC(self%parapoint_window(tid), ierr)
                self%sync_time(tid) = self%sync_time(tid) + (self%global_time() - t_start)
#ifdef WMBT_NO_RMA_THREAD_SAFE
                CALL OMP_UNSET_LOCK(self%rmalock)
#endif
#ifdef WMBT_NO_RMA_TRULY_PASSIVE
                IF (OMP_TEST_LOCK(self%myrmalock)) THEN
                    
                    IF (.NOT. self%mylock_open) THEN
#ifdef WMBT_NO_RMA_THREAD_SAFE
                        CALL OMP_SET_LOCK(self%rmalock)
#endif
                        CALL MPI_WIN_LOCK(MPI_LOCK_SHARED, self%myrank, MPI_MODE_NOCHECK, self%grid_window, ierr)
#ifdef WMBT_NO_RMA_THREAD_SAFE
                        CALL OMP_UNSET_LOCK(self%rmalock)
#endif
                        self%mylock_open = .TRUE.
                    END IF
                    CALL OMP_UNSET_LOCK(self%myrmalock)
                    
                END IF
#endif
                
                !### loop over all mailboxes looking for non-init signals ###
                DO m = 1, self%neighborhood(n)%nmboxes
                    
                    DO b = 1, self%neighborhood(n)%mbox(m)%nbounds
                        
                        !### if the signal is not init we count it as received and start a get if told to ###
                        !### NOTE: strictly speaking this comparison has undefined and possibly unsafe behavior according to the MPI standard.  The reason ###
                        !###  is that MPI only requires atomicity at the B level and this is an 8 B load.  Since really any write to this address could trigger this ###
                        !###  (which is what I want), we are fine.  There's another point below.  Now on IB and Cray interconnects, there are 8 B word transfers, ###
                        !###  so we should be and have been fine in all testing to date ###
                        CALL SIGCMPGT(self%rma_cbuf, self%neighborhood(n)%mbox(m)%b_signal(b,2), self%signal_init, self%sigtol, f, sigval) 
                        IF (f .EQ. 1 .AND. .NOT. self%neighborhood(n)%mbox(m)%in_use(b,1)) THEN

                            !### if we were signalled to issue a GET do so now and mark the message as not empty ###
                            !### NOTE: as above, this is strictly an undefined op according to MPI since magnitudes are compared.  we're aligned on a single 8B word, ###
                            !###  so this should be safe.  I don't like potential caveats, but it's a compromise for performance. ###
                            CALL SIGCMPGT(self%rma_cbuf, self%neighborhood(n)%mbox(m)%b_signal(b,2), self%signal_noget, self%sigtol, f, sigval)
                            IF (f .EQ. 1 .AND. .NOT. self%neighborhood(n)%mbox(m)%in_flight(b,1)) THEN

                                !### get the dest buffer extents ###
                                bsize = INT(sigval,WIS)
                                start = self%neighborhood(n)%mbox(m)%b_start(b,1)
                                self%rma_buf(self%neighborhood(n)%mbox(m)%b_signal(b,2)) = self%signal_init

                                ASSERT(sigval .LE. HUGE(1),'Message size for MPI_GET exceeds an INT type which is invalid')

                                !### if the message size is larger than the mailbox we have a problem ###
                                msize = self%neighborhood(n)%mbox(m)%b_end(b,1)-self%neighborhood(n)%mbox(m)%b_start(b,1)+1

                                IF (bsize .GT. msize) THEN
                                    
                                    PRINT '("ERROR: (rank = ", I4, ") Incoming boundary (b = ", I3, ") data is larger than our mailbox! (BSIZE,MSIZE,OFF) :: ", 3(I9), ES12.5)', &
                                         self%myrank, b, bsize, msize, self%neighborhood(n)%mbox(m)%b_signal(b,2), self%rma_buf(self%neighborhood(n)%mbox(m)%b_signal(b,2))
                                    ASSERT(.FALSE.,'Unrecoverable boundary exchange error.')
                                    
                                END IF
                                
#ifdef WMBT_NO_RMA_THREAD_SAFE
                                CALL OMP_SET_LOCK(self%rmalock)
#endif
                                t_start = self%global_time()
                                CALL MPI_GET(self%rma_buf(start), bsize, MPI_REAL8, self%neighborhood(n)%rank, self%neighborhood(mn)%mbox(m)%b_start(b,2)-1_8, bsize, MPI_REAL8, self%parapoint_window(tid), ierr)
                                self%get_time(tid) = self%get_time(tid) + (self%global_time() - t_start)
#ifdef WMBT_NO_RMA_THREAD_SAFE
                                CALL OMP_UNSET_LOCK(self%rmalock)
#endif
                                IF (ierr .NE. MPI_SUCCESS) THEN
                                    
                                    CALL self%handle_mpi_error(ierr, .FALSE., .FALSE.)
                                    
                                END IF
                                
                                !### flag that we have a GET in flight ###
                                self%neighborhood(n)%mbox(m)%in_flight(b,1) = .TRUE.
                                self%neighborhood(n)%mbox(m)%is_empty(b,1)  = .FALSE.
                                
                                !### increment our outstanding GETs.  If we are over the limit we will flush and reset the counter for all threads. ###
                                IF (self%max_inflightcomms .GT. 0) THEN

                                    !$OMP CRITICAL
                                    self%tcur_inflight(tid) = self%tcur_inflight(tid) + 1
                                    IF (SUM(self%tcur_inflight) .GT. self%max_inflightcomms) THEN

#ifdef WMBT_NO_RMA_THREAD_SAFE
                                        CALL OMP_SET_LOCK(self%rmalock)
#endif
                                        PRINT *, "! IT SHOULD NEVER GET HERE !" 
                                        t_start = self%global_time()
                                        CALL MPI_WIN_FLUSH_LOCAL_ALL(self%grid_window, ierr)
                                        self%flush_local_all_time(tid) = self%flush_local_all_time(tid) + (self%global_time() - t_start)
#ifdef WMBT_NO_RMA_THREAD_SAFE
                                        CALL OMP_UNSET_LOCK(self%rmalock)
#endif
                                        self%tcur_inflight = 0

                                    END IF
                                    !$OMP END CRITICAL

                                END IF

                            ELSE
                                
                                !### set the signal back to init ###
                                self%rma_buf(self%neighborhood(n)%mbox(m)%b_signal(b,2)) = self%signal_init

                            END IF

                            !### mark the mailbox as in_use since we either have a GET in-flight or at least got the signal ###
                            self%neighborhood(n)%mbox(m)%in_use(b,1) = .TRUE.
                            
                        !### set the flag to indicate more looping will be needed ###
                        ELSE IF (.NOT. self%neighborhood(n)%mbox(m)%in_use(b,1)) THEN

                            myall_signals_recv = .FALSE.
                            
                        END IF
                        
                    END DO
                    
                END DO
                
                !### if the MPI library is not truly passive we need to unlock ourself ###
#ifdef WMBT_NO_RMA_TRULY_PASSIVE
                IF (OMP_TEST_LOCK(self%myrmalock)) THEN
                    
                    IF (self%mylock_open) THEN
#ifdef WMBT_NO_RMA_THREAD_SAFE
                        CALL OMP_SET_LOCK(self%rmalock)
#endif
                        CALL MPI_WIN_UNLOCK(self%myrank, self%grid_window, ierr)
#ifdef WMBT_NO_RMA_THREAD_SAFE
                        CALL OMP_UNSET_LOCK(self%rmalock)
#endif
                        self%mylock_open = .FALSE.
                    END IF
                    CALL OMP_UNSET_LOCK(self%myrmalock)
                    
                END IF
#endif
                
            END IF
            
        END DO
        !$OMP END DO NOWAIT

        CALL self%progress_aio()

    END DO

    all_signals_recv = myall_signals_recv
    
    t_start = self%global_time()
    !$OMP BARRIER
    self%barrier_time(tid) = self%barrier_time(tid) + (self%global_time() - t_start)

END SUBROUTINE issue_signalled_gets

!### loop back through the neighbors and check that we recieved some kind of a signal back from them to indicate that send buffers are free to be modified again ###
SUBROUTINE check_completed_gets(self, all_signals_recv)

    CLASS(Decomposition) :: self
    LOGICAL(WIS), INTENT(INOUT) :: all_signals_recv
    INTEGER(WIS) :: rn, n, m, b, ierr, s, f, tid
    LOGICAL(WIS) :: myall_signals_recv
    REAL(WRD) :: t_start

    tid = OMP_GET_THREAD_NUM() + 1

    !### spin on this loop however many times we're set to ###
    DO s = 1, self%nspins

        myall_signals_recv = .TRUE.

        !### do this in a parallel loop since we are only checking and resetting signals ###
        !$OMP DO SCHEDULE(DYNAMIC)
        DO n = 1, self%neighborhood_size

            IF (self%neighborhood(n)%rank .NE. self%myrank) THEN

                rn = self%neighborhood(n)%rr_cn_slot
            
                !### we only monitor for signals from ranks other than ourself that we will actually exchange messages with ###
                CALL OMP_SET_LOCK(self%neighborhood(rn)%tlock)
                IF (self%neighborhood(rn)%n_total_exchanges .GT. 0) THEN
                
                    !### if the MPI library is not truly passive we need to lock ourself to sync any signals ###
#ifdef WMBT_NO_RMA_THREAD_SAFE
                    CALL OMP_SET_LOCK(self%rmalock)
#endif
                    t_start = self%global_time()
                    CALL MPI_WIN_SYNC(self%parapoint_window(tid), ierr)
                    self%sync_time(tid) = self%sync_time(tid) + (self%global_time() - t_start)
#ifdef WMBT_NO_RMA_THREAD_SAFE
                    CALL OMP_UNSET_LOCK(self%rmalock)
#endif
#ifdef WMBT_NO_RMA_TRULY_PASSIVE
                    IF (OMP_TEST_LOCK(self%myrmalock)) THEN
                        
                        IF (.NOT. self%mylock_open) THEN
#ifdef WMBT_NO_RMA_THREAD_SAFE
                            CALL OMP_SET_LOCK(self%rmalock)
#endif
                            CALL MPI_WIN_LOCK(MPI_LOCK_SHARED, self%myrank, MPI_MODE_NOCHECK, self%grid_window, ierr)
#ifdef WMBT_NO_RMA_THREAD_SAFE
                            CALL OMP_UNSET_LOCK(self%rmalock)
#endif
                            self%mylock_open = .TRUE.
                        END IF
                        CALL OMP_UNSET_LOCK(self%myrmalock)
                        
                    END IF
#endif
                    
                    !### loop over all mailboxes looking for non-init signals ###
                    DO m = 1, self%neighborhood(n)%nmboxes
                        
                        DO b = 1, self%neighborhood(n)%mbox(m)%nbounds
                            
                            !### if the signal is not init we count it as received ###
                            !### NOTE: as above, this is strictly an undefined op according to MPI since magnitudes are compared.  we're aligned on a single 8B word, ###
                            !###  so this should be safe.  I don't like potential caveats, but it's a compromise for performance. ###
                            CALL SIGCMPLT(self%rma_cbuf, self%neighborhood(n)%mbox(m)%b_signal(b,1), self%signal_noget, self%sigtol, f)
                            IF (f .EQ. 1 .AND. self%neighborhood(n)%mbox(m)%in_use(b,2)) THEN
                               
                                myall_signals_recv = .FALSE.
                                
                            ELSE IF (self%neighborhood(n)%mbox(m)%in_use(b,2)) THEN

                                !### reset the signal ###
                                self%rma_buf(self%neighborhood(n)%mbox(m)%b_signal(b,1)) = self%signal_init
                                
                                !### mark this an no longer in use, reset the signals and decrement the counter ###
                                self%neighborhood(n)%mbox(m)%in_use(b,1:2)    = .FALSE.
                                self%neighborhood(n)%mbox(m)%in_flight(b,1:2) = .FALSE.
                                
                                !### if there was data in those mailboxes decrement the counter accordingly ###
                                IF (.NOT. self%neighborhood(n)%mbox(m)%is_empty(b,1)) THEN
                                    
                                    self%neighborhood(rn)%n_total_exchanges = self%neighborhood(rn)%n_total_exchanges - 1
                                    
                                END IF
                                IF (.NOT. self%neighborhood(n)%mbox(m)%is_empty(b,2)) THEN
                                    
                                    self%neighborhood(rn)%n_total_exchanges = self%neighborhood(rn)%n_total_exchanges - 1
                                    
                                END IF
                                self%neighborhood(n)%mbox(m)%is_empty(b,1:2) = .TRUE.
                                
                            END IF
                            
                        END DO
                        
                    END DO
                    
                    !### if the MPI library is not truly passive we need to unlock ourself ###
#ifdef WMBT_NO_RMA_TRULY_PASSIVE
                    IF (OMP_TEST_LOCK(self%myrmalock)) THEN
                        
                        IF (self%mylock_open) THEN
#ifdef WMBT_NO_RMA_THREAD_SAFE
                            CALL OMP_SET_LOCK(self%rmalock)
#endif
                            CALL MPI_WIN_UNLOCK(self%myrank, self%grid_window, ierr)
#ifdef WMBT_NO_RMA_THREAD_SAFE
                            CALL OMP_UNSET_LOCK(self%rmalock)
#endif
                            self%mylock_open = .FALSE.
                        END IF
                        CALL OMP_UNSET_LOCK(self%myrmalock)
                        
                    END IF
#endif
                    
                END IF
                CALL OMP_UNSET_LOCK(self%neighborhood(rn)%tlock)
                
            END IF
            
        END DO
        !$OMP END DO NOWAIT

        CALL self%progress_aio()

    END DO

    all_signals_recv = myall_signals_recv
    
    t_start = self%global_time()
    !$OMP BARRIER
    self%barrier_time(tid) = self%barrier_time(tid) + (self%global_time() - t_start)

END SUBROUTINE check_completed_gets

!### INTERNAL HANDLERS ###

!### error handling for this class ###
!### note that this only functions properly for a world detected error.  for local MPI comm errors this would not ###
!### be known.  There we'll sutdown in a very ugly way by slamming down the errored rank so that the error is obvious ###
SUBROUTINE handle_mpi_error(self, ierr, all_print, clean_shutdown)

    CLASS(Decomposition) :: self
    INTEGER(WIS), INTENT(IN) :: ierr
    LOGICAL(WIS), INTENT(IN) :: all_print, clean_shutdown
    INTEGER(WIS) :: str_len, myierr
    CHARACTER(MPI_MAX_ERROR_STRING) :: err_string
    CHARACTER(WMAX_ERR_LEN) :: werr_string

    !### get the MPI error string ###
    CALL MPI_ERROR_STRING(ierr, err_string, str_len, myierr)

    !### create a more descriptive string (add on the rank etc...) ###
    werr_string = 'MPI Lib Error => ' // err_string

    !### if everyone is suppose to print then do so ###
    IF (all_print .OR. self%world_myrank .EQ. 0) THEN

        MSG(werr_string)
        
    END IF

    IF (clean_shutdown) THEN

        !### if there are AIO servers let's bring them down ###
        IF (self%naio .GT. 0) THEN

            IF (self%world_myrank .EQ. 0) PRINT '("AIO shutdown sequence has been initiated...")'
            CALL AIO_FINALIZE()

        END IF

        CALL self%destroy()

    END IF

    STOP

END SUBROUTINE handle_mpi_error

END MODULE Mod_Decomposition
