#include "config.h"

MODULE Mod_PassivePool

!######################################################################
!#
!#
!# FILENAME: mod_passivepool.f90
!#
!# DESCRIPTION: This module defines an object for work arrays for use
!#              by the Passive object.
!#
!# DEPENDENCIES: Mod_Globals for defining data types
!#
!# HISTORY:
!#    9/9/2015  - Peter Mendygral
!#
!######################################################################

!### require the needed modules ###
USE Mod_Globals
USE Mod_DataPool

IMPLICIT NONE

!### default private but make the data type available ###
PRIVATE
PUBLIC :: PassivePool

!### define a data type for this module ###
TYPE, EXTENDS(DataPool) :: PassivePool

    !### default everything as private ###
    PRIVATE

    REAL(WRD), PUBLIC :: dt, dx, max_wave
    REAL(WRD), PUBLIC, ALLOCATABLE :: slope2d(:,:), &
                                      flux2d(:,:,:), flux3d(:,:,:,:),    &
                                      workFlux2d(:,:,:), &
                                      pwork2d(:,:), &
                                      vtavg2d(:,:)

    CONTAINS

    !### default everything as private ###
    PRIVATE

    PROCEDURE :: initpassivepool
    PROCEDURE, PUBLIC :: destroy

END TYPE PassivePool

INTERFACE PassivePool

    MODULE PROCEDURE constructor

END INTERFACE

CONTAINS

!### constructor call ###
FUNCTION constructor(ndim, nx, ny, nz, nb)

    TYPE(PassivePool) :: constructor
    INTEGER(WIS), INTENT(IN) :: ndim, nx, ny, nz, nb

    CALL constructor%initpassivepool(ndim, nx, ny, nz, nb)
    RETURN

END FUNCTION constructor

!### allocate all of the necessary arrays ###
!### immediately start filling them as a first touch.  the caller is an individual thread. ###
SUBROUTINE initpassivepool(self, ndim, nx, ny, nz, nb)

    CLASS(PassivePool) :: self
    INTEGER(WIS), INTENT(IN) :: ndim, nx, ny, nz, nb
    INTEGER(WIS) :: maxl

    !### 2 and 3d work arrays must be able to handle rotations, so find the max length of any dimension and use that ###
    IF (ndim .EQ. 2) THEN
        maxl = MAX(nx, ny)
    ELSE IF (ndim .EQ. 3) THEN
        maxl = MAX(nx, ny, nz)
    END IF

    !### for 2 and 3d we basically do updates in planes (2d is a single plane).  reuse the same work arrays for those cases to make that clear ###
    IF (ndim .GT. 1) THEN

        ALLOCATE(self%pwork2d(1-nb:maxl+nb,1-nb:maxl+nb), self%slope2d(1-nb:maxl+nb,1-nb:maxl+nb), &
             self%flux2d(1-nb:maxl+nb,1-nb:maxl+nb,ndim), self%workFlux2d(1-nb:maxl+nb,1-nb:maxl+nb,ndim), self%vtavg2d(1-nb:maxl+nb,1-nb:maxl+nb))
        self%slope2d    = 0.0_WRD
        self%flux2d     = 0.0_WRD
        self%workFlux2d = 0.0_WRD
        self%pwork2d    = 0.0_WRD
        self%vtavg2d    = 0.0_WRD

    END IF

    IF (ndim .EQ. 1) THEN

        ALLOCATE(self%pwork2d(1-nb:nx+nb,1), self%slope2d(1-nb:nx+nb,1), self%flux2d(1-nb:nx+nb,1,1), self%vtavg2d(1-nb:nx+nb,1))
        self%pwork2d = 0.0_WRD
        self%slope2d = 0.0_WRD
        self%flux2d  = 0.0_WRD
        self%vtavg2d = 0.0_WRD

    ELSE IF (ndim .EQ. 3) THEN

        ALLOCATE(self%flux3d(1-nb:nx+nb,1-nb:ny+nb,1-nb:nz+nb,ndim))
        self%flux3d  = 0.0_WRD

    END IF
	
    !### store the constant sizes ###
    self%ndim = ndim
    self%nb     = nb
    self%nx     = nx
    self%ny     = ny
    self%nz     = nz

END SUBROUTINE initpassivepool

!### clean up all allocated memory ###
SUBROUTINE destroy(self)

    CLASS(PassivePool) :: self

    IF (ALLOCATED(self%pwork2d)) THEN

        DEALLOCATE(self%pwork2d)
        DEALLOCATE(self%slope2d)
        DEALLOCATE(self%flux2d)
        DEALLOCATE(self%vtavg2d)

    END IF

    IF (self%ndim .GT. 1) THEN

        DEALLOCATE(self%workFlux2d)

    END IF

    IF (self%ndim .EQ. 3) THEN

        DEALLOCATE(self%flux3d)

    END IF

END SUBROUTINE destroy

END MODULE
