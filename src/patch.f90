#include "config.h"

MODULE Mod_Patch

!######################################################################
!#
!# FILENAME: mod_patch.f90
!#
!# DESCRIPTION: This module defines a data type patch that can be used
!#  for blocking and managing pieces of a multidimensional mesh.
!#
!# DEPENDENCIES: Mod_Globals for defining data types
!#
!# HISTORY:
!#    5/7/12  - Peter Mendygral
!#
!######################################################################

!### require the needed modules ###
USE Mod_Globals
USE Mod_Error
USE OMP_LIB

IMPLICIT NONE

!### default private but make the data type available ###
PRIVATE
PUBLIC :: Patch

!### define a data type for this module ###
TYPE :: Patch

   !### default everything as private ###
    PRIVATE

    !### these are the actual state variable storage arrays ###
    REAL(WRD), PUBLIC, ALLOCATABLE :: grid1d(:,:), grid2d(:,:,:), grid3d(:,:,:,:),    &
                                      vtavg1d(:,:), vtavg2d(:,:,:), vtavg3d(:,:,:,:), &
                                      pass1d(:,:), pass2d(:,:,:), pass3d(:,:,:,:),    &
                                      crs1d(:,:),   crs2d(:,:,:),  crs3d(:,:,:,:),    &
                                      grav1d(:,:), grav2d(:,:,:), grav3d(:,:,:,:),    &
                                      emf2d(:,:,:), emf3d(:,:,:,:),                   &
                                      bface2d(:,:,:), bface3d(:,:,:,:)

    !### these are the boundary management arrays, storage, length bookkeeping, and message handles ###
    REAL(WRD), PUBLIC, ALLOCATABLE :: gridbounds(:)
    INTEGER(WIS), PUBLIC, ALLOCATABLE :: stateoffs(:,:), emfoffs(:,:), passoffs(:,:), crsoffs(:,:), gravoffs(:,:), pmoffs(:,:), &
                                         statelens(:), emflens(:), passlens(:), crslens(:), gravlens(:), pmlens(:)

    !### scalar properties ###
    REAL(WRD), PUBLIC :: x0, y0, z0, dx, dt, t
    INTEGER(WIS), PUBLIC :: ndim, xi0, yi0, zi0, nx, ny, nz, nb, nvars, npass, ncrs, nemfs, mg_counter, update_pass_count
    LOGICAL(WIS), PUBLIC :: hasZones, isLive, is1d, is2d, is3d, mhdOn, passOn, crsOn, statgravOn, vargravOn, dmOn, updated, update_cmplt
    LOGICAL(WIS), PUBLIC, ALLOCATABLE :: flavor_bound_resolved(:,:,:), flavor_resolved(:,:)
    INTEGER(WIS) :: ngravvars
    INTEGER(WOMP_LOCK_SIZE) :: tlock

    CONTAINS

    !### default everything as private ###
    PRIVATE

    PROCEDURE :: initpatch
    PROCEDURE, PUBLIC :: build
    PROCEDURE, PUBLIC :: unbuild
    PROCEDURE, PUBLIC :: destroy
    PROCEDURE, PUBLIC :: mark_unresolved
    PROCEDURE, PUBLIC :: bounds_pup
    PROCEDURE, PUBLIC :: bounds_direct_pup
    PROCEDURE :: pack_array1d
    PROCEDURE :: unpack_array1d
    PROCEDURE :: pack_array2d
    PROCEDURE :: unpack_array2d
    PROCEDURE :: pack_array3d
    PROCEDURE :: unpack_array3d

    !NOTE: add to this list the pack/unpack boundary routines AND we'll want a routine that packs up the entire patch into a contiguous buffer
    ! for use in load balancing.  I'll add a variable for the size for buffers, and another for the total size of the object including scalars.


    ! ADD AN ARRAY AND ROUTINES FOR TRACKING RESOLUTION OF BOUNDARY REQUIREMENTS (ROUTINE TO SET ALL TO FALSE AND THEN MARK AS TRUE AS THEY ARE UNPACKED), GLOBAL FLAG FOR ALL BOUNDS OF A FLAVOR

END TYPE Patch

!### create an interface to the constructor ###
INTERFACE Patch

    MODULE PROCEDURE constructor

END INTERFACE Patch

!### object methods ###
CONTAINS

!### constructor for the class ###
!### accepts: ndim = # of dimensions, nvars = # of variables in grid, npass = # of passive variables, ###
!###          nx[yz] = # of zones in that dimension ###
!###          nb = # of boundary zones, [xyz]io = world grid index of (1,1,1) origin of patch, ###
!###          [xyz]o = world grid position of (1,1,1) origin of patch (zone centered) ###
FUNCTION constructor(ndim, nvars, npass, ncrs, mhdOn, passOn, crsOn, statgravOn, vargravOn, DM_On, nx, ny, nz, nb, xi0, yi0, zi0, x0, y0, z0, dx, dt, t, ntotbms, ndmspbm, nslotspdm, nstepspdefrag, DM_buff_fac)

    TYPE(Patch) :: constructor 
    INTEGER(WIS), INTENT(IN) :: ndim, nvars, npass, ncrs, nx, ny, nz, nb, xi0, yi0, zi0, ntotbms, ndmspbm, nslotspdm, nstepspdefrag
    REAL(WRD), INTENT(IN) :: x0, y0, z0, dx, dt, t, DM_buff_fac
    LOGICAL(WIS), INTENT(IN) :: mhdOn, passOn, crsOn, statgravOn, vargravOn, DM_On

    CALL constructor%initpatch(ndim, nvars, npass, ncrs, mhdOn, passOn, crsOn, statgravOn, vargravOn, DM_On, nx, ny, nz, nb, xi0, yi0, zi0, x0, y0, z0, dx, dt, t, ntotbms, ndmspbm, nslotspdm, nstepspdefrag, DM_buff_fac)
    RETURN

END FUNCTION constructor

!### actual initialization of object (not directly called by user) ###
SUBROUTINE initpatch(self, ndim, nvars, npass, ncrs, mhdOn, passOn, crsOn, statgravOn, vargravOn, DM_On, nx, ny, nz, nb, xi0, yi0, zi0, x0, y0, z0, dx, dt, t, ntotbms, ndmspbm, nslotspdm, nstepspdefrag, DM_buff_fac)

    CLASS(Patch) :: self
    INTEGER(WIS), INTENT(IN) :: ndim, nvars, npass, ncrs, nx, ny, nz, nb, xi0, yi0, zi0, ntotbms, ndmspbm, nslotspdm, nstepspdefrag
    REAL(WRD), INTENT(IN) :: x0, y0, z0, dx, dt, t, DM_buff_fac
    LOGICAL(WIS), INTENT(IN) :: mhdOn, passOn, crsOn, statgravOn, vargravOn, DM_On
    REAL(WRD) :: xlo, xhi, ylo, yhi, zlo, zhi

    !### assign the settings ###
    self%ndim   = ndim
    self%nvars  = nvars
    self%npass  = npass
    self%ncrs   = ncrs
    self%mhdOn  = mhdOn
    self%passOn = passOn
    IF (self%npass .LT. 1) self%passOn = .FALSE.
    self%crsOn  = crsOn
    IF (self%ncrs .LT. 1) self%crsOn = .FALSE.
    self%statgravOn = statgravOn
    self%vargravOn = vargravOn
    self%dmOn   = DM_On
    self%nx     = nx
    self%ny     = ny
    self%nz     = nz
    self%nb     = nb
    self%xi0    = xi0
    self%yi0    = yi0
    self%zi0    = zi0
    self%x0     = x0
    self%y0     = y0
    self%z0     = z0
    self%dx     = dx
    self%dt     = dt
    self%t      = t
    self%is1d   = .FALSE.
    self%is2d   = .FALSE.
    self%is3d   = .FALSE.
    IF (ndim .EQ. 1) THEN
        self%is1d = .TRUE.
    ELSE IF (ndim .EQ. 2) THEN
        self%is2d = .TRUE.
    ELSE IF (ndim .EQ. 3) THEN
        self%is3d = .TRUE.
    ELSE
        !CALL Error("Mod_Patch", "Unsupported number of dimensions requested")
    END IF

    !### default to inactive ###
    self%hasZones = .FALSE.
    self%isLive   = .FALSE.

    xlo = self%x0 - 0.5_WRD * self%dx
    xhi = xlo + self%nx * self%dx
    ylo = self%y0 - 0.5_WRD * self%dx
    yhi = ylo + self%ny * self%dx
    zlo = self%z0 - 0.5_WRD * self%dx
    zhi = zlo + self%nz * self%dx

    RETURN

END SUBROUTINE initpatch

!### allocates grid array and performs first touch with zero data ###
SUBROUTINE build(self)

    CLASS(Patch) :: self
    INTEGER(WIS) :: neighbors, n, curoff

    !### we have one key data structure that must be thread protected ###
    CALL OMP_INIT_LOCK(self%tlock)

    !### perform the necessary allocate and touch (explicit zero touch should count as this really shouldn't become a calloc or memset) ###
    IF (self%is1d .AND. .NOT. ALLOCATED(self%grid1d)) THEN

        self%nemfs = 0
        ALLOCATE(self%grid1d(1-self%nb:self%nx+self%nb, self%nvars))
        self%grid1d = 0.0_WRD

        !### we only need some fields if MHD is enabled for this Patch ###
        IF (self%mhdOn) THEN

            ALLOCATE(self%vtavg1d(1-self%nb:self%nx+self%nb, 3))
            self%vtavg1d = 0.0_WRD

        END IF

        neighbors = 2
        ALLOCATE(self%stateoffs(neighbors,2), self%statelens(neighbors))  ! last index is 1 = send, 2 = recv
        self%statelens  = self%nb * self%nvars

        !### allocate and touch the passive array if On ###
        IF (self%passOn) THEN

            ALLOCATE(self%pass1d(1-self%nb:self%nx+self%nb, self%npass))
            self%pass1d = 0.0_WRD

            ALLOCATE(self%passoffs(neighbors,2), self%passlens(neighbors))
            self%passlens  = self%nb * self%npass

        END IF

        !### allocate and touch the cosmic rays array if On ###
        IF (self%crsOn) THEN

            ALLOCATE(self%crs1d(1-self%nb:self%nx+self%nb, self%ncrs))
            self%crs1d = 0.0_WRD

            ALLOCATE(self%crsoffs(neighbors,2), self%crslens(neighbors))
            self%crslens  = self%nb * self%ncrs

        END IF

        !### allocate and touch the gravity arrays if On ###
        IF (self%statgravOn .OR. self%vargravOn) THEN

            ALLOCATE(self%gravoffs(neighbors,2), self%gravlens(neighbors))

            !### time varying gravity requires extra space ###
            IF (.NOT. self%vargravOn) THEN

                ALLOCATE(self%grav1d(1-self%nb:self%nx+self%nb, 2))  ! acceleration and potential
                self%ngravvars = 2
                
            ELSE


                ALLOCATE(self%grav1d(1-self%nb:self%nx+self%nb, 3))  ! potential at n-1, n, n+1/2
                self%ngravvars = 3

            END IF
            self%grav1d    = 0.0_WRD
            self%gravlens  = self%nb * self%ngravvars


        END IF

    ELSE IF (self%is2d .AND. .NOT. ALLOCATED(self%grid2d)) THEN

        self%nemfs = 0
        ALLOCATE(self%grid2d(1-self%nb:self%nx+self%nb, 1-self%nb:self%ny+self%nb, self%nvars))
        self%grid2d = 0.0_WRD

        !### we only need some fields if MHD is enabled for this Patch ###
        IF (self%mhdOn) THEN

            self%nemfs = 1
            ALLOCATE(self%emf2d(1-self%nb:self%nx+self%nb, 1-self%nb:self%ny+self%nb, self%nemfs), &
                 self%bface2d(1-self%nb:self%nx+self%nb, 1-self%nb:self%ny+self%nb, 2), &
                 self%vtavg2d(1-self%nb:self%nx+self%nb, 1-self%nb:self%ny+self%nb, 3))
            self%emf2d   = 0.0_WRD
            self%bface2d = 0.0_WRD
            self%vtavg2d = 0.0_WRD

        END IF

        neighbors = 8 ! neighbors are ordered left, right, bottom, top, bottom left, bottom right, top left, top right
        ALLOCATE(self%stateoffs(neighbors,2), self%statelens(neighbors))  ! last index is 1 = send, 2 = recv
        self%statelens(1:2) = self%nb * self%ny * self%nvars
        self%statelens(3:4) = self%nb * self%nx * self%nvars
        self%statelens(5:8) = self%nb**2 * self%nvars

        IF (self%mhdOn) THEN

            ALLOCATE(self%emfoffs(neighbors,2), self%emflens(neighbors))
            self%emflens(1:2) = self%nb * self%ny * self%nemfs
            self%emflens(3:4) = self%nb * self%nx * self%nemfs
            self%emflens(5:8) = self%nb**2 * self%nemfs

        END IF

        !### allocate and touch the passive array if On ###
        IF (self%passOn) THEN

            ALLOCATE(self%pass2d(1-self%nb:self%nx+self%nb, 1-self%nb:self%ny+self%nb, self%npass))
            self%pass2d  = 0.0_WRD

            ALLOCATE(self%passoffs(neighbors,2), self%passlens(neighbors))
            self%passlens(1:2) = self%nb * self%ny * self%npass
            self%passlens(3:4) = self%nb * self%nx * self%npass
            self%passlens(5:8) = self%nb * self%nb * self%npass

        END IF

        !### allocate and touch the cosmic rays array if On ###
        IF (self%crsOn) THEN

            ALLOCATE(self%crs2d(1-self%nb:self%nx+self%nb, 1-self%nb:self%ny+self%nb, self%ncrs))
            self%crs2d                = 0.0_WRD

            ALLOCATE(self%crsoffs(neighbors,2), self%crslens(neighbors))
            self%crslens(1:2) = self%nb * self%ny * self%ncrs
            self%crslens(3:4) = self%nb * self%nx * self%ncrs
            self%crslens(5:8) = self%nb * self%nb * self%ncrs

        END IF

        !### allocate and touch the gravity array if On ###
        IF (self%statgravOn .OR. self%vargravOn) THEN

            ALLOCATE(self%gravoffs(neighbors,2), self%gravlens(neighbors))

            !### time varying gravity requires extra space ###
            IF (.NOT. self%vargravOn) THEN

                ALLOCATE(self%grav2d(1-self%nb:self%nx+self%nb, 1-self%nb:self%ny+self%nb, 2))  ! acceleration and potential
                self%ngravvars = 2
                
            ELSE

                ALLOCATE(self%grav2d(1-self%nb:self%nx+self%nb, 1-self%nb:self%ny+self%nb, 3))  ! potential at n-1, n, n+1/2
                self%ngravvars = 3

            END IF
            self%grav2d        = 0.0_WRD
            self%gravlens(1:2) = self%nb * self%ny * self%ngravvars
            self%gravlens(3:4) = self%nb * self%nx * self%ngravvars
            self%gravlens(5:8) = self%nb * self%nb * self%ngravvars

        END IF

    ELSE IF(self%is3d .AND. .NOT. ALLOCATED(self%grid3d)) THEN

        self%nemfs = 0
        ALLOCATE(self%grid3d(1-self%nb:self%nx+self%nb, 1-self%nb:self%ny+self%nb, 1-self%nb:self%nz+self%nb, self%nvars))
        self%grid3d  = 0.0_WRD

        !### we only need some fields if MHD is enabled for this Patch ###
        IF (self%mhdOn) THEN

            self%nemfs = 3
            ALLOCATE(self%emf3d(1-self%nb:self%nx+self%nb, 1-self%nb:self%ny+self%nb, 1-self%nb:self%nz+self%nb, self%nemfs), &
                 self%bface3d(1-self%nb:self%nx+self%nb, 1-self%nb:self%ny+self%nb, 1-self%nb:self%nz+self%nb, 3), &
                 self%vtavg3d(1-self%nb:self%nx+self%nb, 1-self%nb:self%ny+self%nb, 1-self%nb:self%nz+self%nb, 3))
            self%emf3d   = 0.0_WRD
            self%bface3d = 0.0_WRD
            self%vtavg3d = 0.0_WRD

        END IF

        neighbors = 26 ! neighbors are ordered:
                       ! (6 faces)   left, right, back, front, bottom, top,
                       ! (8 corners) back bottom left, top front right, back bottom right, top front left, top back left, bottom front right, top back right, bottom front left
                       ! (12 edges)  back bottom, front top, back top, front bottom, left bottom, right top, left top, right bottom, back left, front right, back right, front left
        ALLOCATE(self%stateoffs(neighbors,2), self%statelens(neighbors))  ! last index is 1 = send, 2 = recv
        self%statelens(1:2)   = self%nb * self%ny * self%nz * self%nvars
        self%statelens(3:4)   = self%nb * self%nx * self%nz * self%nvars
        self%statelens(5:6)   = self%nb * self%nx * self%ny * self%nvars
        self%statelens(7:14)  = self%nb**3 * self%nvars
        self%statelens(15:18) = self%nb**2 * self%nx * self%nvars
        self%statelens(19:22) = self%nb**2 * self%ny * self%nvars
        self%statelens(23:26) = self%nb**2 * self%nz * self%nvars

        IF (self%mhdOn) THEN

            ALLOCATE(self%emfoffs(neighbors,2), self%emflens(neighbors))
            self%emflens(1:2)   = self%nb * self%ny * self%nz * self%nemfs
            self%emflens(3:4)   = self%nb * self%nx * self%nz * self%nemfs
            self%emflens(5:6)   = self%nb * self%nx * self%ny * self%nemfs
            self%emflens(7:14)  = self%nb**3 * self%nemfs
            self%emflens(15:18) = self%nb**2 * self%nx * self%nemfs
            self%emflens(19:22) = self%nb**2 * self%ny * self%nemfs
            self%emflens(23:26) = self%nb**2 * self%nz * self%nemfs

        END IF

        !### allocate and touch the passive array if On ###
        IF (self%passOn) THEN

            ALLOCATE(self%pass3d(1-self%nb:self%nx+self%nb, 1-self%nb:self%ny+self%nb, 1-self%nb:self%nz+self%nb, self%npass))
            self%pass3d = 0.0_WRD

            ALLOCATE(self%passoffs(neighbors,2), self%passlens(neighbors))
            self%passlens(1:2)   = self%nb * self%ny * self%nz * self%npass
            self%passlens(3:4)   = self%nb * self%nx * self%nz * self%npass
            self%passlens(5:6)   = self%nb * self%nx * self%ny * self%npass
            self%passlens(7:14)  = self%nb**3 * self%npass
            self%passlens(15:18) = self%nb**2 * self%nx * self%npass
            self%passlens(19:22) = self%nb**2 * self%ny * self%npass
            self%passlens(23:26) = self%nb**2 * self%nz * self%npass

        END IF

        !### allocate and touch the cosmic rays array if On ###
        IF (self%crsOn) THEN

            ALLOCATE(self%crs3d(1-self%nb:self%nx+self%nb, 1-self%nb:self%ny+self%nb, 1-self%nb:self%nz+self%nb, self%ncrs))
            self%crs3d = 0.0_WRD

            ALLOCATE(self%crsoffs(neighbors,2), self%crslens(neighbors))
            self%crslens(1:2)   = self%nb * self%ny * self%nz * self%ncrs
            self%crslens(3:4)   = self%nb * self%nx * self%nz * self%ncrs
            self%crslens(5:6)   = self%nb * self%nx * self%ny * self%ncrs
            self%crslens(7:14)  = self%nb**3 * self%ncrs
            self%crslens(15:18) = self%nb**2 * self%nx * self%ncrs
            self%crslens(19:22) = self%nb**2 * self%ny * self%ncrs
            self%crslens(23:26) = self%nb**2 * self%nz * self%ncrs

        END IF

        !### allocate and touch the gravity array if On ###
        IF (self%statgravOn .OR. self%vargravOn) THEN

            ALLOCATE(self%gravoffs(neighbors,2), self%gravlens(neighbors))

            !### time varying gravity requires extra space ###
            IF (.NOT. self%vargravOn) THEN

                ALLOCATE(self%grav3d(1-self%nb:self%nx+self%nb, 1-self%nb:self%ny+self%nb, 1-self%nb:self%nz+self%nb, 2))  ! acceleration and potential
                self%ngravvars = 2
                
            ELSE

                ALLOCATE(self%grav3d(1-self%nb:self%nx+self%nb, 1-self%nb:self%ny+self%nb, 1-self%nb:self%nz+self%nb, 3))  ! potential at n-1, n, n+1/2
                self%ngravvars = 3

            END IF
            self%grav3d          = 0.0_WRD
            self%gravlens(1:2)   = self%nb * self%ny * self%nz * self%ngravvars
            self%gravlens(3:4)   = self%nb * self%nx * self%nz * self%ngravvars
            self%gravlens(5:6)   = self%nb * self%nx * self%ny * self%ngravvars
            self%gravlens(7:14)  = self%nb**3 * self%ngravvars
            self%gravlens(15:18) = self%nb**2 * self%nx * self%ngravvars
            self%gravlens(19:22) = self%nb**2 * self%ny * self%ngravvars
            self%gravlens(23:26) = self%nb**2 * self%nz * self%ngravvars

        END IF

    END IF

    !### determine all of the offsets from the lengths and the final size of the buffer ###
    !### note we pack all messages to the same target contiguously in case we ever want to send them as a single message ###
    !### also note the inclusion of the load balancing header size.  we'll pack LB information along with boundary exchanges ###
    curoff = 1
    DO n = 1, neighbors
        
        !### do all of the sends to this target first ###
        self%stateoffs(n,1) = curoff
        curoff              = curoff + self%statelens(n)
        IF (self%mhdOn .AND. (self%is2d .OR. self%is3d)) THEN
            self%emfoffs(n,1) = curoff
            curoff            = curoff + self%emflens(n)
        END IF
        IF (self%passOn) THEN
            self%passoffs(n,1) = curoff
            curoff             = curoff + self%passlens(n)
        END IF
        IF (self%crsOn) THEN
            self%crsoffs(n,1) = curoff
            curoff            = curoff + self%crslens(n)
        END IF
        IF (self%statgravOn .OR. self%vargravOn) THEN
            self%gravoffs(n,1) = curoff
            curoff             = curoff + self%gravlens(n)
        END IF       
        !### now do all of the receives from this source ###
        self%stateoffs(n,2) = curoff
        curoff              = curoff + self%statelens(n)
        IF (self%mhdOn .AND. (self%is2d .OR. self%is3d)) THEN
            self%emfoffs(n,2) = curoff
            curoff            = curoff + self%emflens(n)
        END IF
        IF (self%passOn) THEN
            self%passoffs(n,2) = curoff
            curoff             = curoff + self%passlens(n)
        END IF
        IF (self%crsOn) THEN
            self%crsoffs(n,2) = curoff
            curoff            = curoff + self%crslens(n)
        END IF
        IF (self%statgravOn .OR. self%vargravOn) THEN
            self%gravoffs(n,2) = curoff
            curoff             = curoff + self%gravlens(n)
        END IF       
        
    END DO

    !### curoff now holds the total length that the comm buffer needs to be ###
    ALLOCATE(self%gridbounds(curoff-1))
    self%gridbounds = 0.0_WRD

    !### make space for boundary resolution tracking (this assumes 6 flavors of variable exchange and will need to be increased when more are added).  the last index is 1 = recv, 2 = send ###
    ALLOCATE(self%flavor_bound_resolved(neighbors, 0:6, 2), self%flavor_resolved(0:6, 2))

    self%flavor_bound_resolved        = .FALSE.
    self%flavor_resolved              = .FALSE.
    self%flavor_bound_resolved(:,0,:) = .TRUE.  ! mark the 0 transfer flavors are done for all time
    self%flavor_resolved(0,:)         = .TRUE.  ! mark the 0 transfer flavors are done for all time
    
    !### set the patch as active ###
    self%hasZones = .TRUE.
    
    ! NOTE: I should add a calculation here for the allocated size of this patch (just real arrays, not boundary related arrays).  This will be needed for load balancing metrics, and it is helpful for
    ! planning/debugging.  Since I page immediately we can expect failure for OOM right away


END SUBROUTINE build

!### deallocates all grid arrays and makes the patch inactive ###
SUBROUTINE unbuild(self)

    CLASS(Patch) :: self

    IF (self%is1d .AND. ALLOCATED(self%grid1d)) THEN

        DEALLOCATE(self%grid1d)
        IF (ALLOCATED(self%vtavg1d)) DEALLOCATE(self%vtavg1d)
        IF (ALLOCATED(self%pass1d )) DEALLOCATE(self%pass1d)
        IF (ALLOCATED(self%crs1d  )) DEALLOCATE(self%crs1d)
        IF (ALLOCATED(self%grav1d )) DEALLOCATE(self%grav1d)

    ELSE IF (self%is2d .AND. ALLOCATED(self%grid2d)) THEN

        DEALLOCATE(self%grid2d)
        IF (ALLOCATED(self%emf2d  )) DEALLOCATE(self%emf2d)
        IF (ALLOCATED(self%bface2d)) DEALLOCATE(self%bface2d)
        IF (ALLOCATED(self%vtavg2d)) DEALLOCATE(self%vtavg2d)
        IF (ALLOCATED(self%pass2d )) DEALLOCATE(self%pass2d)
        IF (ALLOCATED(self%crs2d  )) DEALLOCATE(self%crs2d)
        IF (ALLOCATED(self%grav2d )) DEALLOCATE(self%grav2d)

    ELSE IF (ALLOCATED(self%grid3d)) THEN

        DEALLOCATE(self%grid3d)
        IF (ALLOCATED(self%emf3d  )) DEALLOCATE(self%emf3d)
        IF (ALLOCATED(self%bface3d)) DEALLOCATE(self%bface3d)
        IF (ALLOCATED(self%vtavg3d)) DEALLOCATE(self%vtavg3d)
        IF (ALLOCATED(self%pass3d )) DEALLOCATE(self%pass3d)
        IF (ALLOCATED(self%crs3d  )) DEALLOCATE(self%crs3d)
        IF (ALLOCATED(self%grav3d )) DEALLOCATE(self%grav3d)

    END IF

    !### do the deallocations not tied to the number of dimensions ###
    IF (ALLOCATED(self%gridbounds)) THEN

        DEALLOCATE(self%gridbounds)
        DEALLOCATE(self%stateoffs)
        DEALLOCATE(self%statelens)
        DEALLOCATE(self%flavor_bound_resolved)
        DEALLOCATE(self%flavor_resolved)

    END IF
    IF (ALLOCATED(self%emfoffs)) THEN

        DEALLOCATE(self%emfoffs)
        DEALLOCATE(self%emflens)

    END IF
    IF (ALLOCATED(self%passoffs)) THEN

        DEALLOCATE(self%passoffs)
        DEALLOCATE(self%passlens)

    END IF
    IF (ALLOCATED(self%crsoffs)) THEN

        DEALLOCATE(self%crsoffs)
        DEALLOCATE(self%crslens)

    END IF
    IF (ALLOCATED(self%gravoffs)) THEN

        DEALLOCATE(self%gravoffs)
        DEALLOCATE(self%gravlens)

    END IF

    !### set the patch as active ###
    IF (self%hasZones) THEN

        self%hasZones = .FALSE.
        CALL OMP_DESTROY_LOCK(self%tlock)

    END IF

END SUBROUTINE unbuild

!### mark all boundaries and flavors as unresolved (like the beginning of a time step) (direction = 1 is recv, direction = 2 is send) ###
SUBROUTINE mark_unresolved(self, flavor, bound, direction, reset_pass_count)

    CLASS(Patch) :: self
    INTEGER(WIS), INTENT(IN) :: flavor, bound, direction
    LOGICAL(WIS), INTENT(IN) :: reset_pass_count

    !### skip if flavor == 0 (bounds always resolved for 0) ###
    IF (flavor .NE. 0) THEN
        !### 0 for the bound means flag all boundaries as unresolved ###
        IF (bound .EQ. 0) THEN
        
            self%flavor_bound_resolved(:,flavor,direction) = .FALSE.
            self%flavor_resolved(flavor,direction)         = .FALSE.

        ELSE

            self%flavor_bound_resolved(bound,flavor,direction) = .FALSE.
            self%flavor_resolved(flavor,direction)             = .FALSE.

        END IF
    END IF

    !### any unresolved boundaries implies the Patch is not updated ###
    self%updated      = .FALSE.
    self%update_cmplt = .FALSE.
    IF (reset_pass_count) self%update_pass_count = 0

END SUBROUTINE mark_unresolved

!### a pack/unpack routine.  pack means take values from the grid and put into the gridbounds buffer while unpack is the opposite. ###
!### m indicates which boundary to pack or unpack (1-2 for 1-d, 1-8 for 2-d, 1-26 for 3-d) ###
!### flavor indicates which variable set to operate on ###
SUBROUTINE bounds_pup(self, buf, s, e, m, flavor, pack, was_packed, msize)

    CLASS(Patch) :: self
    REAL(WRD), POINTER, INTENT(INOUT) :: buf(:)
    INTEGER(WID), INTENT(IN) :: s, e
    INTEGER(WIS), INTENT(IN) :: m
    INTEGER(WIS), INTENT(IN) :: flavor
    LOGICAL(WIS), INTENT(IN) :: pack
    LOGICAL(WIS), INTENT(OUT) :: was_packed
    INTEGER(WIS), INTENT(OUT) :: msize
    REAL(WRD) :: mybuf(1:e-s+1_WID)
    LOGICAL(WIS) :: allres
    INTEGER(WIS) :: n, b
    INTEGER(WID) :: buf_len, ms

    !### valid flavors are:
    !### 0 = no exchange
    !### 1 = grid variables
    !### 2 = emf variables
    !### 3 = passive variables
    !### 4 = cosmic rays variables
    !### 5 = gravity variables
    !### 6 = particle variables

    !### verify that the provided buffer is of sufficient size ###
    buf_len    = e-s+1_WID
    was_packed = .TRUE.
    SELECT CASE (flavor)

        CASE(0)        
            msize = 0
        CASE(1)
            IF (self%statelens(m) .GT. buf_len) was_packed = .FALSE.
            msize = self%statelens(m)
        CASE(2)
            IF (self%emflens(m) .GT. buf_len) was_packed = .FALSE.
            msize = self%emflens(m)
        CASE(3)
            IF (self%passlens(m) .GT. buf_len) was_packed = .FALSE.
            msize = self%passlens(m)
        CASE(4)
            IF (self%crslens(m) .GT. buf_len) was_packed = .FALSE.
            msize = self%crslens(m)
        CASE(5)
            IF (self%gravlens(m) .GT. buf_len) was_packed = .FALSE.
            msize = self%gravlens(m)

    END SELECT
    IF (.NOT. was_packed) RETURN

    !### array for passing to the pack routines below ###
    IF (.NOT. pack) THEN
        mybuf(1:e-s+1_WID) = buf(s:e)
    END IF

    !### we will always instruct the packing routines to start at an offset of 1 ###
    ms = 1 

    IF (self%is1d) THEN

        IF (pack) THEN

            SELECT CASE (flavor)

                CASE (0)        
                    CONTINUE
                CASE (1)
                    CALL self%pack_array1d(self%grid1d, mybuf, ms, self%nvars, m)
                CASE (3)
                    CALL self%pack_array1d(self%pass1d, mybuf, ms, self%npass, m)
                CASE (4)
                    CALL self%pack_array1d(self%crs1d, mybuf, ms, self%ncrs, m)
                CASE (5)
                    CALL self%pack_array1d(self%grav1d, mybuf, ms, self%ngravvars, m)

            END SELECT

        ELSE

            SELECT CASE (flavor)

                CASE (0)        
                    CONTINUE
                CASE (1)
                    CALL self%unpack_array1d(mybuf, ms, self%grid1d, self%nvars, m)
                CASE (3)
                    CALL self%unpack_array1d(mybuf, ms, self%pass1d, self%npass, m)
                CASE (4)
                    CALL self%unpack_array1d(mybuf, ms, self%crs1d, self%ncrs, m)
                CASE (5)
                    CALL self%unpack_array1d(mybuf, ms, self%grav1d, self%ngravvars, m)

            END SELECT

        END IF

    ELSE IF (self%is2d) THEN

        IF (pack) THEN

            SELECT CASE (flavor)

                CASE (0)        
                    CONTINUE
                CASE (1)
                    CALL self%pack_array2d(self%grid2d, mybuf, ms, self%nvars, m)
                CASE (2)
                    CALL self%pack_array2d(self%emf2d, mybuf, ms, self%nemfs, m)
                CASE (3)
                    CALL self%pack_array2d(self%pass2d, mybuf, ms, self%npass, m)
                CASE (4)
                    CALL self%pack_array2d(self%crs2d, mybuf, ms, self%ncrs, m)
                CASE (5)
                    CALL self%pack_array2d(self%grav2d, mybuf, ms, self%ngravvars, m)

            END SELECT

        ELSE

            SELECT CASE (flavor)

                CASE (0)        
                    CONTINUE
                CASE (1)
                    CALL self%unpack_array2d(mybuf, ms, self%grid2d, self%nvars, m)
                CASE (2)
                    CALL self%unpack_array2d(mybuf, ms, self%emf2d, self%nemfs, m)
                CASE (3)
                    CALL self%unpack_array2d(mybuf, ms, self%pass2d, self%npass, m)
                CASE (4)
                    CALL self%unpack_array2d(mybuf, ms, self%crs2d, self%ncrs, m)
                CASE (5)
                    CALL self%unpack_array2d(mybuf, ms, self%grav2d, self%ngravvars, m)

            END SELECT

        END IF

    ELSE IF (self%is3d) THEN

        IF (pack) THEN

            SELECT CASE (flavor)

                CASE (0)        
                    CONTINUE
                CASE (1)
                    CALL self%pack_array3d(self%grid3d, mybuf, ms, self%nvars, m)
                CASE (2)
                    CALL self%pack_array3d(self%emf3d, mybuf, ms, self%nemfs, m)
                CASE (3)
                    CALL self%pack_array3d(self%pass3d, mybuf, ms, self%npass, m)
                CASE (4)
                    CALL self%pack_array3d(self%crs3d, mybuf, ms, self%ncrs, m)
                CASE (5)
                    CALL self%pack_array3d(self%grav3d, mybuf, ms, self%ngravvars, m)

            END SELECT

        ELSE

            SELECT CASE (flavor)

                CASE (0)        
                    CONTINUE
                CASE (1)
                    CALL self%unpack_array3d(mybuf, ms, self%grid3d, self%nvars, m)
                CASE (2)
                    CALL self%unpack_array3d(mybuf, ms, self%emf3d, self%nemfs, m)
                CASE (3)
                    CALL self%unpack_array3d(mybuf, ms, self%pass3d, self%npass, m)
                CASE (4)
                    CALL self%unpack_array3d(mybuf, ms, self%crs3d, self%ncrs, m)
                CASE (5)
                    CALL self%unpack_array3d(mybuf, ms, self%grav3d, self%ngravvars, m)

            END SELECT

        END IF

    END IF

    !### flavor 6 packing error (ran out of DMBMs)
    IF (.NOT. was_packed) RETURN

    !### if we are packing we need to copy the data back into the passed pointer ###
    IF (pack) THEN
        buf(s:e) = mybuf(1:e-s+1_WID)
    END IF

    !### mark whatever bound we worked on resolved.  although the rest of this object is not thread safe, for performance reasons I choose to put the thread lock ###
    !### here for max concurrency ###
    CALL OMP_SET_LOCK(self%tlock)
    IF (pack) THEN

        self%flavor_bound_resolved(m,flavor,2) = .TRUE.
        allres = .TRUE.
        DO b = 1, SIZE(self%flavor_bound_resolved,1)

            IF (.NOT. self%flavor_bound_resolved(b,flavor,2)) allres = .FALSE.

        END DO
        self%flavor_resolved(flavor,2) = allres

    ELSE
        
        self%flavor_bound_resolved(m,flavor,1) = .TRUE.
        allres = .TRUE.
        DO b = 1, SIZE(self%flavor_bound_resolved,1)

            IF (.NOT. self%flavor_bound_resolved(b,flavor,1)) allres = .FALSE.

        END DO
        self%flavor_resolved(flavor,1) = allres

    END IF
    CALL OMP_UNSET_LOCK(self%tlock)

END SUBROUTINE bounds_pup

!### a direct pack routine. ###
!### src/dest_bound_id indicates which boundary to pack or unpack (1-2 for 1-d, 1-8 for 2-d, 1-26 for 3-d) ###
!### src_bound_id is NOT read when unpacking and can be set to anything ###
!### flavor indicates which variable set to operate on ###
!### this routine differs from the one above in that it will pack directly into another Patch's boundary buffer.  this is an acceleration for on-rank ###
!### copies between patches and avoids double buffering.  ###
SUBROUTINE bounds_direct_pup(self, dest, src_bound_id, dest_bound_id, flavor, pack, was_packed)

    CLASS(Patch) :: self
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: dest(:)
    INTEGER(WIS), INTENT(IN) :: flavor, src_bound_id, dest_bound_id
    LOGICAL(WIS), INTENT(IN) :: pack
    LOGICAL(WIS), INTENT(OUT) :: was_packed
    LOGICAL(WIS) :: allres
    INTEGER(WIS) :: m, n, b, length, end
    INTEGER(WID) :: offset

    !### valid flavors are:
    !### 0 = no exchange
    !### 1 = grid variables
    !### 2 = emf variables
    !### 3 = passive variables
    !### 4 = cosmic rays variables
    !### 5 = gravity variables
    !### 6 = particle variables

    m = src_bound_id
    n = dest_bound_id

    !### verify that the provided buffer is of exactly the right size (since this should be Patch to Patch) ###
    was_packed = .TRUE.
    IF (SIZE(self%gridbounds) .NE. SIZE(dest)) was_packed = .FALSE.
    IF (.NOT. was_packed) RETURN  

    IF (self%is1d) THEN

        IF (pack) THEN

            SELECT CASE (flavor)

                CASE (0)                
                    CONTINUE
                CASE (1)
                    offset = self%stateoffs(n,2)
                    CALL self%pack_array1d(self%grid1d, dest, offset, self%nvars, m)
                CASE (3)
                    offset = self%passoffs(n,2)
                    CALL self%pack_array1d(self%pass1d, dest, offset, self%npass, m)
                CASE (4)
                    offset = self%crsoffs(n,2)
                    CALL self%pack_array1d(self%crs1d, dest, offset, self%ncrs, m)
                CASE (5)
                    offset = self%gravoffs(n,2)
                    CALL self%pack_array1d(self%grav1d, dest, offset, self%ngravvars, m)

            END SELECT

        ELSE

            SELECT CASE (flavor)
                
                CASE (0)                
                    CONTINUE
                CASE (1)
                    offset = self%stateoffs(n,2)
                    CALL self%unpack_array1d(dest, offset, self%grid1d, self%nvars, n)
                CASE (3)
                    offset = self%passoffs(n,2)
                    CALL self%unpack_array1d(dest, offset, self%pass1d, self%npass, n)
                CASE (4)
                    offset = self%crsoffs(n,2)
                    CALL self%unpack_array1d(dest, offset, self%crs1d, self%ncrs, n)
                CASE (5)
                    offset = self%gravoffs(n,2)
                    CALL self%unpack_array1d(dest, offset, self%grav1d, self%ngravvars, n)
            END SELECT

        END IF

    ELSE IF (self%is2d) THEN

        IF (pack) THEN

            SELECT CASE (flavor)

                CASE (0)                
                    CONTINUE
                CASE (1)
                    offset = self%stateoffs(n,2)
                    CALL self%pack_array2d(self%grid2d, dest, offset, self%nvars, m)
                CASE (2)
                    offset = self%emfoffs(n,2)
                    CALL self%pack_array2d(self%emf2d, dest, offset, self%nemfs, m)
                CASE (3)
                    offset = self%passoffs(n,2)
                    CALL self%pack_array2d(self%pass2d, dest, offset, self%npass, m)
                CASE (4)
                    offset = self%crsoffs(n,2)
                    CALL self%pack_array2d(self%crs2d, dest, offset, self%ncrs, m)
                CASE (5)
                    offset = self%gravoffs(n,2)
                    CALL self%pack_array2d(self%grav2d, dest, offset, self%ngravvars, m)

            END SELECT 

        ELSE

            SELECT CASE (flavor)

                CASE (0)                
                    CONTINUE
                CASE (1)
                    offset = self%stateoffs(n,2)
                    CALL self%unpack_array2d(dest, offset, self%grid2d, self%nvars, n)
                CASE (2)
                    offset = self%emfoffs(n,2)
                    CALL self%unpack_array2d(dest, offset, self%emf2d, self%nemfs, n)
                CASE (3)
                    offset = self%passoffs(n,2)
                    CALL self%unpack_array2d(dest, offset, self%pass2d, self%npass, n)
                CASE (4)
                    offset = self%crsoffs(n,2)
                    CALL self%unpack_array2d(dest, offset, self%crs2d, self%ncrs, n)
                CASE (5)
                    offset = self%gravoffs(n,2)
                    CALL self%unpack_array2d(dest, offset, self%grav2d, self%ngravvars, n)

            END SELECT

        END IF

    ELSE IF (self%is3d) THEN

        IF (pack) THEN

            SELECT CASE (flavor)
                
                CASE (0)                
                    CONTINUE
                CASE (1)
                    offset = self%stateoffs(n,2)
                    CALL self%pack_array3d(self%grid3d, dest, offset, self%nvars, m)
                CASE (2)
                    offset = self%emfoffs(n,2)
                    CALL self%pack_array3d(self%emf3d, dest, offset, self%nemfs, m)
                CASE (3)
                    offset = self%passoffs(n,2)
                    CALL self%pack_array3d(self%pass3d, dest, offset, self%npass, m)
                CASE (4)
                    offset = self%crsoffs(n,2)
                    CALL self%pack_array3d(self%crs3d, dest, offset, self%ncrs, m)
                CASE (5)
                    offset = self%gravoffs(n,2)
                    CALL self%pack_array3d(self%grav3d, dest, offset, self%ngravvars, m)

            END SELECT

        ELSE

            SELECT CASE (flavor)
                
                CASE (0)                
                    CONTINUE
                CASE (1)
                    offset = self%stateoffs(n,2)
                    CALL self%unpack_array3d(dest, offset, self%grid3d, self%nvars, n)
                CASE (2)
                    offset = self%emfoffs(n,2)
                    CALL self%unpack_array3d(dest, offset, self%emf3d, self%nemfs, n)
                CASE (3)
                    offset = self%passoffs(n,2)
                    CALL self%unpack_array3d(dest, offset, self%pass3d, self%npass, n)
                CASE (4)
                    offset = self%crsoffs(n,2)
                    CALL self%unpack_array3d(dest, offset, self%crs3d, self%ncrs, n)
                CASE (5)
                    offset = self%gravoffs(n,2)
                    CALL self%unpack_array3d(dest, offset, self%grav3d, self%ngravvars, n)

            END SELECT

        END IF

    END IF

    !### mark whatever bound we worked on resolved  although the rest of this object is not thread safe, for performance reasons I choose to put the thread lock ###
    !### here for max concurrency ###
    CALL OMP_SET_LOCK(self%tlock)
    IF (pack) THEN

        self%flavor_bound_resolved(m,flavor,2) = .TRUE.
        allres = .TRUE.
        DO b = 1, SIZE(self%flavor_bound_resolved,1)

            IF (.NOT. self%flavor_bound_resolved(b,flavor,2)) allres = .FALSE.

        END DO
        self%flavor_resolved(flavor,2) = allres

    ELSE

        self%flavor_bound_resolved(n,flavor,1) = .TRUE.
        allres = .TRUE.
        DO b = 1, SIZE(self%flavor_bound_resolved,1)

            IF (.NOT. self%flavor_bound_resolved(b,flavor,1)) allres = .FALSE.

        END DO
        self%flavor_resolved(flavor,1) = allres

    END IF
    CALL OMP_UNSET_LOCK(self%tlock)

END SUBROUTINE bounds_direct_pup

!### packing for 1-d contiguous array ###
SUBROUTINE pack_array1d(self, src, dest, o, nvars, m)

    CLASS(Patch) :: self
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: src(:,:)
    REAL(WRD), CONTIGUOUS, INTENT(OUT) :: dest(:)
    INTEGER(WIS), INTENT(IN) :: nvars, m
    INTEGER(WID), INTENT(IN) :: o
    INTEGER(WIS) :: iib, v, offset

    !### m = 1,2 are the left and right neighbors ###
    offset = (m-1)*(self%nx - self%nb) + self%nb + 1
    DO v = 1, nvars
        
        iib = (v-1)*self%nb + o
        dest(iib:iib+self%nb-1) = src(offset:offset+self%nb-1,v)
        
    END DO

END SUBROUTINE pack_array1d

!### unpacking for 1-d contiguous array ###
SUBROUTINE unpack_array1d(self, src, o, dest, nvars, m)

    CLASS(Patch) :: self
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: src(:)
    REAL(WRD), CONTIGUOUS, INTENT(OUT) :: dest(:,:)
    INTEGER(WIS), INTENT(IN) :: nvars, m
    INTEGER(WID), INTENT(IN) :: o
    INTEGER(WIS) :: iib, v, offset
    
    !### m = 1,2 are the left and right neighbors ###
    offset = (m-1)*(self%nx + self%nb) + 1
    DO v = 1, nvars
        
        iib = (v-1)*self%nb + o
        dest(offset:offset+self%nb-1,v) = src(iib:iib+self%nb-1)
            
    END DO

END SUBROUTINE unpack_array1d

!### packing for 2-d contiguous array ###
SUBROUTINE pack_array2d(self, src, dest, o, nvars, m)

    CLASS(Patch) :: self
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: src(:,:,:)
    REAL(WRD), CONTIGUOUS, INTENT(OUT) :: dest(:)
    INTEGER(WIS), INTENT(IN) :: nvars, m
    INTEGER(WID), INTENT(IN) :: o
    INTEGER(WIS) :: j, ijb, iib, v, ioffset, joffset, mm

    !### different copy pathways depending on the boundary number ###
    SELECT CASE (m)
    
        !### m = 1,2 are left and right X edges, which are not contiguous copies.  break it up into short contiguous copies. ###
        CASE (1, 2)

            ioffset = (m-1)*(self%nx - self%nb) + self%nb + 1
            joffset = self%nb
            DO v = 1, nvars
                
                ijb = (v-1)*self%ny*self%nb
                DO j = joffset+1, joffset+self%ny
                    
                    iib = ijb + (j-joffset-1)*self%nb + o
                    dest(iib:iib+self%nb-1) = src(ioffset:ioffset+self%nb-1,j,v)
                    
                END DO
                
            END DO

        !### m = 3,4 are Y boundaries, which are likely to be larger contiguous strips along X. ###
        CASE (3, 4)

            mm      = m-2
            ioffset = self%nb + 1
            joffset = (mm-1)*(self%ny - self%nb) + self%nb
            DO v = 1, nvars
                
                ijb = (v-1)*self%nx*self%nb
                DO j = joffset+1, joffset+self%nb
                    
                    iib = ijb + (j-joffset-1)*self%nx + o
                    dest(iib:iib+self%nx-1) = src(ioffset:ioffset+self%nx-1,j,v)
                    
                END DO
                
            END DO

        !### m = 5,6 are the -90 degree left and right corners ###
        CASE (5, 6)

            mm      = m-4
            ioffset = (2-mm)*(self%nb - self%nx) + self%nx + 1
            joffset = (mm-1)*(self%ny - self%nb) + self%nb
            DO v = 1, nvars
                
                ijb = (v-1)*self%nb*self%nb
                DO j = joffset+1, joffset+self%nb
                    
                    iib = ijb + (j-joffset-1)*self%nb + o
                    dest(iib:iib+self%nb-1) = src(ioffset:ioffset+self%nb-1,j,v)
                    
                END DO
                
            END DO

         !### m = 7,8 are the +90 degree left and right corners ###
        CASE (7, 8)

            mm      = m-6
            ioffset = (2-mm)*(self%nb - self%nx) + self%nx + 1
            joffset = (2-mm)*(self%ny - self%nb) + self%nb
            DO v = 1, nvars
                
                ijb = (v-1)*self%nb*self%nb
                DO j = joffset+1, joffset+self%nb
                    
                    iib = ijb + (j-joffset-1)*self%nb + o
                    dest(iib:iib+self%nb-1) = src(ioffset:ioffset+self%nb-1,j,v)
                    
                END DO
                
            END DO

    END SELECT

END SUBROUTINE pack_array2d

!### unpacking for 2-d contiguous array ###
SUBROUTINE unpack_array2d(self, src, o, dest, nvars, m)

    CLASS(Patch) :: self
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: src(:)
    REAL(WRD), CONTIGUOUS, INTENT(OUT) :: dest(:,:,:)
    INTEGER(WIS), INTENT(IN) :: nvars, m
    INTEGER(WID), INTENT(IN) :: o
    INTEGER(WIS) :: j, ijb, iib, v, ioffset, joffset, mm

    !### different copy pathways depending on the boundary number ###
    SELECT CASE (m)
    
        !### m = 1,2 are left and right X edges, which are not contiguous copies.  break it up into short contiguous copies. ###
        CASE (1, 2)

            ioffset = (m-1)*(self%nx + self%nb) + 1
            joffset = self%nb
            DO v = 1, nvars
                
                ijb = (v-1)*self%ny*self%nb
                DO j = joffset+1, joffset+self%ny
                    
                    iib = ijb + (j-joffset-1)*self%nb + o
                    dest(ioffset:ioffset+self%nb-1,j,v) = src(iib:iib+self%nb-1)
                    
                END DO
                
            END DO

        !### m = 3,4 are Y boundaries, which are likely to be larger contiguous strips along X. ###
        CASE (3, 4)

            mm      = m-2
            ioffset = self%nb + 1
            joffset = (mm-1)*(self%ny + self%nb)
            DO v = 1, nvars
                
                ijb = (v-1)*self%nx*self%nb
                DO j = joffset+1, joffset+self%nb
                    
                    iib = ijb + (j-joffset-1)*self%nx + o
                    dest(ioffset:ioffset+self%nx-1,j,v) = src(iib:iib+self%nx-1)
                    
                END DO
                
            END DO

        !### m = 5,6 are the -90 degree left and right corners ###
        CASE (5, 6)

            mm      = m-4
            ioffset = (mm-1)*(self%nx + self%nb) + 1
            joffset = (mm-1)*(self%ny + self%nb)
            DO v = 1, nvars
                
                ijb = (v-1)*self%nb*self%nb
                DO j = joffset+1, joffset+self%nb
                    
                    iib = ijb + (j-joffset-1)*self%nb + o
                    dest(ioffset:ioffset+self%nb-1,j,v) = src(iib:iib+self%nb-1)
                    
                END DO
                
            END DO

         !### m = 7,8 are the +90 degree left and right corners ###
        CASE (7, 8)

            mm      = m-6
            ioffset = (mm-1)*(self%nx + self%nb) + 1
            joffset = (2-mm)*(self%ny + self%nb)
            DO v = 1, nvars
                
                ijb = (v-1)*self%nb*self%nb
                DO j = joffset+1, joffset+self%nb
                    
                    iib = ijb + (j-joffset-1)*self%nb + o
                    dest(ioffset:ioffset+self%nb-1,j,v) = src(iib:iib+self%nb-1)
                    
                END DO
                
            END DO

    END SELECT

END SUBROUTINE unpack_array2d

!### packing for 3-d contiguous array ###
SUBROUTINE pack_array3d(self, src, dest, o, nvars, m)

    CLASS(Patch) :: self
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: src(:,:,:,:)
    REAL(WRD), CONTIGUOUS, INTENT(OUT) :: dest(:)
    INTEGER(WID), INTENT(IN) :: o
    INTEGER(WIS), INTENT(IN) :: nvars, m
    INTEGER(WIS) :: k, j, ikb, ijb, iib, v, ioffset, joffset, koffset, mm

    !### different copy pathways depending on the boundary number ###
    SELECT CASE (m)
    
        !### m = 1,2 are left and right X edges, which are not contiguous copies.  break it up into short contiguous copies. ###
        CASE (1, 2)

            ioffset = (m-1)*(self%nx - self%nb) + self%nb + 1
            joffset = self%nb
            koffset = self%nb
            DO v = 1, nvars
                
                ikb = (v-1)*self%nz*self%ny*self%nb
                DO k = koffset+1, koffset+self%nz

                    ijb = ikb + (k-koffset-1)*self%ny*self%nb
                    DO j = joffset+1, joffset+self%ny
                    
                        iib = ijb + (j-joffset-1)*self%nb + o
                        dest(iib:iib+self%nb-1) = src(ioffset:ioffset+self%nb-1,j,k,v)
                    
                    END DO
                
                END DO

            END DO

        !### m = 3,4 are Y boundaries, which are likely to be larger contiguous strips along X then Z plane. ###
        CASE (3, 4)

            mm      = m-2
            ioffset = self%nb + 1
            joffset = (mm-1)*(self%ny - self%nb) + self%nb
            koffset = self%nb
            DO v = 1, nvars
                
                ikb = (v-1)*self%nz*self%nx*self%nb
                DO k = koffset+1, koffset+self%nz

                    ijb = ikb + (k-koffset-1)*self%nx*self%nb
                    DO j = joffset+1, joffset+self%nb
                    
                        iib = ijb + (j-joffset-1)*self%nx + o
                        dest(iib:iib+self%nx-1) = src(ioffset:ioffset+self%nx-1,j,k,v)
                    
                    END DO
                
                END DO

            END DO

        !### m = 5,6 are Z boundaries, which are likely to be large contiguous strips in X then Y plane ###
        CASE (5, 6)

            mm      = m-4
            ioffset = self%nb + 1
            joffset = self%nb
            koffset = (mm-1)*(self%nz - self%nb) + self%nb
            DO v = 1, nvars

                ikb = (v-1)*self%ny*self%nx*self%nb
                DO k = koffset+1, koffset+self%nb

                    ijb = ikb + (k-koffset-1)*self%ny*self%nx
                    DO j = joffset+1, joffset+self%ny
                    
                        iib = ijb + (j-joffset-1)*self%nx + o
                        dest(iib:iib+self%nx-1) = src(ioffset:ioffset+self%nx-1,j,k,v)
                    
                    END DO
                
                END DO
    

            END DO

        !### m = 7:14 are the corners ###
        CASE (7 : 14)

            SELECT CASE (m)

                !### back bottom left and front top right ###
                CASE (7, 8)

                    mm      = m-6
                    ioffset = (mm-1)*(self%nx - self%nb) + self%nb + 1
                    IF (mm .EQ. 1) THEN
                        joffset = self%ny
                    ELSE
                        joffset = self%nb
                    END IF
                    koffset = (mm-1)*(self%nz - self%nb) + self%nb

                !### back bottom right and front top left ###
                CASE (9, 10)

                    mm      = m-8
                    IF (mm .EQ. 1) THEN
                        ioffset = self%nx + 1
                        joffset = self%ny
                    ELSE
                        ioffset = self%nb + 1
                        joffset = self%nb
                    END IF
                    koffset = (mm-1)*(self%nz - self%nb) + self%nb

                !### back top left and front bottom right ###
                CASE (11, 12)

                    mm      = m-10
                    ioffset = (mm-1)*(self%nx - self%nb) + self%nb + 1
                    joffset = (mm-1)*(self%nb - self%ny) + self%ny
                    IF (mm .EQ. 1) THEN
                        koffset = self%nz
                    ELSE
                        koffset = self%nb
                    END IF

                !### back top right and front bottom left ###
                CASE (13, 14)

                    mm      = m-12
                    IF (mm .EQ. 1) THEN
                        ioffset = self%nx + 1
                        koffset = self%nz
                    ELSE
                        ioffset = self%nb + 1
                        koffset = self%nb
                    END IF
                    joffset = (mm-1)*(self%nb - self%ny) + self%ny

            END SELECT
            DO v = 1, nvars
                
                ikb = (v-1)*self%nb**3
                DO k = koffset+1, koffset+self%nb

                    ijb = ikb + (k-koffset-1)*self%nb*self%nb
                    DO j = joffset+1, joffset+self%nb
                    
                        iib = ijb + (j-joffset-1)*self%nb + o
                        dest(iib:iib+self%nb-1) = src(ioffset:ioffset+self%nb-1,j,k,v)
                    
                    END DO
                
                END DO

            END DO

        !### m = 15:18 are the edges contiguous along X ###
        CASE (15 : 18)

            SELECT CASE (m)

                !### back bottom and front top ###
                CASE (15, 16)

                    mm      = m-14
                    ioffset = self%nb + 1
                    IF (mm .EQ. 1) THEN
                        joffset = self%ny
                    ELSE
                        joffset = self%nb
                    END IF
                    koffset = (mm-1)*(self%nz - self%nb) + self%nb

                !### back top and front bottom ###
                CASE (17, 18)

                    mm      = m-16
                    ioffset = self%nb + 1
                    IF (mm .EQ. 1) THEN
                        joffset = self%ny
                        koffset = self%nz
                    ELSE
                        joffset = self%nb
                        koffset = self%nb
                    END IF

            END SELECT
            DO v = 1, nvars
                
                ikb = (v-1)*self%nb*self%nb*self%nx
                DO k = koffset+1, koffset+self%nb

                    ijb = ikb + (k-koffset-1)*self%nb*self%nx
                    DO j = joffset+1, joffset+self%nb
                    
                        iib = ijb + (j-joffset-1)*self%nx + o
                        dest(iib:iib+self%nx-1) = src(ioffset:ioffset+self%nx-1,j,k,v)
                    
                    END DO
                
                END DO

            END DO

        !### m = 19:22 are the edges along Y ###
        CASE (19 : 22)

            SELECT CASE (m)

                !### left bottom and right top ###
                CASE (19, 20)

                    mm      = m-18
                    ioffset = (mm-1)*(self%nx - self%nb) + self%nb + 1
                    joffset = self%nb
                    koffset = (mm-1)*(self%nz - self%nb) + self%nb

                !### left top and right bottom ###
                CASE (21, 22)

                    mm      = m-20
                    ioffset = (mm-1)*(self%nx - self%nb) + self%nb + 1
                    joffset = self%nb
                    IF (mm .EQ. 1) THEN
                        koffset = self%nz
                    ELSE
                        koffset = self%nb
                    END IF

            END SELECT
            DO v = 1, nvars
                
                ikb = (v-1)*self%nb*self%nb*self%ny
                DO k = koffset+1, koffset+self%nb

                    ijb = ikb + (k-koffset-1)*self%nb*self%ny
                    DO j = joffset+1, joffset+self%ny
                    
                        iib = ijb + (j-joffset-1)*self%nb + o
                        dest(iib:iib+self%nb-1) = src(ioffset:ioffset+self%nb-1,j,k,v)
                    
                    END DO
                
                END DO

            END DO

        !### m = 23:26 are the edges along Z ###
        CASE (23 : 26)

            SELECT CASE (m)

                !### back left and front right ###
                CASE (23, 24)

                    mm      = m-22
                    ioffset = (mm-1)*(self%nx - self%nb) + self%nb + 1
                    IF (mm .EQ. 1) THEN
                        joffset = self%ny
                    ELSE
                        joffset = self%nb
                    END IF
                    koffset = self%nb

                !### back right and front left ###
                CASE (25, 26)

                    mm      = m-24
                    IF (mm .EQ. 1) THEN
                        ioffset = self%nx + 1
                        joffset = self%ny
                    ELSE
                        ioffset = self%nb + 1
                        joffset = self%nb
                    END IF
                    koffset = self%nb

            END SELECT
            DO v = 1, nvars
                
                ikb = (v-1)*self%nb*self%nb*self%nz
                DO k = koffset+1, koffset+self%nz

                    ijb = ikb + (k-koffset-1)*self%nb*self%nb
                    DO j = joffset+1, joffset+self%nb
                    
                        iib = ijb + (j-joffset-1)*self%nb + o
                        dest(iib:iib+self%nb-1) = src(ioffset:ioffset+self%nb-1,j,k,v)
                    
                    END DO
                
                END DO

            END DO

    END SELECT

END SUBROUTINE pack_array3d

!### unpacking for 3-d contiguous array ###
SUBROUTINE unpack_array3d(self, src, o, dest, nvars, m)

    CLASS(Patch) :: self
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: src(:)
    REAL(WRD), CONTIGUOUS, INTENT(OUT) :: dest(:,:,:,:)
    INTEGER(WID), INTENT(IN) :: o
    INTEGER(WIS), INTENT(IN) :: nvars, m
    INTEGER(WIS) :: k, j, ikb, ijb, iib, v, ioffset, joffset, koffset, mm

    !### different copy pathways depending on the boundary number ###
    SELECT CASE (m)
    
        !### m = 1,2 are left and right X edges, which are not contiguous copies.  break it up into short contiguous copies. ###
        CASE (1, 2)

            ioffset = (m-1)*(self%nx + self%nb) + 1
            joffset = self%nb
            koffset = self%nb
            DO v = 1, nvars
                
                ikb = (v-1)*self%nz*self%ny*self%nb
                DO k = koffset+1, koffset+self%nz

                    ijb = ikb + (k-koffset-1)*self%ny*self%nb
                    DO j = joffset+1, joffset+self%ny
                    
                        iib = ijb + (j-joffset-1)*self%nb + o
                        dest(ioffset:ioffset+self%nb-1,j,k,v) = src(iib:iib+self%nb-1)
                    
                    END DO
                
                END DO

            END DO

        !### m = 3,4 are Y boundaries, which are likely to be larger contiguous strips along X then Z plane. ###
        CASE (3, 4)

            mm      = m-2
            ioffset = self%nb + 1
            joffset = (mm-1)*(self%ny + self%nb)
            koffset = self%nb
            DO v = 1, nvars
                
                ikb = (v-1)*self%nz*self%nx*self%nb
                DO k = koffset+1, koffset+self%nz

                    ijb = ikb + (k-koffset-1)*self%nx*self%nb
                    DO j = joffset+1, joffset+self%nb
                    
                        iib = ijb + (j-joffset-1)*self%nx + o
                        dest(ioffset:ioffset+self%nx-1,j,k,v) = src(iib:iib+self%nx-1)
                    
                    END DO
                
                END DO

            END DO

        !### m = 5,6 are Z boundaries, which are likely to be large contiguous strips in X then Y plane ###
        CASE (5, 6)

            mm      = m-4
            ioffset = self%nb + 1
            joffset = self%nb
            koffset = (mm-1)*(self%nz + self%nb)
            DO v = 1, nvars

                ikb = (v-1)*self%ny*self%nx*self%nb
                DO k = koffset+1, koffset+self%nb

                    ijb = ikb + (k-koffset-1)*self%ny*self%nx
                    DO j = joffset+1, joffset+self%ny
                    
                        iib = ijb + (j-joffset-1)*self%nx + o
                        dest(ioffset:ioffset+self%nx-1,j,k,v) = src(iib:iib+self%nx-1)
                    
                    END DO
                
                END DO
    

            END DO

        !### m = 7:14 are the corners ###
        CASE (7 : 14)

            SELECT CASE (m)

                !### back bottom left and front top right ###
                CASE (7, 8)

                    mm      = m-6
                    ioffset = (mm-1)*(self%nx + self%nb) + 1
                    IF (mm .EQ. 1) THEN
                        joffset = self%ny + self%nb
                    ELSE
                        joffset = 0
                    END IF
                    koffset = (mm-1)*(self%nz + self%nb)

                !### back bottom right and front top left ###
                CASE (9, 10)

                    mm      = m-8
                    IF (mm .EQ. 1) THEN
                        ioffset = self%nx + self%nb + 1
                        joffset = self%ny + self%nb
                    ELSE
                        ioffset = 1
                        joffset = 0
                    END IF
                    koffset = (mm-1)*(self%nz + self%nb)

                !### back top left and front bottom right ###
                CASE (11, 12)

                    mm      = m-10
                    ioffset = (mm-1)*(self%nx + self%nb) + 1
                    IF (mm .EQ. 1) THEN
                        joffset = self%ny + self%nb
                        koffset = self%nz + self%nb
                    ELSE
                        joffset = 0
                        koffset = 0
                    END IF

                !### back top right and front bottom left ###
                CASE (13, 14)

                    mm      = m-12
                    IF (mm .EQ. 1) THEN
                        ioffset = self%nx + self%nb + 1
                        joffset = self%ny + self%nb
                        koffset = self%nz + self%nb
                    ELSE
                        ioffset = 1
                        joffset = 0
                        koffset = 0
                    END IF

            END SELECT
            DO v = 1, nvars
                
                ikb = (v-1)*self%nb**3
                DO k = koffset+1, koffset+self%nb

                    ijb = ikb + (k-koffset-1)*self%nb*self%nb
                    DO j = joffset+1, joffset+self%nb
                    
                        iib = ijb + (j-joffset-1)*self%nb + o
                        dest(ioffset:ioffset+self%nb-1,j,k,v) = src(iib:iib+self%nb-1)
                    
                    END DO
                
                END DO

            END DO

        !### m = 15:18 are the edges contiguous along X ###
        CASE (15 : 18)

            SELECT CASE (m)

                !### back bottom and front top ###
                CASE (15, 16)

                    mm      = m-14
                    ioffset = self%nb + 1
                    IF (mm .EQ. 1) THEN
                        joffset = self%ny + self%nb
                    ELSE
                        joffset = 0
                    END IF
                    koffset = (mm-1)*(self%nz + self%nb)

                !### back top and front bottom ###
                CASE (17, 18)

                    mm      = m-16
                    ioffset = self%nb + 1
                    IF (mm .EQ. 1) THEN
                        joffset = self%ny + self%nb
                        koffset = self%nz + self%nb
                    ELSE
                        joffset = 0
                        koffset = 0
                    END IF

            END SELECT
            DO v = 1, nvars
                
                ikb = (v-1)*self%nb*self%nb*self%nx
                DO k = koffset+1, koffset+self%nb

                    ijb = ikb + (k-koffset-1)*self%nb*self%nx
                    DO j = joffset+1, joffset+self%nb
                    
                        iib = ijb + (j-joffset-1)*self%nx + o
                        dest(ioffset:ioffset+self%nx-1,j,k,v) = src(iib:iib+self%nx-1)
                    
                    END DO
                
                END DO

            END DO

        !### m = 19:22 are the edges along Y ###
        CASE (19 : 22)

            SELECT CASE (m)

                !### left bottom and right top ###
                CASE (19, 20)

                    mm      = m-18
                    ioffset = (mm-1)*(self%nx + self%nb) + 1
                    joffset = self%nb
                    koffset = (mm-1)*(self%nz + self%nb)

                !### left top and right bottom ###
                CASE (21, 22)

                    mm      = m-20
                    ioffset = (mm-1)*(self%nx + self%nb) + 1
                    joffset = self%nb
                    IF (mm .EQ. 1) THEN
                        koffset = self%nz + self%nb
                    ELSE
                        koffset = 0
                    END IF

            END SELECT
            DO v = 1, nvars
                
                ikb = (v-1)*self%nb*self%nb*self%ny
                DO k = koffset+1, koffset+self%nb

                    ijb = ikb + (k-koffset-1)*self%nb*self%ny
                    DO j = joffset+1, joffset+self%ny
                    
                        iib = ijb + (j-joffset-1)*self%nb + o
                        dest(ioffset:ioffset+self%nb-1,j,k,v) = src(iib:iib+self%nb-1)
                    
                    END DO
                
                END DO

            END DO

        !### m = 23:26 are the edges along Z ###
        CASE (23 : 26)

            SELECT CASE (m)

                !### back left and front right ###
                CASE (23, 24)

                    mm      = m-22
                    ioffset = (mm-1)*(self%nx + self%nb) + 1
                    IF (mm .EQ. 1) THEN
                        joffset = self%ny + self%nb
                    ELSE
                        joffset = 0
                    END IF
                    koffset = self%nb

                !### back right and front left ###
                CASE (25, 26)

                    mm      = m-24
                    IF (mm .EQ. 1) THEN
                        ioffset = self%nx + self%nb + 1
                        joffset = self%ny + self%nb
                    ELSE
                        ioffset = 1
                        joffset = 0
                    END IF
                    koffset = self%nb

            END SELECT
            DO v = 1, nvars
                
                ikb = (v-1)*self%nb*self%nb*self%nz
                DO k = koffset+1, koffset+self%nz

                    ijb = ikb + (k-koffset-1)*self%nb*self%nb
                    DO j = joffset+1, joffset+self%nb
                    
                        iib = ijb + (j-joffset-1)*self%nb + o
                        dest(ioffset:ioffset+self%nb-1,j,k,v) = src(iib:iib+self%nb-1)
                    
                    END DO
                
                END DO

            END DO

    END SELECT

END SUBROUTINE unpack_array3d

!### totally disable and clean the object ###
SUBROUTINE destroy(self)

    CLASS(Patch) :: self

    !### deallocate ###
    CALL self%unbuild()

    !### disable ###
    self%isLive = .FALSE.

END SUBROUTINE destroy

END MODULE Mod_Patch
