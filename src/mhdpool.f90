#include "config.h"

MODULE Mod_MHDPool

!######################################################################
!#
!# FILENAME: mod_mhdpool.f90
!#
!# DESCRIPTION: This module defines an object for work arrays for use
!#              by the MHDTVD object.
!#
!# DEPENDENCIES: Mod_Globals for defining data types
!#
!# HISTORY:
!#    5/7/12  - Peter Mendygral
!#
!######################################################################

!### require the needed modules ###
USE Mod_Globals
USE Mod_DataPool

IMPLICIT NONE

!### default private but make the data type available ###
PRIVATE
PUBLIC :: MHDPool

!### define a data type for this module ###
TYPE, EXTENDS(DataPool) :: MHDPool

    !### default everything as private ###
    PRIVATE

    INTEGER(WIS), PUBLIC :: nflux = 8, nwaves = 7, nstate = 8, nspeeds = 4                                 ! we'll do 8 flux components with 7 MHD waves (we will jump up when we introduce the dual energy formalism)
    REAL(WRD), PUBLIC :: dt, dx, max_wave
    REAL(WRD), PUBLIC, ALLOCATABLE :: Pflux1d(:,:,:,:,:), Pflux2d(:,:,:,:,:), Pflux3d(:,:,:,:,:), &        ! flux along primary axis
                                      workPflux2d(:,:,:,:,:), workPflux3d(:,:,:,:,:), &                    ! flux along primary axis used only for the half time step in CTU
                                      protPflux1d(:,:,:,:,:), protPflux2d(:,:,:,:,:), &
                                      protPflux3d(:,:,:,:,:), &                                            ! HLLE protection fluxes used when TVD produces unphysical values
                                      workProtPflux2d(:,:,:,:,:), workProtPflux3d(:,:,:,:,:), &            ! flux along primary axis used only for the half time step in CTU
                                      Bflux2d(:,:), Bflux3d(:,:,:), &                                      ! magnetic flux scratch array needed for transposes in 2 and 3d
                                      EMF2d(:,:,:), EMF3d(:,:,:,:,:), &                                    ! all EMFs used to generate the corner EMFs in CT
                                      protBflux2d(:,:), protBflux3d(:,:,:), &                              ! protection magnetic flux scratch array needed for transposes in 2 and 3d
                                      protEMF2d(:,:,:), protEMF3d(:,:,:,:,:), &                            ! protection EMFs used to generate the corner EMFs in CT
                                      protCTEMF3d(:,:,:,:), &                                              ! since TVD+CTU needs protection EMFs used in the CT update for intermediate steps
                                                                                                           ! we need an extra one that emulates the patch emf3d grid
                                      eigenVec1d(:,:,:,:), eigenVec2d(:,:,:,:), &                          ! right handed eigen vectors
                                      charac1d(:,:,:), charac2d(:,:,:), &                                  ! characteristics
                                      eigenVal1d(:,:,:), eigenVal2d(:,:,:), &                              ! eigenvalues
                                      waves1d(:,:,:), waves2d(:,:,:), &                                    ! wave speeds
                                      state_avg1d(:,:,:), state_avg2d(:,:,:), &                            ! zone averages computed for the eigenvectors (note that 2 more elements are added for the X and pressure values)
                                      pressure1d(:,:), pressure2d(:,:), &                                  ! gas pressure computed from possibly pre-conditioned state variables
                                      qwork2d(:,:,:), qwork3d(:,:,:,:), &                                  ! scratch arrays for the grid needed for transposes in 2 and 3 d
                                      state_save1d(:,:), state_save2d(:,:,:), state_save3d(:,:,:,:), &     ! a pristine backup of the state variables
                                      prot_q1d(:,:), prot_q2d(:,:,:), prot_q3d(:,:,:,:), &                 ! a backup of the state variables prior to a flux application to be used if protection fluxes are needed
                                      bface_save2d(:,:,:), bface_save3d(:,:,:,:), &                        ! a pristine backup of the face centered field
                                      source1d(:,:), source2d(:,:,:), source3d(:,:,:,:)                    ! container for source terms

    LOGICAL(WIS), PUBLIC, ALLOCATABLE :: prot_flags1d(:), prot_flags2d(:,:), prot_flags3d(:,:,:)           ! flags for whether or not protection fluxes need to be used around this zone

    CONTAINS

    !### default everything as private ###
    PRIVATE

    PROCEDURE :: initmhdpool
    PROCEDURE, PUBLIC :: destroy

END TYPE MHDPool

INTERFACE MHDPool

    MODULE PROCEDURE constructor

END INTERFACE

CONTAINS

!### constructor call ###
FUNCTION constructor(ndim, nx, ny, nz, nb)

    TYPE(MHDPool) :: constructor
    INTEGER(WIS), INTENT(IN) :: ndim, nx, ny, nz, nb

    CALL constructor%initmhdpool(ndim, nx, ny, nz, nb)
    RETURN

END FUNCTION constructor

!### allocate all of the necessary arrays ###
!### immediately start filling them as a first touch.  the caller is an individual thread. ###
SUBROUTINE initmhdpool(self, ndim, nx, ny, nz, nb)

    CLASS(MHDPool) :: self
    INTEGER(WIS), INTENT(IN) :: ndim, nx, ny, nz, nb
    INTEGER(WIS) :: maxl

    !### 2 and 3d work arrays must be able to handle rotations, so find the max length of any dimension and use that ###
    IF (ndim .EQ. 2) THEN
        maxl = MAX(nx, ny)
    ELSE IF (ndim .EQ. 3) THEN
        maxl = MAX(nx, ny, nz)
    END IF

    !### for 2 and 3d we basically do updates in planes (2d is a single plane).  reuse the same work arrays for those cases to make that clear ###
    IF (ndim .GT. 1) THEN

        ALLOCATE(self%eigenVec2d(1-nb:maxl+nb,1-nb:maxl+nb,self%nflux,self%nwaves), &
             self%charac2d(1-nb:maxl+nb,1-nb:maxl+nb,self%nwaves), self%eigenVal2d(1-nb:maxl+nb,1-nb:maxl+nb,self%nwaves), self%waves2d(1-nb:maxl+nb,1-nb:maxl+nb,self%nspeeds), &
             self%state_avg2d(1-nb:maxl+nb,1-nb:maxl+nb,self%nstate+2), self%pressure2d(1-nb:maxl+nb,1-nb:maxl+nb), &
             self%qwork2d(1-nb:maxl+nb,1-nb:maxl+nb,self%nstate))
		
        !### do a first touch ###
        self%eigenVec2d   = 0.0_WRD
        self%charac2d     = 0.0_WRD
        self%eigenVal2d   = 0.0_WRD
        self%waves2d      = 0.0_WRD
        self%state_avg2d  = 0.0_WRD
        self%pressure2d   = 0.0_WRD
        self%qwork2d      = 0.0_WRD

    END IF

    IF (ndim .EQ. 1) THEN

        ALLOCATE(self%Pflux1d(1-nb:nx+nb,1,1,self%nflux,1), self%eigenVec1d(1-nb:nx+nb,1,self%nflux,self%nwaves), &
            self%charac1d(1-nb:nx+nb,1,self%nwaves), self%eigenVal1d(1-nb:nx+nb,1,self%nwaves), &
            self%waves1d(1-nb:nx+nb,1,self%nspeeds), self%state_avg1d(1-nb:nx+nb,1,self%nstate+2), self%pressure1d(1-nb:nx+nb,1), self%state_save1d(1-nb:nx+nb,self%nstate), &
            self%qwork2d(1-nb:nx+nb,1,self%nstate),  self%source1d(1-nb:nx+nb,self%nstate), &
            self%protPflux1d(1-nb:nx+nb,1,1,self%nflux,1), self%prot_q1d(1-nb:nx+nb,self%nstate), self%prot_flags1d(1-nb:nx+nb))
    
        !### do a first touch ###
        self%Pflux1d      = 0.0_WRD
        self%eigenVec1d   = 0.0_WRD
        self%charac1d     = 0.0_WRD
        self%eigenVal1d   = 0.0_WRD
        self%waves1d      = 0.0_WRD
        self%state_avg1d  = 0.0_WRD
        self%pressure1d   = 0.0_WRD
        self%state_save1d = 0.0_WRD
        self%qwork2d      = 0.0_WRD
        self%source1d     = 0.0_WRD
        self%protPflux1d  = 0.0_WRD
        self%prot_q1d     = 0.0_WRD
        self%prot_flags1d = .FALSE.
        
    ELSE IF (ndim .EQ. 2) THEN

        ALLOCATE(self%Pflux2d(1-nb:maxl+nb,1-nb:maxl+nb,1,self%nflux,2), self%workPflux2d(1-nb:maxl+nb,1-nb:maxl+nb,1,self%nflux,2), self%Bflux2d(1-nb:maxl+nb,1-nb:maxl+nb), self%EMF2d(1-nb:nx+nb,1-nb:ny+nb,3), &
             self%state_save2d(1-nb:nx+nb,1-nb:ny+nb,self%nstate), self%source2d(1-nb:nx+nb,1-nb:ny+nb,self%nstate), self%bface_save2d(1-nb:nx+nb,1-nb:ny+nb,2), &
             self%protPflux2d(1-nb:maxl+nb,1-nb:maxl+nb,1,self%nflux,2), self%workProtPflux2d(1-nb:maxl+nb,1-nb:maxl+nb,1,self%nflux,2), &
             self%protBflux2d(1-nb:maxl+nb,1-nb:maxl+nb), self%protEMF2d(1-nb:nx+nb,1-nb:ny+nb,3), &
             self%prot_q2d(1-nb:maxl+nb,1-nb:maxl+nb,self%nstate), self%prot_flags2d(1-nb:maxl+nb,1-nb:maxl+nb))
		
        !### do a first touch ###
        self%Pflux2d         = 0.0_WRD
        self%workPflux2d     = 0.0_WRD
        self%Bflux2d         = 0.0_WRD
        self%EMF2d           = 0.0_WRD
        self%state_save2d    = 0.0_WRD
        self%bface_save2d    = 0.0_WRD
        self%source2d        = 0.0_WRD
        self%protPflux2d     = 0.0_WRD
        self%protBflux2d     = 0.0_WRD
        self%protEMF2d       = 0.0_WRD
        self%workProtPflux2d = 0.0_WRD
        self%prot_q2d        = 0.0_WRD
        self%prot_flags2d    = .FALSE.
	
    ELSE IF (ndim .EQ. 3) THEN
	
        ALLOCATE(self%Pflux3d(1-nb:maxl+nb,1-nb:maxl+nb,1-nb:maxl+nb,self%nflux,3), self%workPflux3d(1-nb:maxl+nb,1-nb:maxl+nb,1-nb:maxl+nb,self%nflux,5), self%Bflux3d(1-nb:maxl+nb,1-nb:maxl+nb,2), &
             self%EMF3d(1-nb:nx+nb,1-nb:ny+nb,1-nb:nz+nb,3,3), self%state_save3d(1-nb:nx+nb,1-nb:ny+nb,1-nb:nz+nb,self%nstate), self%source3d(1-nb:nx+nb,1-nb:ny+nb,1-nb:nz+nb,self%nstate), &
             self%bface_save3d(1-nb:nx+nb,1-nb:ny+nb,1-nb:nz+nb,3), self%qwork3d(1-nb:nx+nb,1-nb:ny+nb,1-nb:nz+nb,self%nstate), &
             self%protPflux3d(1-nb:maxl+nb,1-nb:maxl+nb,1-nb:maxl+nb,self%nflux,3), self%workProtPflux3d(1-nb:maxl+nb,1-nb:maxl+nb,1-nb:maxl+nb,self%nflux,5), &
             self%protBflux3d(1-nb:maxl+nb,1-nb:maxl+nb,2), self%protEMF3d(1-nb:nx+nb,1-nb:ny+nb,1-nb:nz+nb,3,3), self%protCTEMF3d(1-nb:nx+nb,1-nb:ny+nb,1-nb:nz+nb,3), &
             self%prot_q3d(1-nb:nx+nb,1-nb:ny+nb,1-nb:nz+nb,self%nstate), self%prot_flags3d(1-nb:nx+nb,1-nb:ny+nb,1-nb:nz+nb))
		
        !### do a first touch ###
        self%Pflux3d         = 0.0_WRD
        self%workPflux3d     = 0.0_WRD
        self%Bflux3d         = 0.0_WRD
        self%EMF3d           = 0.0_WRD
        self%state_save3d    = 0.0_WRD
        self%bface_save3d    = 0.0_WRD
        self%qwork3d         = 0.0_WRD
        self%protPflux3d     = 0.0_WRD
        self%protBflux3d     = 0.0_WRD
        self%protEMF3d       = 0.0_WRD
        self%protCTEMF3d     = 0.0_WRD
        self%workProtPflux3d = 0.0_WRD
        self%prot_q3d        = 0.0_WRD
        self%prot_flags3d    = .FALSE.
        self%source3d        = 0.0_WRD
        
    END IF
	
    !### store the constant sizes ###
    self%ndim = ndim
    self%nb     = nb
    self%nx     = nx
    self%ny     = ny
    self%nz     = nz

END SUBROUTINE initmhdpool

!### clean up all allocated memory ###
SUBROUTINE destroy(self)

    CLASS(MHDPool) :: self

    IF (self%ndim .GT. 1 .AND. ALLOCATED(self%eigenVec2d)) THEN

        DEALLOCATE(self%eigenVec2d)
        DEALLOCATE(self%charac2d)
        DEALLOCATE(self%eigenVal2d)
        DEALLOCATE(self%waves2d)
        DEALLOCATE(self%state_avg2d)
        DEALLOCATE(self%pressure2d)
        DEALLOCATE(self%qwork2d)

    END IF
    
    IF (self%ndim .EQ. 1 .AND. ALLOCATED(self%Pflux1d)) THEN
	
        DEALLOCATE(self%Pflux1d)
        DEALLOCATE(self%eigenVec1d)
        DEALLOCATE(self%charac1d)
        DEALLOCATE(self%eigenVal1d)
        DEALLOCATE(self%waves1d)
        DEALLOCATE(self%state_avg1d)
        DEALLOCATE(self%pressure1d)
        DEALLOCATE(self%state_save1d)
        DEALLOCATE(self%qwork2d)
        DEALLOCATE(self%source1d)
        DEALLOCATE(self%protPflux1d)
        DEALLOCATE(self%prot_q1d)
        DEALLOCATE(self%prot_flags1d)
        
    ELSE IF (self%ndim .EQ. 2 .AND. ALLOCATED(self%Pflux2d)) THEN
	
        DEALLOCATE(self%Pflux2d)
        DEALLOCATE(self%workPflux2d)
        DEALLOCATE(self%Bflux2d)
        DEALLOCATE(self%EMF2d)
        DEALLOCATE(self%state_save2d)
        DEALLOCATE(self%bface_save2d)
        DEALLOCATE(self%source2d)
        DEALLOCATE(self%protPflux2d)
        DEALLOCATE(self%protBflux2d)
        DEALLOCATE(self%protEMF2d)
        DEALLOCATE(self%workProtPflux2d)
        DEALLOCATE(self%prot_q2d)
        DEALLOCATE(self%prot_flags2d)
        
    ELSE IF (self%ndim .EQ. 3 .AND. ALLOCATED(self%Pflux3d)) THEN
	
        DEALLOCATE(self%Pflux3d)
        DEALLOCATE(self%workPflux3d)
        DEALLOCATE(self%Bflux3d)
        DEALLOCATE(self%EMF3d)
        DEALLOCATE(self%state_save3d)
        DEALLOCATE(self%bface_save3d)
        DEALLOCATE(self%qwork3d)
        DEALLOCATE(self%source3d)
        DEALLOCATE(self%protPflux3d)
        DEALLOCATE(self%protBflux3d)
        DEALLOCATE(self%protEMF3d)
        DEALLOCATE(self%protCTEMF3d)
        DEALLOCATE(self%workProtPflux3d)
        DEALLOCATE(self%prot_q3d)
        DEALLOCATE(self%prot_flags3d)
        
    END IF
    
END SUBROUTINE destroy

END MODULE
