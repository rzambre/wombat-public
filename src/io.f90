#include "config.h"

MODULE Mod_IO

!######################################################################
!#
!# FILENAME: mod_io.f90
!#
!# DESCRIPTION: 
!#
!# DEPENDENCIES: Mod_Globals for defining data types
!#
!# HISTORY:
!#    4/19/2014  - Peter Mendygral
!#
!######################################################################

!### require the needed externals ###
USE OMP_LIB

!### require the needed modules ###
USE Mod_Globals
USE Mod_Error
USE Mod_RankLocation
USE Mod_SimulationUnits
USE Mod_Domain

IMPLICIT NONE

!### default private but make the data type available ###
PRIVATE
PUBLIC :: IO

!### define a type for an AIO operation ###
TYPE :: AIO_OPS

    INTEGER(WIS) :: islots(WNUM_AIO_OPS_INFLIGHT)
    INTEGER(WADDRESS_KIND) :: offsets(WNUM_AIO_OPS_INFLIGHT,2), lengths(WNUM_AIO_OPS_INFLIGHT,2)
    LOGICAL(WIS) :: inflight(WNUM_AIO_OPS_INFLIGHT)
    CHARACTER(256) :: manifestFile(WNUM_AIO_OPS_INFLIGHT)

END TYPE AIO_OPS

!### define a data type for this module ###
TYPE :: IO

    !### default everything as private ###
    PRIVATE

    !### we need buffers for movie, compressed and restarts ###
    CHARACTER(1), ALLOCATABLE :: movie_buf(:), comp_buf(:), restart_buf(:)

    !### some bookkeeping for AIO ops ###
    TYPE(AIO_OPS) :: comp_aio_ops, movie_aio_ops

    !### we need info about each type of file ###
    CHARACTER(256) :: movieFileRoot, moviePath, movieHost, compressedFileRoot, compressedPath, compressedHost, &
         restartFileRoot, restartPath, restartHost
    INTEGER(WIS), PUBLIC :: movieNumber, compressedNumber, restartNumber
    LOGICAL(WIS) :: movieOn, compressedOn, restartOn
    REAL(WRD) :: movieInterval, compressedInterval, restartInterval

    !### we need a flag telling if we actually have AIO servers ###
    LOGICAL(WIS) :: aioOn, ioLead

    !### some bookeeping items for file generation ###
    INTEGER(WIS) :: aio_slot, n_patches_added, n_active_patches, bytes_per_patch, remaining_patches, n_patches_in_write, fpart, nranks_x, nranks_y, nranks_z, my_coords(3)
    INTEGER(WADDRESS_KIND) :: manifest_offset, data_offset, boffset
    REAL(WRD), ALLOCATABLE :: fieldbuf(:,:)
    LOGICAL(WIS), ALLOCATABLE :: patch_added1d(:), patch_added2d(:,:), patch_added3d(:,:,:)
    CHARACTER(256) :: dataFile 

    !### accounting for the sizes we'll generate and options for fields ###
    INTEGER(WID) :: restart_buffer_size, compressed_buffer_size, movie_buffer_size
    INTEGER(WIS) :: comp_active_fields, comp_fields(WMAX_IO_FIELDS), movie_active_fields, movie_fields(WMAX_IO_FIELDS)
    CHARACTER(48) :: comp_field_titles(WMAX_IO_FIELDS), movie_field_titles(WMAX_IO_FIELDS)
    CHARACTER(5) :: comp_field_symbs(WMAX_IO_FIELDS), comp_field_precs(WMAX_IO_FIELDS), movie_field_symbs(WMAX_IO_FIELDS), movie_field_precs(WMAX_IO_FIELDS)
    CHARACTER(8) :: comp_field_maps(WMAX_IO_FIELDS), movie_field_maps(WMAX_IO_FIELDS)
    REAL(WRS) :: comp_field_mins(WMAX_IO_FIELDS), comp_field_maxs(WMAX_IO_FIELDS), movie_field_mins(WMAX_IO_FIELDS), movie_field_maxs(WMAX_IO_FIELDS)
    REAL(4) :: comp_sendinfo(64), movie_sendinfo(64)

    !### some information we'll embed into the dumps ###
    CHARACTER(256), PUBLIC :: simulationTitle
    CHARACTER(2048) :: simulationDescription

    !### for compressed and movie dumps we need the format specifier arrays ###
    CHARACTER(WMAX_IO_FORM_LEN) :: movieFormat(WMAX_IO_FIELDS), compressedFormat(WMAX_IO_FIELDS)

    !### set our next dump times ###
    REAL(WRD) :: next_movie_dumpt, next_compressed_dumpt, next_restart_dumpt

    !### we need to know the number of threads ###
    INTEGER(WIS) :: nthreads

    CONTAINS

    !### default everything as private ###
    PRIVATE

    PROCEDURE, PUBLIC :: time_to_write
    PROCEDURE, PUBLIC :: init_dumpset
    PROCEDURE, PUBLIC :: write_dumpset
    PROCEDURE, PUBLIC :: destroy
    PROCEDURE, PUBLIC :: allocate_field_buffer_patch_flags
    PROCEDURE, PUBLIC :: deallocate_field_buffer_patch_flags

    PROCEDURE :: initio
    PROCEDURE :: determine_patch_size
    PROCEDURE :: gen_manifest_header
    PROCEDURE :: gen_patch_manifest_record
    PROCEDURE :: add_line
    PROCEDURE :: append_patch_data
    PROCEDURE :: parse_format
    PROCEDURE :: wait_aio_slots
    PROCEDURE :: get_patch_manifest_length

END TYPE IO

!### create an interface to the constructor ###
INTERFACE IO

    MODULE PROCEDURE constructor

END INTERFACE IO

!### object methods ###
CONTAINS

!### a constructor for the class ###
FUNCTION constructor(nthreads, iolead, naio, rankloc, simunits, namelist)

    TYPE(IO) :: constructor
    LOGICAL(WIS), INTENT(IN) :: iolead
    INTEGER(WIS), INTENT(IN) :: nthreads, naio
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits
    CHARACTER(WMAX_NAMELIST_LEN), INTENT(IN) :: namelist

    CALL constructor%initio(nthreads, iolead, naio, rankloc, simunits, namelist)
    RETURN

END FUNCTION constructor

!### initialize the object (NOT thread safe) ###
SUBROUTINE initio(self, nthreads, iolead, naio, rankloc, simunits, namelist)

    CLASS(IO) :: self
    LOGICAL(WIS), INTENT(IN) :: iolead
    INTEGER(WIS), INTENT(IN) :: nthreads, naio
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits
    CHARACTER(WMAX_NAMELIST_LEN), INTENT(IN) :: namelist
    INTEGER(WIS) :: op, hop, i
    CHARACTER(256) :: movieFileRoot, moviePath, movieHost, compressedFileRoot, compressedPath, compressedHost, &
         restartFileRoot, restartPath, restartHost
    INTEGER(WIS) :: movieNumber, compressedNumber, restartNumber
    LOGICAL(WIS) :: movieOn, compressedOn, restartOn
    CHARACTER(WMAX_IO_FORM_LEN) :: movieFormat(WMAX_IO_FIELDS), compressedFormat(WMAX_IO_FIELDS)
    CHARACTER(256) :: simulationTitle
    CHARACTER(2048) :: simulationDescription
    INTEGER(WID) :: restart_buffer_size, compressed_buffer_size, movie_buffer_size
    REAL(WRD) :: movieInterval, compressedInterval, restartInterval

    NAMELIST /IOOptions/  movieFileRoot, moviePath, movieHost, compressedFileRoot, compressedPath, compressedHost, &
         restartFileRoot, restartPath, restartHost, movieNumber, compressedNumber, restartNumber, movieOn, &
         compressedOn, restartOn, movieFormat, compressedFormat, simulationTitle, simulationDescription, &
         restart_buffer_size, compressed_buffer_size, movie_buffer_size, movieInterval, compressedInterval, restartInterval
 
    !### default the format lists to blank ###
    movieFormat      = ' '
    compressedFormat = ' '

    !### store information about the world set of MPI ranks ###
    self%nthreads  = nthreads
    self%ioLead    = ioLead
    self%nranks_x  = rankloc%nranks(1)
    self%nranks_y  = rankloc%nranks(2)
    self%nranks_z  = rankloc%nranks(3)
    self%my_coords = rankloc%my_coords

    !### gather our parameters from the namelist contents ###
    READ(namelist, NML=IOOptions)

    !### copy those values into our object ###
    self%movieFileRoot          = movieFileRoot
    self%moviePath              = moviePath
    self%movieHost              = movieHost
    self%compressedFileRoot     = compressedFileRoot
    self%compressedPath         = compressedPath
    self%compressedHost         = compressedHost
    self%restartFileRoot        = restartFileRoot
    self%restartPath            = restartPath
    self%restartHost            = restartHost
    self%movieNumber            = movieNumber
    self%compressedNumber       = compressedNumber
    self%restartNumber          = restartNumber
    self%movieOn                = movieOn
    self%compressedOn           = compressedOn
    self%restartOn              = restartOn
    self%movieFormat            = movieFormat
    self%compressedFormat       = compressedFormat
    self%simulationTitle        = simulationTitle
    self%simulationDescription  = simulationDescription
    self%restart_buffer_size    = restart_buffer_size
    self%compressed_buffer_size = compressed_buffer_size
    self%movie_buffer_size      = movie_buffer_size
    self%movieInterval          = movieInterval
    self%compressedInterval     = compressedInterval
    self%restartInterval        = restartInterval

    !### rewind the dump numbers by one sow we can keep the dump counter consistent (NOTE that when restarts are added this must be recalled) ###
    self%compressedNumber = self%compressedNumber - 1
    self%movieNumber      = self%movieNumber - 1
    self%restartNumber    = self%restartNumber - 1

    !### allocate the IO buffers ###
    IF (self%restart_buffer_size .LE. 0   ) self%restart_buffer_size    = WRESTART_IOBUFFER_SIZE
    IF (self%compressed_buffer_size .LE. 0) self%compressed_buffer_size = WCOMPRESSED_IOBUFFER_SIZE
    IF (self%movie_buffer_size .LE. 0     ) self%movie_buffer_size      = WMOVIE_IOBUFFER_SIZE
    self%restart_buffer_size    = self%restart_buffer_size * 1024**2
    self%compressed_buffer_size = self%compressed_buffer_size * 1024**2
    self%movie_buffer_size      = self%movie_buffer_size * 1024**2

    !### TODO, replace these with MPI_ALLOC_MEM
    ALLOCATE(self%restart_buf(self%restart_buffer_size), self%comp_buf(self%compressed_buffer_size), self%movie_buf(self%movie_buffer_size))

    !### page these arrays now.  implied do loops here caused troubles for Intel and CCE
    DO i = 1, self%restart_buffer_size
        self%restart_buf(i) = CHAR(0)
    END DO
    DO i = 1, self%compressed_buffer_size
        self%comp_buf(i) = CHAR(0)
    end do
    DO i = 1, self%movie_buffer_size
        self%movie_buf(i) = CHAR(0)
    END DO

    !### parse the field formats for compressed and movie dumps ###
    CALL self%parse_format(1)
    CALL self%parse_format(2)

    !### initialize out AIO_OPS arrays ###
    DO op = 1, WNUM_AIO_OPS_INFLIGHT

        self%comp_aio_ops%islots(op)    = op
        self%comp_aio_ops%inflight(op)  = .FALSE.
        self%movie_aio_ops%islots(op)   = WNUM_AIO_OPS_INFLIGHT + op
        self%movie_aio_ops%inflight(op) = .FALSE.

        self%comp_aio_ops%offsets(op,:)  = 0
        self%comp_aio_ops%lengths(op,:)  = 0
        self%movie_aio_ops%offsets(op,:) = 0
        self%movie_aio_ops%lengths(op,:) = 0

    END DO

    !### set the flag if there are AIO servers ###
    self%aioOn = .FALSE.
    IF (naio .GT. 0) self%aioOn = .TRUE.

    !### default to writing out the next movie and compressed dumps at the initial time (with restarts these need to be adjusted) ###
    self%next_movie_dumpt      = simunits%t0
    self%next_compressed_dumpt = simunits%t0

END SUBROUTINE initio

!### clean up the object ###
SUBROUTINE destroy(self)

    CLASS(IO) :: self

    !$OMP MASTER

    !### finish up and outstanding IO ###
    CALL self%wait_aio_slots(1, 0, .TRUE.)
    CALL self%wait_aio_slots(2, 0, .TRUE.)

    IF (ALLOCATED(self%restart_buf)) DEALLOCATE(self%restart_buf)
    IF (ALLOCATED(self%comp_buf)   ) DEALLOCATE(self%comp_buf)
    IF (ALLOCATED(self%movie_buf)  ) DEALLOCATE(self%movie_buf)

    !$OMP END MASTER
    !$OMP BARRIER

END SUBROUTINE destroy

!### check if it is time to write a dump set for a given flavor given the current time (pass back up updated dt if we're really close) ###
SUBROUTINE time_to_write(self, flavor, t, dt, flag, dt_mod)

    CLASS(IO) :: self
    INTEGER(WIS), INTENT(IN) :: flavor
    REAL(WRD), INTENT(IN) :: t, dt
    LOGICAL(WIS), INTENT(OUT) :: flag
    REAL(WRD), INTENT(INOUT) :: dt_mod

    !$OMP SINGLE

    dt_mod = dt
    IF (flavor .EQ. 1) THEN

        IF (ABS(t - self%next_compressed_dumpt) .LE. 1.0E-14_WRD .AND. self%compressedOn) THEN

            flag = .TRUE.
            self%next_compressed_dumpt = self%next_compressed_dumpt + self%compressedInterval

        ELSE

            flag = .FALSE.

        END IF
        IF (self%compressedOn) THEN

            IF (dt+t .GT. self%next_compressed_dumpt) THEN
                dt_mod = self%next_compressed_dumpt - t
            END IF

        END IF

    ELSE IF (flavor .EQ. 2) THEN

        IF (ABS(t - self%next_movie_dumpt) .LE. 1.0E-14_WRD .AND. self%movieOn) THEN

            flag = .TRUE.
            self%next_movie_dumpt = self%next_movie_dumpt + self%movieInterval

        ELSE

            flag = .FALSE.

        END IF
        IF (self%movieOn) THEN

            IF (dt+t .GT. self%next_movie_dumpt) THEN
                dt_mod = self%next_movie_dumpt - t
            END IF
            
        END IF
        
    END IF

    !$OMP END SINGLE

END SUBROUTINE time_to_write

!### initialize a new dumpset for a specific type ###
SUBROUTINE init_dumpset(self, flavor)

    CLASS(IO) :: self
    INTEGER(WIS), INTENT(IN) :: flavor

    !$OMP MASTER

    !### increment the dump counter ###
    IF (flavor .EQ. 1) THEN

        self%compressedNumber = self%compressedNumber + 1

    ELSE IF (flavor .EQ. 2) THEN

        self%movieNumber = self%movieNumber + 1

    END IF

    !$OMP END MASTER
    !$OMP BARRIER 

END SUBROUTINE init_dumpset

!### write_dumpset will write out all of the pieces for a compressed or movie dump set ###
SUBROUTINE write_dumpset(self, workd, flavor, mygamma)

    CLASS(IO) :: self
    TYPE(Domain) :: workd
    INTEGER(WIS), INTENT(IN) :: flavor
    REAL(WRD), INTENT(IN) :: mygamma
    INTEGER(WIS) :: mymanifest_offset, manifest_h_length, manifest_p_length, pi, pj, pk, oldest_aio_slot, op, i, p
    INTEGER(WID) :: blength, mydata_offset, slimit
    LOGICAL(WIS) :: keep_writing, write_patch
    CHARACTER(256) :: cRoot

    !### get the number of active patches and how much memory each will consume ###
    CALL self%determine_patch_size(workd, flavor)

    !### the number of loops needed to pack and send data is just the ratio of how much we need to write and the size of our buffer ###
    !### this becoming 1 will provide the best performance since we can get nearly total IO overlap ###
    IF (flavor .EQ. 1) THEN
        slimit  = MIN(INT(WMPI_MAX_MESSAGE,WID) * 1024**2, self%compressed_buffer_size)
        blength = self%compressed_buffer_size
    ELSE IF (flavor .EQ. 2) THEN
        slimit  = MIN(INT(WMPI_MAX_MESSAGE,WID) * 1024**2, self%movie_buffer_size)
        blength = self%movie_buffer_size
    END IF

    !### set the manifest header length if we are the IO lead ###
    IF (self%ioLead) THEN

        manifest_h_length = STORAGE_SIZE(self%simulationDescription)/8 + WMAX_IO_FIELDS * 64 + 1024

    ELSE

        manifest_h_length = 0

    END IF
    manifest_p_length = 320

    !$OMP MASTER 

    !### reset the patch writing flags ###
    IF (workd%is1d) THEN

        self%patch_added1d = .FALSE.

    ELSE IF (workd%is2d) THEN

        self%patch_added2d = .FALSE.

    ELSE IF (workd%is3d) THEN

        self%patch_added3d = .FALSE.

    END IF

    !### so AIO actually does not support having multiple writes in flight while also gathering meta data ###
    !### we would need a feature that keeps meta collection separate by AIO op handle ###
    !### we can make a choice to force all data flavors into a single dumpset.  Do we do that? ###
    !### until that is decided we need all flavors in-flight to be complete ###
    CALL self%wait_aio_slots(1, 0, .TRUE.)
    CALL self%wait_aio_slots(2, 0, .TRUE.)

    !### find an AIO slot we can use that is open ###
    self%aio_slot = 0
    DO op = 1, WNUM_AIO_OPS_INFLIGHT

        IF (flavor .EQ. 1 .AND. .NOT. self%comp_aio_ops%inflight(op)) THEN

            self%aio_slot = op
            self%comp_aio_ops%inflight(op) = .TRUE.
            EXIT
            
        ELSE IF (flavor .EQ. 2 .AND. .NOT. self%movie_aio_ops%inflight(op)) THEN
            
            self%aio_slot = op
            self%movie_aio_ops%inflight(op) = .TRUE.
            EXIT
            
        END IF
        
    END DO

    !### if none are open, wait on the oldest one to complete and use that ###
    IF (self%aio_slot .EQ. 0) THEN

        self%aio_slot = 1
        CALL self%wait_aio_slots(flavor, 0, .TRUE.)
        IF (flavor .EQ. 1) THEN
            self%comp_aio_ops%inflight(self%aio_slot) = .TRUE.
        ELSE IF (flavor .EQ. 2) THEN
            self%movie_aio_ops%inflight(self%aio_slot) = .TRUE.
        END IF

    END IF

    !### find the max buffer offset+length of any inflight IO operation as it will be our starting offset ###
    self%boffset = 1
    DO op = 1, WNUM_AIO_OPS_INFLIGHT

        IF (flavor .EQ. 1 .AND. self%comp_aio_ops%inflight(op)) THEN

            self%boffset = MAX(self%boffset, self%comp_aio_ops%offsets(op,2) + self%comp_aio_ops%lengths(op,2))

        ELSE IF (flavor .EQ. 2 .AND. self%movie_aio_ops%inflight(op)) THEN

            self%boffset = MAX(self%boffset, self%movie_aio_ops%offsets(op,2) + self%movie_aio_ops%lengths(op,2))

        END IF

    END DO

    !### if we are out of room in the buffer then we need to wait on all operations to complete to free the buffer ###
    IF ((self%boffset + manifest_h_length + manifest_p_length + self%bytes_per_patch) .GE. blength) THEN

        CALL self%wait_aio_slots(flavor, 0, .TRUE.)
        self%boffset = 1

    END IF
    
    !### initialize the offsetes and remaining patch counter ###
    self%data_offset        = self%boffset
    self%remaining_patches  = self%n_active_patches
    self%fpart              = 0

    !$OMP END MASTER
    !$OMP BARRIER

    !### add a check that the associated buffer is big enough for at least one Patch otherwise the loop below will be infinite ###
    !### do the loop, but remember this call is thread safe in a parallel region. ###
    DO WHILE (self%remaining_patches .GT. 0)

        !### we'll pack up all the buffer we have noting that the actual messages may need to be broken up limited by the maximum MPI message size ###
        DO WHILE (self%boffset .LT. blength)

            !### if we're doing multiple parts to the write we need to wait on the previous part and then reserve the slot right away again ###
            IF (self%fpart .GT. 0) THEN

                !$OMP MASTER 
                CALL self%wait_aio_slots(flavor, self%aio_slot, .FALSE.)

                IF (flavor .EQ. 1) THEN

                    self%comp_aio_ops%inflight(self%aio_slot) = .TRUE.

                ELSE IF (flavor .EQ. 2) THEN

                    self%movie_aio_ops%inflight(self%aio_slot) = .TRUE.

                END IF

                !$OMP END MASTER
                !$OMP BARRIER

            END IF

            !$OMP MASTER

            !### determine the number of patches we can do in this write ###
            self%n_patches_in_write = MIN(self%remaining_patches, FLOOR(REAL((slimit - manifest_h_length),4) / REAL((manifest_p_length + self%bytes_per_patch),4)))
            self%manifest_offset    = self%boffset + self%n_patches_in_write * self%bytes_per_patch

            IF (flavor .EQ. 1) THEN

                !### set the filenames ###
                IF (self%aioOn) THEN
                    WRITE(self%comp_aio_ops%manifestFile(self%aio_slot), '(A5, "-", I4.4, "-000")') self%compressedFileRoot, self%compressedNumber
                    WRITE(cRoot, '(A5, "-", I4.4, "-part", I2.2)') self%compressedFileRoot, self%compressedNumber, self%fpart
                    CALL AIO_GEN_DATA_FILE_NAME(cRoot, self%dataFile)
                ELSE
                    WRITE(self%comp_aio_ops%manifestFile(self%aio_slot), '(A5, "-", I4.4, "-", I3.3)') self%compressedFileRoot, self%compressedNumber, WRANK
                    WRITE(self%dataFile, '(A5, "-", I4.4, "-part", I2.2, "-", I3.3)') self%compressedFileRoot, self%compressedNumber, self%fpart, WRANK
                END IF

            ELSE IF (flavor .EQ. 2) THEN

                !### set the filenames ###
                IF (self%aioOn) THEN
                    WRITE(self%movie_aio_ops%manifestFile(self%aio_slot), '(A5, "-", I4.4, "-000")') self%movieFileRoot, self%movieNumber
                    WRITE(cRoot, '(A5, "-", I4.4, "-part", I2.2)') self%movieFileRoot, self%movieNumber, self%fpart
                    CALL AIO_GEN_DATA_FILE_NAME(cRoot, self%dataFile)
                ELSE
                    WRITE(self%movie_aio_ops%manifestFile(self%aio_slot), '(A5, "-", I4.4, "-", I3.3)') self%movieFileRoot, self%movieNumber, WRANK
                    WRITE(self%dataFile, '(A5, "-", I4.4, "-part", I2.2, "-", I3.3)') self%movieFileRoot, self%movieNumber, self%fpart, WRANK
                END IF

            END IF

            self%n_patches_added = 0

            !$OMP END MASTER
            !$OMP BARRIER

            !### add on the manifest header ###
            IF (self%fpart .GT. 0) THEN

                manifest_h_length = 0

            ELSE IF (self%fpart .EQ. 0 .AND. self%ioLead) THEN
                
                manifest_h_length = self%manifest_offset
                !$OMP SINGLE
                CALL self%gen_manifest_header(workd, flavor)
                !$OMP END SINGLE
                manifest_h_length = self%manifest_offset - manifest_h_length

            END IF

            manifest_p_length = self%get_patch_manifest_length()

            SELECT CASE (workd%ndim)

                CASE (1)

                    !$OMP DO SCHEDULE(DYNAMIC)
                    DO pi = 1, workd%npx

                        write_patch = .FALSE.

                        !$OMP CRITICAL
                        IF (workd%domain1d(pi)%hasZones .AND. .NOT. self%patch_added1d(pi) .AND. self%n_patches_added .LT. self%n_patches_in_write) THEN

                            !### increment the offsets first thing ###
                            mymanifest_offset    = self%manifest_offset
                            mydata_offset        = self%data_offset
                            self%manifest_offset = self%manifest_offset + manifest_p_length
                            self%data_offset     = self%data_offset + self%bytes_per_patch
                            self%n_patches_added = self%n_patches_added + 1
                            write_patch          = .TRUE.

                        END IF
                        !$OMP END CRITICAL

                        IF (write_patch) THEN

                            !### append to our manifest Patch table record ###
                            CALL self%gen_patch_manifest_record(workd, pi, 1, 1, mymanifest_offset, flavor)

                            !### append the data from this Patch ###
                            CALL self%append_patch_data(workd, pi, 1, 1, mydata_offset, flavor, mygamma)

                            self%patch_added1d(pi) = .TRUE.

                        END IF

                    END DO
                    !$OMP END DO

                CASE (2)

                    !$OMP DO SCHEDULE(DYNAMIC)
                    DO p = 1, workd%npx*workd%npy

                        !### obtain the x/y location of the Patch ###
                        CALL collapse2d(p, workd%npx, workd%npy, pi, pj)

                        write_patch = .FALSE.

                        !$OMP CRITICAL
                        IF (workd%domain2d(pi,pj)%hasZones .AND. .NOT. self%patch_added2d(pi,pj) .AND. self%n_patches_added .LT. self%n_patches_in_write) THEN

                            !### increment the offsets first thing ###
                            mymanifest_offset    = self%manifest_offset
                            mydata_offset        = self%data_offset
                            self%manifest_offset = self%manifest_offset + manifest_p_length
                            self%data_offset     = self%data_offset + self%bytes_per_patch
                            self%n_patches_added = self%n_patches_added + 1
                            write_patch          = .TRUE.
                                
                        END IF
                        !$OMP END CRITICAL
                            
                        IF (write_patch) THEN
                                
                            !### append to our manifest Patch table record ###
                            CALL self%gen_patch_manifest_record(workd, pi, pj, 1,  mymanifest_offset, flavor)
                                
                            !### append the data from this Patch ###
                            CALL self%append_patch_data(workd, pi, pj, 1, mydata_offset, flavor, mygamma)
                                
                            self%patch_added2d(pi,pj) = .TRUE.
                                
                        END IF

                    END DO
                    !$OMP END DO

                CASE (3)

                    !$OMP DO SCHEDULE(DYNAMIC)
                    DO p = 1, workd%npx*workd%npy*workd%npz

                        !### obtain the x/y/z location of the Patch ###
                        CALL collapse3d(p, workd%npx, workd%npy, workd%npz, pi, pj, pk)

                        write_patch = .FALSE.
                                
                        !$OMP CRITICAL
                        IF (workd%domain3d(pi,pj,pk)%hasZones .AND. .NOT. self%patch_added3d(pi,pj,pk) .AND. self%n_patches_added .LT. self%n_patches_in_write) THEN
                                
                            !### increment the offsets first thing ###
                            mymanifest_offset    = self%manifest_offset
                            mydata_offset        = self%data_offset
                            self%manifest_offset = self%manifest_offset + manifest_p_length
                            self%data_offset     = self%data_offset + self%bytes_per_patch
                            self%n_patches_added = self%n_patches_added + 1
                            write_patch          = .TRUE.

                        END IF
                        !$OMP END CRITICAL
                            
                        IF (write_patch) THEN
                                    
                            !### append to our manifest Patch table record ###
                            CALL self%gen_patch_manifest_record(workd, pi, pj, pk,  mymanifest_offset, flavor)
                                    
                            !### append the data from this Patch ###
                            CALL self%append_patch_data(workd, pi, pj, pk, mydata_offset, flavor, mygamma)
                                    
                            self%patch_added3d(pi,pj,pk) = .TRUE.
                                
                        END IF

                    END DO
                    !$OMP END DO

            END SELECT

            !### we've packed a single send now.  increment our overall buffer offset to handle the next send ###
            !$OMP MASTER

            !### register the starting address and length for the write and fire off the write ###
            IF (flavor .EQ. 1) THEN

                self%comp_aio_ops%offsets(self%aio_slot,1) = self%boffset
                self%comp_aio_ops%lengths(self%aio_slot,1) = self%n_patches_in_write * self%bytes_per_patch
                self%comp_aio_ops%offsets(self%aio_slot,2) = self%comp_aio_ops%lengths(self%aio_slot,1) + 1
                self%comp_aio_ops%lengths(self%aio_slot,2) = manifest_h_length + self%n_patches_in_write * manifest_p_length

                !### either do an AIO write or a local write (AIO manifest completion is done at the final wait on the slot) ###
                IF (self%aioOn) THEN

                    CALL AIO_WRITE_BUF_META(TRIM(self%compressedPath) // '/' // TRIM(cRoot), self%comp_aio_ops%lengths(self%aio_slot,1), &
                          self%comp_aio_ops%lengths(self%aio_slot,2), self%comp_buf(self%comp_aio_ops%offsets(self%aio_slot,1)), self%comp_aio_ops%islots(self%aio_slot), 0)

                ELSE

                    OPEN(UNIT=12, FILE=TRIM(self%compressedPath) // '/' // self%dataFile, FORM="UNFORMATTED", ACCESS="STREAM", STATUS="REPLACE")
                    WRITE(12) (self%comp_buf(i), i=self%comp_aio_ops%offsets(self%aio_slot,1), self%comp_aio_ops%offsets(self%aio_slot,1)+self%comp_aio_ops%lengths(self%aio_slot,1)-1)
                    CLOSE(12)
                    
                    IF (self%fpart .EQ. 0) THEN
                        OPEN(UNIT=12, FILE=TRIM(self%compressedPath) // '/' // self%comp_aio_ops%manifestFile(self%aio_slot), FORM="UNFORMATTED", ACCESS="STREAM", STATUS="REPLACE")
                    ELSE
                        OPEN(UNIT=12, FILE=TRIM(self%compressedPath) // '/' // self%comp_aio_ops%manifestFile(self%aio_slot), FORM="UNFORMATTED", ACCESS="STREAM", STATUS="OLD", POSITION='APPEND')
                    END IF

                    ! TODO, add in MPI_GATHER for other ranks' manifests

                    WRITE(12) (self%comp_buf(i), i=self%comp_aio_ops%offsets(self%aio_slot,2), self%comp_aio_ops%offsets(self%aio_slot,2)+self%comp_aio_ops%lengths(self%aio_slot,2)-1)
                    CLOSE(12)

                END IF

            ELSE IF (flavor .EQ. 2) THEN

                self%movie_aio_ops%offsets(self%aio_slot,1) = self%boffset
                self%movie_aio_ops%lengths(self%aio_slot,1) = self%n_patches_in_write * self%bytes_per_patch
                self%movie_aio_ops%offsets(self%aio_slot,2) = self%movie_aio_ops%lengths(self%aio_slot,1) + 1
                self%movie_aio_ops%lengths(self%aio_slot,2) = manifest_h_length + self%n_patches_in_write * manifest_p_length

                !### either do an AIO write or a local write ###
                IF (self%aioOn) THEN

                    CALL AIO_WRITE_BUF_META(TRIM(self%moviePath) // '/' // TRIM(cRoot), self%movie_aio_ops%lengths(self%aio_slot,1), &
                         self%movie_aio_ops%lengths(self%aio_slot,2), self%movie_buf(self%movie_aio_ops%offsets(self%aio_slot,1)), self%movie_aio_ops%islots(self%aio_slot), 0)

                ELSE
                    
                    OPEN(UNIT=12, FILE=TRIM(self%moviePath) // '/' // self%dataFile, FORM="UNFORMATTED", ACCESS="STREAM", STATUS="REPLACE")
                    WRITE(12) (self%movie_buf(i), i=self%movie_aio_ops%offsets(self%aio_slot,1), self%movie_aio_ops%offsets(self%aio_slot,1)+self%movie_aio_ops%lengths(self%aio_slot,1)-1)
                    CLOSE(12)

                    IF (self%fpart .EQ. 0) THEN
                        OPEN(UNIT=12, FILE=TRIM(self%moviePath) // '/' // self%movie_aio_ops%manifestFile(self%aio_slot), FORM="UNFORMATTED", ACCESS="STREAM", STATUS="REPLACE")
                    ELSE
                        OPEN(UNIT=12, FILE=TRIM(self%moviePath) // '/' // self%movie_aio_ops%manifestFile(self%aio_slot), FORM="UNFORMATTED", ACCESS="STREAM", STATUS="OLD", POSITION='APPEND')
                    END IF

                    ! TODO, add in MPI_GATHER for other ranks' manifests

                    WRITE(12) (self%movie_buf(i), i=self%movie_aio_ops%offsets(self%aio_slot,2), self%movie_aio_ops%offsets(self%aio_slot,2)+self%movie_aio_ops%lengths(self%aio_slot,2)-1)
                    CLOSE(12)

                END IF

            END IF

            self%boffset           = self%data_offset
            self%remaining_patches = self%remaining_patches - self%n_patches_added
            self%fpart             = self%fpart + 1

            !### if there are patches left and we don't have enough room to add even one of them we need to trigger a loop exit ###
            IF (self%remaining_patches .GT. 0 .AND. (self%boffset + manifest_p_length + self%bytes_per_patch) .GT. blength) THEN
                
                self%boffset = blength
            
            !### otherwise if there are no more patcvhes we also need to trigger a loop exit ###
            ELSE IF (self%remaining_patches .LE. 0) THEN

                self%boffset = blength

            END IF

            !$OMP END MASTER
            !$OMP BARRIER

        END DO

        !### reset the boffset in case there are more loops over our buffer needed ###
        !$OMP BARRIER
        !$OMP SINGLE
        self%boffset     = 1
        self%data_offset = self%boffset
        !$OMP END SINGLE

    END DO

END SUBROUTINE write_dumpset

!### determine the amount of memory needed to write a single Patch worth of data for this format ###
SUBROUTINE determine_patch_size(self, workd, flavor)

    CLASS(IO) :: self
    TYPE(Domain) :: workd
    INTEGER(WIS), INTENT(IN) :: flavor
    INTEGER(WIS) :: bpp, nx, ny, nz, nb, pi, pj, pk, ii
    INTEGER(WID) :: nzones

    !$OMP SINGLE

    !### loop through each patch to determine how many we must work with in total ###
    self%n_active_patches = 0
    nx                    = 0
    ny                    = 0
    nz                    = 0
    nb                    = 0
    IF (workd%is1d) THEN

        DO pi = 1, workd%npx

            IF (workd%domain1d(pi)%hasZones) self%n_active_patches = self%n_active_patches + 1
            IF (workd%domain1d(pi)%hasZones .AND. nx .EQ. 0) THEN

                nx = workd%domain1d(pi)%nx
                ny = workd%domain1d(pi)%ny
                nz = workd%domain1d(pi)%nz
                nb = workd%domain1d(pi)%nb

            END IF

        END DO

    ELSE IF (workd%is2d) THEN

        DO pj = 1, workd%npy

            DO pi = 1, workd%npx

                IF (workd%domain2d(pi,pj)%hasZones) self%n_active_patches = self%n_active_patches + 1
                IF (workd%domain2d(pi,pj)%hasZones .AND. nx .EQ. 0) THEN

                    nx = workd%domain2d(pi,pj)%nx
                    ny = workd%domain2d(pi,pj)%ny
                    nz = workd%domain2d(pi,pj)%nz
                    nb = workd%domain2d(pi,pj)%nb

                END IF

            END DO

        END DO

    ELSE IF (workd%is3d) THEN

        DO pk = 1, workd%npz
            
            DO pj = 1, workd%npy

                DO pi = 1, workd%npx

                    IF (workd%domain3d(pi,pj,pk)%hasZones) self%n_active_patches = self%n_active_patches + 1  
                    IF (workd%domain3d(pi,pj,pk)%hasZones .AND. nx .EQ. 0) THEN
                        
                        nx = workd%domain3d(pi,pj,pk)%nx
                        ny = workd%domain3d(pi,pj,pk)%ny
                        nz = workd%domain3d(pi,pj,pk)%nz
                        nb = workd%domain3d(pi,pj,pk)%nb
                       
                    END IF

                END DO

            END DO

        END DO

    END IF

    !### determine the bytes per patch now that we know the patch size and have the field precisions ###
    bpp = 0
    SELECT CASE (flavor)

        CASE (1)

            DO ii = 1, self%comp_active_fields

                SELECT CASE (self%comp_field_precs(ii))
                    
                    CASE('byte1')
                        bpp = bpp + 1
                    CASE('byte2')
                        bpp = bpp + 2
                    CASE('real4')
                        bpp = bpp + 4
                    CASE('real8')
                        bpp = bpp + 8
                        
                END SELECT

            END DO

        CASE (2)

            DO ii = 1, self%movie_active_fields
                
                SELECT CASE (self%movie_field_precs(ii))
                    
                    CASE('byte1')
                        bpp = bpp + 1
                    CASE('byte2')
                        bpp = bpp + 2
                    CASE('real4')
                        bpp = bpp + 4
                    CASE('real8')
                        bpp = bpp + 8
                        
                END SELECT

            END DO

    END SELECT

    !### then add on how much each Patch contributes to the size of the manifest (we'll say 128 Bytes per Patch manifest line, should be plenty) ###
    nzones = 2*nb + nx
    IF (workd%is2d .OR. workd%is3d) nzones = nzones * (2*nb + ny)
    IF (workd%is3d) nzones = nzones * (2*nb + nz)
    self%bytes_per_patch = bpp * nzones

    !$OMP END SINGLE

END SUBROUTINE determine_patch_size

!### generate the header portion of the manifest (excluding the Patch table) ###
SUBROUTINE gen_manifest_header(self, workd, flavor)

    CLASS(IO) :: self
    TYPE(Domain) :: workd
    INTEGER(WIS), INTENT(IN) :: flavor
    INTEGER :: date_time(8), slen, off
    INTEGER(WIS) :: ii, nx, ny, nz, nb, pi, pj, pk, n, fn, formn
    REAL(WRD) :: llim, ulim
    CHARACTER(10) :: dt(3)
    CHARACTER(36) :: dateString, pdt, pt, pnsteps, vard, cubestr
    CHARACTER(48) :: limss
    CHARACTER(WMAX_IO_FORM_LEN) :: forms
    CHARACTER(20) :: user, host
    CHARACTER(1) :: header(2 * STORAGE_SIZE(self%simulationDescription))

    !### title and description first ###
    off                  = 1
    slen                 = 38
    CALL self%add_line('# SIMULATION DESCRIPTION AND STATUS #' // CHAR(10), header(off:off+slen-1), slen)
    off                  = off + slen
    slen                 = LEN_TRIM(self%simulationTitle) + 15
    CALL self%add_line('title         ' // TRIM(self%simulationTitle) // CHAR(10), header(off:off+slen-1), slen)
    off                  = off + slen
    slen                 = LEN_TRIM(self%simulationDescription) + 15
    CALL self%add_line('comment       ' // TRIM(self%simulationDescription) // CHAR(10), header(off:off+slen-1), slen)
    off                  = off + slen

    !### get the date/time information ###
    CALL DATE_AND_TIME(dt(1), dt(2), dt(3), date_time)
    WRITE(dateString, '("date          ", I4, "-", I2.2, "-", I2.2, " ", I2.2, ":", I2.2, ":", I2.2)') date_time(1), date_time(2), date_time(3), date_time(5), date_time(6), date_time(7)

    !### get the system and user ###
    CALL GET_ENVIRONMENT_VARIABLE("USER", user)
    CALL GET_ENVIRONMENT_VARIABLE("HOST", host)

    !### add each of those on ###
    slen                 = LEN_TRIM(dateString) + 1
    CALL self%add_line(TRIM(dateString) // CHAR(10), header(off:off+slen-1), slen)
    off                  = off + slen
    slen                 = LEN_TRIM(user) + 15
    CALL self%add_line('user          ' // TRIM(user) // CHAR(10), header(off:off+slen-1), slen)
    off                  = off + slen
    slen                 = LEN_TRIM(host) + 15
    CALL self%add_line('host          ' // TRIM(host) // CHAR(10), header(off:off+slen-1), slen)
    off                  = off + slen

    !### add on the rank, timestep size, current time, dump number, number of steps in ###
    WRITE(pdt, '(ES22.15)') workd%dt
    WRITE(pt, '(ES22.15)') workd%t
    WRITE(pnsteps, '(I22)') workd%nsteps
    !slen                 = LEN_TRIM(ADJUSTL(WRANKSTR)) + 15
    !CALL self%add_line('Rank          ' // TRIM(ADJUSTL(WRANKSTR)) // CHAR(10), header(off:off+slen-1), slen)
    !off                  = off + slen
    slen                 = LEN_TRIM(ADJUSTL(pdt)) + 15
    CALL self%add_line('dtime         ' // TRIM(ADJUSTL(pdt)) // CHAR(10), header(off:off+slen-1), slen)
    off                  = off + slen
    slen                 = LEN_TRIM(ADJUSTL(pt)) + 15
    CALL self%add_line('time          ' // TRIM(ADJUSTL(pt)) // CHAR(10), header(off:off+slen-1), slen)
    off                  = off + slen
    slen                 = LEN_TRIM(ADJUSTL(pnsteps)) + 15
    CALL self%add_line('step          ' // TRIM(ADJUSTL(pnsteps)) // CHAR(10), header(off:off+slen-1), slen)
    off                  = off + slen

    IF (workd%is1d) THEN

        DO pi = 1, workd%npx
            IF (workd%domain1d(pi)%hasZones) THEN
                nx = workd%domain1d(pi)%nx
                ny = 1
                nz = 1
                nb = workd%domain1d(pi)%nb
                EXIT
            END IF
        END DO

    ELSE IF(workd%is2d) THEN

        DO pj = 1, workd%npy
            DO pi = 1, workd%npx
                IF (workd%domain2d(pi,pj)%hasZones) THEN
                    nx = workd%domain2d(pi,pj)%nx
                    ny = workd%domain2d(pi,pj)%ny
                    nz = 1
                    nb = workd%domain2d(pi,pj)%nb
                    EXIT
                END IF
            END DO
        END DO
        
    ELSE IF(workd%is3d) THEN

        DO pk = 1, workd%npz
            DO pj = 1, workd%npy
                DO pi = 1, workd%npx
                    IF (workd%domain3d(pi,pj,pk)%hasZones) THEN
                        nx = workd%domain3d(pi,pj,pk)%nx
                        ny = workd%domain3d(pi,pj,pk)%ny
                        nz = workd%domain3d(pi,pj,pk)%nz
                        nb = workd%domain3d(pi,pj,pk)%nb
                        EXIT
                    END IF
                END DO
            END DO
        END DO

    END IF

    WRITE(cubestr, '(3(I10))') nx, ny, nz
    slen                 = LEN_TRIM(cubestr) + 15
    CALL self%add_line('ncubesxyz     ' // TRIM(cubestr) // CHAR(10), header(off:off+slen-1), slen)
    off                  = off + slen
    WRITE(cubestr, '(3(I10))') self%nranks_x*workd%npx, self%nranks_y*workd%npy, self%nranks_z*workd%npz
    slen                 = LEN_TRIM(cubestr) + 15
    CALL self%add_line('nbrickxyz     ' // TRIM(cubestr) // CHAR(10), header(off:off+slen-1), slen)
    off                  = off + slen
    IF (workd%is1d) THEN
        WRITE(cubestr, '(3(I10))') nb, 0, 0
    ELSE IF (workd%is2d) THEN
        WRITE(cubestr, '(3(I10))') nb, nb, 0
    ELSE IF (workd%is3d) THEN
        WRITE(cubestr, '(3(I10))') nb, nb, nb
    END IF
    slen                 = LEN_TRIM(cubestr) + 15
    CALL self%add_line('nbdry_xyz     ' // TRIM(cubestr) // CHAR(10), header(off:off+slen-1), slen)
    off                  = off + slen

    WRITE(limss, '(2(ES24.15))') -0.5_WRD*workd%dx*self%nranks_x*workd%npx*nx, 0.5_WRD*workd%dx*self%nranks_x*workd%npx*nx
    slen                 = LEN_TRIM(ADJUSTL(limss)) + 15
    CALL self%add_line('xlimits       ' // TRIM(ADJUSTL(limss)) // CHAR(10), header(off:off+slen-1), slen)
    off                  = off + slen
    WRITE(limss, '(2(ES24.15))') -0.5_WRD*workd%dx*self%nranks_y*workd%npy*ny, 0.5_WRD*workd%dx*self%nranks_y*workd%npy*ny
    slen                 = LEN_TRIM(ADJUSTL(limss)) + 15
    CALL self%add_line('ylimits       ' // TRIM(ADJUSTL(limss)) // CHAR(10), header(off:off+slen-1), slen)
    off                  = off + slen
    WRITE(limss, '(2(ES24.15))') -0.5_WRD*workd%dx*self%nranks_z*workd%npz*nz, 0.5_WRD*workd%dx*self%nranks_z*workd%npz*nz
    slen                 = LEN_TRIM(ADJUSTL(limss)) + 15
    CALL self%add_line('zlimits       ' // TRIM(ADJUSTL(limss)) // CHAR(10), header(off:off+slen-1), slen)
    off                  = off + slen

    slen                 = 55
    CALL self%add_line('lobndry   continuation    continuation    continuation' // CHAR(10), header(off:off+slen-1), slen)
    off                  = off + slen
    CALL self%add_line('hibndry   continuation    continuation    continuation' // CHAR(10), header(off:off+slen-1), slen)
    off                  = off + slen

    !### add on the variable formatting bits ###
    slen                 = 37
    CALL self%add_line(CHAR(10) // '# VARIABLE FORMATTING INFORMATION #' // CHAR(10), header(off:off+slen-1), slen)
    off                  = off + slen
    forms                = ' '
    forms(1:10)          = 'field3d   '
    SELECT CASE (flavor)

        CASE (1)

            DO ii = 1, WMAX_IO_FIELDS

                fn    = 0
                formn = 11
                
                IF (self%compressedFormat(ii) .NE. ' ') THEN

                    DO n = 2, WMAX_IO_FORM_LEN
                        IF ((self%compressedFormat(ii)(n-1:n-1) .EQ. ' ' .AND. self%compressedFormat(ii)(n:n) .NE. ' ') .OR. (self%compressedFormat(ii)(n:n) .NE. ' ' .AND. n .EQ. 2)) fn = fn + 1
                        IF (fn .EQ. 2 .OR. fn .EQ. 3 .OR. fn .EQ. 4 .OR. fn .EQ. 6 .OR. fn .EQ. 7) THEN
                            forms(formn:formn) = self%compressedFormat(ii)(n:n)
                            formn = formn + 1
                        END IF
                    END DO
                    slen = LEN_TRIM(forms) + 1
                    CALL self%add_line(TRIM(forms) // CHAR(10), header(off:off+slen-1), slen)
                    off = off + slen

                END IF

            END DO

        CASE (2)

            DO ii = 1, WMAX_IO_FIELDS

                fn    = 0
                formn = 11
                
                IF (self%movieFormat(ii) .NE. ' ') THEN

                    DO n = 2, WMAX_IO_FORM_LEN
                        IF ((self%movieFormat(ii)(n-1:n-1) .EQ. ' ' .AND. self%movieFormat(ii)(n:n) .NE. ' ') .OR. (self%movieFormat(ii)(n:n) .NE. ' ' .AND. n .EQ. 2)) fn = fn + 1
                        IF (fn .EQ. 2 .OR. fn .EQ. 3 .OR. fn .EQ. 4 .OR. fn .EQ. 6 .OR. fn .EQ. 7) THEN
                            forms(formn:formn) = self%movieFormat(ii)(n:n)
                            formn = formn + 1
                        END IF
                    END DO
                    slen = LEN_TRIM(forms) + 1
                    CALL self%add_line(TRIM(forms) // CHAR(10), header(off:off+slen-1), slen)
                    off = off + slen

                END IF

            END DO

    END SELECT
    slen                 = 14
    CALL self%add_line(CHAR(10) // '# MANIFEST #' // CHAR(10), header(off:off+slen-1), slen)
    off                  = off + slen
    slen                 = off - 1

    !### finally write the header to the correct buffer ###
    IF (flavor .EQ. 1) THEN
        self%comp_buf(self%manifest_offset:self%manifest_offset+slen) = header(1:1+slen)
    ELSE IF (flavor .EQ. 2) THEN
        self%movie_buf(self%manifest_offset:self%manifest_offset+slen) = header(1:1+slen)
    END IF
    self%manifest_offset = self%manifest_offset + off - 1

END SUBROUTINE gen_manifest_header

!### add on a Patch manifest record ###
SUBROUTINE gen_patch_manifest_record(self, workd, pi, pj, pk, offset, flavor)

    CLASS(IO) :: self
    TYPE(Domain) :: workd
    INTEGER(WIS), INTENT(IN) :: pi, pj, pk, offset, flavor
    INTEGER(WIS) :: nx, ny, nz, nb, slen
    REAL(WRD) :: x0, y0, z0, dl
    CHARACTER(320) :: record

    IF (workd%is1d) THEN

        nx = workd%domain1d(pi)%nx
        ny = workd%domain1d(pi)%ny
        nz = workd%domain1d(pi)%nz
        nb = workd%domain1d(pi)%nb
        x0 = workd%domain1d(pi)%x0
        y0 = workd%domain1d(pi)%y0
        z0 = workd%domain1d(pi)%z0
        dl = workd%domain1d(pi)%dx

    ELSE IF (workd%is2d) THEN

        nx = workd%domain2d(pi,pj)%nx
        ny = workd%domain2d(pi,pj)%ny
        nz = workd%domain2d(pi,pj)%nz
        nb = workd%domain2d(pi,pj)%nb
        x0 = workd%domain2d(pi,pj)%x0
        y0 = workd%domain2d(pi,pj)%y0
        z0 = workd%domain2d(pi,pj)%z0
        dl = workd%domain2d(pi,pj)%dx

    ELSE IF (workd%is3d) THEN

        nx = workd%domain3d(pi,pj,pk)%nx
        ny = workd%domain3d(pi,pj,pk)%ny
        nz = workd%domain3d(pi,pj,pk)%nz
        nb = workd%domain3d(pi,pj,pk)%nb
        x0 = workd%domain3d(pi,pj,pk)%x0
        y0 = workd%domain3d(pi,pj,pk)%y0
        z0 = workd%domain3d(pi,pj,pk)%z0
        dl = workd%domain3d(pi,pj,pk)%dx

    END IF

    WRITE(record, '("boxb        1   ", 3(I6), "      ", A)')  self%my_coords(1)*workd%npx+pi-1, self%my_coords(2)*workd%npy+pj-1, &
         self%my_coords(3)*workd%npz+pk-1, TRIM(ADJUSTL(self%dataFile)) // CHAR(10)
    slen = LEN_TRIM(record)
    IF (flavor .EQ. 1) THEN
        CALL self%add_line(TRIM(record), self%comp_buf(offset:offset+slen-1), slen)
    ELSE IF (flavor .EQ. 2) THEN
        CALL self%add_line(TRIM(record), self%movie_buf(offset:offset+slen-1), slen)
    END IF

END SUBROUTINE gen_patch_manifest_record

INTEGER FUNCTION get_patch_manifest_length(self)

    CLASS(IO) :: self
    get_patch_manifest_length = LEN_TRIM(ADJUSTL(self%dataFile)) + 16 + 18 + 6 + 1
    RETURN

END FUNCTION get_patch_manifest_length

!### add a line to a buffer ###
SUBROUTINE add_line(self, string, buffer, slen)

    CLASS(IO) :: self
    INTEGER(WIS), INTENT(IN) :: slen
    CHARACTER(slen) :: string
    CHARACTER(1), INTENT(INOUT) :: buffer(slen)
    INTEGER(WIS) :: i
    
    DO i = 1, slen

        buffer(i) = string(i:i)

    END DO

END SUBROUTINE add_line

!### allocate a field buffer for each thread to use ###
SUBROUTINE allocate_field_buffer_patch_flags(self, workd)

    CLASS(IO) :: self
    TYPE(Domain) :: workd
    INTEGER(WIS) :: lb1, lb2, lb3, ub1, ub2, ub3, fblen, pi, pj, pk, ppi, ppj, ppk

    !$OMP SINGLE

    !### allocate space to the field buffer ###
    IF (workd%is1d) THEN

        DO ppi = 1, workd%npx

            IF (workd%domain1d(ppi)%hasZones) THEN
                pi = ppi
                EXIT
            END IF

        END DO
        
        lb1   = LBOUND(workd%domain1d(pi)%grid1d,1,WIS)
        ub1   = UBOUND(workd%domain1d(pi)%grid1d,1,WIS)
        fblen = ub1 - lb1 + 1
        ALLOCATE(self%fieldbuf(fblen,self%nthreads))
        ALLOCATE(self%patch_added1d(workd%npx))

    ELSE IF (workd%is2d) THEN

        DO ppj = 1, workd%npy
            DO ppi = 1, workd%npx

                IF (workd%domain2d(ppi,ppj)%hasZones) THEN
                    pi = ppi
                    pj = ppj
                    EXIT
                END IF

            END DO
        END DO
        lb1   = LBOUND(workd%domain2d(pi,pj)%grid2d,1,WIS)
        lb2   = LBOUND(workd%domain2d(pi,pj)%grid2d,2,WIS)
        ub1   = UBOUND(workd%domain2d(pi,pj)%grid2d,1,WIS)
        ub2   = UBOUND(workd%domain2d(pi,pj)%grid2d,2,WIS)
        fblen = (ub2 - lb2 + 1) * (ub1 - lb1 + 1)
        ALLOCATE(self%fieldbuf(fblen,self%nthreads))
        ALLOCATE(self%patch_added2d(workd%npx, workd%npy))

    ELSE IF (workd%is3d) THEN
        
        DO ppk = 1, workd%npz
            DO ppj = 1, workd%npy
                DO ppi = 1, workd%npx

                    IF (workd%domain3d(ppi,ppj,ppk)%hasZones) THEN
                        pi = ppi
                        pj = ppj
                        pk = ppk
                        EXIT
                    END IF

                END DO
            END DO
        END DO
        lb1   = LBOUND(workd%domain3d(pi,pj,pk)%grid3d,1,WIS)
        lb2   = LBOUND(workd%domain3d(pi,pj,pk)%grid3d,2,WIS)
        lb3   = LBOUND(workd%domain3d(pi,pj,pk)%grid3d,3,WIS)
        ub1   = UBOUND(workd%domain3d(pi,pj,pk)%grid3d,1,WIS)
        ub2   = UBOUND(workd%domain3d(pi,pj,pk)%grid3d,2,WIS)
        ub3   = UBOUND(workd%domain3d(pi,pj,pk)%grid3d,3,WIS)
        fblen = (ub3 - lb3 + 1) * (ub2 - lb2 + 1) * (ub1 - lb1 + 1)
        ALLOCATE(self%fieldbuf(fblen,self%nthreads))
        ALLOCATE(self%patch_added3d(workd%npx, workd%npy, workd%npz))

    END IF

    !$OMP END SINGLE

END SUBROUTINE allocate_field_buffer_patch_flags

!### clean up the domain specific arrays ###
SUBROUTINE deallocate_field_buffer_patch_flags(self, workd)

    CLASS(IO) :: self
    TYPE(Domain) :: workd

    !$OMP SINGLE

    IF (ALLOCATED(self%fieldbuf)) DEALLOCATE(self%fieldbuf)

    IF (workd%is1d .AND. ALLOCATED(self%patch_added1d)) THEN
        
        DEALLOCATE(self%patch_added1d)

    ELSE IF (workd%is2d .AND. ALLOCATED(self%patch_added2d)) THEN

        DEALLOCATE(self%patch_added2d)

    ELSE IF (workd%is3d .AND. ALLOCATED(self%patch_added3d)) THEN
        
        DEALLOCATE(self%patch_added3d)

    END IF

    !$OMP END SINGLE

END SUBROUTINE deallocate_field_buffer_patch_flags

!### add a Patch's data to a buffer ###
SUBROUTINE append_patch_data(self, workd, pi, pj, pk, offset, flavor, mygamma)

    CLASS(IO) :: self
    TYPE(Domain) :: workd
    INTEGER(WID), INTENT(IN) :: offset
    INTEGER(WIS), INTENT(IN) :: pi, pj, pk, flavor
    REAL(WRD), INTENT(IN) :: mygamma
    INTEGER(WIS) :: myoffset, f, fmax, lb1, lb2, lb3, ub1, ub2, ub3, fblen, fields(WMAX_IO_FIELDS), i, j, k, ioff, mykind, mylen, tid, r, c
    REAL(WRS) :: minv, maxv, scaling
    REAL(WRD) :: wrdc, gammam1, idx
    REAL(8) :: dp_val
    REAL(4) :: sp_val
    INTEGER(2) :: tb_val
    INTEGER(1) :: sb_val
    CHARACTER(5) :: precs
    CHARACTER(8) :: map

    !### for pressure output we need (mygamma - 1) ###
    gammam1 = mygamma - 1.0_WRD
    idx     = 1.0_WRD / workd%dx

    !### get our thread id so we know which piece of the field buf to use ###
    tid = OMP_GET_THREAD_NUM() + 1

    myoffset = offset
    IF (flavor .EQ. 1) THEN
        fmax   = self%comp_active_fields
        fields = self%comp_fields
    ELSE IF (flavor .EQ. 2) THEN
        fmax   = self%movie_active_fields
        fields = self%movie_fields
    END IF

    !### allocate space to the field buffer ###
    IF (workd%is1d) THEN
        
        lb1   = LBOUND(workd%domain1d(pi)%grid1d,1,WIS)
        ub1   = UBOUND(workd%domain1d(pi)%grid1d,1,WIS)
        fblen = ub1 - lb1 + 1

    ELSE IF (workd%is2d) THEN

        lb1   = LBOUND(workd%domain2d(pi,pj)%grid2d,1,WIS)
        lb2   = LBOUND(workd%domain2d(pi,pj)%grid2d,2,WIS)
        ub1   = UBOUND(workd%domain2d(pi,pj)%grid2d,1,WIS)
        ub2   = UBOUND(workd%domain2d(pi,pj)%grid2d,2,WIS)
        fblen = (ub2 - lb2 + 1) * (ub1 - lb1 + 1)

    ELSE IF (workd%is3d) THEN
        
        lb1   = LBOUND(workd%domain3d(pi,pj,pk)%grid3d,1,WIS)
        lb2   = LBOUND(workd%domain3d(pi,pj,pk)%grid3d,2,WIS)
        lb3   = LBOUND(workd%domain3d(pi,pj,pk)%grid3d,3,WIS)
        ub1   = UBOUND(workd%domain3d(pi,pj,pk)%grid3d,1,WIS)
        ub2   = UBOUND(workd%domain3d(pi,pj,pk)%grid3d,2,WIS)
        ub3   = UBOUND(workd%domain3d(pi,pj,pk)%grid3d,3,WIS)
        fblen = (ub3 - lb3 + 1) * (ub2 - lb2 + 1) * (ub1 - lb1 + 1)

    END IF

    !### loop over each requested field ###
    DO f = 1, fmax

        IF (flavor .EQ. 1) THEN
            precs = self%comp_field_precs(f)
            map   = self%comp_field_maps(f)
            minv  = self%comp_field_mins(f)
            maxv  = self%comp_field_maxs(f)
        ELSE IF (flavor .EQ. 2) THEN
            precs = self%movie_field_precs(f)
            map   = self%movie_field_maps(f)
            minv  = self%movie_field_mins(f)
            maxv  = self%movie_field_maxs(f)
        END IF

        !### copy the field value for the full patch into our intermediate buffer ###
        SELECT CASE (fields(f))

            !### raw state vector fields ###
            CASE (1:8)

                IF (workd%is1d) THEN

                    self%fieldbuf(:,tid) = workd%domain1d(pi)%grid1d(:,fields(f))

                ELSE IF (workd%is2d) THEN

                    ioff = 1
                    DO j = lb2, ub2

                        self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = workd%domain2d(pi,pj)%grid2d(:,j,fields(f))
                        ioff = ioff + (ub1-lb1) + 1

                    END DO

                ELSE IF (workd%is3d) THEN

                    ioff = 1
                    DO k = lb3, ub3

                        DO j = lb2, ub2

                            self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = workd%domain3d(pi,pj,pk)%grid3d(:,j,k,fields(f))
                            ioff = ioff + (ub1-lb1) + 1

                        END DO

                    END DO

                END IF

            !### gas pressure ###
            CASE (10)

                IF (workd%is1d) THEN

                    self%fieldbuf(:,tid) = gammam1 * (workd%domain1d(pi)%grid1d(:,8) - (0.5_WRD / workd%domain1d(pi)%grid1d(:,1)) * (workd%domain1d(pi)%grid1d(:,2)**2 + workd%domain1d(pi)%grid1d(:,3)**2 + &
                         workd%domain1d(pi)%grid1d(:,4)**2) - 0.5_WRD * (workd%domain1d(pi)%grid1d(:,5)**2 + workd%domain1d(pi)%grid1d(:,6)**2 + workd%domain1d(pi)%grid1d(:,7)**2))

                ELSE IF (workd%is2d) THEN

                    ioff = 1
                    DO j = lb2, ub2

                        self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = gammam1 * (workd%domain2d(pi,pj)%grid2d(:,j,8) - (0.5_WRD / workd%domain2d(pi,pj)%grid2d(:,j,1)) * &
                             (workd%domain2d(pi,pj)%grid2d(:,j,2)**2 + workd%domain2d(pi,pj)%grid2d(:,j,3)**2 + workd%domain2d(pi,pj)%grid2d(:,j,4)**2) - 0.5_WRD * &
                             (workd%domain2d(pi,pj)%grid2d(:,j,5)**2 + workd%domain2d(pi,pj)%grid2d(:,j,6)**2 + workd%domain2d(pi,pj)%grid2d(:,j,7)**2))
                        ioff = ioff + (ub1-lb1) + 1

                    END DO

                ELSE IF (workd%is3d) THEN

                    ioff = 1
                    DO k = lb3, ub3

                        DO j = lb2, ub2

                            self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = gammam1 * (workd%domain3d(pi,pj,pk)%grid3d(:,j,k,8) - (0.5_WRD / workd%domain3d(pi,pj,pk)%grid3d(:,j,k,1)) * &
                                 (workd%domain3d(pi,pj,pk)%grid3d(:,j,k,2)**2 + workd%domain3d(pi,pj,pk)%grid3d(:,j,k,3)**2 + workd%domain3d(pi,pj,pk)%grid3d(:,j,k,4)**2) - 0.5_WRD * &
                                 (workd%domain3d(pi,pj,pk)%grid3d(:,j,k,5)**2 + workd%domain3d(pi,pj,pk)%grid3d(:,j,k,6)**2 + workd%domain3d(pi,pj,pk)%grid3d(:,j,k,7)**2))
                            ioff = ioff + (ub1-lb1) + 1

                        END DO

                    END DO

                END IF

            !### a raw passive field ###
            CASE(11:80)

                 c = fields(f) - 10

                 IF (workd%is1d) THEN

                    self%fieldbuf(:,tid) = workd%domain1d(pi)%pass1d(:,c)

                ELSE IF (workd%is2d) THEN

                    ioff = 1
                    DO j = lb2, ub2

                        self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = workd%domain2d(pi,pj)%pass2d(:,j,c)
                        ioff = ioff + (ub1-lb1) + 1

                    END DO

                ELSE IF (workd%is3d) THEN

                    ioff = 1
                    DO k = lb3, ub3

                        DO j = lb2, ub2

                            self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = workd%domain3d(pi,pj,pk)%pass3d(:,j,k,c)
                            ioff = ioff + (ub1-lb1) + 1

                        END DO

                    END DO

                END IF

            !### magnitude of the velocity ###
            CASE (81)

                IF (workd%is1d) THEN

                    self%fieldbuf(:,tid) = SQRT(workd%domain1d(pi)%grid1d(:,2)**2 + workd%domain1d(pi)%grid1d(:,3)**2 + workd%domain1d(pi)%grid1d(:,4)**2)

                ELSE IF (workd%is2d) THEN

                    ioff = 1
                    DO j = lb2, ub2

                        self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = SQRT(workd%domain2d(pi,pj)%grid2d(:,j,2)**2 + workd%domain2d(pi,pj)%grid2d(:,j,3)**2 + workd%domain2d(pi,pj)%grid2d(:,j,4)**2)
                        ioff = ioff + (ub1-lb1) + 1

                    END DO

                ELSE IF (workd%is3d) THEN

                    ioff = 1
                    DO k = lb3, ub3

                        DO j = lb2, ub2

                            self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = SQRT(workd%domain3d(pi,pj,pk)%grid3d(:,j,k,2)**2 + workd%domain3d(pi,pj,pk)%grid3d(:,j,k,3)**2 + workd%domain3d(pi,pj,pk)%grid3d(:,j,k,4)**2)
                            ioff = ioff + (ub1-lb1) + 1

                        END DO

                    END DO

                END IF

            !### magnitude of the magentic field ###
            CASE (82)

                IF (workd%is1d) THEN

                    self%fieldbuf(:,tid) = SQRT(workd%domain1d(pi)%grid1d(:,5)**2 + workd%domain1d(pi)%grid1d(:,6)**2 + workd%domain1d(pi)%grid1d(:,7)**2)

                ELSE IF (workd%is2d) THEN

                    ioff = 1
                    DO j = lb2, ub2

                        self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = SQRT(workd%domain2d(pi,pj)%grid2d(:,j,5)**2 + workd%domain2d(pi,pj)%grid2d(:,j,6)**2 + workd%domain2d(pi,pj)%grid2d(:,j,7)**2)
                        ioff = ioff + (ub1-lb1) + 1

                    END DO

                ELSE IF (workd%is3d) THEN

                    ioff = 1
                    DO k = lb3, ub3

                        DO j = lb2, ub2

                            self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = SQRT(workd%domain3d(pi,pj,pk)%grid3d(:,j,k,5)**2 + workd%domain3d(pi,pj,pk)%grid3d(:,j,k,6)**2 + workd%domain3d(pi,pj,pk)%grid3d(:,j,k,7)**2)
                            ioff = ioff + (ub1-lb1) + 1

                        END DO

                    END DO

                END IF

            !### kinetic energy density ###
            CASE (83)

                IF (workd%is1d) THEN

                    self%fieldbuf(:,tid) = 0.5_WRD * (workd%domain1d(pi)%grid1d(:,2)**2 + workd%domain1d(pi)%grid1d(:,3)**2 + workd%domain1d(pi)%grid1d(:,4)**2)

                ELSE IF (workd%is2d) THEN

                    ioff = 1
                    DO j = lb2, ub2

                        self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = 0.5_WRD * (workd%domain2d(pi,pj)%grid2d(:,j,2)**2 + workd%domain2d(pi,pj)%grid2d(:,j,3)**2 + workd%domain2d(pi,pj)%grid2d(:,j,4)**2)
                        ioff = ioff + (ub1-lb1) + 1

                    END DO

                ELSE IF (workd%is3d) THEN

                    ioff = 1
                    DO k = lb3, ub3

                        DO j = lb2, ub2

                            self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = 0.5_WRD * (workd%domain3d(pi,pj,pk)%grid3d(:,j,k,2)**2 + workd%domain3d(pi,pj,pk)%grid3d(:,j,k,3)**2 + workd%domain3d(pi,pj,pk)%grid3d(:,j,k,4)**2)
                            ioff = ioff + (ub1-lb1) + 1

                        END DO

                    END DO

                END IF

            !### magnetic energy density ###
            CASE (84)

                IF (workd%is1d) THEN

                    self%fieldbuf(:,tid) = 0.5_WRD * (workd%domain1d(pi)%grid1d(:,5)**2 + workd%domain1d(pi)%grid1d(:,6)**2 + workd%domain1d(pi)%grid1d(:,7)**2)

                ELSE IF (workd%is2d) THEN

                    ioff = 1
                    DO j = lb2, ub2

                        self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = 0.5_WRD * (workd%domain2d(pi,pj)%grid2d(:,j,5)**2 + workd%domain2d(pi,pj)%grid2d(:,j,6)**2 + workd%domain2d(pi,pj)%grid2d(:,j,7)**2)
                        ioff = ioff + (ub1-lb1) + 1

                    END DO

                ELSE IF (workd%is3d) THEN

                    ioff = 1
                    DO k = lb3, ub3

                        DO j = lb2, ub2

                            self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = 0.5_WRD * (workd%domain3d(pi,pj,pk)%grid3d(:,j,k,5)**2 + workd%domain3d(pi,pj,pk)%grid3d(:,j,k,6)**2 + workd%domain3d(pi,pj,pk)%grid3d(:,j,k,7)**2)
                            ioff = ioff + (ub1-lb1) + 1

                        END DO

                    END DO

                END IF

            !### thermal energy density ###
            CASE(85)

                IF (workd%is1d) THEN

                    self%fieldbuf(:,tid) = workd%domain1d(pi)%grid1d(:,8) - (0.5_WRD / workd%domain1d(pi)%grid1d(:,1)) * (workd%domain1d(pi)%grid1d(:,2)**2 + workd%domain1d(pi)%grid1d(:,3)**2 + &
                         workd%domain1d(pi)%grid1d(:,4)**2) - 0.5_WRD * (workd%domain1d(pi)%grid1d(:,5)**2 + workd%domain1d(pi)%grid1d(:,6)**2 + workd%domain1d(pi)%grid1d(:,7)**2)

                ELSE IF (workd%is2d) THEN

                    ioff = 1
                    DO j = lb2, ub2

                        self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = workd%domain2d(pi,pj)%grid2d(:,j,8) - (0.5_WRD / workd%domain2d(pi,pj)%grid2d(:,j,1)) * &
                             (workd%domain2d(pi,pj)%grid2d(:,j,2)**2 + workd%domain2d(pi,pj)%grid2d(:,j,3)**2 + workd%domain2d(pi,pj)%grid2d(:,j,4)**2) - 0.5_WRD * &
                             (workd%domain2d(pi,pj)%grid2d(:,j,5)**2 + workd%domain2d(pi,pj)%grid2d(:,j,6)**2 + workd%domain2d(pi,pj)%grid2d(:,j,7)**2)
                        ioff = ioff + (ub1-lb1) + 1

                    END DO

                ELSE IF (workd%is3d) THEN

                    ioff = 1
                    DO k = lb3, ub3

                        DO j = lb2, ub2

                            self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = workd%domain3d(pi,pj,pk)%grid3d(:,j,k,8) - (0.5_WRD / workd%domain3d(pi,pj,pk)%grid3d(:,j,k,1)) * &
                                 (workd%domain3d(pi,pj,pk)%grid3d(:,j,k,2)**2 + workd%domain3d(pi,pj,pk)%grid3d(:,j,k,3)**2 + workd%domain3d(pi,pj,pk)%grid3d(:,j,k,4)**2) - 0.5_WRD * &
                                 (workd%domain3d(pi,pj,pk)%grid3d(:,j,k,5)**2 + workd%domain3d(pi,pj,pk)%grid3d(:,j,k,6)**2 + workd%domain3d(pi,pj,pk)%grid3d(:,j,k,7)**2)
                            ioff = ioff + (ub1-lb1) + 1

                        END DO

                    END DO

                END IF

            ! others to come when gravity and dark matter are in ###

            !### Vx ###
            CASE (101)

                IF (workd%is1d) THEN

                    self%fieldbuf(:,tid) = workd%domain1d(pi)%grid1d(:,2) / workd%domain1d(pi)%grid1d(:,1)

                ELSE IF (workd%is2d) THEN

                    ioff = 1
                    DO j = lb2, ub2

                        self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = workd%domain2d(pi,pj)%grid2d(:,j,2) / workd%domain2d(pi,pj)%grid2d(:,j,1)
                        ioff = ioff + (ub1-lb1) + 1

                    END DO

                ELSE IF (workd%is3d) THEN

                    ioff = 1
                    DO k = lb3, ub3

                        DO j = lb2, ub2

                            self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = workd%domain3d(pi,pj,pk)%grid3d(:,j,k,2) / workd%domain3d(pi,pj,pk)%grid3d(:,j,k,1)
                            ioff = ioff + (ub1-lb1) + 1

                        END DO

                    END DO

                END IF

            !### Vy ###
            CASE (102)

                IF (workd%is1d) THEN

                    self%fieldbuf(:,tid) = workd%domain1d(pi)%grid1d(:,3) / workd%domain1d(pi)%grid1d(:,1)

                ELSE IF (workd%is2d) THEN

                    ioff = 1
                    DO j = lb2, ub2

                        self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = workd%domain2d(pi,pj)%grid2d(:,j,3) / workd%domain2d(pi,pj)%grid2d(:,j,1)
                        ioff = ioff + (ub1-lb1) + 1

                    END DO

                ELSE IF (workd%is3d) THEN

                    ioff = 1
                    DO k = lb3, ub3

                        DO j = lb2, ub2

                            self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = workd%domain3d(pi,pj,pk)%grid3d(:,j,k,3) / workd%domain3d(pi,pj,pk)%grid3d(:,j,k,1)
                            ioff = ioff + (ub1-lb1) + 1

                        END DO

                    END DO

                END IF

            !### Vz ###
            CASE (103)

                IF (workd%is1d) THEN

                    self%fieldbuf(:,tid) = workd%domain1d(pi)%grid1d(:,4) / workd%domain1d(pi)%grid1d(:,1)

                ELSE IF (workd%is2d) THEN

                    ioff = 1
                    DO j = lb2, ub2

                        self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = workd%domain2d(pi,pj)%grid2d(:,j,4) / workd%domain2d(pi,pj)%grid2d(:,j,1)
                        ioff = ioff + (ub1-lb1) + 1

                    END DO

                ELSE IF (workd%is3d) THEN

                    ioff = 1
                    DO k = lb3, ub3

                        DO j = lb2, ub2

                            self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = workd%domain3d(pi,pj,pk)%grid3d(:,j,k,4) / workd%domain3d(pi,pj,pk)%grid3d(:,j,k,1)
                            ioff = ioff + (ub1-lb1) + 1

                        END DO

                    END DO

                END IF

            !### Bx face ###
            CASE(98)

                IF (workd%is2d) THEN

                    ioff = 1
                    DO j = lb2, ub2

                        self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = workd%domain2d(pi,pj)%bface2d(:,j,1)
                        ioff = ioff + (ub1-lb1) + 1

                    END DO

                ELSE IF (workd%is3d) THEN

                    ioff = 1
                    DO k = lb3, ub3

                        DO j = lb2, ub2

                            self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = workd%domain3d(pi,pj,pk)%bface3d(:,j,k,1)
                            ioff = ioff + (ub1-lb1) + 1

                        END DO

                    END DO

                END IF

            !### By face ###
            CASE(99)

                IF (workd%is2d) THEN

                    ioff = 1
                    DO j = lb2, ub2

                        self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = workd%domain2d(pi,pj)%bface2d(:,j,2)
                        ioff = ioff + (ub1-lb1) + 1

                    END DO

                ELSE IF (workd%is3d) THEN

                    ioff = 1
                    DO k = lb3, ub3

                        DO j = lb2, ub2

                            self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = workd%domain3d(pi,pj,pk)%bface3d(:,j,k,2)
                            ioff = ioff + (ub1-lb1) + 1

                        END DO

                    END DO

                END IF

            !### Bz face ###
            CASE(100)

                IF (workd%is3d) THEN

                    ioff = 1
                    DO k = lb3, ub3

                        DO j = lb2, ub2

                            self%fieldbuf(ioff:ioff+(ub1-lb1),tid) = workd%domain3d(pi,pj,pk)%bface3d(:,j,k,3)
                            ioff = ioff + (ub1-lb1) + 1

                        END DO

                    END DO

                END IF

            !### current density in Z ###
            CASE(104)

                IF (workd%is2d) THEN

                    ioff = 1
                    DO j = lb2, ub2

                        DO i = lb1, ub1

                            IF (j .GT. lb2 .AND. i .GT. lb1) THEN

                                self%fieldbuf(ioff,tid) = idx * ((workd%domain2d(pi,pj)%bface2d(i+1,j,2) - workd%domain2d(pi,pj)%bface2d(i,j,2)) - &
                                     (workd%domain2d(pi,pj)%bface2d(i,j+1,1) - workd%domain2d(pi,pj)%bface2d(i,j,1)))

                            END IF
                            ioff = ioff + 1

                        END DO

                    END DO

                END IF
                
        END SELECT

        !### if the precision is real8 and WRD is 64 bits or real4 and WRD is 32 bit then we're set for straight large copy ###
        IF ((precs .EQ. 'real8' .AND. STORAGE_SIZE(wrdc) .EQ. 64) .OR. (precs .EQ. 'real4' .AND. STORAGE_SIZE(wrdc) .EQ. 32)) THEN
            
            mykind = 4
            IF (precs .EQ. 'real8') mykind = 8
            mylen = mykind*fblen
            
            IF (flavor .EQ. 1) THEN
                CALL memcpy(self%comp_buf(myoffset), self%fieldbuf(1,tid), mylen)
                self%comp_buf(myoffset:myoffset+mylen-1) = TRANSFER(self%fieldbuf(1:fblen,tid), self%comp_buf(myoffset:myoffset+mylen-1))
            ELSE IF (flavor .EQ. 2) THEN
                !CALL memcpy(self%movie_buf(myoffset), self%fieldbuf(1,tid), mylen)
                self%movie_buf(myoffset:myoffset+mylen-1) = TRANSFER(self%fieldbuf(1:fblen,tid), self%movie_buf(myoffset:myoffset+mylen-1))
            END IF
            
            myoffset = myoffset + mylen
            
            !### if the precision is real8 and we're not 64 bits (or real4 and we're not 32 bit) we'll need to do a cast and an element by element copy ###
        ELSE IF ((precs .EQ. 'real8' .AND. STORAGE_SIZE(wrdc) .NE. 64) .OR. (precs .EQ. 'real4' .AND. STORAGE_SIZE(wrdc) .NE. 32)) THEN
            
            mykind = 4
            IF (precs .EQ. 'real8') mykind = 8
            mylen = mykind
            
            IF (flavor .EQ. 1) THEN
                
                IF (precs .EQ. 'real8') THEN
                    DO i = 1, fblen
                        
                        dp_val = REAL(self%fieldbuf(i,tid), 8)
                        CALL memcpy(self%comp_buf(myoffset), dp_val, mylen)
                        myoffset = myoffset + mylen
                        
                    END DO
                ELSE
                    DO i = 1, fblen
                        
                        sp_val = REAL(self%fieldbuf(i,tid), 4)
                        CALL memcpy(self%comp_buf(myoffset), sp_val, mylen)
                        myoffset = myoffset + mylen
                        
                    END DO
                END IF
                
            ELSE IF (flavor .EQ. 2) THEN
                
                IF (precs .EQ. 'real8') THEN
                    DO i = 1, fblen
                        
                        dp_val = REAL(self%fieldbuf(i,tid), 8)
                        CALL memcpy(self%movie_buf(myoffset), dp_val, mylen)
                        myoffset = myoffset + mylen
                        
                    END DO
                ELSE
                    DO i = 1, fblen
                        
                        sp_val = REAL(self%fieldbuf(i,tid), 4)
                        CALL memcpy(self%movie_buf(myoffset), sp_val, mylen)
                        myoffset = myoffset + mylen
                        
                    END DO
                END IF
                
            END IF
            
            !### if we're set to do a byte2 quantity we scale to 65536 values ###
        ELSE IF (precs .EQ. 'byte2') THEN
            
            !### create the map avoiding divide by zero ###
            IF (map .EQ. 'linear') THEN
                scaling = 65535.0_WRS / MAX(1.0E-6_WRS,(maxv - minv))
            ELSE IF (map .EQ. 'log_e') THEN
                scaling = 65535.0_WRS / MAX(1.0E-6_WRS,LOG(MAX(1.0E-36_WRS,maxv)) - LOG(MAX(1.0E-36_WRS,minv)))
            END IF
            
            !### we'll clip values to within the min and max values given ###
            IF (flavor .EQ. 1) THEN
                
                IF (map .EQ. 'linear') THEN
                    DO i = 1, fblen
                        
                        tb_val = INT(scaling * MIN(maxv, MAX(minv, REAL(self%fieldbuf(i,tid),WRS))) - minv, 2)
                        CALL memcpy(self%comp_buf(myoffset), tb_val, 2)
                        myoffset = myoffset + 2
                        
                    END DO
                ELSE IF (map .EQ. 'log_e') THEN
                    DO i = 1, fblen
                        
                        tb_val = INT(scaling * MIN(maxv, MAX(minv, LOG(MAX(1.0E-36_WRS, REAL(self%fieldbuf(i,tid),WRS))) - minv)), 2)
                        CALL memcpy(self%comp_buf(myoffset), tb_val, 2)
                        myoffset = myoffset + 2
                        
                    END DO
                END IF
                
            ELSE IF (flavor .EQ. 2) THEN
                
                IF (map .EQ. 'linear') THEN
                    DO i = 1, fblen
                        
                        tb_val = INT(scaling * MIN(maxv, MAX(minv, REAL(self%fieldbuf(i,tid),WRS))) - minv, 2)
                        CALL memcpy(self%movie_buf(myoffset), tb_val, 2)
                        myoffset = myoffset + 2
                        
                    END DO
                ELSE IF (map .EQ. 'log_e') THEN
                    DO i = 1, fblen
                        
                        tb_val = INT(scaling * MIN(maxv, MAX(minv, LOG(MAX(1.0E-36_WRS, REAL(self%fieldbuf(i,tid),WRS))) - minv)), 2)
                        CALL memcpy(self%movie_buf(myoffset), tb_val, 2)
                        myoffset = myoffset + 2
                        
                    END DO
                END IF
                
            END IF
            
            !### similar to 2 byte mapping, but now over a more narrow range of buckets ###
        ELSE IF (precs .EQ. 'byte1') THEN
            
            !### create the map avoiding divide by zero ###
            IF (map .EQ. 'linear') THEN
                scaling = 255.0_WRS / MAX(1.0E-4_WRS,(maxv - minv))
            ELSE IF (map .EQ. 'log_e') THEN
                scaling = 255.0_WRS / MAX(1.0E-4_WRS,LOG(MAX(1.0E-36_WRS,maxv)) - LOG(MAX(1.0E-36_WRS,minv)))
            END IF
            
            !### we'll clip values to within the min and max values given ###
            IF (flavor .EQ. 1) THEN
                
                IF (map .EQ. 'linear') THEN
                    DO i = 1, fblen
                        
                        sb_val = INT(scaling * MIN(maxv, MAX(minv, REAL(self%fieldbuf(i,tid),WRS))) - minv, 1)
                        CALL memcpy(self%comp_buf(myoffset), sb_val, 1)
                        myoffset = myoffset + 1
                        
                    END DO
                ELSE IF (map .EQ. 'log_e') THEN
                    DO i = 1, fblen
                        
                        sb_val = INT(scaling * MIN(maxv, MAX(minv, LOG(MAX(1.0E-36_WRS, REAL(self%fieldbuf(i,tid),WRS))) - minv)), 1)
                        CALL memcpy(self%comp_buf(myoffset), sb_val, 1)
                        myoffset = myoffset + 1
                        
                    END DO
                END IF
                
            ELSE IF (flavor .EQ. 2) THEN
                
                IF (map .EQ. 'linear') THEN
                    DO i = 1, fblen
                        
                        sb_val = INT(scaling * MIN(maxv, MAX(minv, REAL(self%fieldbuf(i,tid),WRS))) - minv, 1)
                        CALL memcpy(self%movie_buf(myoffset), sb_val, 1)
                        myoffset = myoffset + 1
                        
                    END DO
                ELSE IF (map .EQ. 'log_e') THEN
                    DO i = 1, fblen
                        
                        sb_val = INT(scaling * MIN(maxv, MAX(minv, LOG(MAX(1.0E-36_WRS, REAL(self%fieldbuf(i,tid),WRS))) - minv)), 1)
                        CALL memcpy(self%movie_buf(myoffset), sb_val, 1)
                        myoffset = myoffset + 1
                        
                    END DO
                END IF
                
            END IF
            
        END IF

    END DO

END SUBROUTINE append_patch_data

!### parse out the fields and mapping information from a format string array ###
SUBROUTINE parse_format(self, flavor)

    CLASS(IO) :: self
    INTEGER(WIS), INTENT(IN) :: flavor
    INTEGER(WIS) :: i, ii, l, r, j
    CHARACTER(WMAX_IO_FORM_LEN) :: mystring(7)
    LOGICAL(WIS) :: parse

    !### parse the appropriate array ###
    ii = 0
    SELECT CASE (flavor)

        !### compressed ###
        CASE (1)

            DO i = 1, WMAX_IO_FIELDS

                IF (self%compressedFormat(i) .NE. ' ') THEN

                    ii = ii + 1

                    mystring = CHAR(0)
                    j = 1
                    parse = .TRUE.
                    DO l = 1, WMAX_IO_FORM_LEN

                        IF (self%compressedFormat(i)(l:l) .NE. ' ' .AND. parse) THEN

                            DO r = l, WMAX_IO_FORM_LEN

                                IF (self%compressedFormat(i)(r:r) .EQ. ' ') THEN

                                    IF (j .LE. 7) THEN

                                        mystring(j) = self%compressedFormat(i)(l:r)
                                        j = j + 1
                                        parse = .FALSE.
                                        EXIT
                                        
                                    ENDIF
                                        
                                END IF

                            END DO

                        ELSE IF (self%compressedFormat(i)(l:l) .EQ. ' ') THEN
                            
                            parse = .TRUE.

                        END IF

                    END DO

                    READ(mystring(1), *) self%comp_fields(ii)
                    self%comp_field_symbs(ii)  = TRIM(ADJUSTL(mystring(2)))
                    self%comp_field_titles(ii) = TRIM(ADJUSTL(mystring(3)))
                    self%comp_field_precs(ii)  = TRIM(ADJUSTL(mystring(4)))
                    self%comp_field_maps(ii)   = TRIM(ADJUSTL(mystring(5)))
                    READ(mystring(6), *) self%comp_field_mins(ii)
                    READ(mystring(7), *) self%comp_field_maxs(ii)

                END IF

            END DO

            self%comp_active_fields = ii

        !### movie dumps ###
        CASE (2)

            DO i = 1, WMAX_IO_FIELDS

                IF (self%movieFormat(i) .NE. ' ') THEN
                
                    ii = ii + 1

                    mystring = CHAR(0)
                    j = 1
                    parse = .TRUE.
                    DO l = 1, WMAX_IO_FORM_LEN

                        IF (self%movieFormat(i)(l:l) .NE. ' ' .AND. parse) THEN

                            DO r = l, WMAX_IO_FORM_LEN

                                IF (self%movieFormat(i)(r:r) .EQ. ' ') THEN

                                    IF (j .LE. 7) THEN
                                        
                                        mystring(j) = self%movieFormat(i)(l:r)
                                        j = j + 1
                                        parse = .FALSE.
                                        EXIT

                                    END IF

                                END IF

                            END DO

                        ELSE IF (self%movieFormat(i)(l:l) .EQ. ' ') THEN

                            parse = .TRUE.

                        END IF

                    END DO

                    READ(mystring(1), *) self%movie_fields(ii)
                    self%movie_field_symbs(ii)  = TRIM(ADJUSTL(mystring(2)))
                    self%movie_field_titles(ii) = TRIM(ADJUSTL(mystring(3)))
                    self%movie_field_precs(ii)  = TRIM(ADJUSTL(mystring(4)))
                    self%movie_field_maps(ii)   = TRIM(ADJUSTL(mystring(5)))
                    READ(mystring(6), *) self%movie_field_mins(ii)
                    READ(mystring(7), *) self%movie_field_maxs(ii)
                
                END IF

            END DO

            self%movie_active_fields = ii

    END SELECT

END SUBROUTINE parse_format

!### wait on all or a specified AIO slot to complete.  if slot = 0 this means wait on all inflight slots ###
SUBROUTINE wait_aio_slots(self, flavor, slot, writeMeta)

    CLASS(IO) :: self
    INTEGER(WIS), INTENT(IN) :: flavor, slot
    INTEGER(WIS) :: op
    LOGICAL(WIS), INTENT(IN) :: writeMeta

    !### below we do everything whether or AIO is in use (except of course AIO calls) ###
    IF (self%aioOn) CALL AIO_PROGRESS()

    !### wait on a specific slot ###
    IF (slot .GT. 0) THEN

        IF (flavor .EQ. 1 .AND. self%comp_aio_ops%inflight(slot)) THEN

            IF (self%aioOn) THEN

                CALL AIO_WAIT(self%comp_aio_ops%islots(slot))

                IF (writeMeta) THEN

                    CALL AIO_WRITE_BUF_META(TRIM(self%compressedPath) // '/' // self%comp_aio_ops%manifestFile(slot), 0, 0, self%comp_buf(self%comp_aio_ops%offsets(slot,1)), self%comp_aio_ops%islots(slot), 2)
                    CALL AIO_WAIT(self%comp_aio_ops%islots(slot))

                END IF

            END IF
            self%comp_aio_ops%inflight(slot)  = .FALSE.
            self%comp_aio_ops%offsets(slot,:) = 0
            self%comp_aio_ops%lengths(slot,:) = 0

        ELSE IF (flavor .EQ. 2 .AND. self%movie_aio_ops%inflight(slot)) THEN

            IF (self%aioOn) THEN

                CALL AIO_WAIT(self%movie_aio_ops%islots(slot))

                IF (writeMeta) THEN

                    CALL AIO_WRITE_BUF_META(TRIM(self%moviePath) // '/' // self%movie_aio_ops%manifestFile(slot), 0, 0, self%movie_buf(self%movie_aio_ops%offsets(slot,1)), self%movie_aio_ops%islots(slot), 2)
                    CALL AIO_WAIT(self%movie_aio_ops%islots(slot))

                END IF

            END IF
            self%movie_aio_ops%inflight(slot)  = .FALSE.
            self%movie_aio_ops%offsets(slot,:) = 0
            self%movie_aio_ops%lengths(slot,:) = 0

        END IF

    !### wait on all inflight slots ###
    ELSE

        IF (flavor .EQ. 1) THEN

            DO op = 1, WNUM_AIO_OPS_INFLIGHT

                IF (self%comp_aio_ops%inflight(op)) THEN

                    IF (self%aioOn) THEN

                        CALL AIO_WAIT(self%comp_aio_ops%islots(op))

                        IF (writeMeta) THEN

                            CALL AIO_WRITE_BUF_META(TRIM(self%compressedPath) // '/' // self%comp_aio_ops%manifestFile(op), 0, 0, self%comp_buf(self%comp_aio_ops%offsets(op,1)), self%comp_aio_ops%islots(op), 2)
                            CALL AIO_WAIT(self%comp_aio_ops%islots(op))

                        END IF

                    END IF
                    self%comp_aio_ops%inflight(op)  = .FALSE.
                    self%comp_aio_ops%offsets(op,:) = 0
                    self%comp_aio_ops%lengths(op,:) = 0

                END IF

            END DO

        ELSE IF (flavor .EQ. 2) THEN

            DO op = 1, WNUM_AIO_OPS_INFLIGHT

                IF (self%movie_aio_ops%inflight(op)) THEN

                    IF (self%aioOn) THEN

                        CALL AIO_WAIT(self%movie_aio_ops%islots(op))

                        IF (writeMeta) THEN

                            CALL AIO_WRITE_BUF_META(TRIM(self%moviePath) // '/' // self%movie_aio_ops%manifestFile(op), 0, 0, self%movie_buf(self%movie_aio_ops%offsets(op,1)), self%movie_aio_ops%islots(op), 2)
                            CALL AIO_WAIT(self%movie_aio_ops%islots(op))

                        END IF

                    END IF
                    self%movie_aio_ops%inflight(op)  = .FALSE.
                    self%movie_aio_ops%offsets(op,:) = 0
                    self%movie_aio_ops%lengths(op,:) = 0

                END IF

            END DO

        END IF

    END IF

END SUBROUTINE wait_aio_slots

END MODULE Mod_IO
