#include "config.h"

MODULE Mod_SimulationUnits

!######################################################################
!#
!# FILENAME: simulation_units.f90
!#
!# DESCRIPTION: 
!#
!# DEPENDENCIES: Mod_Globals for defining data types
!#
!# HISTORY:
!#    5/19/16  - Peter Mendygral
!#
!######################################################################

!### require the needed modules ###
USE Mod_Globals
USE Mod_Error
USE Mod_RankLocation

IMPLICIT NONE

!### default private but make the data type available ###
PRIVATE
PUBLIC :: SimulationUnits

!### define a data type for this module ###
TYPE :: SimulationUnits

    !### default everything as private ###
    PRIVATE

    REAL(WRD), PUBLIC :: boxsize(3),          &       ! length of the world grid along each dimension in code units
                         dx,                  &       ! zone size in code units for the root domain (to be extended for mesh refinement later)
                         dt,                  &       ! time step size in code units for the root domain (to be extended for mesh refinement later)
                         t0,                  &       ! initial simulation time in code units
                         t,                   &       ! current time in code units
                         t_end,               &       ! ending time in code units

                         boxsize_cgs(3),      &       ! length of the world grid along each dimension in CGS units
                         dx_cgs,              &       ! zone size in CGS units for the root domain
                         dt_cgs,              &       ! time step size in CGS units
                         t0_cgs,              &       ! initial time in CGS units
                         t_cgs,               &       ! time in CGS units
                         t_end_cgs,           &       ! ending time in CGS units

                         mass_cgs,            &       ! conversion factor to CGS units for mass
                         mass_density_cgs,    &       ! conversion factor to CGS units for mass density
                         velocity_cgs,        &       ! conversion factor to CGS units for velocity
                         time_cgs,            &       ! conversion factor to CGS units for time
                         length_cgs,          &       ! conversion factor to CGS units for length
                         energy_cgs,          &       ! conversion factor to CGS units for energy
                         energy_density_cgs,  &       ! conversion factor to CGS units for energy density
                         magnetic_cgs,        &       ! conversion factor to CGS units for the magnetic field

                         cfl,                 &       ! the CFL parameter to maintain when determining the time step

                         pi                           ! PI

    INTEGER(WIS), PUBLIC :: max_steps                 ! maximum number of time steps to take on the root grid

    CONTAINS

    PRIVATE

    PROCEDURE :: init_simulationunits
    PROCEDURE, PUBLIC :: update_code2cgs
    PROCEDURE, PUBLIC :: propose_timestep

END TYPE SimulationUnits

INTERFACE SimulationUnits
   
   MODULE PROCEDURE constructor
   
END INTERFACE

!### object methods ###
CONTAINS
  
FUNCTION constructor(namelist, rankloc)

    CHARACTER(WMAX_NAMELIST_LEN), INTENT(IN) :: namelist
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: constructor
    
    CALL constructor%init_simulationunits(namelist, rankloc)
    RETURN
  
END FUNCTION constructor

!### init_simulationunits will prepare the object for use ###
SUBROUTINE init_simulationunits(self, namelist, rankloc)

    CLASS(SimulationUnits) :: self
    CHARACTER(WMAX_NAMELIST_LEN), INTENT(IN) :: namelist
    TYPE(RankLocation) :: rankloc
    REAL(WRD) :: xboxsize, dt0, t0, t_end, cfl, mass_cgs, length_cgs, velocity_cgs
    INTEGER(WIS) :: max_steps

    NAMELIST /Simulation/ xboxsize, dt0, t0, t_end, cfl, max_steps, mass_cgs, length_cgs, velocity_cgs
    READ(namelist, NML=Simulation)

    self%pi = 2.0_WRD * ASIN(1.0_WRD)

    self%max_steps = max_steps
    self%cfl       = cfl

    !### now determine the relevant conversion factors ###
    self%mass_cgs           = mass_cgs
    self%length_cgs         = length_cgs
    self%velocity_cgs       = velocity_cgs

    self%time_cgs           = self%length_cgs / self%velocity_cgs
    self%mass_density_cgs   = self%mass_cgs / self%length_cgs**3
    self%energy_cgs         = self%mass_cgs * self%length_cgs**2 / self%time_cgs**2
    self%energy_density_cgs = self%energy_cgs / self%length_cgs**3
    self%magnetic_cgs       = SQRT(4.0_WRD * self%pi * self%energy_density_cgs)

    !### determine all of the values specified in code units ###
    self%dx          = xboxsize / (self%length_cgs * rankloc%nranks(1) * rankloc%npatches(1) * rankloc%patch_size(1))
    self%boxsize(1)  = self%dx * rankloc%nranks(2) * rankloc%npatches(2) * rankloc%patch_size(2)
    self%boxsize(2)  = self%dx * rankloc%nranks(2) * rankloc%npatches(2) * rankloc%patch_size(2)
    self%boxsize(3)  = self%dx * rankloc%nranks(2) * rankloc%npatches(2) * rankloc%patch_size(2)
    self%dt          = dt0 / self%time_cgs
    self%t0          = t0 / self%time_cgs
    self%t           = self%t0
    self%t_end       = t_end / self%time_cgs

    self%dx_cgs      = self%dx * self%length_cgs
    self%t0_cgs      = self%t0 * self%time_cgs
    self%t_cgs       = self%t * self%time_cgs
    self%t_end_cgs   = self%t_end * self%time_cgs
    self%boxsize_cgs = self%boxsize * self%length_cgs

    !### now call our conversion for all variables that change with the simulation ###
    CALL self%update_code2cgs()

END SUBROUTINE init_simulationunits

!### update any CGS values we have with code values that vary ###
SUBROUTINE update_code2cgs(self)

    CLASS(SimulationUnits) :: self

    self%t_cgs  = self%t * self%time_cgs
    self%dt_cgs = self%dt * self%time_cgs

END SUBROUTINE update_code2cgs

!### given an input signal speed, compute a proposed time step size ###
REAL(WRD) FUNCTION propose_timestep(self, nsteps, maxsignal)

    CLASS(SimulationUnits) :: self
    INTEGER(WIS), INTENT(IN) :: nsteps
    REAL(WRD), INTENT(IN) :: maxsignal
    REAL(WRD) :: newdt

    newdt = self%cfl * self%dx / maxsignal 

    !### do an exponential ramp from the base timestep to the calculated timestep as a precaution ###
    IF (nsteps .LE. 4) &
        newdt = newdt * EXP(REAL(nsteps-4,WRD))
    IF (newdt .LT. self%dt) THEN
        newdt = MAX(1.0E-8_WRD, newdt + (newdt - self%dt) * 2)
    ELSE
        newdt = self%dt + 0.1_WRD * (newdt - self%dt)
    END IF
#ifdef WMBT_COMM_BENCHMARK  
    newdt = self%dt
#endif

    propose_timestep = newdt

END FUNCTION propose_timestep

END MODULE Mod_SimulationUnits
