#include "config.h"

MODULE Mod_RankLocation

!######################################################################
!#
!# FILENAME: rank_location.f90
!#
!# DESCRIPTION: 
!#
!# DEPENDENCIES: Mod_Globals for defining data types
!#
!# HISTORY:
!#    5/19/16  - Peter Mendygral
!#
!######################################################################

!### require the needed modules ###
USE Mod_Globals
USE Mod_Error

IMPLICIT NONE

!### default private but make the data type available ###
PRIVATE
PUBLIC :: RankLocation

!### define a data type for this module ###
TYPE :: RankLocation

    !### default everything as private ###
    PRIVATE

    INTEGER(WIS), PUBLIC :: izone_origin(3), &   ! world grid zone integer location of this rank's root domain origin
                            my_coords(3),    &   ! this rank's location in the MPI Cartesian communicator
                            nranks(3),       &   ! number of ranks in each dimension  
                            npatches(3),     &   ! number of root domain Patches in each dimension within a MPI rank
                            patch_size(3)        ! number of zones in each dimension for a root domain Patch
    REAL(WRD), PUBLIC ::    rzone_origin(3)         ! world grid zone real location of this rank's root domain origin (code units)
    LOGICAL(WIS), PUBLIC :: is_boundary(2,3)     ! true if for given dimension and edge this rank is a world grid boundary

    CONTAINS

    PRIVATE

    PROCEDURE :: initranklocation
    PROCEDURE, PUBLIC :: calc_location

END TYPE RankLocation

INTERFACE RankLocation
   
   MODULE PROCEDURE constructor
   
END INTERFACE

!### object methods ###
CONTAINS
  
FUNCTION constructor(my_coords, is_boundary, nranks, npatches, patch_size)
  
    TYPE(RankLocation) :: constructor
    INTEGER(WIS), INTENT(IN) :: my_coords(3), nranks(3), npatches(3), patch_size(3)
    LOGICAL(WIS), INTENT(IN) :: is_boundary(2,3)
    
    CALL constructor%initranklocation(my_coords, is_boundary, nranks, npatches, patch_size)
    RETURN
  
END FUNCTION constructor

!### initranklocation will prepare the object for use ###
SUBROUTINE initranklocation(self, my_coords, is_boundary, nranks, npatches, patch_size)

    CLASS(RankLocation) :: self
    INTEGER(WIS), INTENT(IN) :: my_coords(3), nranks(3), npatches(3), patch_size(3)
    LOGICAL(WIS), INTENT(IN) :: is_boundary(2,3)

    !### store these values as we will need them to initialize ###
    self%my_coords   = my_coords
    self%is_boundary = is_boundary
    self%nranks      = nranks
    self%npatches    = npatches
    self%patch_size  = patch_size

END SUBROUTINE initranklocation

!### given dx compute everything about this rank's world grid location ###
SUBROUTINE calc_location(self, dx)

    CLASS(RankLocation) :: self
    REAL(WRD), INTENT(IN) :: dx
    INTEGER(WIS) :: i

    DO i = 1, 3

        self%izone_origin(i) = self%my_coords(i) * self%npatches(i) * self%patch_size(i) - INT(0.5_WRD * self%nranks(i) * self%npatches(i) * self%patch_size(i)) + 1
        self%rzone_origin(i) = dx * self%my_coords(i) * self%npatches(i) * self%patch_size(i) + 0.5_WRD * dx - 0.5_WRD * dx * (self%nranks(i) * self%npatches(i) * self%patch_size(i))

    END DO

END SUBROUTINE calc_location

END MODULE Mod_RankLocation
