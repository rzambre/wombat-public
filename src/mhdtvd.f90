#include "config.h"

MODULE Mod_MHDTVD

!######################################################################
!#
!# FILENAME: mod_mhdtvd.f90
!#
!# DESCRIPTION: This module 
!#
!# DEPENDENCIES: Mod_MHDTVD for defining data types
!#
!# HISTORY:
!#    5/7/12  - Peter Mendygral
!#
!######################################################################

!### require the needed modules ###
USE Mod_Globals
USE Mod_Error
USE Mod_Patch
USE Mod_MHDPool

IMPLICIT NONE

!### default private but make the data type available ###
PRIVATE
PUBLIC :: MHDTVD

!### define a data type for this module ###
TYPE :: MHDTVD

    !### default everything as private ###
    PRIVATE

    REAL(WRD) :: gamma                              ! this will need to be fancier to allow for variable EOS
    REAL(WRD) :: zero_value, min_pressure, min_density, prot_min_density, prot_min_pressure, eps(4)

    CONTAINS    !### default everything as private ###
    PRIVATE

    PROCEDURE, PUBLIC :: initmhd
    PROCEDURE, PUBLIC :: mhd_solve
    PROCEDURE, PUBLIC :: ct_solve
    PROCEDURE :: mhd_solve1d
    PROCEDURE :: mhd_solve2d
    PROCEDURE :: mhd_solve3d
    PROCEDURE :: ct_solve2d
    PROCEDURE :: ct_solve3d
    PROCEDURE :: apply_protections1d
    PROCEDURE :: compute_pressure1d
    PROCEDURE :: compute_zone_averages1d
    PROCEDURE :: compute_speeds_eigenvals1d
    PROCEDURE :: compute_eigenvecs1d
    PROCEDURE :: compute_fluxes1d
    PROCEDURE :: update_states1d
    PROCEDURE :: get_maxwave1d
    PROCEDURE :: apply_protections2d
    PROCEDURE :: compute_Xfluxes2d
    PROCEDURE :: compute_Yfluxes2d
    PROCEDURE :: halfXupdate_states2d
    PROCEDURE :: halfYupdate_states2d
    PROCEDURE :: update_states2d
    PROCEDURE :: compute_reference_emf2d
    PROCEDURE :: compute_corner_emf2d
    PROCEDURE :: applyct2d
    PROCEDURE :: get_maxwave2d
    PROCEDURE :: apply_protections3d
    PROCEDURE :: compute_Zplane_XYfluxes3d
    PROCEDURE :: compute_Zfluxes3d
    PROCEDURE :: halfXupdate_states3d
    PROCEDURE :: halfYupdate_states3d
    PROCEDURE :: halfZupdate_states3d
    PROCEDURE :: update_states3d
    PROCEDURE :: compute_reference_emf3d
    PROCEDURE :: compute_corner_emf3d
    PROCEDURE :: applyct3d
    PROCEDURE :: get_maxwave3d
    PROCEDURE :: get_eps
    PROCEDURE :: init_vtavg1d
    PROCEDURE :: init_vtavg2d
    PROCEDURE :: init_vtavg3d
    PROCEDURE :: compute_vtavg1d
    PROCEDURE :: compute_vtavg2d
    PROCEDURE :: compute_vtavg3d

END TYPE MHDTVD

INTERFACE MHDTVD

    MODULE PROCEDURE constructor

END INTERFACE

!### object methods ###
CONTAINS

!### constructor ###
FUNCTION constructor(namelist, gamma)

    CHARACTER(WMAX_NAMELIST_LEN), INTENT(IN) :: namelist
    REAL(WRD), INTENT(IN) :: gamma
    TYPE(MHDTVD) :: constructor

    CALL constructor%initmhd(namelist, gamma)
    RETURN

END FUNCTION constructor

!### initialize the object ###
SUBROUTINE initmhd(self, namelist, gamma)

    CLASS(MHDTVD) :: self
    CHARACTER(WMAX_NAMELIST_LEN), INTENT(IN) :: namelist
    REAL(WRD), INTENT(IN) :: gamma
    REAL(WRD) :: eps1, eps2, eps3, eps4, zero_value, min_pressure, min_density, prot_min_density, prot_min_pressure
    
    NAMELIST /MHD/ eps1, eps2, eps3, eps4, zero_value, min_pressure, min_density, prot_min_density, prot_min_pressure

    !### gather our parameters from the namelist contents ###
    READ(namelist, NML=MHD)

    self%eps               = (/eps1, eps2, eps3, eps4/)
    self%gamma             = gamma
    self%zero_value        = zero_value
    self%min_pressure      = min_pressure
    self%min_density       = min_density
    self%prot_min_pressure = prot_min_pressure
    self%prot_min_density  = prot_min_density

END SUBROUTINE initmhd

!### accepting a patch object, mhd_solve will move the patch in time from n to n+1 with time step dt from the patch ###
SUBROUTINE mhd_solve(self, workp, pool, maxsignal, update_cmplt)

    CLASS(MHDTVD) :: self
    TYPE(Patch) :: workp
    TYPE(MHDPool) :: pool
    REAL(WRD), INTENT(INOUT) :: maxsignal
    LOGICAL(WIS), INTENT(OUT) :: update_cmplt

    !### assume we will not complete the update in this pass ###
    update_cmplt = .FALSE.

    !### based on the number of dimensions we'll forward this to the appropriate solver ###
    IF (workp%is1d) THEN
	
       IF (workp%update_pass_count .EQ. 0) THEN
           CALL self%init_vtavg1d(workp)
           CALL self%mhd_solve1d(workp, pool)
       ELSE IF (workp%update_pass_count .EQ. 1) THEN
           CALL self%compute_vtavg1d(workp)
           update_cmplt = .TRUE.
       END IF

    ELSE IF (workp%is2d) THEN

       IF (workp%update_pass_count .EQ. 0) THEN
           CALL self%init_vtavg2d(workp)
           CALL self%mhd_solve2d(workp, pool)
       ELSE IF (workp%update_pass_count .EQ. 1) THEN
           CALL self%compute_vtavg2d(workp)
           update_cmplt = .TRUE.
       END IF

    ELSE IF (workp%is3d) THEN
	
       IF (workp%update_pass_count .EQ. 0) THEN
           CALL self%init_vtavg3d(workp)
           CALL self%mhd_solve3d(workp, pool)
       ELSE IF (workp%update_pass_count .EQ. 1) THEN
           CALL self%compute_vtavg3d(workp)
           update_cmplt = .TRUE.
       END IF

    END IF

    !### possibly update the maximum signal value with our computed maximum wave speed ###
    maxsignal = MAX(maxsignal, pool%max_wave)

END SUBROUTINE mhd_solve

!### the 1-d solver assumes integration along X ###
SUBROUTINE mhd_solve1d(self, workp, pool)

    CLASS(MHDTVD) :: self
    TYPE(Patch) :: workp
    TYPE(MHDPool) :: pool
    INTEGER(WIS) :: ilow, ihi, v

    pool%dt = workp%dt
    pool%dx = workp%dx
    ilow    = 1 + workp%nb - 2
    ihi     = workp%nx + workp%nb + 2

    !### we cannot accept unphysical values from the user's boundary or init routines ###
    CALL self%apply_protections1d(workp%grid1d, ilow, ihi)

    !### save a backup of the state variables before any preconditioning ###
    pool%state_save1d = workp%grid1d

    !### do preconditioning for gravity, etc. ###

    !### realizing this is a redundant copy, place the state variables into a 2d compliant work grid for use in the plane-updating routines below ###
    DO v = 1, workp%nvars

        pool%qwork2d(:,1,v) = workp%grid1d(:,v)

    END DO

    !### first we need to compute the gas pressure for the full patch ###
    CALL self%compute_pressure1d(pool%qwork2d, pool%pressure1d, ilow, ihi, 1, 1)

    !### next we need the Roe average state variable values at zone interfaces at time n along with the 'X' weighting factor from CG97 ###
    CALL self%compute_zone_averages1d(pool%state_avg1d, pool%qwork2d, pool%pressure1d, ilow, ihi, 1, 1)

    !### compute wave speeds and eigenvalues ###
    CALL self%compute_speeds_eigenvals1d(pool%eigenVal1d, pool%waves1d, pool%state_avg1d, ilow, ihi, 1, 1)

    !### construct the left-handed eigenvectors and characteristics ###
    CALL self%compute_eigenvecs1d(pool%eigenVec1d, pool%charac1d, pool%eigenVal1d, pool%waves1d, &
         pool%qwork2d, pool%state_avg1d, pool%pressure1d, ilow, ihi, 1, 1)

    !### compute the fluxes ###
    CALL self%compute_fluxes1d(pool%nstate, pool%nwaves, 1, pool%dt, pool%dx, pool%Pflux1d, pool%protPflux1d, pool%waves1d, pool%eigenVec1d, pool%charac1d, pool%eigenVal1d, &
         pool%pressure1d, pool%qwork2d, pool%Bflux2d, pool%Bflux3d, pool%protBflux2d, pool%protBflux3d, .FALSE., .FALSE., ilow, ihi, 1, 1, 1, 1)

    !### finally apply the fluxes for an update ###
    CALL self%update_states1d(pool%dt, pool%dx, workp%grid1d, pool%Pflux1d, pool%prot_q1d, pool%protPflux1d, pool%prot_flags1d, ilow, ihi)

    !### determine maximum wave speed after the update ###
    CALL self%get_maxwave1d(workp, pool, ilow, ihi)

END SUBROUTINE mhd_solve1d

SUBROUTINE mhd_solve2d(self, workp, pool)

    CLASS(MHDTVD) :: self
    TYPE(Patch) :: workp
    TYPE(MHDPool) :: pool
    INTEGER(WIS) :: ilow, ihi, jlow, jhi, j, v, lilow, lihi, ljlow, ljhi

    pool%dt = workp%dt
    pool%dx = workp%dx
    ilow    = 1 + workp%nb - 5
    ihi     = workp%nx + workp%nb + 5
    jlow    = 1 + workp%nb - 5
    jhi     = workp%ny + workp%nb + 5
    lilow   = 1 - 5
    lihi    = workp%nx + 5
    ljlow   = 1 - 5
    ljhi    = workp%ny + 5

    !### we cannot accept unphysical values from the user's boundary or init routines ###
    CALL self%apply_protections2d(workp%grid2d, ilow, ihi, jlow, jhi)

    !### backup the current state vectors and face centered field ###
    DO v = 1, workp%nvars

        DO j = ljlow, ljhi

            pool%state_save2d(lilow:lihi,j,v) = workp%grid2d(lilow:lihi,j,v)

        END DO

    END DO
    DO v = 1, 2

        DO j = ljlow, ljhi

            pool%bface_save2d(lilow:lihi,j,v) = workp%bface2d(lilow:lihi,j,v)

        END DO

    END DO

    !### do preconditioning for gravity, etc. ###

    !### the following is adapted from the CTU+CT algorithm ###
    !### Step 1) compute interface states in both X and Y simultaneously using same initial state (same as one would with 1d solver)
    !### Step 2) compute X and Y fluxes simultaneously
    !### Step 3) compute magnetic fluxes in X and Y
    !### Step 4) evolve state to t+1/2 using transverse flux gradients and source terms
    !### Step 5) perform CT update over dt/2
    !### Step 6+7) compute new state and magnetic fluxes with preconditioned state
    !### Step 8) perform update to t
    !### Step 9) apply final CT update

    !### Steps 1-2) compute the fluxes along X ###
    CALL self%compute_Xfluxes2d(workp, pool, ljlow+1, ljhi-1, jlow+1, jhi-1, lilow, lihi, ilow, ihi, .TRUE., .FALSE.)

    !### Steps 1-2) compute fluxes along Y ###
    CALL self%compute_Yfluxes2d(workp, pool, lilow+1, lihi-1, ilow+1, ihi-1, ljlow, ljhi, jlow, jhi, .TRUE., .FALSE.)

    !### Step 3) now compute the cell-centered reference emf for use in the half step CT update ###
    CALL self%compute_reference_emf2d(pool%dt, pool%dx, workp%grid2d, pool%workPflux2d, pool%EMF2d, ilow, ihi, jlow, jhi, .FALSE.)

    !### adjust the indices by one in each direction and then do a CT update so the half updates can use this for the cell centered fields ###
    ilow  = ilow + 1
    ihi   = ihi - 1
    jlow  = jlow + 1
    jhi   = jhi - 1
    lilow = lilow + 1
    lihi  = lihi - 1
    ljlow = ljlow + 1
    ljhi  = ljhi - 1

    !### Step 4) perform the CT update over 1/2 dt ###
    CALL self%compute_corner_emf2d(workp%grid2d, pool%EMF2d, workp%emf2d, ilow, ihi, jlow, jhi)
    CALL self%applyct2d(pool%dt, pool%dx, workp%grid2d, workp%bface2d, workp%emf2d, ilow, ihi, jlow, jhi, .TRUE.)

    !### the design here requires a minimum of 4 boundary zones since we will NOT communicate the half step values ###
    !### we now adjust the lower and upper bounds to account for this fact ###
    ilow  = ilow + 2
    ihi   = ihi - 2
    jlow  = jlow + 2
    jhi   = jhi - 2
    lilow = lilow + 2
    lihi  = lihi - 2
    ljlow = ljlow + 2
    ljhi  = ljhi - 2

    !### Step 4,6) evolve state to t+1/2 and compute final X fluxes for final update (including magnetic fluxes) ###
    CALL self%compute_Xfluxes2d(workp, pool, ljlow+1, ljhi-1, jlow+1, jhi-1, lilow, lihi, ilow, ihi, .FALSE., .FALSE.)

    !### Step 4,6) evolve state to t+1/2 and compute final Y fluxes for final update (including magnetic fluxes) ###
    CALL self%compute_Yfluxes2d(workp, pool, lilow+1, lihi-1, ilow+1, ihi-1, ljlow, ljhi, jlow, jhi, .FALSE., .FALSE.)

    !### restore the original grid to undo any preconditioning ###
    DO v = 1, workp%nvars

        DO j = ljlow, ljhi

            workp%grid2d(lilow:lihi,j,v) = pool%state_save2d(lilow:lihi,j,v)

        END DO

    END DO

    !### Step 5) now compute the cell-centered reference emf for use in the final CT update ###
    CALL self%compute_reference_emf2d(pool%dt, pool%dx, workp%grid2d, pool%workPflux2d, pool%EMF2d, ilow, ihi, jlow, jhi, .TRUE.)

    !### Step 7) compute the final cell corner EMF for use in a later CT update ###
    CALL self%compute_corner_emf2d(workp%grid2d, pool%EMF2d, workp%emf2d, ilow, ihi, jlow, jhi)

    !### Step 8) apply final update ###
    CALL self%update_states2d(pool%dt, pool%dx, workp%grid2d, pool%Pflux2d, pool%EMF2d, pool%prot_q2d, pool%protPflux2d, pool%protEMF2d, pool%prot_flags2d, ilow, ihi, jlow, jhi)

    !### restore the original face centered field for final update later ###
    DO v = 1, 2

        DO j = ljlow-3, ljhi+3

             workp%bface2d(lilow-3:lihi+3,j,v) = pool%bface_save2d(lilow-3:lihi+3,j,v)

        END DO

    END DO

    !### we cannot compute the max wave signal until after the CT update, so leave ours as 0 for now ###
    pool%max_wave = 0.0_WRD

END SUBROUTINE mhd_solve2d

SUBROUTINE mhd_solve3d(self, workp, pool)

    CLASS(MHDTVD) :: self
    TYPE(Patch) :: workp
    TYPE(MHDPool) :: pool
    INTEGER(WIS) :: ilow, ihi, jlow, jhi, klow, khi, j, k, v, lilow, lihi, ljlow, ljhi, lklow, lkhi, koff

    pool%dt = workp%dt
    pool%dx = workp%dx
    ilow    = 1 + workp%nb - 5
    ihi     = workp%nx + workp%nb + 5
    jlow    = 1 + workp%nb - 5
    jhi     = workp%ny + workp%nb + 5
    klow    = 1 + workp%nb - 5
    khi     = workp%nz + workp%nb + 5
    lilow   = 1 - 5
    lihi    = workp%nx + 5
    ljlow   = 1 - 5
    ljhi    = workp%ny + 5
    lklow   = 1 - 5
    lkhi    = workp%nz + 5
    koff    = klow-lklow

    !### we cannot accept unphysical values from the user's boundary or init routines ###
    CALL self%apply_protections3d(workp%grid3d, ilow, ihi, jlow, jhi, klow, khi)

    !### backup the current state vectors and face centered field ###
    DO v = 1, workp%nvars

        DO k = lklow, lkhi

            DO j = ljlow, ljhi

                pool%state_save3d(lilow:lihi,j,k,v) = workp%grid3d(lilow:lihi,j,k,v)

            END DO

        END DO

    END DO
    DO v = 1, 3

        DO k = lklow, lkhi

            DO j = ljlow, ljhi

                pool%bface_save3d(lilow:lihi,j,k,v) = workp%bface3d(lilow:lihi,j,k,v)

            END DO

        END DO

    END DO

    !### do preconditioning for gravity, etc. ###

    !### the following is adapted from the CTU+CT algorithm ###
    !### Step 1) compute interface states in both X and Y simultaneously using same initial state (same as one would with 1d solver)
    !### Step 2) compute X and Y fluxes simultaneously
    !### Step 3) compute magnetic fluxes in X and Y
    !### Step 4) evolve state to t+1/2 using transverse flux gradients and source terms
    !### Step 5) perform CT update over dt/2
    !### Step 6+7) compute new state and magnetic fluxes with preconditioned state
    !### Step 8) perform update to t
    !### Step 9) apply final CT update

    !### Steps 1-2) compute the fluxes along X and Y ###
    CALL self%compute_Zplane_XYfluxes3d(workp, pool, lklow, lkhi, klow, khi, koff, ljlow, ljhi, jlow, jhi, lilow, lihi, ilow, ihi, .TRUE., .FALSE.)

    !### Steps 1-2) compute fluxes along Z ###
    CALL self%compute_Zfluxes3d(workp, pool, ljlow, ljhi, jlow, jhi, koff, lklow, lkhi, klow, khi, lilow, lihi, ilow, ihi, .TRUE., .FALSE.)

    !### Step 3) now compute the cell-centered reference emf for use in the half step CT update ###
    CALL self%compute_reference_emf3d(pool%dt, pool%dx, workp%grid3d, workp%bface3d, pool%workPflux3d, pool%EMF3d, ilow, ihi, jlow, jhi, klow, khi, .FALSE.)

    !### adjust the indices by one in each direction and then do a CT update so the half updates can use this for the cell centered fields ###
    ilow  = ilow + 1
    ihi   = ihi - 1
    jlow  = jlow + 1
    jhi   = jhi - 1
    klow  = klow + 1
    khi   = khi - 1
    lilow = lilow + 1
    lihi  = lihi - 1
    ljlow = ljlow + 1
    ljhi  = ljhi - 1
    lklow = lklow + 1
    lkhi  = lkhi - 1
    koff  = klow-lklow

    !### Step 4) perform the CT update over 1/2 dt ###
    CALL self%compute_corner_emf3d(workp%grid3d, pool%EMF3d, workp%emf3d, ilow, ihi, jlow, jhi, klow, khi)
    CALL self%compute_corner_emf3d(workp%grid3d, pool%protEMF3d, pool%protCTEMF3d, ilow, ihi, jlow, jhi, klow, khi)
    CALL self%applyct3d(pool%dt, pool%dx, workp%grid3d, workp%bface3d, workp%emf3d, ilow, ihi, jlow, jhi, klow, khi, .TRUE.)

    !### Step 5) now compute the cell-centered reference emf for use in the final CT update ###
    CALL self%compute_reference_emf3d(pool%dt, pool%dx, workp%grid3d, workp%bface3d, pool%workPflux3d, pool%EMF3d, ilow, ihi, jlow, jhi, klow, khi, .TRUE.)

    !### the design here requires a minimum of 4 boundary zones since we will NOT communicate the half step values ###
    !### we now adjust the lower and upper bounds to account for this fact ###
    ilow  = ilow + 2
    ihi   = ihi - 2
    jlow  = jlow + 2
    jhi   = jhi - 2
    klow  = klow + 2
    khi   = khi - 2
    lilow = lilow + 2
    lihi  = lihi - 2
    ljlow = ljlow + 2
    ljhi  = ljhi - 2
    lklow = lklow + 2
    lkhi  = lkhi - 2
    koff  = klow-lklow

    !### Step 4,6) evolve state to t+1/2 and compute final X and Y fluxes for final update (including magnetic fluxes) ###
    CALL self%compute_Zplane_XYfluxes3d(workp, pool, lklow, lkhi, klow, khi, koff, ljlow, ljhi, jlow, jhi, lilow, lihi, ilow, ihi, .FALSE., .FALSE.)

    !### Step 4,6) evolve state to t+1/2 and compute final Z fluxes for final update (including magnetic fluxes) ###
    CALL self%compute_Zfluxes3d(workp, pool, ljlow, ljhi, jlow, jhi, koff, lklow, lkhi, klow, khi, lilow, lihi, ilow, ihi, .FALSE., .FALSE.)
    
    !### restore the original grid to undo any preconditioning ###
    DO v = 1, workp%nvars

        DO k = lklow, lkhi

            DO j = ljlow, ljhi

                workp%grid3d(lilow:lihi,j,k,v) = pool%state_save3d(lilow:lihi,j,k,v)

            END DO

        END DO

    END DO

    !### Step 7) compute the final cell corner EMF after update for use in a later CT update ###
    CALL self%compute_corner_emf3d(workp%grid3d, pool%EMF3d, workp%emf3d, ilow, ihi, jlow, jhi, klow, khi)
    CALL self%compute_corner_emf3d(workp%grid3d, pool%protEMF3d, pool%protCTEMF3d, ilow, ihi, jlow, jhi, klow, khi)

    !### Step 8) apply final update ###
    CALL self%update_states3d(pool%dt, pool%dx, workp%grid3d, pool%Pflux3d, workp%emf3d, pool%prot_q3d, pool%protPflux3d, &
         pool%protCTEMF3d, pool%prot_flags3d, ilow, ihi, jlow, jhi, klow, khi)

    !### restore the original face centered field for final update later ###
    DO v = 1, 3

        DO k = lklow-3, lkhi+3

            DO j = ljlow-3, ljhi+3

                workp%bface3d(lilow-3:lihi+3,j,k,v) = pool%bface_save3d(lilow-3:lihi+3,j,k,v)

            END DO

        END DO

    END DO

    !### we cannot compute the max wave signal until after the CT update, so leave ours as 0 for now ###
    pool%max_wave = 0.0_WRD

END SUBROUTINE mhd_solve3d

!### accepting a patch object, ct_solve will move the patch (magnetic fields only) in time from n to n+1 with time step dt from the patch ###
SUBROUTINE ct_solve(self, workp, pool, maxsignal, update_cmplt)

    CLASS(MHDTVD) :: self
    TYPE(Patch) :: workp
    TYPE(MHDPool) :: pool
    REAL(WRD), INTENT(INOUT) :: maxsignal
    LOGICAL(WIS), INTENT(OUT) :: update_cmplt

    !### we only need a single pass to complete this update ###
    update_cmplt = .TRUE.

    !### based on the number of dimensions we'll forward this to the appropriate solver ###
    IF (workp%is1d) THEN
	
        !### no CT in 1d ###
        RETURN

    ELSE IF (workp%is2d) THEN

       CALL self%ct_solve2d(workp, pool)

    ELSE IF (workp%is3d) THEN
	
       CALL self%ct_solve3d(workp, pool)

    END IF

    !### possibly update the maximum signal value with our computed maximum wave speed ###
    maxsignal = MAX(maxsignal, pool%max_wave)

END SUBROUTINE ct_solve

!### the 2d CT update ###
SUBROUTINE ct_solve2d(self, workp, pool)

    CLASS(MHDTVD) :: self
    TYPE(Patch) :: workp
    TYPE(MHDPool) :: pool
    INTEGER(WIS) :: lilow, lihi, ljlow, ljhi, ilow, ihi, jlow, jhi

    pool%dt = workp%dt
    pool%dx = workp%dx
    ilow    = 1 + workp%nb - 5
    ihi     = workp%nx + workp%nb + 5
    jlow    = 1 + workp%nb - 5
    jhi     = workp%ny + workp%nb + 5
    lilow   = 1 - 5
    lihi    = workp%nx + 5
    ljlow   = 1 - 5
    ljhi    = workp%ny + 5

    !### we cannot accept unphysical values from the user's boundary or init routines ###
    CALL self%apply_protections2d(workp%grid2d, ilow, ihi, jlow, jhi)

    !### perform the CT update ###
    CALL self%applyct2d(pool%dt, pool%dx, workp%grid2d, workp%bface2d, workp%emf2d, ilow, ihi, jlow, jhi, .FALSE.)

    !### determine maximum wave speed after the CT update ###
    CALL self%get_maxwave2d(workp, pool, lilow, lihi, ljlow, ljhi, ilow, ihi, jlow, jhi)

END SUBROUTINE ct_solve2d

SUBROUTINE ct_solve3d(self, workp, pool)

    CLASS(MHDTVD) :: self
    TYPE(Patch) :: workp
    TYPE(MHDPool) :: pool
    INTEGER(WIS) :: koff, lilow, lihi, ljlow, ljhi, lklow, lkhi, ilow, ihi, jlow, jhi, klow, khi

    pool%dt = workp%dt
    pool%dx = workp%dx
    ilow    = 1 + workp%nb - 5
    ihi     = workp%nx + workp%nb + 5
    jlow    = 1 + workp%nb - 5
    jhi     = workp%ny + workp%nb + 5
    klow    = 1 + workp%nb - 5
    khi     = workp%nz + workp%nb + 5
    lilow   = 1 - 5
    lihi    = workp%nx + 5
    ljlow   = 1 - 5
    ljhi    = workp%ny + 5
    lklow   = 1 - 5
    lkhi    = workp%nz + 5
    koff    = klow-lklow

    !### we cannot accept unphysical values from the user's boundary or init routines ###
    CALL self%apply_protections3d(workp%grid3d, ilow, ihi, jlow, jhi, klow, khi)

    !### perform the CT update ###
    CALL self%applyct3d(pool%dt, pool%dx, workp%grid3d, workp%bface3d, workp%emf3d, ilow, ihi, jlow, jhi, klow, khi, .FALSE.)

    !### determine maximum wave speed after the CT update ###
    CALL self%get_maxwave3d(workp, pool, koff, lilow, lihi, ljlow, ljhi, lklow, lkhi, ilow, ihi, jlow, jhi, klow, khi)

END SUBROUTINE ct_solve3d

!##############
!# INTERNAL ROUTINES
!##############

!##############
!# 1-d internals (goal is that each is vectorizable)
!# Every routine below ASSUMES that input and output arrays are:
!# - contiguous (not sliced)
!# - already rotated such that the direction of integr/lus/scratch/pjm/wmb/ation is along X
!# Multi-dimensional integrations must copy and rotate vectors into a buffer and extract back out.
!##############

!### construct the pressure from the state variables assuming a simple gamma law EOS ###
SUBROUTINE compute_pressure1d(self, q, pressure, ilow, ihi, jlow, jhi)

    CLASS(MHDTVD) :: self
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, jlow, jhi
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: q(:,:,:)
    REAL(WRD), CONTIGUOUS, INTENT(OUT) :: pressure(:,:)
    REAL(WRD) :: gamma
    INTEGER(WIS) :: i, j

    gamma = self%gamma
    DO j = jlow, jhi

        DO i = ilow, ihi
	
            pressure(i,j) = (gamma - 1.0_WRD) * (q(i,j,8) - 0.5_WRD * (q(i,j,5)**2 + q(i,j,6)**2 + q(i,j,7)**2) - (0.5_WRD / q(i,j,1)) * (q(i,j,2)**2 + q(i,j,3)**2 + q(i,j,4)**2))

        END DO
	
    END DO

END SUBROUTINE compute_pressure1d

!### compute the state variable values at zone interfaces according to the averaging scheme in Eq. 4.6 of CG97 ###
!### this routine certainly could be vectorized more.  i'll do it if it needs it ###
SUBROUTINE compute_zone_averages1d(self, qavg, q, pressure, ilow, ihi, jlow, jhi)

    CLASS(MHDTVD) :: self
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, jlow, jhi
    REAL(WRD), CONTIGUOUS, INTENT(OUT) :: qavg(:,:,:)
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: q(:,:,:), pressure(:,:)
    REAL(WRD) :: rhol, rhor, rhorl, rhorr, irholrprhorr, Hl, Hr, idenl, idenr
    INTEGER(WIS) :: i, j
    
    !### we'll treat the loop simply doing some duplicate work in the hopes of vectorization ###
    !### the interface is defined on the right-hand side of the zone, so the loop goes from the bottom of the array to the top minus 1 ###
    DO j = jlow, jhi

        DO i = ilow, ihi-1
            
            rhol  = q(i,j,1)
            rhor  = q(i+1,j,1)
            idenl = 1.0_WRD / rhol
            idenr = 1.0_WRD / rhor
            rhorl = SQRT(rhol)
            rhorr = SQRT(rhor)
            Hl    = (q(i,j,8) + pressure(i,j) + 0.5d0 * (q(i,j,5)**2 + q(i,j,6)**2 + q(i,j,7)**2)) * idenl
            Hr    = (q(i+1,j,8) + pressure(i+1,j) + 0.5d0 * (q(i+1,j,5)**2 + q(i+1,j,6)**2 + q(i+1,j,7)**2)) * idenr
            
            !### derive the common denomonator ###
            irholrprhorr = 1.0_WRD / (rhorl + rhorr)
            
            !### first the the "bar on top" averages (note that the momentum components are now velocity here, and "energy" has units of entropy) ###
            qavg(i,j,2) = ((rhorl * q(i,j,2) * idenl) + (rhorr * q(i+1,j,2) * idenr)) * irholrprhorr
            qavg(i,j,3) = ((rhorl * q(i,j,3) * idenl) + (rhorr * q(i+1,j,3) * idenr)) * irholrprhorr
            qavg(i,j,4) = ((rhorl * q(i,j,4) * idenl) + (rhorr * q(i+1,j,4) * idenr)) * irholrprhorr
            qavg(i,j,8) = ((rhorl * Hl) + (rhorr * Hr)) * irholrprhorr
            
            !### the "bar on bottom" averages ###
            qavg(i,j,5) = ((rhorr * q(i,j,5)) + (rhorl * q(i+1,j,5))) * irholrprhorr
            qavg(i,j,6) = ((rhorr * q(i,j,6)) + (rhorl * q(i+1,j,6))) * irholrprhorr
            qavg(i,j,7) = ((rhorr * q(i,j,7)) + (rhorl * q(i+1,j,7))) * irholrprhorr
            qavg(i,j,1) = ((rhorr * rhol) + (rhorl * rhor)) * irholrprhorr
            
            !### get X from Eqn. 4.12 of CG97 ###
            qavg(i,j,9) = 0.5_WRD * ((q(i+1,j,6) - q(i,j,6))**2 + (q(i+1,j,7) - q(i,j,7))**2) * irholrprhorr**2

            ! NOTE, the following was in the old code and then used to back-compute gamma... i'm not prepared to deal with EOS yet but there is another index available in state_avg ###
            !### compute an average gas pressure (which is needed for the EOS) ###
            !q%pressure = 0.5d0 * ((self%mhdobj%pressure(i) + 0.5d0 * SUM(self%mhdobj%q(5:7,i)**2)) + &
            !     (self%mhdobj%pressure(i+1) + 0.5d0 * SUM(self%mhdobj%q(5:7,i+1)**2))) - 0.5d0 * (q%bxB**2 + q%byB**2 + q%bzB**2)
            
        END DO
        
    END DO
    
END SUBROUTINE compute_zone_averages1d

!### compute the wave speeds and eigenvalues.  special weighting is from CG97 ###
!### everything in this subroutine really should vectorize (with a peel at the beginning or end).  loads and stores will probably all be gather+scatters, however.  the strides are regular. ###
SUBROUTINE compute_speeds_eigenvals1d(self, eigenvalues, waves, qavg, ilow, ihi, jlow, jhi)

    CLASS(MHDTVD) :: self
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, jlow, jhi
    REAL(WRD), CONTIGUOUS, INTENT(OUT) :: eigenvalues(:,:,:), waves(:,:,:)
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: qavg(:,:,:)
    REAL(WRD) :: irden, brx, bry, brz, bmag2, bpmag2, a2, awave, gamma, zero
    INTEGER(WIS) :: i, j

    !### these are pulled out to prevent aliasing worries ###
    gamma = self%gamma
    zero  = self%zero_value

    !### NOTE* wave speeds are stored in order as 1 => sound, 2 => slow mode, 3 => alfven, 4 => fast mode ###
    DO j = jlow, jhi

        DO i = ilow, ihi-1
       
            !### compute the wave speeds first ###
            
            !### get the alfven velocity components ###
            irden   = 1.0_WRD / SQRT(qavg(i,j,1))
            brx     = qavg(i,j,5) * irden
            bry     = qavg(i,j,6) * irden
            brz     = qavg(i,j,7) * irden
            bmag2   = brx**2 + bry**2 + brz**2
            bpmag2  = (bry**2 + brz**2)
            
            !### determine the sound speed from Eqn. 4.17 in CG97 (it cannot be zero) ###
            a2 = (2.0_WRD - gamma)*qavg(i,j,9) + (gamma - 1.0_WRD)*(qavg(i,j,8) - 0.5d0 * (qavg(i,j,2)**2 + qavg(i,j,3)**2 + qavg(i,j,4)**2) - bmag2)
            IF (zero .GT. a2) THEN
                waves(i,j,1) = SQRT(zero)
            ELSE
                waves(i,j,1) = SQRT(a2)
            END IF
            
            !### store the alfven speed ###
            waves(i,j,3) = ABS(brx)
            
            !### solve the fast mode wave speed (modified form of Eq. 2.18 from RJ95) ###
            awave        = SQRT((a2 - bmag2)**2 + (4.0_WRD * a2 * bpmag2))
            waves(i,j,4) = SQRT(0.5_WRD * (a2 + bmag2 + awave))
            
            !### solve the slow mode wave speed (modified form of Eq. 2.19 from RJ95) ###
            waves(i,j,2) = SQRT(MAX(0.0_WRD, 0.5_WRD * (a2 + bmag2 - awave)))
            
            !### store the eigenvalues in Eq. 2.10 - 2.16 (qavg offset 1 for density) ###
            eigenvalues(i,j,1) = qavg(i,j,2) - waves(i,j,4)
            eigenvalues(i,j,2) = qavg(i,j,2) - waves(i,j,3) 
            eigenvalues(i,j,3) = qavg(i,j,2) - waves(i,j,2)
            eigenvalues(i,j,4) = qavg(i,j,2)
            eigenvalues(i,j,5) = qavg(i,j,2) + waves(i,j,2)
            eigenvalues(i,j,6) = qavg(i,j,2) + waves(i,j,3)
            eigenvalues(i,j,7) = qavg(i,j,2) + waves(i,j,4)

        END DO

    END DO

END SUBROUTINE compute_speeds_eigenvals1d

!### compute the right handed eigenvectors and characteristics again following CG97 ###
SUBROUTINE compute_eigenvecs1d(self, eigenvectors, characteristics, eigenvalues, waves, q, qavg, pressure, ilow, ihi, jlow, jhi)

    CLASS(MHDTVD) :: self
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, jlow, jhi
    REAL(WRD), CONTIGUOUS, INTENT(OUT) :: eigenvectors(:,:,:,:), characteristics(:,:,:)
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: eigenvalues(:,:,:), waves(:,:,:), q(:,:,:), qavg(:,:,:), pressure(:,:)
    REAL(WRD) :: alpha(ilow:ihi-1,jlow:jhi,2), beta(ilow:ihi-1,jlow:jhi,2)
    REAL(WRD) :: iden, ia2, idena2, sqrtden, signbx, vmag2, bmag2, bTmag, const_fast1, const_fast2, const_fast3, const_fast4, &
                 const_slow1, const_slow2, const_slow3, const_slow4, alphaf, alphas, isqrtw, isqrtb, betay, betaz, igammam1, &
                 delrho, delvx, delvy, delvz, delby, delbz, delp, idenl, idenr, betaB, betaV, zero
    INTEGER(WIS) :: i, j

    !### this is loop overiant until later when we re-implement variable EOS ###
    igammam1 = 1.0_WRD / (self%gamma - 1.0_WRD)
    zero     = self%zero_value

    !### we'll do alphas, alphaf, betay and betaz (Eq. 4.21 in CG97) in a separate loop so that the next loop can vectorize.  there is simply too much ###
    !### flow control ###
    DO j = jlow, jhi

        DO i = ilow, ihi-1

            bTmag = qavg(i,j,6)**2 + qavg(i,j,7)**2
            
            !### first do alphas ###
            IF ((waves(i,j,4)**2 - waves(i,j,2)**2) .LT. zero) THEN
                alpha(i,j,1) = 1.0_WRD
                alpha(i,j,2) = 0.0_WRD
            ELSE IF ((waves(i,j,1)**2 - waves(i,j,2)**2) .LT. zero) THEN
                alpha(i,j,1) = 0.0_WRD
                alpha(i,j,2) = 1.0_WRD
            ELSE IF ((waves(i,j,4)**2 - waves(i,j,1)**2) .LT. zero) THEN
                alpha(i,j,1) = 1.0_WRD
                alpha(i,j,2) = 0.0_WRD
            ELSE
                isqrtw       = 1.0_WRD / SQRT(waves(i,j,4)**2 - waves(i,j,2)**2)
                alpha(i,j,1) = SQRT(MAX(0.0_WRD,waves(i,j,1)**2 - waves(i,j,2)**2)) * isqrtw
                alpha(i,j,2) = SQRT(MAX(0.0_WRD,waves(i,j,4)**2 - waves(i,j,1)**2)) * isqrtw
            END IF
            
            !### now do the betas ###
            
            !### according to Eq. 2.35, 2.36 and 2.37 in RJ95 beta takes on a specific value for by = bz = 0 ###
            IF (bTmag .GE. zero) THEN
                
                isqrtb      = 1.d0 / SQRT(bTmag)
                beta(i,j,1) = qavg(i,j,6) * isqrtb
                beta(i,j,2) = qavg(i,j,7) * isqrtb
                
            ELSE
                
                beta(i,j,1) = 1.0_WRD / SQRT(2.0_WRD)
                beta(i,j,2) = beta(i,j,1)
                
            END IF
            
        END DO

    END DO

    !### this loop may very well be too big.  we'll see how each compiler does vectorizing this (GNU cannot do it, Cray can, Intel unknown right now) ###
    DO j = jlow, jhi

        DO i = ilow, ihi-1

            !### many of the eigenvectors below need the SQRT(density) and other related quantities ###
            iden    = 1.0_WRD / qavg(i,j,1)
            idena2  = iden / waves(i,j,1)**2
            sqrtden = SQRT(qavg(i,j,1))
            signbx  = SIGN(1.0_WRD, q(i,j,5))
            bmag2   = qavg(i,j,5)**2 + qavg(i,j,6)**2 + qavg(i,j,7)**2
            bTmag   = SQRT(qavg(i,j,6)**2 + qavg(i,j,7)**2)
            
            !### get the alphas and betas from aboove computes the values for alpha and beta in Eq. 4.21 in CG97 ###
            alphaf = alpha(i,j,1)
            alphas = alpha(i,j,2)
            betay  = beta(i,j,1)
            betaz  = beta(i,j,2)
            
            !### now compute the fast mode vectors (minus, Eq. 4.19) ###
            const_fast1 = qavg(i,j,1) * alphaf
            const_fast2 = alphas * waves(i,j,2) * signbx
            const_fast3 = sqrtden * alphas * waves(i,j,1)
            const_fast4 = (qavg(i,j,3) * betay) + (qavg(i,j,4) * betaz)
            
            eigenvectors(i,j,1,1) = idena2 * const_fast1 
            eigenvectors(i,j,2,1) = idena2 * const_fast1 * eigenvalues(i,j,1)
            eigenvectors(i,j,3,1) = idena2 * qavg(i,j,1) * ((alphaf * qavg(i,j,3)) + (const_fast2 * betay))
            eigenvectors(i,j,4,1) = idena2 * qavg(i,j,1) * ((alphaf * qavg(i,j,4)) + (const_fast2 * betaz))
            eigenvectors(i,j,5,1) = 0.0_WRD
            eigenvectors(i,j,6,1) = idena2 * const_fast3 * betay
            eigenvectors(i,j,7,1) = idena2 * const_fast3 * betaz
            eigenvectors(i,j,8,1) = idena2 * (const_fast1 * (qavg(i,j,8) - (bmag2 * iden) - qavg(i,j,2) * waves(i,j,4)) + &
                 qavg(i,j,1) * const_fast2 * const_fast4 + const_fast3 * bTmag)
            
            !### now compute the fast mode vectors (plus) ###
            eigenvectors(i,j,1,7) = eigenvectors(i,j,1,1)
            eigenvectors(i,j,2,7) = idena2 * const_fast1 * eigenvalues(i,j,7)
            eigenvectors(i,j,3,7) = idena2 * qavg(i,j,1) * ((alphaf * qavg(i,j,3)) - (const_fast2 * betay))
            eigenvectors(i,j,4,7) = idena2 * qavg(i,j,1) * ((alphaf * qavg(i,j,4)) - (const_fast2 * betaz))
            eigenvectors(i,j,5,7) = 0.0_WRD
            eigenvectors(i,j,6,7) = idena2 * const_fast3 * betay
            eigenvectors(i,j,7,7) = idena2 * const_fast3 * betaz
            eigenvectors(i,j,8,7) = idena2 * (const_fast1 * (qavg(i,j,8) - (bmag2 * iden) + qavg(i,j,2) * waves(i,j,4)) - &
                 qavg(i,j,1) * const_fast2 * const_fast4 + const_fast3 * bTmag)

        END DO
            
        DO i = ilow, ihi-1

            !### many of the eigenvectors below need the SQRT(density) and other related quantities ###
            sqrtden = SQRT(qavg(i,j,1))
            signbx  = SIGN(1.0_WRD, q(i,j,5))
            
            !### get the alphas and betas from aboove computes the values for alpha and beta in Eq. 4.21 in CG97 ###
            betay  = beta(i,j,1)
            betaz  = beta(i,j,2)

            !### now compute the alfven mode (minus) ###
            eigenvectors(i,j,1,2) = 0.0_WRD
            eigenvectors(i,j,2,2) = 0.0_WRD
            eigenvectors(i,j,3,2) = -qavg(i,j,1) * betaz
            eigenvectors(i,j,4,2) = qavg(i,j,1) * betay
            eigenvectors(i,j,5,2) = 0.0_WRD
            eigenvectors(i,j,6,2) = -signbx * sqrtden * betaz
            eigenvectors(i,j,7,2) = signbx * sqrtden * betay
            eigenvectors(i,j,8,2) = -qavg(i,j,1) * (qavg(i,j,3) * betaz - qavg(i,j,4) * betay)
            
            !### now compute the alfven mode (plus) ###
            eigenvectors(i,j,1,6) = 0.0_WRD
            eigenvectors(i,j,2,6) = 0.0_WRD
            eigenvectors(i,j,3,6) = qavg(i,j,1) * betaz
            eigenvectors(i,j,4,6) = -qavg(i,j,1) * betay
            eigenvectors(i,j,5,6) = 0.0_WRD
            eigenvectors(i,j,6,6) = -signbx * sqrtden * betaz
            eigenvectors(i,j,7,6) = signbx * sqrtden * betay
            eigenvectors(i,j,8,6) = qavg(i,j,1) * (qavg(i,j,3) * betaz - qavg(i,j,4) * betay)

        END DO
            
        DO i = ilow, ihi-1

            !### many of the eigenvectors below need the SQRT(density) and other related quantities ###
            iden    = 1.0_WRD / qavg(i,j,1)
            idena2  = iden / waves(i,j,1)**2
            sqrtden = SQRT(qavg(i,j,1))
            signbx  = SIGN(1.0_WRD, q(i,j,5))
            bmag2   = qavg(i,j,5)**2 + qavg(i,j,6)**2 + qavg(i,j,7)**2
            bTmag   = SQRT(qavg(i,j,6)**2 + qavg(i,j,7)**2)
            
            !### get the alphas and betas from aboove computes the values for alpha and beta in Eq. 4.21 in CG97 ###
            alphaf = alpha(i,j,1)
            alphas = alpha(i,j,2)
            betay  = beta(i,j,1)
            betaz  = beta(i,j,2)
            
            !### now compute the slow mode (minus) ###
            const_slow1 = qavg(i,j,1) * alphas
            const_slow2 = alphaf * waves(i,j,4) * signbx
            const_slow3 = sqrtden * alphaf * waves(i,j,1)
            const_slow4 = (qavg(i,j,3) * betay) + (qavg(i,j,4) * betaz)
            
            eigenvectors(i,j,1,3) = idena2 * const_slow1
            eigenvectors(i,j,2,3) = idena2 * const_slow1 * eigenvalues(i,j,3)
            eigenvectors(i,j,3,3) = idena2 * qavg(i,j,1) * ((alphas * qavg(i,j,3)) - (const_slow2 * betay))
            eigenvectors(i,j,4,3) = idena2 * qavg(i,j,1) * ((alphas * qavg(i,j,4)) - (const_slow2 * betaz))
            eigenvectors(i,j,5,3) = 0.0_WRD
            eigenvectors(i,j,6,3) = idena2 * (-const_slow3 * betay)
            eigenvectors(i,j,7,3) = idena2 * (-const_slow3 * betaz)
            eigenvectors(i,j,8,3) = idena2 * (const_slow1 * (qavg(i,j,8) - (bmag2 * iden) - qavg(i,j,2) * waves(i,j,2)) - &
                 qavg(i,j,1) * const_slow2 * const_slow4 - const_slow3 * bTmag)
            
            !### now compute the slow mode (plus) ###
            eigenvectors(i,j,1,5) = idena2 * const_slow1
            eigenvectors(i,j,2,5) = idena2 * const_slow1 * eigenvalues(i,j,5)
            eigenvectors(i,j,3,5) = idena2 * qavg(i,j,1) * ((alphas * qavg(i,j,3)) + (const_slow2 * betay))
            eigenvectors(i,j,4,5) = idena2 * qavg(i,j,1) * ((alphas * qavg(i,j,4)) + (const_slow2 * betaz))
            eigenvectors(i,j,5,5) = 0.0_WRD
            eigenvectors(i,j,6,5) = idena2 * (-const_slow3 * betay)
            eigenvectors(i,j,7,5) = idena2 * (-const_slow3 * betaz)
            eigenvectors(i,j,8,5) = idena2 * (const_slow1 * (qavg(i,j,8) - (bmag2 * iden) + qavg(i,j,2) * waves(i,j,2)) + &
                 qavg(i,j,1) * const_slow2 * const_slow4 - const_slow3 * bTmag)

        END DO

        DO i = ilow, ihi-1

            !### many of the eigenvectors below need the SQRT(density) and other related quantities ###
            ia2   = 1.0_WRD / waves(i,j,1)**2
            vmag2 = qavg(i,j,2)**2 + qavg(i,j,3)**2 + qavg(i,j,4)**2
            
            !### now compute the entropy mode ###
            eigenvectors(i,j,1,4) = ia2 * 1.0_WRD
            eigenvectors(i,j,2,4) = ia2 * qavg(i,j,2)
            eigenvectors(i,j,3,4) = ia2 * qavg(i,j,3)
            eigenvectors(i,j,4,4) = ia2 * qavg(i,j,4)
            eigenvectors(i,j,5,4) = 0.0_WRD
            eigenvectors(i,j,6,4) = 0.0_WRD
            eigenvectors(i,j,7,4) = 0.0_WRD
            eigenvectors(i,j,8,4) = ia2 * (0.5_WRD * vmag2 + (self%gamma - 2.0_WRD) * qavg(i,j,9) * igammam1)

        END DO

        !### loop back through again and compute the characteristics (done in separate loop since a few unamed compilers -GNU- cannot vectorize the full loop) ###
        DO i = ilow, ihi-1

            sqrtden = SQRT(qavg(i,j,1))
            iden    = 1.0_WRD / qavg(i,j,1)
            signbx  = SIGN(1.0_WRD, q(i,j,5))
            
            !### get the alphas and betas from aboove computes the values for alpha and beta in Eq. 4.21 in CG97 ###
            alphaf = alpha(i,j,1)
            alphas = alpha(i,j,2)
            betay  = beta(i,j,1)
            betaz  = beta(i,j,2)
            
            !### now compute the fast mode vectors (minus, Eq. 4.19) ###
            const_fast1 = qavg(i,j,1) * alphaf
            const_fast2 = alphas * waves(i,j,2) * signbx
            const_fast3 = sqrtden * alphas * waves(i,j,1)
            const_fast4 = (qavg(i,j,3) * betay) + (qavg(i,j,4) * betaz)
            
            !### now compute the slow mode (minus) ###
            const_slow1 = qavg(i,j,1) * alphas
            const_slow2 = alphaf * waves(i,j,4) * signbx
            const_slow3 = sqrtden * alphaf * waves(i,j,1)
            const_slow4 = (qavg(i,j,3) * betay) + (qavg(i,j,4) * betaz)
            
            !### we need a few constants ###
            idenl  = 1.0_WRD / q(i,j,1)
            idenr  = 1.0_WRD / q(i+1,j,1)
            delrho = q(i+1,j,1) - q(i,j,1)
            delvx  = q(i+1,j,2) * idenr - q(i,j,2) * idenl
            delvy  = q(i+1,j,3) * idenr - q(i,j,3) * idenl
            delvz  = q(i+1,j,4) * idenr - q(i,j,4) * idenl
            delby  = q(i+1,j,6) - q(i,j,6)
            delbz  = q(i+1,j,7) - q(i,j,7)
            delp   = pressure(i+1,j) - pressure(i,j)
            
            betaB  = (betay * delby) + (betaz * delbz)
            betaV  = (betay * delvy) + (betaz * delvz)
            
            characteristics(i,j,1) = 0.5_WRD * (alphaf * (qavg(i,j,9) * delrho + delp) + qavg(i,j,1) * const_fast2 * betaV - &
                 const_fast1 * waves(i,j,4) * delvx + const_fast3 * betaB)
            characteristics(i,j,2) = 0.5_WRD * (betay * delvz - betaz * delvy + signbx * SQRT(iden) * &
                 (betay * delbz - betaz * delby))
            characteristics(i,j,3) = 0.5_WRD * (alphas * (qavg(i,j,9) * delrho + delp) - qavg(i,j,1) * const_slow2 * betaV - &
                 const_slow1 * waves(i,j,2) * delvx - const_slow3 * betaB)
            
            characteristics(i,j,4) = (waves(i,j,1)**2 - qavg(i,j,9)) * delrho - delp

            characteristics(i,j,5) = 0.5_WRD * (alphas * (qavg(i,j,9) * delrho + delp) + qavg(i,j,1) * const_slow2 * betaV + &
                 const_slow1 * waves(i,j,2) * delvx - const_slow3 * betaB)
            characteristics(i,j,6) = 0.5_WRD * (-betay * delvz + betaz * delvy + signbx * SQRT(iden) * &
                 (betay * delbz - betaz * delby))
            characteristics(i,j,7) = 0.5_WRD * (alphaf * (qavg(i,j,9) * delrho + delp) - qavg(i,j,1) * const_fast2 * betaV + &
                 const_fast1 * waves(i,j,4) * delvx + const_fast3 * betaB)
            
        END DO

    END DO

END SUBROUTINE compute_eigenvecs1d

!### compute the TVD fluxes following RJ95 ###
SUBROUTINE compute_fluxes1d(self, nstate, nwaves, ndim, dt, dx, fluxes, prot_fluxes, waves, eigenvectors, characteristics, eigenvalues, pressure, q, bflux2d, bflux3d, &
     prot_bflux2d, prot_bflux3d, compbflux2d, compbflux3d, ilow, ihi, jlow, jhi, z, d)

    CLASS(MHDTVD) :: self
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, jlow, jhi, z, d, nstate, nwaves, ndim
    REAL(WRD), INTENT(IN) :: dt, dx
    LOGICAL(WIS), INTENT(IN) :: compbflux2d, compbflux3d
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: eigenvectors(:,:,:,:), characteristics(:,:,:), &
         eigenvalues(:,:,:), pressure(:,:), q(:,:,:), waves(:,:,:)
    REAL(WRD), CONTIGUOUS, INTENT(OUT) :: fluxes(:,:,:,:,:), prot_fluxes(:,:,:,:,:), bflux2d(:,:), bflux3d(:,:,:), prot_bflux2d(:,:), prot_bflux3d(:,:,:)
    REAL(WRD) :: gk(ilow:ihi,jlow:jhi,nwaves), eps(nwaves), F(ilow:ihi,jlow:jhi,nstate)
    REAL(WRD) :: dtdx, dxdt, dtdxakm, dtdxak, gtildam, gtilda, qkm, qk, signg, alpham, &
         alpha, alphasum, theta, sigmam, sigma, signa, gthetabar, gamma, chi, beta
    REAL(WRD) :: idenl, idenr, fpressurel, fpressurer, bp, bm, ibpmbm, bbb, minev, maxev, bfl, bfr
    INTEGER(WIS) :: i, j, k

    !### calculate dtdx and dxdt ###
    dtdx = dt / dx
    dxdt = dx / dt

    !### fluxes index as i+1/2 in RJ95 are referred to as i here throughout ###

    DO k = 1, nwaves

       !### get the dissipation constant for this wave ###
       eps(k) = self%get_eps(ndim, k)

    END DO

    !### initalize the modified flux array ###
    F = 0.0_WRD

    !### loop over waves and get the flux limiter ##
    DO k = 1, nwaves

       DO j = jlow, jhi

           DO i = ilow+1, ihi-1
               
               !### compute the Gk value (flux limiter) from RJ95 with the following ###
               
               !### compute the value of the argument to Qk in Eq. 2.93 ###
               dtdxakm = dtdx * eigenvalues(i-1,j,k)
               dtdxak  = dtdx * eigenvalues(i,j,k)
               
               !### this does not prevent vectorization since it is a simple mask ###
               IF (ABS(dtdxakm) .LT. (2.0_WRD * eps(k))) THEN
                   
                   qkm = ((0.25_WRD * dtdxakm**2) / eps(k)) + eps(k)
                   
               ELSE
                   
                   qkm = ABS(dtdxakm)
                   
               END IF
               IF (ABS(dtdxak) .LT. (2.0_WRD * eps(k))) THEN
                   
                   qk = ((0.25_WRD * dtdxak**2) / eps(k)) + eps(k)
                   
               ELSE
                   
                   qk = ABS(dtdxak)
                   
               END IF
               
               !### g tilda in Eq. 2.93 ###
               gtildam = 0.5_WRD * (qkm - dtdxakm**2) * characteristics(i-1,j,k)
               gtilda  = 0.5_WRD * (qk - dtdxak**2) * characteristics(i,j,k)
               
               !### Sweby's limiter ###
               signg     = SIGN(1.0_WRD, gtilda)
               gk(i,j,k) = signg * MAX(0.0_WRD, MIN(ABS(gtilda), WSWEBY_BETA * signg * gtildam), MIN(WSWEBY_BETA * ABS(gtilda), signg * gtildam))
               
           END DO
           
       END DO

    END DO

    !### loop over waves and get beta from Eqn 2.89 ###
    DO k = 1, nwaves

        DO j = jlow, jhi

            DO i = ilow+1, ihi-2
               
               !### we'll then compute gamma via Eqn. 2.92 and 2.89 from RJ95 ###
               IF (ABS(characteristics(i,j,k)) .GT. self%zero_value) THEN
                   
                   gamma = (gk(i+1,j,k) - gk(i,j,k)) / characteristics(i,j,k)
                   
               ELSE
                   
                   gamma = 0.0_WRD
                   
               END IF
               chi = dtdx * eigenvalues(i,j,k) + gamma
               IF (ABS(chi) .LT. (2.0_WRD * eps(k))) THEN
                   
                   qk = ((0.25_WRD * chi**2) / eps(k)) + eps(k)
                   
               ELSE
                   
                   qk = ABS(chi)
                   
               END IF
               beta = qk * characteristics(i,j,k) - (gk(i,j,k) + gk(i+1,j,k))
               
               !### compute the modified fluxes per Eq. 2.88 ###
               F(i,j,1) = F(i,j,1) + (beta * eigenvectors(i,j,1,k))
               F(i,j,2) = F(i,j,2) + (beta * eigenvectors(i,j,2,k))
               F(i,j,3) = F(i,j,3) + (beta * eigenvectors(i,j,3,k))
               F(i,j,4) = F(i,j,4) + (beta * eigenvectors(i,j,4,k))
               F(i,j,6) = F(i,j,6) + (beta * eigenvectors(i,j,6,k))
               F(i,j,7) = F(i,j,7) + (beta * eigenvectors(i,j,7,k))
               F(i,j,8) = F(i,j,8) + (beta * eigenvectors(i,j,8,k))
               
           END DO
               
       END DO

    END DO

    !### we have pretty much everything we need now to march ahead with the flux calculation ###
    DO j = jlow, jhi

        DO i = ilow+1, ihi-2

            !### a few constants ###
            idenl      = 1.0_WRD / q(i,j,1)
            idenr      = 1.0_WRD / q(i+1,j,1)
            fpressurel = pressure(i,j) + 0.5_WRD * (q(i,j,5)**2 + q(i,j,6)**2 + q(i,j,7)**2)
            fpressurer = pressure(i+1,j) + 0.5_WRD * (q(i+1,j,5)**2 + q(i+1,j,6)**2 + q(i+1,j,7)**2)
            
            !### compute the TVD fluxes (one may ask, why bother setting the flux of bx... i say to keep alignments the same and it is assumed in later routines) ###
            fluxes(i,j,z,1,d) = 0.5_WRD * (q(i,j,2) + q(i+1,j,2)) - 0.5_WRD * dxdt * F(i,j,1)
            fluxes(i,j,z,2,d) = 0.5_WRD * (q(i,j,2)**2 * idenl + q(i+1,j,2)**2 * idenr + fpressurel + fpressurer - q(i,j,5)**2 - q(i+1,j,5)**2) - 0.5_WRD * dxdt * F(i,j,2)
            fluxes(i,j,z,3,d) = 0.5_WRD * (q(i,j,2) * q(i,j,3) * idenl + q(i+1,j,2) * q(i+1,j,3) * idenr - q(i,j,5) * q(i,j,6) - q(i+1,j,5) * q(i+1,j,6)) - 0.5_WRD * dxdt * F(i,j,3)
            fluxes(i,j,z,4,d) = 0.5_WRD * (q(i,j,2) * q(i,j,4) * idenl + q(i+1,j,2) * q(i+1,j,4) * idenr - q(i,j,5) * q(i,j,7) - q(i+1,j,5) * q(i+1,j,7)) - 0.5_WRD * dxdt * F(i,j,4)
            fluxes(i,j,z,5,d) = 0.0_WRD
            fluxes(i,j,z,6,d) = 0.5_WRD * (q(i,j,6) * q(i,j,2) * idenl + q(i+1,j,6) * q(i+1,j,2) * idenr - q(i,j,5) * q(i,j,3) * idenl - q(i+1,j,5) * q(i+1,j,3) * idenr) - 0.5_WRD * dxdt * F(i,j,6)
            fluxes(i,j,z,7,d) = 0.5_WRD * (q(i,j,7) * q(i,j,2) * idenl + q(i+1,j,7) * q(i+1,j,2) * idenr - q(i,j,5) * q(i,j,4) * idenl - q(i+1,j,5) * q(i+1,j,4) * idenr) - 0.5_WRD * dxdt * F(i,j,7)
            fluxes(i,j,z,8,d) = 0.5_WRD * ((q(i,j,8) + fpressurel) * q(i,j,2) * idenl + (q(i+1,j,8) + fpressurer) * q(i+1,j,2) * idenr - q(i,j,5) * (q(i,j,5) * q(i,j,2) * idenl + &
                 q(i,j,6) * q(i,j,3) * idenl + q(i,j,7) * q(i,j,4) * idenl) - q(i+1,j,5) * (q(i+1,j,5) * q(i+1,j,2) * idenr + q(i+1,j,6) * q(i+1,j,3) * idenr + q(i+1,j,7) * q(i+1,j,4) * idenr)) - &
                 0.5_WRD * dxdt * F(i,j,8)

        END DO

        !### at the same time we'll calculate HLLE fluxes with equation 4.4b from Einfeldt et. al. 1991, Jo. Comp. Phys., 92 (E91) ###
        !### these always produce a positive density and pressure under their assumptions regarding wave speeds.  ###
        !### the fluxes are extremely diffusive, and will be used to rescue the TVD solution when the linearized Riemann solution fails ###
        DO i = ilow+1, ihi-2

            !### a few constants ###
            idenl      = 1.0_WRD / q(i,j,1)
            idenr      = 1.0_WRD / q(i+1,j,1)
            fpressurel = pressure(i,j) + 0.5_WRD * (q(i,j,5)**2 + q(i,j,6)**2 + q(i,j,7)**2)
            fpressurer = pressure(i+1,j) + 0.5_WRD * (q(i+1,j,5)**2 + q(i+1,j,6)**2 + q(i+1,j,7)**2)

            !### get the max and min eigenvalues ###
            maxev = MAX(eigenvalues(i,j,1), eigenvalues(i,j,2), eigenvalues(i,j,3), eigenvalues(i,j,4), eigenvalues(i,j,5), eigenvalues(i,j,6), eigenvalues(i,j,7))
            minev = MIN(eigenvalues(i,j,1), eigenvalues(i,j,2), eigenvalues(i,j,3), eigenvalues(i,j,4), eigenvalues(i,j,5), eigenvalues(i,j,6), eigenvalues(i,j,7))

            !### a few constants ###
            bp     = MAX(MAX(maxev, q(i+1,j,2) * idenr + waves(i+1,j,4)), 0.0_WRD)
            bm     = MIN(MIN(minev, q(i,j,2) * idenl - waves(i,j,4)), 0.0_WRD)
            ibpmbm = 1.0_WRD / (bp - bm)
            bbb    = bp * bm * ibpmbm

            !### complete the flux calculation per Eq. 2.88 (Eq. 2.7 shows the values for F_k used in 2.88 here) ###
            prot_fluxes(i,j,z,1,d) = (bp * q(i,j,2) - bm * q(i+1,j,2)) * ibpmbm + bbb * (q(i+1,j,1) - q(i,j,1))
            prot_fluxes(i,j,z,2,d) = (bp * (q(i,j,2)**2 * idenl + fpressurel - q(i,j,5)**2) - bm * (q(i+1,j,2)**2 * idenr + fpressurer - q(i+1,j,5)**2)) * ibpmbm + bbb * (q(i+1,j,2) - q(i,j,2))
            prot_fluxes(i,j,z,3,d) = (bp * (q(i,j,2) * q(i,j,3) * idenl - q(i,j,5) * q(i,j,6)) - bm * (q(i+1,j,2) * q(i+1,j,3) * idenr - q(i+1,j,5) * q(i+1,j,6))) * ibpmbm + bbb * (q(i+1,j,3) - q(i,j,3))
            prot_fluxes(i,j,z,4,d) = (bp * (q(i,j,2) * q(i,j,4) * idenl - q(i,j,5) * q(i,j,7)) - bm * (q(i+1,j,2) * q(i+1,j,4) * idenr - q(i+1,j,5) * q(i+1,j,7))) * ibpmbm + bbb * (q(i+1,j,4) - q(i,j,4))
            prot_fluxes(i,j,z,5,d) = 0.0_WRD
            prot_fluxes(i,j,z,6,d) = (bp * (q(i,j,2) * idenl * q(i,j,6) - q(i,j,3) * idenl * q(i,j,5)) - bm * (q(i+1,j,2) * idenr * q(i+1,j,6) - q(i+1,j,3) * idenr * q(i+1,j,5))) * ibpmbm + bbb * (q(i+1,j,6) - q(i,j,6))
            prot_fluxes(i,j,z,7,d) = (bp * (q(i,j,2) * idenl * q(i,j,7) - q(i,j,4) * idenl * q(i,j,5)) - bm * (q(i+1,j,2) * idenr * q(i+1,j,7) - q(i+1,j,4) * idenr * q(i+1,j,5))) * ibpmbm + bbb * (q(i+1,j,7) - q(i,j,7))
            prot_fluxes(i,j,z,8,d) = (bp * ((q(i,j,8) + fpressurel) * q(i,j,2) * idenl - q(i,j,5) * (q(i,j,5) * q(i,j,2) * idenl + &
                 q(i,j,6) * q(i,j,3) * idenl + q(i,j,7) * q(i,j,4) * idenl)) - bm * ((q(i+1,j,8) + fpressurer) * q(i+1,j,2) * idenr - &
                 q(i+1,j,5) * (q(i+1,j,5) * q(i+1,j,2) * idenr + q(i+1,j,6) * q(i+1,j,3) * idenr + q(i+1,j,7) * q(i+1,j,4) * idenr))) * ibpmbm + bbb * (q(i+1,j,8) - q(i,j,8))

        END DO

    END DO

    !### if we are computing magnetic fluxes do so now ###
    IF (compbflux2d) THEN

        DO j = jlow, jhi

            DO i = ilow+1, ihi-2

                bflux2d(i,j)      = fluxes(i,j,z,6,d)
                prot_bflux2d(i,j) = prot_fluxes(i,j,z,6,d)

            END DO

        END DO

    ELSE IF (compbflux3d) THEN

        DO j = jlow, jhi

            DO i = ilow+1, ihi-2

                bflux3d(i,j,1)      = fluxes(i,j,z,6,d)
                bflux3d(i,j,2)      = fluxes(i,j,z,7,d)
                prot_bflux3d(i,j,1) = prot_fluxes(i,j,z,6,d)
                prot_bflux3d(i,j,2) = prot_fluxes(i,j,z,7,d)
            
            END DO

        END DO

    END IF

END SUBROUTINE compute_fluxes1d

!### get the dissipation constants given the number of dimensions and wave of interest ###
REAL(WRD) FUNCTION get_eps(self, ndim, k)

    CLASS(MHDTVD) :: self
    INTEGER(WIS), INTENT(IN) :: ndim, k
    REAL(WRD) :: epsilon

    !### first we need a value for epsilon, which depends on k in Eq. 2.95 ###
    SELECT CASE (k)

        !### the fast mode waves (multi-dimensional dissapation) ###
        CASE (1,7)

            epsilon = 0.1_WRD
            IF (ndim .EQ. 2) epsilon = 0.1d0
            IF (ndim .EQ. 3) epsilon = 0.2d0
            IF (self%eps(1) .GE. 0.0_WRD) epsilon = self%eps(1)

        !### the alfven mode waves ###
        CASE (2,6)

            epsilon = 0.2_WRD
            IF (ndim .EQ. 2) epsilon = 0.05_WRD
            IF (ndim .EQ. 3) epsilon = 0.1_WRD
            IF (self%eps(2) .GE. 0.0_WRD) epsilon = self%eps(2)

        !### the slow mode waves ###
        CASE (3,5)

            epsilon = 0.1_WRD
            IF (ndim .EQ. 2) epsilon = 0.0_WRD
            IF (ndim .EQ. 3) epsilon = 0.05_WRD
            IF (self%eps(3) .GE. 0.0_WRD) epsilon = self%eps(3)

        !### the entropy mode wave ###
        CASE (4)

            epsilon = 0.0_WRD
            IF (ndim .EQ. 2) epsilon = 0.0_WRD
            IF (ndim .EQ. 3) epsilon = 0.1_WRD
            IF (self%eps(4) .GE. 0.0_WRD) epsilon = self%eps(4)

    END SELECT

    get_eps = epsilon
    RETURN

END FUNCTION get_eps

!### search the given state vector for unphysical density or pressure values ###
SUBROUTINE apply_protections1d(self, q, ilow, ihi)
    
    CLASS(MHDTVD) :: self
    INTEGER(WIS), INTENT(IN) :: ilow, ihi
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: q(:,:)
    REAL(WRD) :: p
    INTEGER(WIS) :: i
    
    !### loop through and check for violations against the minimum density and pressure ###
    DO i = ilow, ihi

        !### as a last safety check, apply density and pressure minima (TODO, warn the user that this is happening because it is not ideal) ###

        IF (q(i,1) .LT. self%min_density) THEN

            q(i,1) = MAX(self%min_density, q(i,1))

        END IF

        p = (self%gamma - 1.0_WRD) * (q(i,8) - 0.5_WRD * (q(i,5)**2 + q(i,6)**2 + q(i,7)**2) - (0.5_WRD / q(i,1)) * (q(i,2)**2 + q(i,3)**2 + q(i,4)**2))
        IF (p .LT. self%min_pressure) THEN

            p      = MAX(self%min_pressure, p)
            q(i,8) = p / (self%gamma - 1.0_WRD) + 0.5_WRD * (q(i,5)**2 + q(i,6)**2 + q(i,7)**2) + (0.5_WRD / q(i,1)) * (q(i,2)**2 + q(i,3)**2 + q(i,4)**2)

        END IF

    END DO

END SUBROUTINE apply_protections1d

!### actually update the state variables given fluxes ###
SUBROUTINE update_states1d(self, dt, dx, q, fluxes, prot_q, prot_fluxes, prot_flags, ilow, ihi)

    CLASS(MHDTVD) :: self
    INTEGER(WIS), INTENT(IN) :: ilow, ihi
    REAL(WRD), INTENT(IN) :: dt, dx
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: fluxes(:,:,:,:,:), prot_fluxes(:,:,:,:,:)
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: q(:,:), prot_q(:,:)
    LOGICAL(WIS), CONTIGUOUS, INTENT(INOUT) :: prot_flags(:)
    REAL(WRD) :: dtdx, p, gamma
    INTEGER(WIS) :: i

    dtdx  = dt / dx
    gamma = self%gamma

    prot_flags = .FALSE.

    !### save the current values in case this update causes unphysical values ###
    DO i = ilow+2, ihi-2

        prot_q(i,1) = q(i,1)
        prot_q(i,2) = q(i,2)
        prot_q(i,3) = q(i,3)
        prot_q(i,4) = q(i,4)
        prot_q(i,6) = q(i,6)
        prot_q(i,7) = q(i,7)
        prot_q(i,8) = q(i,8)
        
    END DO

    !### apply the update ###
    DO i = ilow+2, ihi-2
    
        q(i,1) = q(i,1) - (dtdx * (fluxes(i,1,1,1,1) - fluxes(i-1,1,1,1,1)))
        q(i,2) = q(i,2) - (dtdx * (fluxes(i,1,1,2,1) - fluxes(i-1,1,1,2,1)))
        q(i,3) = q(i,3) - (dtdx * (fluxes(i,1,1,3,1) - fluxes(i-1,1,1,3,1)))
        q(i,4) = q(i,4) - (dtdx * (fluxes(i,1,1,4,1) - fluxes(i-1,1,1,4,1)))
        q(i,6) = q(i,6) - (dtdx * (fluxes(i,1,1,6,1) - fluxes(i-1,1,1,6,1)))
        q(i,7) = q(i,7) - (dtdx * (fluxes(i,1,1,7,1) - fluxes(i-1,1,1,7,1)))
        q(i,8) = q(i,8) - (dtdx * (fluxes(i,1,1,8,1) - fluxes(i-1,1,1,8,1)))
        
    END DO

    !### look for unphysical density and pressure ###
    DO i = ilow+2, ihi-2

        p = (gamma - 1.0_WRD) * (q(i,8) - 0.5_WRD * (q(i,5)**2 + q(i,6)**2 + q(i,7)**2) - (0.5_WRD / MAX(q(i,1), self%prot_min_density)) * (q(i,2)**2 + q(i,3)**2 + q(i,4)**2))

        !### set the flag to replace TVD fluxes with protection fluxes around this zone ###
        IF (q(i,1) .LT. self%prot_min_density .OR. p .LT. self%prot_min_pressure) THEN

            prot_flags(i)       = .TRUE.
            fluxes(i,1,1,:,1)   = prot_fluxes(i,1,1,:,1)
            fluxes(i-1,1,1,:,1) = prot_fluxes(i-1,1,1,:,1)

        END IF

    END DO

    !### apply protection fluxes if needed ###
    DO i = ilow+2, ihi-2

        IF (prot_flags(i-1) .OR. prot_flags(i) .OR. prot_flags(i+1)) THEN

            q(i,1) = prot_q(i,1) - (dtdx * (fluxes(i,1,1,1,1) - fluxes(i-1,1,1,1,1)))
            q(i,2) = prot_q(i,2) - (dtdx * (fluxes(i,1,1,2,1) - fluxes(i-1,1,1,2,1)))
            q(i,3) = prot_q(i,3) - (dtdx * (fluxes(i,1,1,3,1) - fluxes(i-1,1,1,3,1)))
            q(i,4) = prot_q(i,4) - (dtdx * (fluxes(i,1,1,4,1) - fluxes(i-1,1,1,4,1)))
            q(i,6) = prot_q(i,6) - (dtdx * (fluxes(i,1,1,6,1) - fluxes(i-1,1,1,6,1)))
            q(i,7) = prot_q(i,7) - (dtdx * (fluxes(i,1,1,7,1) - fluxes(i-1,1,1,7,1)))
            q(i,8) = prot_q(i,8) - (dtdx * (fluxes(i,1,1,8,1) - fluxes(i-1,1,1,8,1)))

        END IF

    END DO

    !### finally do a last check for unphysical values that came out of the update (should be exceedingly rare after protection fluxes) ###
    CALL self%apply_protections1d(q, ilow, ihi)

END SUBROUTINE update_states1d

!### compute the maximum wave speed from interface states ###
SUBROUTINE get_maxwave1d(self, workp, pool, ilow, ihi)

    CLASS(MHDTVD) :: self
    TYPE(Patch) :: workp
    TYPE(MHDPool) :: pool
    INTEGER(WIS), INTENT(IN) :: ilow, ihi
    INTEGER :: i, v

    !### realizing this is a redundant copy, place the state variables into a 2d compliant work grid for use in the plane-updating routines below ###
    DO v = 1, workp%nvars

        pool%qwork2d(:,1,v) = workp%grid1d(:,v)

    END DO

    !### first we need to compute the gas pressure for the full patch ###
    CALL self%compute_pressure1d(pool%qwork2d, pool%pressure1d, ilow, ihi, 1, 1)

    !### next we need the state variable values at zone interfaces at time n along with the 'X' weighting factor from CG97 ###
    CALL self%compute_zone_averages1d(pool%state_avg1d, pool%qwork2d, pool%pressure1d, ilow, ihi, 1, 1)

    !### compute wave speeds and eigenvalues ###
    CALL self%compute_speeds_eigenvals1d(pool%eigenVal1d, pool%waves1d, pool%state_avg1d, ilow, ihi, 1, 1)

    !### determine the maximum wavespeed + vx (this is used elsewhere to determine the next timestep) ###
    DO i = 1-2, workp%nx+1

       pool%max_wave = MAX(pool%max_wave, MAXVAL(pool%waves1d(i,1,:)) + ABS(workp%grid1d(i,2)) / workp%grid1d(i,1))

    END DO

END SUBROUTINE get_maxwave1d

!### search the given state vector for unphysical density or pressure values ###
SUBROUTINE apply_protections2d(self, q, ilow, ihi, jlow, jhi)
    
    CLASS(MHDTVD) :: self
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, jlow, jhi
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: q(:,:,:)
    REAL(WRD) :: p
    INTEGER(WIS) :: i, j
    
    !### loop through and check for violations against the minimum density and pressure ###
    DO j = jlow, jhi

        DO i = ilow, ihi

            !### as a last safety check, apply density and pressure minima (TODO, warn the user that this is happening because it is not ideal) ###
            q(i,j,1) = MAX(self%min_density, q(i,j,1))

            p = (self%gamma - 1.0_WRD) * (q(i,j,8) - 0.5_WRD * (q(i,j,5)**2 + q(i,j,6)**2 + q(i,j,7)**2) - (0.5_WRD / q(i,j,1)) * (q(i,j,2)**2 + q(i,j,3)**2 + q(i,j,4)**2))
            IF (p .LT. self%min_pressure) THEN

                p        = self%min_pressure
                q(i,j,8) = p / (self%gamma - 1.0_WRD) + 0.5_WRD * (q(i,j,5)**2 + q(i,j,6)**2 + q(i,j,7)**2) + (0.5_WRD / q(i,j,1)) * (q(i,j,2)**2 + q(i,j,3)**2 + q(i,j,4)**2)

            END IF

        END DO

    END DO

END SUBROUTINE apply_protections2d

!### compute the X state and magnetic fluxes for a given strip of Y ###
SUBROUTINE compute_Xfluxes2d(self, workp, pool, ljlow, ljhi, jlow, jhi, lilow, lihi, ilow, ihi, halfdt, speedsonly)

    CLASS(MHDTVD) :: self
    TYPE(Patch) :: workp
    TYPE(MHDPool) :: pool
    INTEGER(WIS), INTENT(IN) :: ljlow, ljhi, jlow,jhi, lilow, lihi, ilow, ihi
    LOGICAL(WIS), INTENT(IN) :: halfdt, speedsonly
    INTEGER(WIS) :: v, j, i

    !### pack the state vector contiguously (no rotation needed) ###
    DO v = 1, workp%nvars

        DO j = ljlow, ljhi

            pool%qwork2d(lilow:lihi,j,v) = workp%grid2d(lilow:lihi,j,v)

        END DO

    END DO

    !### on the second pass we must immediately update the state a half time step (along X only) ###
    IF (.NOT. halfdt .AND. .NOT. speedsonly) THEN

        CALL self%halfXupdate_states2d(pool%dt, pool%dx, pool%qwork2d, pool%workPflux2d, pool%bface_save2d, pool%prot_q2d, pool%workProtPflux2d, &
             pool%prot_flags2d, jlow-2, jhi+2, ilow-2, ihi+2)

    END IF

    !### compute pressure ###
    CALL self%compute_pressure1d(pool%qwork2d, pool%pressure2d, ilow, ihi, jlow, jhi)

    !### compute Roe average states ###
    CALL self%compute_zone_averages1d(pool%state_avg2d, pool%qwork2d, pool%pressure2d, ilow, ihi, jlow, jhi)

    !### compute wave speeds and eigenvalues ###
    CALL self%compute_speeds_eigenvals1d(pool%eigenVal2d, pool%waves2d, pool%state_avg2d, ilow, ihi, jlow, jhi)

    !### if we're only computing wave speeds (such as for computing the time step) we are done ###
    IF (speedsonly) RETURN

    !### construct the left-handed eigenvectors and characteristics ###
    CALL self%compute_eigenvecs1d(pool%eigenVec2d, pool%charac2d, pool%eigenVal2d, pool%waves2d, &
         pool%qwork2d, pool%state_avg2d, pool%pressure2d, ilow, ihi, jlow, jhi)

    !### compute the fluxes ###
    IF (halfdt) THEN

        CALL self%compute_fluxes1d(pool%nstate, pool%nwaves, 2, 0.5_WRD * pool%dt, pool%dx, pool%workPflux2d, pool%workProtPflux2d, pool%waves2d, pool%eigenVec2d, pool%charac2d, pool%eigenVal2d, &
             pool%pressure2d, pool%qwork2d, pool%Bflux2d, pool%Bflux3d, pool%protBflux2d, pool%protBflux3d, .TRUE., .FALSE., ilow, ihi, jlow, jhi, 1, 1)

    ELSE

        !### on the second pass we place fluxes directly into the final flux array ###
        CALL self%compute_fluxes1d(pool%nstate, pool%nwaves, 2, pool%dt, pool%dx, pool%Pflux2d, pool%protPflux2d, pool%waves2d, pool%eigenVec2d, pool%charac2d, pool%eigenVal2d, &
             pool%pressure2d, pool%qwork2d, pool%Bflux2d, pool%Bflux3d, pool%protBflux2d, pool%protBflux3d, .TRUE., .FALSE., ilow, ihi, jlow, jhi, 1, 1)

    END IF

    !### store the magnetic fluxes (take care of the sign flip here so that these are really E_z) ###
    DO j = ljlow, ljhi
        
        pool%EMF2d(lilow:lihi,j,1)     = -pool%Bflux2d(lilow:lihi,j)
        pool%protEMF2d(lilow:lihi,j,1) = -pool%protBflux2d(lilow:lihi,j)

    END DO

END SUBROUTINE compute_Xfluxes2d

!### compute the X state and magnetic fluxes for a given strip of X ###
SUBROUTINE compute_Yfluxes2d(self, workp, pool, lilow, lihi, ilow, ihi, ljlow, ljhi, jlow, jhi, halfdt, speedsonly)

    CLASS(MHDTVD) :: self
    TYPE(Patch) :: workp
    TYPE(MHDPool) :: pool
    INTEGER(WIS), INTENT(IN) :: lilow, lihi, ilow, ihi, ljlow, ljhi, jlow, jhi
    LOGICAL(WIS), INTENT(IN) :: halfdt, speedsonly
    INTEGER(WIS) :: i, j

    !### pack the state vector contiguously (rotation needed) (MAY REQUIRE MODIFICATION IF MORE STATES ARE ADDED) ###
    DO i = lilow, lihi

        DO j = ljlow, ljhi

            pool%qwork2d(j,i,1) = workp%grid2d(i,j,1)
            pool%qwork2d(j,i,2) = workp%grid2d(i,j,3)
            pool%qwork2d(j,i,3) = workp%grid2d(i,j,2)
            pool%qwork2d(j,i,4) = workp%grid2d(i,j,4)
            pool%qwork2d(j,i,5) = workp%grid2d(i,j,6)
            pool%qwork2d(j,i,6) = workp%grid2d(i,j,5)
            pool%qwork2d(j,i,7) = workp%grid2d(i,j,7)
            pool%qwork2d(j,i,8) = workp%grid2d(i,j,8)
            
        END DO

    END DO

    !### on the second pass we must immediately update the state a half time step (along Y only) ###
    IF (.NOT. halfdt .AND. .NOT. speedsonly) THEN

        CALL self%halfYupdate_states2d(pool%dt, pool%dx, pool%qwork2d, pool%workPflux2d, pool%bface_save2d, pool%prot_q2d, pool%workProtPflux2d, &
             pool%prot_flags2d, ilow-2, ihi+2, jlow-2, jhi+2)

    END IF

    !### compute pressure ###
    CALL self%compute_pressure1d(pool%qwork2d, pool%pressure2d, jlow, jhi, ilow, ihi)

    !### compute Roe average states ###
    CALL self%compute_zone_averages1d(pool%state_avg2d, pool%qwork2d, pool%pressure2d, jlow, jhi, ilow, ihi)

    !### compute wave speeds and eigenvalues ###
    CALL self%compute_speeds_eigenvals1d(pool%eigenVal2d, pool%waves2d, pool%state_avg2d, jlow, jhi, ilow, ihi)

    !### if we're only computing wave speeds (such as for computing the time step) we are done ###
    IF (speedsonly) RETURN
    
    !### construct the left-handed eigenvectors and characteristics ###
    CALL self%compute_eigenvecs1d(pool%eigenVec2d, pool%charac2d, pool%eigenVal2d, pool%waves2d, &
         pool%qwork2d, pool%state_avg2d, pool%pressure2d, jlow, jhi, ilow, ihi)
    
    !### compute the fluxes ###
    !### if we're doing halfdt we need to place the final fluxes into the work flux array ###
    IF (halfdt) THEN

        CALL self%compute_fluxes1d(pool%nstate, pool%nwaves, 2, 0.5_WRD * pool%dt, pool%dx, pool%Pflux2d, pool%protPflux2d, pool%waves2d, pool%eigenVec2d, pool%charac2d, pool%eigenVal2d, &
             pool%pressure2d, pool%qwork2d, pool%Bflux2d, pool%Bflux3d, pool%protBflux2d, pool%protBflux3d, .TRUE., .FALSE., jlow, jhi, ilow, ihi, 1, 2)
    
        !### rotate the flux into the work flux arrays ###
        DO j = ljlow, ljhi

            DO i = lilow, lihi
            
                pool%workPflux2d(i,j,1,1,2) = pool%Pflux2d(j,i,1,1,2)
                pool%workPflux2d(i,j,1,3,2) = pool%Pflux2d(j,i,1,2,2)
                pool%workPflux2d(i,j,1,2,2) = pool%Pflux2d(j,i,1,3,2)
                pool%workPflux2d(i,j,1,4,2) = pool%Pflux2d(j,i,1,4,2)
                pool%workPflux2d(i,j,1,6,2) = pool%Pflux2d(j,i,1,5,2)
                pool%workPflux2d(i,j,1,5,2) = pool%Pflux2d(j,i,1,6,2)
                pool%workPflux2d(i,j,1,7,2) = pool%Pflux2d(j,i,1,7,2)
                pool%workPflux2d(i,j,1,8,2) = pool%Pflux2d(j,i,1,8,2)

            END DO

            DO i = lilow, lihi

                pool%workProtPflux2d(i,j,1,1,2) = pool%protPflux2d(j,i,1,1,2)
                pool%workProtPflux2d(i,j,1,3,2) = pool%protPflux2d(j,i,1,2,2)
                pool%workProtPflux2d(i,j,1,2,2) = pool%protPflux2d(j,i,1,3,2)
                pool%workProtPflux2d(i,j,1,4,2) = pool%protPflux2d(j,i,1,4,2)
                pool%workProtPflux2d(i,j,1,6,2) = pool%protPflux2d(j,i,1,5,2)
                pool%workProtPflux2d(i,j,1,5,2) = pool%protPflux2d(j,i,1,6,2)
                pool%workProtPflux2d(i,j,1,7,2) = pool%protPflux2d(j,i,1,7,2)
                pool%workProtPflux2d(i,j,1,8,2) = pool%protPflux2d(j,i,1,8,2)

            END DO
        
        END DO

    ELSE

        CALL self%compute_fluxes1d(pool%nstate, pool%nwaves, 2, pool%dt, pool%dx, pool%workPflux2d, pool%workProtPflux2d, pool%waves2d, pool%eigenVec2d, pool%charac2d, pool%eigenVal2d, &
             pool%pressure2d, pool%qwork2d, pool%Bflux2d, pool%Bflux3d, pool%protBflux2d, pool%protBflux3d, .TRUE., .FALSE., jlow, jhi, ilow, ihi, 1, 2)
    
        !### rotate the flux into the work flux arrays ###
        DO j = ljlow, ljhi
            
            DO i = lilow, lihi
            
                pool%Pflux2d(i,j,1,1,2) = pool%workPflux2d(j,i,1,1,2)
                pool%Pflux2d(i,j,1,3,2) = pool%workPflux2d(j,i,1,2,2)
                pool%Pflux2d(i,j,1,2,2) = pool%workPflux2d(j,i,1,3,2)
                pool%Pflux2d(i,j,1,4,2) = pool%workPflux2d(j,i,1,4,2)
                pool%Pflux2d(i,j,1,6,2) = pool%workPflux2d(j,i,1,5,2)
                pool%Pflux2d(i,j,1,5,2) = pool%workPflux2d(j,i,1,6,2)
                pool%Pflux2d(i,j,1,7,2) = pool%workPflux2d(j,i,1,7,2)
                pool%Pflux2d(i,j,1,8,2) = pool%workPflux2d(j,i,1,8,2)

            END DO

            DO i = lilow, lihi
            
                pool%protPflux2d(i,j,1,1,2) = pool%workProtPflux2d(j,i,1,1,2)
                pool%protPflux2d(i,j,1,3,2) = pool%workProtPflux2d(j,i,1,2,2)
                pool%protPflux2d(i,j,1,2,2) = pool%workProtPflux2d(j,i,1,3,2)
                pool%protPflux2d(i,j,1,4,2) = pool%workProtPflux2d(j,i,1,4,2)
                pool%protPflux2d(i,j,1,6,2) = pool%workProtPflux2d(j,i,1,5,2)
                pool%protPflux2d(i,j,1,5,2) = pool%workProtPflux2d(j,i,1,6,2)
                pool%protPflux2d(i,j,1,7,2) = pool%workProtPflux2d(j,i,1,7,2)
                pool%protPflux2d(i,j,1,8,2) = pool%workProtPflux2d(j,i,1,8,2)

            END DO
        
        END DO

    END IF

    !### store the magnetic fluxes ###
    DO j = ljlow, ljhi

        DO i = lilow, lihi
        
            pool%EMF2d(i,j,2)     = pool%Bflux2d(j,i)
            pool%protEMF2d(i,j,2) = pool%protBflux2d(j,i)

        END DO
        
    END DO
    
END SUBROUTINE compute_Yfluxes2d

!### apply a half time step update to a contiguous single strip using transverse fluxes computed from the initial state and apply the div(B) source term ###
SUBROUTINE halfXupdate_states2d(self, dt, dx, q, fluxes, bface, prot_q, prot_fluxes, prot_flags, jlow, jhi, ilow, ihi)

    CLASS(MHDTVD) :: self
    REAL(WRD), INTENT(IN) :: dt, dx
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: bface(:,:,:), prot_fluxes(:,:,:,:,:)
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: fluxes(:,:,:,:,:), q(:,:,:), prot_q(:,:,:)
    LOGICAL(WIS), CONTIGUOUS, INTENT(INOUT) :: prot_flags(:,:)
    INTEGER(WIS), INTENT(IN) :: jlow, jhi, ilow, ihi
    REAL(WRD) :: dtdx, idx, Smx, Smy, Smz, Sbz, Se, divB, p, gamma
    INTEGER(WIS) :: i, j

    idx   = 1.0_WRD / dx
    dtdx  = 0.5_WRD * dt * idx
    gamma = self%gamma

    prot_flags = .FALSE.

    !### perform the update ###
    DO j = jlow+1, jhi

        DO i = ilow+2, ihi-2

            !### save the current values in case this update causes unphysical values ###
            prot_q(i,j,:) = q(i,j,:)

        END DO

        DO i = ilow+2, ihi-2

            !### derive the divB source terms ###
            divB = bface(i,j,1) - bface(i-1,j,1)
            Smx  = q(i,j,5) * divB
            Smy  = q(i,j,6) * divB
            Smz  = q(i,j,7) * divB
            Sbz  = (q(i,j,4) / q(i,j,1)) * divB
            Se   = (q(i,j,7) * q(i,j,4) / q(i,j,1)) * divB
            
            q(i,j,1) = q(i,j,1) - (dtdx * (fluxes(i,j,1,1,2) - fluxes(i,j-1,1,1,2)))
            q(i,j,2) = q(i,j,2) - (dtdx * (fluxes(i,j,1,2,2) - fluxes(i,j-1,1,2,2))) + dtdx * Smx
            q(i,j,3) = q(i,j,3) - (dtdx * (fluxes(i,j,1,3,2) - fluxes(i,j-1,1,3,2))) + dtdx * Smy
            q(i,j,4) = q(i,j,4) - (dtdx * (fluxes(i,j,1,4,2) - fluxes(i,j-1,1,4,2))) + dtdx * Smz

            q(i,j,5) = q(i,j,5) - (dtdx * (fluxes(i,j,1,5,2) - fluxes(i,j-1,1,5,2)))

            q(i,j,7) = q(i,j,7) - (dtdx * (fluxes(i,j,1,7,2) - fluxes(i,j-1,1,7,2))) + dtdx * Sbz
            q(i,j,8) = q(i,j,8) - (dtdx * (fluxes(i,j,1,8,2) - fluxes(i,j-1,1,8,2))) + dtdx * Se

        END DO

    END DO

    !### look for unphysical density and pressure ###
    DO j = jlow+1, jhi

        DO i = ilow+2, ihi-2

            p = (gamma - 1.0_WRD) * (q(i,j,8) - 0.5_WRD * (q(i,j,5)**2 + q(i,j,6)**2 + q(i,j,7)**2) - (0.5_WRD / MAX(q(i,j,1), self%prot_min_density)) * (q(i,j,2)**2 + q(i,j,3)**2 + q(i,j,4)**2))

            !### set the flag to replace TVD fluxes with protection fluxes around this zone ###
            IF (q(i,j,1) .LT. self%prot_min_density .OR. p .LT. self%prot_min_pressure) THEN

                prot_flags(i,j)     = .TRUE.
                fluxes(i,j,1,:,2)   = prot_fluxes(i,j,1,:,2)
                fluxes(i,j-1,1,:,2) = prot_fluxes(i,j-1,1,:,2)

            END IF

        END DO

    END DO

    !### apply protection fluxes if needed ###
    DO j = jlow+1, jhi

        DO i = ilow+2, ihi-2

            IF (prot_flags(i,j-1) .OR. prot_flags(i,j) .OR. prot_flags(i,j+1)) THEN

                !### derive the divB source terms ###
                divB = bface(i,j,1) - bface(i-1,j,1)
                Smx  = prot_q(i,j,5) * divB
                Smy  = prot_q(i,j,6) * divB
                Smz  = prot_q(i,j,7) * divB
                Sbz  = (prot_q(i,j,4) / prot_q(i,j,1)) * divB
                Se   = (prot_q(i,j,7) * prot_q(i,j,4) / prot_q(i,j,1)) * divB
                
                q(i,j,1) = prot_q(i,j,1) - (dtdx * (fluxes(i,j,1,1,2) - fluxes(i,j-1,1,1,2)))
                q(i,j,2) = prot_q(i,j,2) - (dtdx * (fluxes(i,j,1,2,2) - fluxes(i,j-1,1,2,2))) + dtdx * Smx
                q(i,j,3) = prot_q(i,j,3) - (dtdx * (fluxes(i,j,1,3,2) - fluxes(i,j-1,1,3,2))) + dtdx * Smy
                q(i,j,4) = prot_q(i,j,4) - (dtdx * (fluxes(i,j,1,4,2) - fluxes(i,j-1,1,4,2))) + dtdx * Smz

                q(i,j,5) = prot_q(i,j,5) - (dtdx * (fluxes(i,j,1,5,2) - fluxes(i,j-1,1,5,2)))

                q(i,j,7) = prot_q(i,j,7) - (dtdx * (fluxes(i,j,1,7,2) - fluxes(i,j-1,1,7,2))) + dtdx * Sbz
                q(i,j,8) = prot_q(i,j,8) - (dtdx * (fluxes(i,j,1,8,2) - fluxes(i,j-1,1,8,2))) + dtdx * Se
                
            END IF

        END DO

    END DO

    !### finally do a last check for unphysical values that came out of the update (should be exceedingly rare after protection fluxes) ###
    CALL self%apply_protections2d(q, ilow, ihi, jlow, jhi)

END SUBROUTINE halfXupdate_states2d

!### apply a half time step update to a contiguous single strip using transverse fluxes computed from the initial state and apply the div(B) source term ###
SUBROUTINE halfYupdate_states2d(self, dt, dx, q, fluxes, bface, prot_q, prot_fluxes, prot_flags, ilow, ihi, jlow, jhi)

    CLASS(MHDTVD) :: self
    REAL(WRD), INTENT(IN) :: dt, dx
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: bface(:,:,:), prot_fluxes(:,:,:,:,:)
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: fluxes(:,:,:,:,:), q(:,:,:), prot_q(:,:,:)
    LOGICAL(WIS), CONTIGUOUS, INTENT(INOUT) :: prot_flags(:,:)
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, jlow, jhi
    REAL(WRD) :: dtdx, idx, Smx, Smy, Smz, Sbz, Se, divB, p, gamma
    INTEGER(WIS) :: i, j

    idx   = 1.0_WRD / dx
    dtdx  = 0.5_WRD * dt * idx
    gamma = self%gamma

    prot_flags = .FALSE.

    !### note that the state variables have already been rotated (right-handed) and the fluxes have not, so we need to be careful to match up the correct fluxes ###
    DO i = ilow+1, ihi

        DO j = jlow+2, jhi-2

            !### save the current values in case this update causes unphysical values ###
            prot_q(j,i,:) = q(j,i,:)

        END DO

        DO j = jlow+2, jhi-2
        
            !### derive the divB source terms ###
            divB = bface(i,j,2) - bface(i,j-1,2)
            Smx  = q(j,i,5) * divB
            Smy  = q(j,i,6) * divB
            Smz  = q(j,i,7) * divB
            Sbz  = (q(j,i,4) / q(j,i,1)) * divB
            Se   = (q(j,i,7) * q(j,i,4) / q(j,i,1)) * divB
            
            q(j,i,1) = q(j,i,1) - (dtdx * (fluxes(i,j,1,1,1) - fluxes(i-1,j,1,1,1)))
            q(j,i,2) = q(j,i,2) - (dtdx * (fluxes(i,j,1,3,1) - fluxes(i-1,j,1,3,1))) + dtdx * Smx
            q(j,i,3) = q(j,i,3) - (dtdx * (fluxes(i,j,1,2,1) - fluxes(i-1,j,1,2,1))) + dtdx * Smy
            q(j,i,4) = q(j,i,4) - (dtdx * (fluxes(i,j,1,4,1) - fluxes(i-1,j,1,4,1))) + dtdx * Smz

            q(j,i,5) = q(j,i,5) - (dtdx * (fluxes(i,j,1,6,1) - fluxes(i-1,j,1,6,1)))

            q(j,i,7) = q(j,i,7) - (dtdx * (fluxes(i,j,1,7,1) - fluxes(i-1,j,1,7,1))) + dtdx * Sbz
            q(j,i,8) = q(j,i,8) - (dtdx * (fluxes(i,j,1,8,1) - fluxes(i-1,j,1,8,1))) + dtdx * Se

        END DO

    END DO

    !### look for unphysical density and pressure ###
    DO i = ilow+1, ihi

        DO j = jlow+2, jhi-2

            p = (gamma - 1.0_WRD) * (q(j,i,8) - 0.5_WRD * (q(j,i,5)**2 + q(j,i,6)**2 + q(j,i,7)**2) - (0.5_WRD / MAX(q(j,i,1), self%prot_min_density)) * (q(j,i,2)**2 + q(j,i,3)**2 + q(j,i,4)**2))

            !### set the flag to replace TVD fluxes with protection fluxes around this zone ###
            IF (q(j,i,1) .LT. self%prot_min_density .OR. p .LT. self%prot_min_pressure) THEN

                prot_flags(j,i)     = .TRUE.
                fluxes(i,j,1,:,1)   = prot_fluxes(i,j,1,:,1)
                fluxes(i-1,j,1,:,1) = prot_fluxes(i-1,j,1,:,1)

            END IF

        END DO

    END DO

    !### apply protection fluxes if needed ###
    DO i = ilow+1, ihi

        DO j = jlow+2, jhi-2

            IF (prot_flags(j,i-1) .OR. prot_flags(j,i) .OR. prot_flags(j,i+1)) THEN

                !### derive the divB source terms ###
                divB = bface(i,j,2) - bface(i,j-1,2)
                Smx  = prot_q(j,i,5) * divB
                Smy  = prot_q(j,i,6) * divB
                Smz  = prot_q(j,i,7) * divB
                Sbz  = (prot_q(j,i,4) / prot_q(j,i,1)) * divB
                Se   = (prot_q(j,i,7) * prot_q(j,i,4) / prot_q(j,i,1)) * divB
                
                q(j,i,1) = prot_q(j,i,1) - (dtdx * (fluxes(i,j,1,1,1) - fluxes(i-1,j,1,1,1)))
                q(j,i,2) = prot_q(j,i,2) - (dtdx * (fluxes(i,j,1,3,1) - fluxes(i-1,j,1,3,1))) + dtdx * Smx
                q(j,i,3) = prot_q(j,i,3) - (dtdx * (fluxes(i,j,1,2,1) - fluxes(i-1,j,1,2,1))) + dtdx * Smy
                q(j,i,4) = prot_q(j,i,4) - (dtdx * (fluxes(i,j,1,4,1) - fluxes(i-1,j,1,4,1))) + dtdx * Smz

                q(j,i,5) = prot_q(j,i,5) - (dtdx * (fluxes(i,j,1,6,1) - fluxes(i-1,j,1,6,1)))

                q(j,i,7) = prot_q(j,i,7) - (dtdx * (fluxes(i,j,1,7,1) - fluxes(i-1,j,1,7,1))) + dtdx * Sbz
                q(j,i,8) = prot_q(j,i,8) - (dtdx * (fluxes(i,j,1,8,1) - fluxes(i-1,j,1,8,1))) + dtdx * Se
                
            END IF

        END DO

    END DO

    !### finally do a last check for unphysical values that came out of the update (should be exceedingly rare after protection fluxes) ###
    CALL self%apply_protections2d(q, jlow, jhi, ilow, ihi)

END SUBROUTINE halfYupdate_states2d

!### actually update the state variables given fluxes ###
SUBROUTINE update_states2d(self, dt, dx, q, fluxes, emf, prot_q, prot_fluxes, prot_emf, prot_flags, ilow, ihi, jlow, jhi)

    CLASS(MHDTVD) :: self
    REAL(WRD), INTENT(IN) :: dt, dx
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, jlow, jhi
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: q(:,:,:), prot_q(:,:,:), emf(:,:,:), fluxes(:,:,:,:,:)
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: prot_fluxes(:,:,:,:,:), prot_emf(:,:,:)
    LOGICAL(WIS), CONTIGUOUS, INTENT(INOUT) :: prot_flags(:,:)
    REAL(WRD) :: dtdx, p, gamma
    INTEGER(WIS) :: i, j

    dtdx  = dt / dx
    gamma = self%gamma

    prot_flags = .FALSE.

    !### apply the directionally un-split update ###
    DO j = jlow+2, jhi-2
            
        DO i = ilow+2, ihi-2
                
            !### save the current values in case this update causes unphysical values ###
            prot_q(i,j,:) = q(i,j,:)

        END DO

        DO i = ilow+2, ihi-2

            q(i,j,1) = q(i,j,1) - (dtdx * ((fluxes(i,j,1,1,2) - fluxes(i,j-1,1,1,2)) + (fluxes(i,j,1,1,1) - fluxes(i-1,j,1,1,1))))
            q(i,j,2) = q(i,j,2) - (dtdx * ((fluxes(i,j,1,2,2) - fluxes(i,j-1,1,2,2)) + (fluxes(i,j,1,2,1) - fluxes(i-1,j,1,2,1))))
            q(i,j,3) = q(i,j,3) - (dtdx * ((fluxes(i,j,1,3,2) - fluxes(i,j-1,1,3,2)) + (fluxes(i,j,1,3,1) - fluxes(i-1,j,1,3,1))))
            q(i,j,4) = q(i,j,4) - (dtdx * ((fluxes(i,j,1,4,2) - fluxes(i,j-1,1,4,2)) + (fluxes(i,j,1,4,1) - fluxes(i-1,j,1,4,1))))
            q(i,j,5) = q(i,j,5) - (dtdx * ((fluxes(i,j,1,5,2) - fluxes(i,j-1,1,5,2)) + (fluxes(i,j,1,5,1) - fluxes(i-1,j,1,5,1))))
            q(i,j,6) = q(i,j,6) - (dtdx * ((fluxes(i,j,1,6,2) - fluxes(i,j-1,1,6,2)) + (fluxes(i,j,1,6,1) - fluxes(i-1,j,1,6,1))))
            q(i,j,7) = q(i,j,7) - (dtdx * ((fluxes(i,j,1,7,2) - fluxes(i,j-1,1,7,2)) + (fluxes(i,j,1,7,1) - fluxes(i-1,j,1,7,1))))
            q(i,j,8) = q(i,j,8) - (dtdx * ((fluxes(i,j,1,8,2) - fluxes(i,j-1,1,8,2)) + (fluxes(i,j,1,8,1) - fluxes(i-1,j,1,8,1))))
            
        END DO
            
    END DO

    !### look for unphysical density and pressure ###
    DO j = jlow+2, jhi-2

        DO i = ilow+2, ihi-2

            p = (gamma - 1.0_WRD) * (q(i,j,8) - 0.5_WRD * (q(i,j,5)**2 + q(i,j,6)**2 + q(i,j,7)**2) - (0.5_WRD / MAX(q(i,j,1), self%prot_min_density)) * (q(i,j,2)**2 + q(i,j,3)**2 + q(i,j,4)**2))

            !### set the flag to replace TVD fluxes with protection fluxes around this zone ###
            IF (q(i,j,1) .LT. self%prot_min_density .OR. p .LT. self%prot_min_pressure) THEN

                prot_flags(i,j)     = .TRUE.
                fluxes(i,j,1,:,1)   = prot_fluxes(i,j,1,:,1)
                fluxes(i-1,j,1,:,1) = prot_fluxes(i-1,j,1,:,1)
                fluxes(i,j,1,:,2)   = prot_fluxes(i,j,1,:,2)
                fluxes(i,j-1,1,:,2) = prot_fluxes(i,j-1,1,:,2)

                !### we also need to swap in the protection EMFs ###
                emf(i,j,1)   = prot_emf(i,j,1)
                emf(i,j,2)   = prot_emf(i,j,2)

                emf(i-1,j,1) = prot_emf(i-1,j,1)
                emf(i-1,j,2) = prot_emf(i-1,j,2)

                emf(i,j-1,1) = prot_emf(i,j-1,1)
                emf(i,j-1,2) = prot_emf(i,j-1,2)

            END IF

        END DO

    END DO

    !### apply protection fluxes if needed ###
    DO j = jlow+2, jhi-2

        DO i = ilow+2, ihi-2

            IF (prot_flags(i,j) .OR. prot_flags(i,j-1) .OR. prot_flags(i,j+1) .OR. prot_flags(i-1,j) .OR. prot_flags(i+1,j)) THEN

                q(i,j,1) = prot_q(i,j,1) - (dtdx * ((fluxes(i,j,1,1,2) - fluxes(i,j-1,1,1,2)) + (fluxes(i,j,1,1,1) - fluxes(i-1,j,1,1,1))))
                q(i,j,2) = prot_q(i,j,2) - (dtdx * ((fluxes(i,j,1,2,2) - fluxes(i,j-1,1,2,2)) + (fluxes(i,j,1,2,1) - fluxes(i-1,j,1,2,1))))
                q(i,j,3) = prot_q(i,j,3) - (dtdx * ((fluxes(i,j,1,3,2) - fluxes(i,j-1,1,3,2)) + (fluxes(i,j,1,3,1) - fluxes(i-1,j,1,3,1))))
                q(i,j,4) = prot_q(i,j,4) - (dtdx * ((fluxes(i,j,1,4,2) - fluxes(i,j-1,1,4,2)) + (fluxes(i,j,1,4,1) - fluxes(i-1,j,1,4,1))))
                q(i,j,5) = prot_q(i,j,5) - (dtdx * ((fluxes(i,j,1,5,2) - fluxes(i,j-1,1,5,2)) + (fluxes(i,j,1,5,1) - fluxes(i-1,j,1,5,1))))
                q(i,j,6) = prot_q(i,j,6) - (dtdx * ((fluxes(i,j,1,6,2) - fluxes(i,j-1,1,6,2)) + (fluxes(i,j,1,6,1) - fluxes(i-1,j,1,6,1))))
                q(i,j,7) = prot_q(i,j,7) - (dtdx * ((fluxes(i,j,1,7,2) - fluxes(i,j-1,1,7,2)) + (fluxes(i,j,1,7,1) - fluxes(i-1,j,1,7,1))))
                q(i,j,8) = prot_q(i,j,8) - (dtdx * ((fluxes(i,j,1,8,2) - fluxes(i,j-1,1,8,2)) + (fluxes(i,j,1,8,1) - fluxes(i-1,j,1,8,1))))
                
            END IF

        END DO

    END DO

    !### finally do a last check for unphysical values that came out of the update (should be exceedingly rare after protection fluxes) ###
    CALL self%apply_protections2d(q, ilow, ihi, jlow, jhi)

END SUBROUTINE update_states2d

!### 3d algorithm routines ###

!### search the given state vector for unphysical density or pressure values ###
SUBROUTINE apply_protections3d(self, q, ilow, ihi, jlow, jhi, klow, khi)
    
    CLASS(MHDTVD) :: self
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, jlow, jhi, klow, khi
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: q(:,:,:,:)
    REAL(WRD) :: p
    INTEGER(WIS) :: i, j, k
    
    !### loop through and check for violations against the minimum density and pressure ###
    DO k = klow, khi

        DO j = jlow, jhi
            
            DO i = ilow, ihi
                
                !### as a last safety check, apply density and pressure minima (TODO, warn the user that this is happening because it is not ideal) ###
                
                IF (q(i,j,k,1) .LT. self%min_density) THEN

                    q(i,j,k,1) = MAX(self%min_density, q(i,j,k,1))

                END IF

                p = (self%gamma - 1.0_WRD) * (q(i,j,k,8) - 0.5_WRD * (q(i,j,k,5)**2 + q(i,j,k,6)**2 + q(i,j,k,7)**2) - (0.5_WRD / q(i,j,k,1)) * (q(i,j,k,2)**2 + q(i,j,k,3)**2 + q(i,j,k,4)**2))
                IF (p .LT. self%min_pressure) THEN
                    
                    p          = MAX(self%min_pressure, p)
                    q(i,j,k,8) = p / (self%gamma - 1.0_WRD) + 0.5_WRD * (q(i,j,k,5)**2 + q(i,j,k,6)**2 + q(i,j,k,7)**2) + (0.5_WRD / q(i,j,k,1)) * (q(i,j,k,2)**2 + q(i,j,k,3)**2 + q(i,j,k,4)**2)

                END IF

            END DO

        END DO

    END DO

END SUBROUTINE apply_protections3d

!### compute the X and Y state and magnetic fluxes ###
SUBROUTINE compute_Zplane_XYfluxes3d(self, workp, pool, lklow, lkhi, klow, khi, koff, ljlow, ljhi, jlow, jhi, lilow, lihi, ilow, ihi, halfdt, speedsonly)

    CLASS(MHDTVD) :: self
    TYPE(Patch) :: workp
    TYPE(MHDPool) :: pool
    INTEGER(WIS), INTENT(IN) :: koff, lklow, lkhi, klow, khi, ljlow, ljhi, jlow, jhi, lilow, lihi, ilow, ihi
    LOGICAL(WIS), INTENT(IN) :: halfdt, speedsonly
    INTEGER(WIS) :: v, j, k, i

    !### if we're on the second pass we need to perform a half step update along X ###
    IF (.NOT. halfdt .AND. .NOT. speedsonly) THEN

        !### pack the current state vector into our 3D work array ###
        DO v = 1, workp%nvars

            DO k = lklow, lkhi

                DO j = ljlow, ljhi

                    pool%qwork3d(lilow:lihi,j,k,v) = workp%grid3d(lilow:lihi,j,k,v)

                END DO

            END DO

        END DO

        !### update over the half step ###
        CALL self%halfXupdate_states3d(pool%dt, pool%dx, pool%qwork3d, pool%workPflux3d, pool%bface_save3d, workp%emf3d, pool%prot_q3d, &
             pool%workProtPflux3d, pool%protCTEMF3d, pool%prot_flags3d, klow-2, khi+2, jlow-2, jhi+2, ilow-2, ihi+2)
                
    END IF

    !### compute fluxes in X looping over Z planes ###
    DO k = lklow, lkhi

        !### pack the state vector into the 2d work array selecting either from the intermediate state or the original state vector ###
        IF (.NOT. halfdt .AND. .NOT. speedsonly) THEN

            DO v = 1, workp%nvars
                
                DO j = ljlow, ljhi
                    
                    pool%qwork2d(lilow:lihi,j,v) = pool%qwork3d(lilow:lihi,j,k,v)
                    
                END DO
                
            END DO
            
        ELSE

            DO v = 1, workp%nvars
                
                DO j = ljlow, ljhi
                    
                    pool%qwork2d(lilow:lihi,j,v) = workp%grid3d(lilow:lihi,j,k,v)
                    
                END DO
                
            END DO

        END IF
            
        !### compute pressure ###
        CALL self%compute_pressure1d(pool%qwork2d, pool%pressure2d, ilow, ihi, jlow, jhi)
        
        !### compute Roe average states ###
        CALL self%compute_zone_averages1d(pool%state_avg2d, pool%qwork2d, pool%pressure2d, ilow, ihi, jlow, jhi)
        
        !### compute wave speeds and eigenvalues ###
        CALL self%compute_speeds_eigenvals1d(pool%eigenVal2d, pool%waves2d, pool%state_avg2d, ilow, ihi, jlow, jhi)
        
        !### if we're only computing wave speeds first find the max and then move on ###
        IF (speedsonly) THEN

            DO j = ljlow, ljhi-1

                DO i = lilow, lihi-1
                    
                    pool%max_wave = MAX(pool%max_wave, MAXVAL(pool%waves2d(i,j,:)) + ABS(pool%qwork2d(i,j,2)) / pool%qwork2d(i,j,1))
                    
                END DO
                
            END DO
            
        !### if we're only computing wave speeds (such as for computing the time step) we skip the rest of the X work ###
        ELSE
                
            !### construct the left-handed eigenvectors and characteristics ###
            CALL self%compute_eigenvecs1d(pool%eigenVec2d, pool%charac2d, pool%eigenVal2d, pool%waves2d, &
                 pool%qwork2d, pool%state_avg2d, pool%pressure2d, ilow, ihi, jlow, jhi)
            
            !### compute the fluxes ###
            IF (halfdt) THEN

                CALL self%compute_fluxes1d(pool%nstate, pool%nwaves, 3, 0.5_WRD * pool%dt, pool%dx, pool%workPflux3d, pool%workProtPflux3d, pool%waves2d, pool%eigenVec2d, pool%charac2d, pool%eigenVal2d, &
                     pool%pressure2d, pool%qwork2d, pool%Bflux2d, pool%Bflux3d, pool%protBflux2d, pool%protBflux3d, .FALSE., .TRUE., ilow, ihi, jlow, jhi, k+koff, 1)
                
            ELSE

                !### on the second pass we place fluxes directly into the final flux array ###
                CALL self%compute_fluxes1d(pool%nstate, pool%nwaves, 3, pool%dt, pool%dx, pool%Pflux3d, pool%protPflux3d, pool%waves2d, pool%eigenVec2d, pool%charac2d, pool%eigenVal2d, &
                     pool%pressure2d, pool%qwork2d, pool%Bflux2d, pool%Bflux3d, pool%protBflux2d, pool%protBflux3d, .FALSE., .TRUE., ilow, ihi, jlow, jhi, k+koff, 1)
                
            END IF
            
            !### store the magnetic fluxes (take care of the sign flip here so that these are really E_z) ###
            DO j = ljlow, ljhi

                pool%EMF3d(lilow:lihi,j,k,1,1) = -pool%Bflux3d(lilow:lihi,j,1)
                pool%EMF3d(lilow:lihi,j,k,1,2) = pool%Bflux3d(lilow:lihi,j,2)

                pool%protEMF3d(lilow:lihi,j,k,1,1) = -pool%protBflux3d(lilow:lihi,j,1)
                pool%protEMF3d(lilow:lihi,j,k,1,2) = pool%protBflux3d(lilow:lihi,j,2)
                
            END DO
            
        END IF

    END DO

    !### if we're on the second pass we need to perform a half step update along Y ###
    IF (.NOT. halfdt .AND. .NOT. speedsonly) THEN

        !### pack the current state vector into our 3D work array ###
        DO v = 1, workp%nvars

            DO k = lklow, lkhi

                DO j = ljlow, ljhi

                    pool%qwork3d(lilow:lihi,j,k,v) = workp%grid3d(lilow:lihi,j,k,v)

                END DO

            END DO

        END DO

        !### update over the half step ###
        CALL self%halfYupdate_states3d(pool%dt, pool%dx, pool%qwork3d, pool%workPflux3d, pool%bface_save3d, workp%emf3d, pool%prot_q3d, &
             pool%workProtPflux3d, pool%protCTEMF3d, pool%prot_flags3d, klow-2, khi+2, ilow-2, ihi+2, jlow-2, jhi+2)
                
    END IF

    !### now compute the Y fluxes looping over planes in Z###
    DO k = lklow, lkhi

        !### transpose the plane selecting either from the intermediate state or original state vector ###
        IF (.NOT. halfdt .AND. .NOT. speedsonly) THEN

            DO i = lilow, lihi
                
                DO j = ljlow, ljhi, 2
                    
                    pool%qwork2d(j,i,1) = pool%qwork3d(i,j,k,1)
                    pool%qwork2d(j,i,2) = pool%qwork3d(i,j,k,3)
                    pool%qwork2d(j,i,3) = pool%qwork3d(i,j,k,4)
                    pool%qwork2d(j,i,4) = pool%qwork3d(i,j,k,2)
                    pool%qwork2d(j,i,5) = pool%qwork3d(i,j,k,6)
                    pool%qwork2d(j,i,6) = pool%qwork3d(i,j,k,7)
                    pool%qwork2d(j,i,7) = pool%qwork3d(i,j,k,5)
                    pool%qwork2d(j,i,8) = pool%qwork3d(i,j,k,8)

                    pool%qwork2d(j+1,i,1) = pool%qwork3d(i,j+1,k,1)
                    pool%qwork2d(j+1,i,2) = pool%qwork3d(i,j+1,k,3)
                    pool%qwork2d(j+1,i,3) = pool%qwork3d(i,j+1,k,4)
                    pool%qwork2d(j+1,i,4) = pool%qwork3d(i,j+1,k,2)
                    pool%qwork2d(j+1,i,5) = pool%qwork3d(i,j+1,k,6)
                    pool%qwork2d(j+1,i,6) = pool%qwork3d(i,j+1,k,7)
                    pool%qwork2d(j+1,i,7) = pool%qwork3d(i,j+1,k,5)
                    pool%qwork2d(j+1,i,8) = pool%qwork3d(i,j+1,k,8)
                    
                END DO

                IF (MOD(ljhi-ljlow+1,2) .NE. 0) THEN

                    j = ljhi
                    pool%qwork2d(j,i,1) = pool%qwork3d(i,j,k,1)
                    pool%qwork2d(j,i,2) = pool%qwork3d(i,j,k,3)
                    pool%qwork2d(j,i,3) = pool%qwork3d(i,j,k,4)
                    pool%qwork2d(j,i,4) = pool%qwork3d(i,j,k,2)
                    pool%qwork2d(j,i,5) = pool%qwork3d(i,j,k,6)
                    pool%qwork2d(j,i,6) = pool%qwork3d(i,j,k,7)
                    pool%qwork2d(j,i,7) = pool%qwork3d(i,j,k,5)
                    pool%qwork2d(j,i,8) = pool%qwork3d(i,j,k,8)

                END IF
                
            END DO

        ELSE

            DO i = lilow, lihi
                
                DO j = ljlow, ljhi, 2
                    
                    pool%qwork2d(j,i,1) = workp%grid3d(i,j,k,1)
                    pool%qwork2d(j,i,2) = workp%grid3d(i,j,k,3)
                    pool%qwork2d(j,i,3) = workp%grid3d(i,j,k,4)
                    pool%qwork2d(j,i,4) = workp%grid3d(i,j,k,2)
                    pool%qwork2d(j,i,5) = workp%grid3d(i,j,k,6)
                    pool%qwork2d(j,i,6) = workp%grid3d(i,j,k,7)
                    pool%qwork2d(j,i,7) = workp%grid3d(i,j,k,5)
                    pool%qwork2d(j,i,8) = workp%grid3d(i,j,k,8)
                    
                    pool%qwork2d(j+1,i,1) = workp%grid3d(i,j+1,k,1)
                    pool%qwork2d(j+1,i,2) = workp%grid3d(i,j+1,k,3)
                    pool%qwork2d(j+1,i,3) = workp%grid3d(i,j+1,k,4)
                    pool%qwork2d(j+1,i,4) = workp%grid3d(i,j+1,k,2)
                    pool%qwork2d(j+1,i,5) = workp%grid3d(i,j+1,k,6)
                    pool%qwork2d(j+1,i,6) = workp%grid3d(i,j+1,k,7)
                    pool%qwork2d(j+1,i,7) = workp%grid3d(i,j+1,k,5)
                    pool%qwork2d(j+1,i,8) = workp%grid3d(i,j+1,k,8)
                    
                END DO

                IF (MOD(ljhi-ljlow+1,2) .NE. 0) THEN

                    j = ljhi
                    pool%qwork2d(j,i,1) = workp%grid3d(i,j,k,1)
                    pool%qwork2d(j,i,2) = workp%grid3d(i,j,k,3)
                    pool%qwork2d(j,i,3) = workp%grid3d(i,j,k,4)
                    pool%qwork2d(j,i,4) = workp%grid3d(i,j,k,2)
                    pool%qwork2d(j,i,5) = workp%grid3d(i,j,k,6)
                    pool%qwork2d(j,i,6) = workp%grid3d(i,j,k,7)
                    pool%qwork2d(j,i,7) = workp%grid3d(i,j,k,5)
                    pool%qwork2d(j,i,8) = workp%grid3d(i,j,k,8)

                END IF
                
            END DO

        END IF
            
        !### compute pressure ###
        CALL self%compute_pressure1d(pool%qwork2d, pool%pressure2d, jlow, jhi, ilow, ihi)
        
        !### compute Roe average states ###
        CALL self%compute_zone_averages1d(pool%state_avg2d, pool%qwork2d, pool%pressure2d, jlow, jhi, ilow, ihi)

        !### compute wave speeds and eigenvalues ###
        CALL self%compute_speeds_eigenvals1d(pool%eigenVal2d, pool%waves2d, pool%state_avg2d, jlow, jhi, ilow, ihi)
        
        !### if we're only computing wave speeds first find the max and then move on ###
        IF (speedsonly) THEN

            DO i = lilow, lihi      
                
                DO j = ljlow, ljhi-1
                    
                    pool%max_wave = MAX(pool%max_wave, MAXVAL(pool%waves2d(j,i,:)) + ABS(pool%qwork2d(j,i,2)) / pool%qwork2d(j,i,1))
                    
                END DO
                
            END DO
            
        !### if we're only computing wave speeds (such as for computing the time step) we skip the rest of the X work ###
        ELSE
                
            !### construct the left-handed eigenvectors and characteristics ###
            CALL self%compute_eigenvecs1d(pool%eigenVec2d, pool%charac2d, pool%eigenVal2d, pool%waves2d, &
                 pool%qwork2d, pool%state_avg2d, pool%pressure2d, jlow, jhi, ilow, ihi)
                
            !### if we're doing halfdt we need to place the final fluxes into the work flux array ###
            IF (halfdt) THEN

                CALL self%compute_fluxes1d(pool%nstate, pool%nwaves, 3, 0.5_WRD * pool%dt, pool%dx, pool%Pflux3d, pool%protPflux3d, pool%waves2d, pool%eigenVec2d, pool%charac2d, pool%eigenVal2d, &
                     pool%pressure2d, pool%qwork2d, pool%Bflux2d, pool%Bflux3d, pool%protBflux2d, pool%protBflux3d, .FALSE., .TRUE., jlow, jhi, ilow, ihi, k+koff, 2)
                
                !### rotate the flux into the work flux array ### ###
                DO j = ljlow, ljhi
                    
                    DO i = lilow, lihi
                        
                        pool%workPflux3d(i,j,k,1,2) = pool%Pflux3d(j,i,k,1,2)
                        pool%workPflux3d(i,j,k,3,2) = pool%Pflux3d(j,i,k,2,2)
                        pool%workPflux3d(i,j,k,4,2) = pool%Pflux3d(j,i,k,3,2)
                        pool%workPflux3d(i,j,k,2,2) = pool%Pflux3d(j,i,k,4,2)
                        pool%workPflux3d(i,j,k,6,2) = pool%Pflux3d(j,i,k,5,2)
                        pool%workPflux3d(i,j,k,7,2) = pool%Pflux3d(j,i,k,6,2)
                        pool%workPflux3d(i,j,k,5,2) = pool%Pflux3d(j,i,k,7,2)
                        pool%workPflux3d(i,j,k,8,2) = pool%Pflux3d(j,i,k,8,2)

                        pool%workProtPflux3d(i,j,k,1,2) = pool%protPflux3d(j,i,k,1,2)
                        pool%workProtPflux3d(i,j,k,3,2) = pool%protPflux3d(j,i,k,2,2)
                        pool%workProtPflux3d(i,j,k,4,2) = pool%protPflux3d(j,i,k,3,2)
                        pool%workProtPflux3d(i,j,k,2,2) = pool%protPflux3d(j,i,k,4,2)
                        pool%workProtPflux3d(i,j,k,6,2) = pool%protPflux3d(j,i,k,5,2)
                        pool%workProtPflux3d(i,j,k,7,2) = pool%protPflux3d(j,i,k,6,2)
                        pool%workProtPflux3d(i,j,k,5,2) = pool%protPflux3d(j,i,k,7,2)
                        pool%workProtPflux3d(i,j,k,8,2) = pool%protPflux3d(j,i,k,8,2)
                        
                    END DO
                    
                END DO
                
            ELSE

                CALL self%compute_fluxes1d(pool%nstate, pool%nwaves, 3, pool%dt, pool%dx, pool%workPflux3d, pool%workProtPflux3d, pool%waves2d, pool%eigenVec2d, pool%charac2d, pool%eigenVal2d, &
                     pool%pressure2d, pool%qwork2d, pool%Bflux2d, pool%Bflux3d, pool%protBflux2d, pool%protBflux3d, .FALSE., .TRUE., jlow, jhi, ilow, ihi, k+koff, 2)
                
                !### rotate the flux into the work flux array ### ###
                DO j = ljlow, ljhi
                    
                    DO i = lilow, lihi
                        
                        pool%Pflux3d(i,j,k,1,2) = pool%workPflux3d(j,i,k,1,2)
                        pool%Pflux3d(i,j,k,3,2) = pool%workPflux3d(j,i,k,2,2)
                        pool%Pflux3d(i,j,k,4,2) = pool%workPflux3d(j,i,k,3,2)
                        pool%Pflux3d(i,j,k,2,2) = pool%workPflux3d(j,i,k,4,2)
                        pool%Pflux3d(i,j,k,6,2) = pool%workPflux3d(j,i,k,5,2)
                        pool%Pflux3d(i,j,k,7,2) = pool%workPflux3d(j,i,k,6,2)
                        pool%Pflux3d(i,j,k,5,2) = pool%workPflux3d(j,i,k,7,2)
                        pool%Pflux3d(i,j,k,8,2) = pool%workPflux3d(j,i,k,8,2)

                        pool%protPflux3d(i,j,k,1,2) = pool%workProtPflux3d(j,i,k,1,2)
                        pool%protPflux3d(i,j,k,3,2) = pool%workProtPflux3d(j,i,k,2,2)
                        pool%protPflux3d(i,j,k,4,2) = pool%workProtPflux3d(j,i,k,3,2)
                        pool%protPflux3d(i,j,k,2,2) = pool%workProtPflux3d(j,i,k,4,2)
                        pool%protPflux3d(i,j,k,6,2) = pool%workProtPflux3d(j,i,k,5,2)
                        pool%protPflux3d(i,j,k,7,2) = pool%workProtPflux3d(j,i,k,6,2)
                        pool%protPflux3d(i,j,k,5,2) = pool%workProtPflux3d(j,i,k,7,2)
                        pool%protPflux3d(i,j,k,8,2) = pool%workProtPflux3d(j,i,k,8,2)
                        
                    END DO
                    
                END DO
                
            END IF
            
            !### store the magnetic fluxes ###
            DO j = ljlow, ljhi
                
                DO i = lilow, lihi
                    
                    pool%EMF3d(i,j,k,2,1) = pool%Bflux3d(j,i,2)
                    pool%EMF3d(i,j,k,2,2) = -pool%Bflux3d(j,i,1)

                    pool%protEMF3d(i,j,k,2,1) = pool%protBflux3d(j,i,2)
                    pool%protEMF3d(i,j,k,2,2) = -pool%protBflux3d(j,i,1)
                    
                END DO
                
            END DO
                
        END IF

    END DO
    
    !### if we're set to do 1/2 dt, backup the original fluxes as they are needed for the Z pass (they get overwritten otherwise) ###
    IF (halfdt) THEN

        DO v = 1, workp%nvars

            DO k = lklow, lkhi

                DO j = ljlow, ljhi

                    pool%workPflux3d(lilow:lihi,j,k,v,4) = pool%workPflux3d(lilow:lihi,j,k,v,1)
                    pool%workPflux3d(lilow:lihi,j,k,v,5) = pool%workPflux3d(lilow:lihi,j,k,v,2)

                END DO

            END DO

            DO k = lklow, lkhi

                DO j = ljlow, ljhi

                    pool%workProtPflux3d(lilow:lihi,j,k,v,4) = pool%workProtPflux3d(lilow:lihi,j,k,v,1)
                    pool%workProtPflux3d(lilow:lihi,j,k,v,5) = pool%workProtPflux3d(lilow:lihi,j,k,v,2)

                END DO

            END DO

        END DO

    END IF

END SUBROUTINE compute_Zplane_XYfluxes3d

!### compute the Z state and magnetic fluxes ###
SUBROUTINE compute_Zfluxes3d(self, workp, pool, ljlow, ljhi, jlow, jhi, joff, lklow, lkhi, klow, khi, lilow, lihi, ilow, ihi, halfdt, speedsonly)

    CLASS(MHDTVD) :: self
    TYPE(Patch) :: workp
    TYPE(MHDPool) :: pool
    INTEGER(WIS), INTENT(IN) :: joff, ljlow, ljhi, jlow, jhi, lklow, lkhi, klow, khi, lilow, lihi, ilow, ihi
    LOGICAL(WIS), INTENT(IN) :: halfdt, speedsonly
    INTEGER(WIS) :: v, j, k, i

    !### if we're not doing a half dt, we need to pull in the back-up fluxes from the Y and X passes for use here ###
    IF (.NOT. halfdt .AND. .NOT. speedsonly) THEN

        DO v = 1, workp%nvars

            DO k = lklow, lkhi

                DO j = ljlow, ljhi

                    pool%workPflux3d(lilow:lihi,j,k,v,1) = pool%workPflux3d(lilow:lihi,j,k,v,4)
                    pool%workPflux3d(lilow:lihi,j,k,v,2) = pool%workPflux3d(lilow:lihi,j,k,v,5)

                END DO

            END DO

        END DO

        !### on the second pass we must immediately update the state a half time step (along Z only) ###
        !### pack the current state vector into our 3D work array ###
        DO v = 1, workp%nvars

            DO k = lklow, lkhi

                DO j = ljlow, ljhi

                    pool%qwork3d(lilow:lihi,j,k,v) = workp%grid3d(lilow:lihi,j,k,v)

                END DO

            END DO

        END DO

        !### update over the half step ###
        CALL self%halfZupdate_states3d(pool%dt, pool%dx, pool%qwork3d, pool%workPflux3d, pool%bface_save3d, workp%emf3d, pool%prot_q3d, &
             pool%workProtPflux3d, pool%protCTEMF3d, pool%prot_flags3d, jlow-2, jhi+2, ilow-2, ihi+2, klow-2, khi+2)

    END IF

    !### loop over Y planes ###
    DO j = ljlow, ljhi

        !### transpose the plane selecting either from the intermediate state or original state vector ###
        IF (.NOT. halfdt .AND. .NOT. speedsonly) THEN

            DO i = lilow, lihi
            
                DO k = lklow, lkhi, 2

                    pool%qwork2d(k,i,1) = pool%qwork3d(i,j,k,1)
                    pool%qwork2d(k,i,2) = pool%qwork3d(i,j,k,4)
                    pool%qwork2d(k,i,3) = pool%qwork3d(i,j,k,2)
                    pool%qwork2d(k,i,4) = pool%qwork3d(i,j,k,3)
                    pool%qwork2d(k,i,5) = pool%qwork3d(i,j,k,7)
                    pool%qwork2d(k,i,6) = pool%qwork3d(i,j,k,5)
                    pool%qwork2d(k,i,7) = pool%qwork3d(i,j,k,6)
                    pool%qwork2d(k,i,8) = pool%qwork3d(i,j,k,8)

                    pool%qwork2d(k+1,i,1) = pool%qwork3d(i,j,k+1,1)
                    pool%qwork2d(k+1,i,2) = pool%qwork3d(i,j,k+1,4)
                    pool%qwork2d(k+1,i,3) = pool%qwork3d(i,j,k+1,2)
                    pool%qwork2d(k+1,i,4) = pool%qwork3d(i,j,k+1,3)
                    pool%qwork2d(k+1,i,5) = pool%qwork3d(i,j,k+1,7)
                    pool%qwork2d(k+1,i,6) = pool%qwork3d(i,j,k+1,5)
                    pool%qwork2d(k+1,i,7) = pool%qwork3d(i,j,k+1,6)
                    pool%qwork2d(k+1,i,8) = pool%qwork3d(i,j,k+1,8)
            
                END DO

                IF (MOD(lkhi-lklow+1,2) .NE. 0) THEN

                    k = lkhi
                    pool%qwork2d(k,i,1) = pool%qwork3d(i,j,k,1)
                    pool%qwork2d(k,i,2) = pool%qwork3d(i,j,k,4)
                    pool%qwork2d(k,i,3) = pool%qwork3d(i,j,k,2)
                    pool%qwork2d(k,i,4) = pool%qwork3d(i,j,k,3)
                    pool%qwork2d(k,i,5) = pool%qwork3d(i,j,k,7)
                    pool%qwork2d(k,i,6) = pool%qwork3d(i,j,k,5)
                    pool%qwork2d(k,i,7) = pool%qwork3d(i,j,k,6)
                    pool%qwork2d(k,i,8) = pool%qwork3d(i,j,k,8)

                END IF

            END DO

        ELSE
            
            DO i = lilow, lihi
            
                DO k = lklow, lkhi, 2

                    pool%qwork2d(k,i,1) = workp%grid3d(i,j,k,1)
                    pool%qwork2d(k,i,2) = workp%grid3d(i,j,k,4)
                    pool%qwork2d(k,i,3) = workp%grid3d(i,j,k,2)
                    pool%qwork2d(k,i,4) = workp%grid3d(i,j,k,3)
                    pool%qwork2d(k,i,5) = workp%grid3d(i,j,k,7)
                    pool%qwork2d(k,i,6) = workp%grid3d(i,j,k,5)
                    pool%qwork2d(k,i,7) = workp%grid3d(i,j,k,6)
                    pool%qwork2d(k,i,8) = workp%grid3d(i,j,k,8)

                    pool%qwork2d(k+1,i,1) = workp%grid3d(i,j,k+1,1)
                    pool%qwork2d(k+1,i,2) = workp%grid3d(i,j,k+1,4)
                    pool%qwork2d(k+1,i,3) = workp%grid3d(i,j,k+1,2)
                    pool%qwork2d(k+1,i,4) = workp%grid3d(i,j,k+1,3)
                    pool%qwork2d(k+1,i,5) = workp%grid3d(i,j,k+1,7)
                    pool%qwork2d(k+1,i,6) = workp%grid3d(i,j,k+1,5)
                    pool%qwork2d(k+1,i,7) = workp%grid3d(i,j,k+1,6)
                    pool%qwork2d(k+1,i,8) = workp%grid3d(i,j,k+1,8)
            
                END DO

                IF (MOD(lkhi-lklow+1,2) .NE. 0) THEN

                    k = lkhi
                    pool%qwork2d(k,i,1) = workp%grid3d(i,j,k,1)
                    pool%qwork2d(k,i,2) = workp%grid3d(i,j,k,4)
                    pool%qwork2d(k,i,3) = workp%grid3d(i,j,k,2)
                    pool%qwork2d(k,i,4) = workp%grid3d(i,j,k,3)
                    pool%qwork2d(k,i,5) = workp%grid3d(i,j,k,7)
                    pool%qwork2d(k,i,6) = workp%grid3d(i,j,k,5)
                    pool%qwork2d(k,i,7) = workp%grid3d(i,j,k,6)
                    pool%qwork2d(k,i,8) = workp%grid3d(i,j,k,8)

                END IF

            END DO


        END IF
            
        !### compute pressure ###
        CALL self%compute_pressure1d(pool%qwork2d, pool%pressure2d, klow, khi, ilow, ihi)

        !### compute Roe average states ###
        CALL self%compute_zone_averages1d(pool%state_avg2d, pool%qwork2d, pool%pressure2d, klow, khi, ilow, ihi)

        !### compute wave speeds and eigenvalues ###
        CALL self%compute_speeds_eigenvals1d(pool%eigenVal2d, pool%waves2d, pool%state_avg2d, klow, khi, ilow, ihi)

        !### if we're only doing wave speeds, compute the max now and move on ###
        IF (speedsonly) THEN

            DO i = lilow, lihi      
                
                DO k = lklow, lkhi-1

                    pool%max_wave = MAX(pool%max_wave, MAXVAL(pool%waves2d(k,i,:)) + ABS(pool%qwork2d(k,i,2)) / pool%qwork2d(k,i,1))

                END DO

            END DO

        !### if we're only computing wave speeds (such as for computing the time step) we skip the rest of the work ###
        ELSE
                
            !### construct the left-handed eigenvectors and characteristics ###
            CALL self%compute_eigenvecs1d(pool%eigenVec2d, pool%charac2d, pool%eigenVal2d, pool%waves2d, &
                 pool%qwork2d, pool%state_avg2d, pool%pressure2d, klow, khi, ilow, ihi)

            !### if we're doing halfdt we need to place the final fluxes into the work flux array ###
            IF (halfdt) THEN

                CALL self%compute_fluxes1d(pool%nstate, pool%nwaves, 3, 0.5_WRD * pool%dt, pool%dx, pool%Pflux3d, pool%protPflux3d, pool%waves2d, pool%eigenVec2d, pool%charac2d, pool%eigenVal2d, &
                     pool%pressure2d, pool%qwork2d, pool%Bflux2d, pool%Bflux3d, pool%protBflux2d, pool%protBflux3d, .FALSE., .TRUE., klow, khi, ilow, ihi, j+joff, 3)
    
                !### rotate the flux into the work flux array ###
                DO k = lklow, lkhi

                    DO i = lilow, lihi
            
                        pool%workPflux3d(i,j,k,1,3) = pool%Pflux3d(k,i,j,1,3)
                        pool%workPflux3d(i,j,k,4,3) = pool%Pflux3d(k,i,j,2,3)
                        pool%workPflux3d(i,j,k,2,3) = pool%Pflux3d(k,i,j,3,3)
                        pool%workPflux3d(i,j,k,3,3) = pool%Pflux3d(k,i,j,4,3)
                        pool%workPflux3d(i,j,k,7,3) = pool%Pflux3d(k,i,j,5,3)
                        pool%workPflux3d(i,j,k,5,3) = pool%Pflux3d(k,i,j,6,3)
                        pool%workPflux3d(i,j,k,6,3) = pool%Pflux3d(k,i,j,7,3)
                        pool%workPflux3d(i,j,k,8,3) = pool%Pflux3d(k,i,j,8,3)

                        pool%workProtPflux3d(i,j,k,1,3) = pool%protPflux3d(k,i,j,1,3)
                        pool%workProtPflux3d(i,j,k,4,3) = pool%protPflux3d(k,i,j,2,3)
                        pool%workProtPflux3d(i,j,k,2,3) = pool%protPflux3d(k,i,j,3,3)
                        pool%workProtPflux3d(i,j,k,3,3) = pool%protPflux3d(k,i,j,4,3)
                        pool%workProtPflux3d(i,j,k,7,3) = pool%protPflux3d(k,i,j,5,3)
                        pool%workProtPflux3d(i,j,k,5,3) = pool%protPflux3d(k,i,j,6,3)
                        pool%workProtPflux3d(i,j,k,6,3) = pool%protPflux3d(k,i,j,7,3)
                        pool%workProtPflux3d(i,j,k,8,3) = pool%protPflux3d(k,i,j,8,3)
                        
                    END DO
        
                END DO

            ELSE

                CALL self%compute_fluxes1d(pool%nstate, pool%nwaves, 3, pool%dt, pool%dx, pool%workPflux3d, pool%workProtPflux3d, pool%waves2d, pool%eigenVec2d, pool%charac2d, pool%eigenVal2d, &
                     pool%pressure2d, pool%qwork2d, pool%Bflux2d, pool%Bflux3d, pool%protBflux2d, pool%protBflux3d, .FALSE., .TRUE., klow, khi, ilow, ihi, j+joff, 3)
    
                !### rotate the flux into the work flux array ### ###
                DO k = lklow, lkhi
            
                    DO i = lilow, lihi
            
                        pool%Pflux3d(i,j,k,1,3) = pool%workPflux3d(k,i,j,1,3)
                        pool%Pflux3d(i,j,k,4,3) = pool%workPflux3d(k,i,j,2,3)
                        pool%Pflux3d(i,j,k,2,3) = pool%workPflux3d(k,i,j,3,3)
                        pool%Pflux3d(i,j,k,3,3) = pool%workPflux3d(k,i,j,4,3)
                        pool%Pflux3d(i,j,k,7,3) = pool%workPflux3d(k,i,j,5,3)
                        pool%Pflux3d(i,j,k,5,3) = pool%workPflux3d(k,i,j,6,3)
                        pool%Pflux3d(i,j,k,6,3) = pool%workPflux3d(k,i,j,7,3)
                        pool%Pflux3d(i,j,k,8,3) = pool%workPflux3d(k,i,j,8,3)

                        pool%protPflux3d(i,j,k,1,3) = pool%workProtPflux3d(k,i,j,1,3)
                        pool%protPflux3d(i,j,k,4,3) = pool%workProtPflux3d(k,i,j,2,3)
                        pool%protPflux3d(i,j,k,2,3) = pool%workProtPflux3d(k,i,j,3,3)
                        pool%protPflux3d(i,j,k,3,3) = pool%workProtPflux3d(k,i,j,4,3)
                        pool%protPflux3d(i,j,k,7,3) = pool%workProtPflux3d(k,i,j,5,3)
                        pool%protPflux3d(i,j,k,5,3) = pool%workProtPflux3d(k,i,j,6,3)
                        pool%protPflux3d(i,j,k,6,3) = pool%workProtPflux3d(k,i,j,7,3)
                        pool%protPflux3d(i,j,k,8,3) = pool%workProtPflux3d(k,i,j,8,3)

                    END DO
        
                END DO

            END IF

            !### store the magnetic fluxes ###
            DO k = lklow, lkhi

                DO i = lilow, lihi

                    pool%EMF3d(i,j,k,3,1) = -pool%Bflux3d(k,i,1)
                    pool%EMF3d(i,j,k,3,2) = pool%Bflux3d(k,i,2)

                    pool%protEMF3d(i,j,k,3,1) = -pool%protBflux3d(k,i,1)
                    pool%protEMF3d(i,j,k,3,2) = pool%protBflux3d(k,i,2)

                END DO
        
            END DO

        END IF

    END DO

END SUBROUTINE compute_Zfluxes3d

!### apply a half time step update to a plane using transverse fluxes computed from the initial state and apply the div(B) source term ###
SUBROUTINE halfXupdate_states3d(self, dt, dx, q, fluxes, bface, emf, prot_q, prot_fluxes, prot_emf, prot_flags, klow, khi, jlow, jhi, ilow, ihi)

    CLASS(MHDTVD) :: self
    REAL(WRD), INTENT(IN) :: dt, dx
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: bface(:,:,:,:), prot_fluxes(:,:,:,:,:), prot_emf(:,:,:,:)
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: q(:,:,:,:), prot_q(:,:,:,:), emf(:,:,:,:), fluxes(:,:,:,:,:)
    LOGICAL(WIS), CONTIGUOUS, INTENT(INOUT) :: prot_flags(:,:,:)
    INTEGER(WIS), INTENT(IN) :: klow, khi, jlow, jhi, ilow, ihi
    REAL(WRD) :: dtdx, idx, Smx, Smy, Smz, Sby, Sbz, Se, divBx, divBy, divBz, mmzx, mmyx, p, gamma
    INTEGER(WIS) :: i, j, k

    idx   = 1.0_WRD / dx
    dtdx  = 0.5_WRD * dt * idx
    gamma = self%gamma

    prot_flags = .FALSE.

    !### perform the update ###
    DO k = klow+1, khi

        DO j = jlow+1, jhi

            !### save the current values in case this update causes unphysical values ###
            prot_q(ilow+2:ihi-2,j,k,:) = q(ilow+2:ihi-2,j,k,:)

            DO i = ilow+2, ihi-2

                !### derive the divB source terms ###
                divBx = bface(i,j,k,1) - bface(i-1,j,k,1)
                divBy = bface(i,j,k,2) - bface(i,j-1,k,2)
                divBz = bface(i,j,k,3) - bface(i,j,k-1,3)
                Smx   = q(i,j,k,5) * divBx
                Smy   = q(i,j,k,6) * divBx
                Smz   = q(i,j,k,7) * divBx
                mmzx  = 0.0_WRD
                IF ((-divBx * divBz) .GT. 0.0_WRD) mmzx = SIGN(1.0_WRD,divBz) * MIN(ABS(divBx), ABS(divBz))
                mmyx  = 0.0_WRD
                IF ((-divBx * divBy) .GT. 0.0_WRD) mmyx = SIGN(1.0_WRD,divBy) * MIN(ABS(divBx), ABS(divBy))
                Se    = -(q(i,j,k,6) * q(i,j,k,3) / q(i,j,k,1)) * mmzx - (q(i,j,k,7) * q(i,j,k,4) / q(i,j,k,1)) * mmyx
                Sby   = (q(i,j,k,3) / q(i,j,k,1)) * mmzx
                Sbz   = (q(i,j,k,4) / q(i,j,k,1)) * mmyx

                q(i,j,k,1) = q(i,j,k,1) - (dtdx * ((fluxes(i,j,k,1,2) - fluxes(i,j-1,k,1,2)) + (fluxes(i,j,k,1,3) - fluxes(i,j,k-1,1,3))))
                q(i,j,k,2) = q(i,j,k,2) - (dtdx * ((fluxes(i,j,k,2,2) - fluxes(i,j-1,k,2,2)) + (fluxes(i,j,k,2,3) - fluxes(i,j,k-1,2,3)))) + dtdx * Smx
                q(i,j,k,3) = q(i,j,k,3) - (dtdx * ((fluxes(i,j,k,3,2) - fluxes(i,j-1,k,3,2)) + (fluxes(i,j,k,3,3) - fluxes(i,j,k-1,3,3)))) + dtdx * Smy
                q(i,j,k,4) = q(i,j,k,4) - (dtdx * ((fluxes(i,j,k,4,2) - fluxes(i,j-1,k,4,2)) + (fluxes(i,j,k,4,3) - fluxes(i,j,k-1,4,3)))) + dtdx * Smz

                q(i,j,k,5) = q(i,j,k,5) - (dtdx * ((fluxes(i,j,k,5,2) - fluxes(i,j-1,k,5,2)) + (fluxes(i,j,k,5,3) - fluxes(i,j,k-1,5,3))))
                q(i,j,k,6) = q(i,j,k,6) - 0.5_WRD * dtdx * ((emf(i,j,k,1) - emf(i,j,k-1,1)) + (emf(i,j-1,k,1) - emf(i,j-1,k-1,1))) + dtdx * Sby
                q(i,j,k,7) = q(i,j,k,7) + 0.5_WRD * dtdx * ((emf(i,j,k,1) - emf(i,j-1,k,1)) + (emf(i,j,k-1,1) - emf(i,j-1,k-1,1))) + dtdx * Sbz
                
                q(i,j,k,8) = q(i,j,k,8) - (dtdx * ((fluxes(i,j,k,8,2) - fluxes(i,j-1,k,8,2)) + (fluxes(i,j,k,8,3) - fluxes(i,j,k-1,8,3)))) + dtdx * Se

            END DO

        END DO

    END DO

    !### look for unphysical density and pressure ###
    DO k = klow+1, khi

        DO j = jlow+1, jhi

            DO i = ilow+2, ihi-2

                p = (gamma - 1.0_WRD) * (q(i,j,k,8) - 0.5_WRD * (q(i,j,k,5)**2 + q(i,j,k,6)**2 + q(i,j,k,7)**2) - (0.5_WRD / MAX(q(i,j,k,1), self%prot_min_density)) * (q(i,j,k,2)**2 + q(i,j,k,3)**2 + q(i,j,k,4)**2))

                !### for these updates we just set the flag and do not replace the fluxes as we confine protection to just this partial update ###
                IF (q(i,j,k,1) .LT. self%prot_min_density .OR. p .LT. self%prot_min_pressure) THEN

                    prot_flags(i,j,k)   = .TRUE.
                    fluxes(i,j,k,:,2)   = prot_fluxes(i,j,k,:,2)
                    fluxes(i,j-1,k,:,2) = prot_fluxes(i,j-1,k,:,2)
                    fluxes(i,j,k,:,3)   = prot_fluxes(i,j,k,:,3)
                    fluxes(i,j,k-1,:,3) = prot_fluxes(i,j,k-1,:,3)

                    emf(i,j,k,1)        = prot_emf(i,j,k,1)
                    emf(i,j,k-1,1)      = prot_emf(i,j,k-1,1)
                    emf(i,j-1,k,1)      = prot_emf(i,j-1,k,1)
                    emf(i,j-1,k-1,1)    = prot_emf(i,j-1,k-1,1)

                END IF

            END DO

        END DO

    END DO

    !### apply the protection fluxes if needed ###
    DO k = klow+1, khi

        DO j = jlow+1, jhi

            DO i = ilow+2, ihi-2

                IF (prot_flags(i,j,k) .OR. prot_flags(i,j-1,k) .OR.prot_flags(i,j+1,k) .OR. prot_flags(i,j,k-1) .OR. prot_flags(i,j,k+1)) THEN

                    !### derive the divB source terms ###
                    divBx = bface(i,j,k,1) - bface(i-1,j,k,1)
                    divBy = bface(i,j,k,2) - bface(i,j-1,k,2)
                    divBz = bface(i,j,k,3) - bface(i,j,k-1,3)
                    Smx   = prot_q(i,j,k,5) * divBx
                    Smy   = prot_q(i,j,k,6) * divBx
                    Smz   = prot_q(i,j,k,7) * divBx
                    mmzx  = 0.0_WRD
                    IF ((-divBx * divBz) .GT. 0.0_WRD) mmzx = SIGN(1.0_WRD,divBz) * MIN(ABS(divBx), ABS(divBz))
                    mmyx  = 0.0_WRD
                    IF ((-divBx * divBy) .GT. 0.0_WRD) mmyx = SIGN(1.0_WRD,divBy) * MIN(ABS(divBx), ABS(divBy))
                    Se    = -(prot_q(i,j,k,6) * prot_q(i,j,k,3) / prot_q(i,j,k,1)) * mmzx - (prot_q(i,j,k,7) * prot_q(i,j,k,4) / prot_q(i,j,k,1)) * mmyx
                    Sby   = (prot_q(i,j,k,3) / prot_q(i,j,k,1)) * mmzx
                    Sbz   = (prot_q(i,j,k,4) / prot_q(i,j,k,1)) * mmyx
                
                    q(i,j,k,1) = prot_q(i,j,k,1) - (dtdx * ((fluxes(i,j,k,1,2) - fluxes(i,j-1,k,1,2)) + (fluxes(i,j,k,1,3) - fluxes(i,j,k-1,1,3))))
                    q(i,j,k,2) = prot_q(i,j,k,2) - (dtdx * ((fluxes(i,j,k,2,2) - fluxes(i,j-1,k,2,2)) + (fluxes(i,j,k,2,3) - fluxes(i,j,k-1,2,3)))) + dtdx * Smx
                    q(i,j,k,3) = prot_q(i,j,k,3) - (dtdx * ((fluxes(i,j,k,3,2) - fluxes(i,j-1,k,3,2)) + (fluxes(i,j,k,3,3) - fluxes(i,j,k-1,3,3)))) + dtdx * Smy
                    q(i,j,k,4) = prot_q(i,j,k,4) - (dtdx * ((fluxes(i,j,k,4,2) - fluxes(i,j-1,k,4,2)) + (fluxes(i,j,k,4,3) - fluxes(i,j,k-1,4,3)))) + dtdx * Smz

                    q(i,j,k,5) = prot_q(i,j,k,5) - (dtdx * ((fluxes(i,j,k,5,2) - fluxes(i,j-1,k,5,2)) + (fluxes(i,j,k,5,3) - fluxes(i,j,k-1,5,3))))
                    q(i,j,k,6) = prot_q(i,j,k,6) - 0.5_WRD * dtdx * ((emf(i,j,k,1) - emf(i,j,k-1,1)) + (emf(i,j-1,k,1) - emf(i,j-1,k-1,1))) + dtdx * Sby
                    q(i,j,k,7) = prot_q(i,j,k,7) + 0.5_WRD * dtdx * ((emf(i,j,k,1) - emf(i,j-1,k,1)) + (emf(i,j,k-1,1) - emf(i,j-1,k-1,1))) + dtdx * Sbz
                    
                    q(i,j,k,8) = prot_q(i,j,k,8) - (dtdx * ((fluxes(i,j,k,8,2) - fluxes(i,j-1,k,8,2)) + (fluxes(i,j,k,8,3) - fluxes(i,j,k-1,8,3)))) + dtdx * Se

                END IF

            END DO

        END DO

    END DO
    
    !### finally do a last check for unphysical values that came out of the update (should be exceedingly rare after protection fluxes) ###
    CALL self%apply_protections3d(q, ilow, ihi, jlow, jhi, klow, khi)

END SUBROUTINE halfXupdate_states3d

!### apply a half time step update to a plane using transverse fluxes computed from the initial state and apply the div(B) source term ###
SUBROUTINE halfYupdate_states3d(self, dt, dx, q, fluxes, bface, emf, prot_q, prot_fluxes, prot_emf, prot_flags, klow, khi, ilow, ihi, jlow, jhi)

    CLASS(MHDTVD) :: self
    REAL(WRD), INTENT(IN) :: dt, dx
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: bface(:,:,:,:), prot_fluxes(:,:,:,:,:), prot_emf(:,:,:,:)
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: q(:,:,:,:), prot_q(:,:,:,:), emf(:,:,:,:), fluxes(:,:,:,:,:)
    LOGICAL(WIS), CONTIGUOUS, INTENT(INOUT) :: prot_flags(:,:,:)
    INTEGER(WIS), INTENT(IN) :: klow, khi, jlow, jhi, ilow, ihi
    REAL(WRD) :: dtdx, idx, Smx, Smy, Smz, Sbx, Sbz, Se, divBx, divBy, divBz, mmzy, mmxy, p, gamma
    INTEGER(WIS) :: i, j, k

    idx   = 1.0_WRD / dx
    dtdx  = 0.5_WRD * dt * idx
    gamma = self%gamma

    prot_flags = .FALSE.

    !### perform the update ###
    DO k = klow+1, khi

        DO j = jlow+2, jhi-2

            !### save the current values in case this update causes unphysical values ###
            prot_q(ilow+1:ihi,j,k,:) = q(ilow+1:ihi,j,k,:)

            DO i = ilow+1, ihi

                !### derive the divB source terms ###
                divBx = bface(i,j,k,1) - bface(i-1,j,k,1)
                divBy = bface(i,j,k,2) - bface(i,j-1,k,2)
                divBz = bface(i,j,k,3) - bface(i,j,k-1,3)
                Smx   = q(i,j,k,5) * divBy
                Smy   = q(i,j,k,6) * divBy
                Smz   = q(i,j,k,7) * divBy
                mmzy  = 0.0_WRD
                IF ((-divBy * divBz) .GT. 0.0_WRD) mmzy = SIGN(1.0_WRD,divBz) * MIN(ABS(divBy), ABS(divBz))
                mmxy  = 0.0_WRD
                IF ((-divBy * divBx) .GT. 0.0_WRD) mmxy = SIGN(1.0_WRD,divBx) * MIN(ABS(divBy), ABS(divBx))
                Se    = -(q(i,j,k,7) * q(i,j,k,4) / q(i,j,k,1)) * mmxy - (q(i,j,k,5) * q(i,j,k,2) / q(i,j,k,1)) * mmzy
                Sbx   = (q(i,j,k,2) / q(i,j,k,1)) * mmzy
                Sbz   = (q(i,j,k,4) / q(i,j,k,1)) * mmxy
            
                q(i,j,k,1) = q(i,j,k,1) - (dtdx * ((fluxes(i,j,k,1,1) - fluxes(i-1,j,k,1,1)) + (fluxes(i,j,k,1,3) - fluxes(i,j,k-1,1,3))))
                q(i,j,k,2) = q(i,j,k,2) - (dtdx * ((fluxes(i,j,k,2,1) - fluxes(i-1,j,k,2,1)) + (fluxes(i,j,k,2,3) - fluxes(i,j,k-1,2,3)))) + dtdx * Smx
                q(i,j,k,3) = q(i,j,k,3) - (dtdx * ((fluxes(i,j,k,3,1) - fluxes(i-1,j,k,3,1)) + (fluxes(i,j,k,3,3) - fluxes(i,j,k-1,3,3)))) + dtdx * Smy
                q(i,j,k,4) = q(i,j,k,4) - (dtdx * ((fluxes(i,j,k,4,1) - fluxes(i-1,j,k,4,1)) + (fluxes(i,j,k,4,3) - fluxes(i,j,k-1,4,3)))) + dtdx * Smz

                q(i,j,k,5) = q(i,j,k,5) + 0.5_WRD * dtdx * ((emf(i,j,k,2) - emf(i,j,k-1,2)) + (emf(i-1,j,k,2) - emf(i-1,j,k-1,2))) + dtdx * Sbx
                q(i,j,k,6) = q(i,j,k,6) - (dtdx * ((fluxes(i,j,k,6,1) - fluxes(i-1,j,k,6,1)) + (fluxes(i,j,k,6,3) - fluxes(i,j,k-1,6,3))))
                q(i,j,k,7) = q(i,j,k,7) - 0.5_WRD * dtdx * ((emf(i,j,k,2) - emf(i-1,j,k,2)) + (emf(i,j,k-1,2) - emf(i-1,j,k-1,2))) + dtdx * Sbz
                                   
                q(i,j,k,8) = q(i,j,k,8) - (dtdx * ((fluxes(i,j,k,8,1) - fluxes(i-1,j,k,8,1)) + (fluxes(i,j,k,8,3) - fluxes(i,j,k-1,8,3)))) + dtdx * Se

            END DO

        END DO

    END DO

    !### look for unphysical density and pressure ###
    DO k = klow+1, khi

        DO j = jlow+2, jhi-2

            DO i = ilow+1, ihi

                p = (gamma - 1.0_WRD) * (q(i,j,k,8) - 0.5_WRD * (q(i,j,k,5)**2 + q(i,j,k,6)**2 + q(i,j,k,7)**2) - (0.5_WRD / MAX(q(i,j,k,1), self%prot_min_density)) * (q(i,j,k,2)**2 + q(i,j,k,3)**2 + q(i,j,k,4)**2))

                !### for these updates we just set the flag and do not replace the fluxes as we confine protection to just this partial update ###
                IF (q(i,j,k,1) .LT. self%prot_min_density .OR. p .LT. self%prot_min_pressure) THEN

                    prot_flags(i,j,k)   = .TRUE.
                    fluxes(i,j,k,:,1)   = prot_fluxes(i,j,k,:,1)
                    fluxes(i-1,j,k,:,1) = prot_fluxes(i-1,j,k,:,1)
                    fluxes(i,j,k,:,3)   = prot_fluxes(i,j,k,:,3)
                    fluxes(i,j,k-1,:,3) = prot_fluxes(i,j,k-1,:,3)

                    emf(i,j,k,2)        = prot_emf(i,j,k,2)
                    emf(i,j,k-1,2)      = prot_emf(i,j,k-1,2)
                    emf(i-1,j,k-1,2)    = prot_emf(i-1,j,k-1,2)
                    emf(i-1,j,k,2)      = prot_emf(i-1,j,k,2)

                END IF

            END DO

        END DO

    END DO

    !### apply the protection fluxes if needed ###
    DO k = klow+1, khi

        DO j = jlow+2, jhi-2
            
            DO i = ilow+1, ihi

                IF (prot_flags(i,j,k) .OR. prot_flags(i-1,j,k) .OR. prot_flags(i+1,j,k) .OR. prot_flags(i,j,k-1) .OR. prot_flags(i,j,k+1)) THEN
                
                    !### derive the divB source terms ###
                    divBx = bface(i,j,k,1) - bface(i-1,j,k,1)
                    divBy = bface(i,j,k,2) - bface(i,j-1,k,2)
                    divBz = bface(i,j,k,3) - bface(i,j,k-1,3)
                    Smx   = prot_q(i,j,k,5) * divBy
                    Smy   = prot_q(i,j,k,6) * divBy
                    Smz   = prot_q(i,j,k,7) * divBy
                    mmzy  = 0.0_WRD
                    IF ((-divBy * divBz) .GT. 0.0_WRD) mmzy = SIGN(1.0_WRD,divBz) * MIN(ABS(divBy), ABS(divBz))
                    mmxy  = 0.0_WRD
                    IF ((-divBy * divBx) .GT. 0.0_WRD) mmxy = SIGN(1.0_WRD,divBx) * MIN(ABS(divBy), ABS(divBx))
                    Se    = -(prot_q(i,j,k,7) * prot_q(i,j,k,4) / prot_q(i,j,k,1)) * mmxy - (prot_q(i,j,k,5) * prot_q(i,j,k,2) / prot_q(i,j,k,1)) * mmzy
                    Sbx   = (prot_q(i,j,k,2) / prot_q(i,j,k,1)) * mmzy
                    Sbz   = (prot_q(i,j,k,4) / prot_q(i,j,k,1)) * mmxy
                    
                    q(i,j,k,1) = prot_q(i,j,k,1) - (dtdx * ((fluxes(i,j,k,1,1) - fluxes(i-1,j,k,1,1)) + (fluxes(i,j,k,1,3) - fluxes(i,j,k-1,1,3))))
                    q(i,j,k,2) = prot_q(i,j,k,2) - (dtdx * ((fluxes(i,j,k,3,1) - fluxes(i-1,j,k,3,1)) + (fluxes(i,j,k,3,3) - fluxes(i,j,k-1,3,3)))) + dtdx * Smx
                    q(i,j,k,3) = prot_q(i,j,k,3) - (dtdx * ((fluxes(i,j,k,2,1) - fluxes(i-1,j,k,2,1)) + (fluxes(i,j,k,2,3) - fluxes(i,j,k-1,2,3)))) + dtdx * Smy
                    q(i,j,k,4) = prot_q(i,j,k,4) - (dtdx * ((fluxes(i,j,k,4,1) - fluxes(i-1,j,k,4,1)) + (fluxes(i,j,k,4,3) - fluxes(i,j,k-1,4,3)))) + dtdx * Smz

                    q(i,j,k,5) = q(i,j,k,5) + 0.5_WRD * dtdx * ((emf(i,j,k,2) - emf(i,j,k-1,2)) + (emf(i-1,j,k,2) - emf(i-1,j,k-1,2))) + dtdx * Sbx
                    q(i,j,k,6) = q(i,j,k,6) - (dtdx * ((fluxes(i,j,k,6,1) - fluxes(i-1,j,k,6,1)) + (fluxes(i,j,k,6,3) - fluxes(i,j,k-1,6,3))))
                    q(i,j,k,7) = q(i,j,k,7) - 0.5_WRD * dtdx * ((emf(i,j,k,2) - emf(i-1,j,k,2)) + (emf(i,j,k-1,2) - emf(i-1,j,k-1,2))) + dtdx * Sbz
                                            
                    q(i,j,k,8) = prot_q(i,j,k,8) - (dtdx * ((fluxes(i,j,k,8,1) - prot_fluxes(i-1,j,k,8,1)) + (fluxes(i,j,k,8,3) - fluxes(i,j,k-1,8,3)))) + dtdx * Se

                END IF

            END DO

        END DO

    END DO
    
    !### finally do a last check for unphysical values that came out of the update (should be exceedingly rare after protection fluxes) ###
    CALL self%apply_protections3d(q, ilow, ihi, jlow, jhi, klow, khi)

END SUBROUTINE halfYupdate_states3d

!### apply a half time step update to a plane using transverse fluxes computed from the initial state and apply the div(B) source term ###
SUBROUTINE halfZupdate_states3d(self, dt, dx, q, fluxes, bface, emf, prot_q, prot_fluxes, prot_emf, prot_flags, jlow, jhi, ilow, ihi, klow, khi)

    CLASS(MHDTVD) :: self
    REAL(WRD), INTENT(IN) :: dt, dx
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: bface(:,:,:,:), prot_fluxes(:,:,:,:,:), prot_emf(:,:,:,:)
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: q(:,:,:,:), prot_q(:,:,:,:), emf(:,:,:,:), fluxes(:,:,:,:,:)
    LOGICAL(WIS), CONTIGUOUS, INTENT(INOUT) :: prot_flags(:,:,:)
    INTEGER(WIS), INTENT(IN) :: jlow, jhi, klow, khi, ilow, ihi
    REAL(WRD) :: dtdx, idx, Smx, Smy, Smz, Sbx, Sby, Se, divBx, divBy, divBz, mmyz, mmxz, p, gamma
    INTEGER(WIS) :: i, j, k

    idx   = 1.0_WRD / dx
    dtdx  = 0.5_WRD * dt * idx
    gamma = self%gamma

    prot_flags = .FALSE.

    !### perform the update ###
    DO k = klow+2, khi-2

        DO j = jlow+1, jhi

            !### save the current values in case this update causes unphysical values ###
            prot_q(ilow+1:ihi,j,k,:) = q(ilow+1:ihi,j,k,:)

            DO i = ilow+1, ihi

                !### derive the divB source terms ###
                divBx = bface(i,j,k,1) - bface(i-1,j,k,1)
                divBy = bface(i,j,k,2) - bface(i,j-1,k,2)
                divBz = bface(i,j,k,3) - bface(i,j,k-1,3)
                Smx   = q(i,j,k,5) * divBz
                Smy   = q(i,j,k,6) * divBz
                Smz   = q(i,j,k,7) * divBz
                mmyz  = 0.0_WRD
                IF ((-divBz * divBy) .GT. 0.0_WRD) mmyz = SIGN(1.0_WRD,divBy) * MIN(ABS(divBz), ABS(divBy))
                mmxz  = 0.0_WRD
                IF ((-divBz * divBx) .GT. 0.0_WRD) mmxz = SIGN(1.0_WRD,divBx) * MIN(ABS(divBz), ABS(divBx))
                Se    = -(q(i,j,k,5) * q(i,j,k,2) / q(i,j,k,1)) * mmyz - (q(i,j,k,6) * q(i,j,k,3) / q(i,j,k,1)) * mmxz
                Sbx   = (q(i,j,k,2) / q(i,j,k,1)) * mmyz
                Sby   = (q(i,j,k,3) / q(i,j,k,1)) * mmxz
            
                q(i,j,k,1) = q(i,j,k,1) - (dtdx * ((fluxes(i,j,k,1,2) - fluxes(i,j-1,k,1,2)) + (fluxes(i,j,k,1,1) - fluxes(i-1,j,k,1,1))))
                q(i,j,k,2) = q(i,j,k,2) - (dtdx * ((fluxes(i,j,k,2,2) - fluxes(i,j-1,k,2,2)) + (fluxes(i,j,k,2,1) - fluxes(i-1,j,k,2,1)))) + dtdx * Smx
                q(i,j,k,3) = q(i,j,k,3) - (dtdx * ((fluxes(i,j,k,3,2) - fluxes(i,j-1,k,3,2)) + (fluxes(i,j,k,3,1) - fluxes(i-1,j,k,3,1)))) + dtdx * Smy
                q(i,j,k,4) = q(i,j,k,4) - (dtdx * ((fluxes(i,j,k,4,2) - fluxes(i,j-1,k,4,2)) + (fluxes(i,j,k,4,1) - fluxes(i-1,j,k,4,1)))) + dtdx * Smz
 
                q(i,j,k,5) = q(i,j,k,5) - 0.5_WRD * dtdx * ((emf(i,j,k,3) - emf(i,j-1,k,3)) + (emf(i-1,j,k,3) - emf(i-1,j-1,k,3))) + dtdx * Sbx
                q(i,j,k,6) = q(i,j,k,6) + 0.5_WRD * dtdx * ((emf(i,j,k,3) - emf(i-1,j,k,3)) + (emf(i,j-1,k,3) - emf(i-1,j-1,k,3))) + dtdx * Sby
                q(i,j,k,7) = q(i,j,k,7) - (dtdx * ((fluxes(i,j,k,7,1) - fluxes(i-1,j,k,7,1)) + (fluxes(i,j,k,7,2) - fluxes(i,j-1,k,7,2))))
                                   
                q(i,j,k,8) = q(i,j,k,8) - (dtdx * ((fluxes(i,j,k,8,2) - fluxes(i,j-1,k,8,2)) + (fluxes(i,j,k,8,1) - fluxes(i-1,j,k,8,1)))) + dtdx * Se

            END DO

        END DO

    END DO

    !### look for unphysical density and pressure ###
    DO k = klow+2, khi-2

        DO j = jlow+1, jhi

            DO i = ilow+1, ihi

                p = (gamma - 1.0_WRD) * (q(i,j,k,8) - 0.5_WRD * (q(i,j,k,5)**2 + q(i,j,k,6)**2 + q(i,j,k,7)**2) - (0.5_WRD / MAX(q(i,j,k,1), self%prot_min_density)) * (q(i,j,k,2)**2 + q(i,j,k,3)**2 + q(i,j,k,4)**2))

                !### for these updates we just set the flag and do not replace the fluxes as we confine protection to just this partial update ###
                IF (q(i,j,k,1) .LT. self%prot_min_density .OR. p .LT. self%prot_min_pressure) THEN

                    prot_flags(i,j,k) = .TRUE.
                    fluxes(i,j,k,:,1)   = prot_fluxes(i,j,k,:,1)
                    fluxes(i-1,j,k,:,1) = prot_fluxes(i-1,j,k,:,1)
                    fluxes(i,j,k,:,2)   = prot_fluxes(i,j,k,:,2)
                    fluxes(i,j-1,k,:,2) = prot_fluxes(i,j-1,k,:,2)

                    emf(i,j,k,3)        = prot_emf(i,j,k,3)
                    emf(i,j-1,k,3)      = prot_emf(i,j-1,k,3)
                    emf(i-1,j,k,3)      = prot_emf(i-1,j,k,3)
                    emf(i-1,j-1,k,3)    = prot_emf(i-1,j-1,k,3)

                END IF

            END DO

        END DO

    END DO

    !### apply the protection fluxes if needed ###
    DO k = klow+2, khi-2

        DO j = jlow+1, jhi

            DO i = ilow+1, ihi

                IF (prot_flags(i,j,k) .OR. prot_flags(i-1,j,k) .OR. prot_flags(i+1,j,k) .OR. prot_flags(i,j-1,k) .OR. prot_flags(i,j+1,k)) THEN

                    !### derive the divB source terms ###
                    divBx = bface(i,j,k,1) - bface(i-1,j,k,1)
                    divBy = bface(i,j,k,2) - bface(i,j-1,k,2)
                    divBz = bface(i,j,k,3) - bface(i,j,k-1,3)
                    Smx   = prot_q(i,j,k,5) * divBz
                    Smy   = prot_q(i,j,k,6) * divBz
                    Smz   = prot_q(i,j,k,7) * divBz
                    mmyz  = 0.0_WRD
                    IF ((-divBz * divBy) .GT. 0.0_WRD) mmyz = SIGN(1.0_WRD,divBy) * MIN(ABS(divBz), ABS(divBy))
                    mmxz  = 0.0_WRD
                    IF ((-divBz * divBx) .GT. 0.0_WRD) mmxz = SIGN(1.0_WRD,divBx) * MIN(ABS(divBz), ABS(divBx))
                    Se    = -(prot_q(i,j,k,5) * prot_q(i,j,k,2) / prot_q(i,j,k,1)) * mmyz - (prot_q(i,j,k,6) * prot_q(i,j,k,3) / prot_q(i,j,k,1)) * mmxz
                    Sbx   = (prot_q(i,j,k,2) / prot_q(i,j,k,1)) * mmyz
                    Sby   = (prot_q(i,j,k,3) / prot_q(i,j,k,1)) * mmxz
            
                    q(i,j,k,1) = prot_q(i,j,k,1) - (dtdx * ((prot_fluxes(i,j,k,1,2) - prot_fluxes(i,j-1,k,1,2)) + (prot_fluxes(i,j,k,1,1) - prot_fluxes(i-1,j,k,1,1))))
                    q(i,j,k,2) = prot_q(i,j,k,2) - (dtdx * ((prot_fluxes(i,j,k,4,2) - prot_fluxes(i,j-1,k,4,2)) + (prot_fluxes(i,j,k,4,1) - prot_fluxes(i-1,j,k,4,1)))) + dtdx * Smx
                    q(i,j,k,3) = prot_q(i,j,k,3) - (dtdx * ((prot_fluxes(i,j,k,2,2) - prot_fluxes(i,j-1,k,2,2)) + (prot_fluxes(i,j,k,2,1) - prot_fluxes(i-1,j,k,2,1)))) + dtdx * Smy
                    q(i,j,k,4) = prot_q(i,j,k,4) - (dtdx * ((prot_fluxes(i,j,k,3,2) - prot_fluxes(i,j-1,k,3,2)) + (prot_fluxes(i,j,k,3,1) - prot_fluxes(i-1,j,k,3,1)))) + dtdx * Smz

                    q(i,j,k,5) = q(i,j,k,5) - 0.5_WRD * dtdx * ((emf(i,j,k,3) - emf(i,j-1,k,3)) + (emf(i-1,j,k,3) - emf(i-1,j-1,k,3))) + dtdx * Sbx
                    q(i,j,k,6) = q(i,j,k,6) + 0.5_WRD * dtdx * ((emf(i,j,k,3) - emf(i-1,j,k,3)) + (emf(i,j-1,k,3) - emf(i-1,j-1,k,3))) + dtdx * Sby
                    q(i,j,k,7) = q(i,j,k,7) - (dtdx * ((fluxes(i,j,k,7,1) - fluxes(i-1,j,k,7,1)) + (fluxes(i,j,k,7,2) - fluxes(i,j-1,k,7,2))))
                                       
                    q(i,j,k,8) = prot_q(i,j,k,8) - (dtdx * ((prot_fluxes(i,j,k,8,2) - prot_fluxes(i,j-1,k,8,2)) + (prot_fluxes(i,j,k,8,1) - prot_fluxes(i-1,j,k,8,1)))) + dtdx * Se

                END IF

            END DO

        END DO

    END DO
    
    !### finally do a last check for unphysical values that came out of the update (should be exceedingly rare after protection fluxes) ###
    CALL self%apply_protections3d(q, ilow, ihi, jlow, jhi, klow, khi)

END SUBROUTINE halfZupdate_states3d

!### actually update the state variables given fluxes ###
SUBROUTINE update_states3d(self, dt, dx, q, fluxes, emf, prot_q, prot_fluxes, prot_emf, prot_flags, ilow, ihi, jlow, jhi, klow, khi)

    CLASS(MHDTVD) :: self
    REAL(WRD), INTENT(IN) :: dt, dx
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, jlow, jhi, klow, khi
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: prot_fluxes(:,:,:,:,:), prot_emf(:,:,:,:)
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: q(:,:,:,:), prot_q(:,:,:,:), emf(:,:,:,:), fluxes(:,:,:,:,:)
    LOGICAL(WIS), CONTIGUOUS, INTENT(INOUT) :: prot_flags(:,:,:)
    REAL(WRD) :: dtdx, p, gamma
    INTEGER(WIS) :: i, j, k

    dtdx  = dt / dx
    gamma = self%gamma

    prot_flags = .FALSE.

    !### apply the directionally un-split update ###
    DO k = klow+2, khi-2

        DO j = jlow+2, jhi-2
            
            !### save the current values in case this update causes unphysical values ###
            prot_q(ilow+2:ihi-2,j,k,:) = q(ilow+2:ihi-2,j,k,:)

            DO i = ilow+2, ihi-2
                
                q(i,j,k,1) = q(i,j,k,1) - (dtdx * ((fluxes(i,j,k,1,3) - fluxes(i,j,k-1,1,3)) + (fluxes(i,j,k,1,2) - fluxes(i,j-1,k,1,2)) + (fluxes(i,j,k,1,1) - fluxes(i-1,j,k,1,1))))
                q(i,j,k,2) = q(i,j,k,2) - (dtdx * ((fluxes(i,j,k,2,3) - fluxes(i,j,k-1,2,3)) + (fluxes(i,j,k,2,2) - fluxes(i,j-1,k,2,2)) + (fluxes(i,j,k,2,1) - fluxes(i-1,j,k,2,1))))
                q(i,j,k,3) = q(i,j,k,3) - (dtdx * ((fluxes(i,j,k,3,3) - fluxes(i,j,k-1,3,3)) + (fluxes(i,j,k,3,2) - fluxes(i,j-1,k,3,2)) + (fluxes(i,j,k,3,1) - fluxes(i-1,j,k,3,1))))
                q(i,j,k,4) = q(i,j,k,4) - (dtdx * ((fluxes(i,j,k,4,3) - fluxes(i,j,k-1,4,3)) + (fluxes(i,j,k,4,2) - fluxes(i,j-1,k,4,2)) + (fluxes(i,j,k,4,1) - fluxes(i-1,j,k,4,1))))
                q(i,j,k,5) = q(i,j,k,5) - (dtdx * ((fluxes(i,j,k,5,3) - fluxes(i,j,k-1,5,3)) + (fluxes(i,j,k,5,2) - fluxes(i,j-1,k,5,2)) + (fluxes(i,j,k,5,1) - fluxes(i-1,j,k,5,1))))
                q(i,j,k,6) = q(i,j,k,6) - (dtdx * ((fluxes(i,j,k,6,3) - fluxes(i,j,k-1,6,3)) + (fluxes(i,j,k,6,2) - fluxes(i,j-1,k,6,2)) + (fluxes(i,j,k,6,1) - fluxes(i-1,j,k,6,1))))
                q(i,j,k,7) = q(i,j,k,7) - (dtdx * ((fluxes(i,j,k,7,3) - fluxes(i,j,k-1,7,3)) + (fluxes(i,j,k,7,2) - fluxes(i,j-1,k,7,2)) + (fluxes(i,j,k,7,1) - fluxes(i-1,j,k,7,1))))
                q(i,j,k,8) = q(i,j,k,8) - (dtdx * ((fluxes(i,j,k,8,3) - fluxes(i,j,k-1,8,3)) + (fluxes(i,j,k,8,2) - fluxes(i,j-1,k,8,2)) + (fluxes(i,j,k,8,1) - fluxes(i-1,j,k,8,1))))
            
            END DO

        END DO
            
    END DO

    !### look for unphysical density and pressure ###
    DO k = klow+2, khi-2

        DO j = jlow+2, jhi-2

            DO i = ilow+2, ihi-2

                p = (gamma - 1.0_WRD) * (q(i,j,k,8) - 0.5_WRD * (q(i,j,k,5)**2 + q(i,j,k,6)**2 + q(i,j,k,7)**2) - (0.5_WRD / MAX(q(i,j,k,1), self%prot_min_density)) * (q(i,j,k,2)**2 + q(i,j,k,3)**2 + q(i,j,k,4)**2))

                !### flag that protection is needed and replace EMFs ###
                IF (q(i,j,k,1) .LT. self%prot_min_density .OR. p .LT. self%prot_min_pressure) THEN

                    prot_flags(i,j,k) = .TRUE.
                    fluxes(i,j,k,:,1)   = prot_fluxes(i,j,k,:,1)
                    fluxes(i-1,j,k,:,1) = prot_fluxes(i-1,j,k,:,1)
                    fluxes(i,j,k,:,2)   = prot_fluxes(i,j,k,:,2)
                    fluxes(i,j-1,k,:,2) = prot_fluxes(i,j-1,k,:,2)
                    fluxes(i,j,k,:,3)   = prot_fluxes(i,j,k,:,3)
                    fluxes(i,j,k-1,:,3) = prot_fluxes(i,j,k-1,:,3)

                    !### we also need to swap in the protection EMFs ###
                    emf(i,j,k,1)   = prot_emf(i,j,k,1)
                    emf(i,j,k,2)   = prot_emf(i,j,k,2)
                    emf(i,j,k,3)   = prot_emf(i,j,k,3)
                    
                    emf(i-1,j,k,1) = prot_emf(i-1,j,k,1)
                    emf(i-1,j,k,2) = prot_emf(i-1,j,k,2)
                    emf(i-1,j,k,3) = prot_emf(i-1,j,k,3)
                    
                    emf(i,j-1,k,1) = prot_emf(i,j-1,k,1)
                    emf(i,j-1,k,2) = prot_emf(i,j-1,k,2)
                    emf(i,j-1,k,3) = prot_emf(i,j-1,k,3)
                    
                    emf(i,j,k-1,1) = prot_emf(i,j,k-1,1)
                    emf(i,j,k-1,2) = prot_emf(i,j,k-1,2)
                    emf(i,j,k-1,3) = prot_emf(i,j,k-1,3)

                END IF

            END DO

        END DO

    END DO
 
    !### apply the protection fluxes if needed ###
    DO k = klow+2, khi-2

        DO j = jlow+2, jhi-2
            
            DO i = ilow+2, ihi-2

                IF (prot_flags(i,j,k) .OR. prot_flags(i-1,j,k) .OR. prot_flags(i+1,j,k) .OR. prot_flags(i,j-1,k) .OR. prot_flags(i,j+1,k) .OR. prot_flags(i,j,k-1) .OR. prot_flags(i,j,k+1)) THEN
                
                    q(i,j,k,1) = prot_q(i,j,k,1) - (dtdx * ((fluxes(i,j,k,1,3) - fluxes(i,j,k-1,1,3)) + (fluxes(i,j,k,1,2) - fluxes(i,j-1,k,1,2)) + (fluxes(i,j,k,1,1) - fluxes(i-1,j,k,1,1))))
                    q(i,j,k,2) = prot_q(i,j,k,2) - (dtdx * ((fluxes(i,j,k,2,3) - fluxes(i,j,k-1,2,3)) + (fluxes(i,j,k,2,2) - fluxes(i,j-1,k,2,2)) + (fluxes(i,j,k,2,1) - fluxes(i-1,j,k,2,1))))
                    q(i,j,k,3) = prot_q(i,j,k,3) - (dtdx * ((fluxes(i,j,k,3,3) - fluxes(i,j,k-1,3,3)) + (fluxes(i,j,k,3,2) - fluxes(i,j-1,k,3,2)) + (fluxes(i,j,k,3,1) - fluxes(i-1,j,k,3,1))))
                    q(i,j,k,4) = prot_q(i,j,k,4) - (dtdx * ((fluxes(i,j,k,4,3) - fluxes(i,j,k-1,4,3)) + (fluxes(i,j,k,4,2) - fluxes(i,j-1,k,4,2)) + (fluxes(i,j,k,4,1) - fluxes(i-1,j,k,4,1))))
                    q(i,j,k,5) = prot_q(i,j,k,5) - (dtdx * ((fluxes(i,j,k,5,3) - fluxes(i,j,k-1,5,3)) + (fluxes(i,j,k,5,2) - fluxes(i,j-1,k,5,2)) + (fluxes(i,j,k,5,1) - fluxes(i-1,j,k,5,1))))
                    q(i,j,k,6) = prot_q(i,j,k,6) - (dtdx * ((fluxes(i,j,k,6,3) - fluxes(i,j,k-1,6,3)) + (fluxes(i,j,k,6,2) - fluxes(i,j-1,k,6,2)) + (fluxes(i,j,k,6,1) - fluxes(i-1,j,k,6,1))))
                    q(i,j,k,7) = prot_q(i,j,k,7) - (dtdx * ((fluxes(i,j,k,7,3) - fluxes(i,j,k-1,7,3)) + (fluxes(i,j,k,7,2) - fluxes(i,j-1,k,7,2)) + (fluxes(i,j,k,7,1) - fluxes(i-1,j,k,7,1))))
                    q(i,j,k,8) = prot_q(i,j,k,8) - (dtdx * ((fluxes(i,j,k,8,3) - fluxes(i,j,k-1,8,3)) + (fluxes(i,j,k,8,2) - fluxes(i,j-1,k,8,2)) + (fluxes(i,j,k,8,1) - fluxes(i-1,j,k,8,1))))
            
                END IF

            END DO

        END DO
            
    END DO
    
    !### finally do a last check for unphysical values that came out of the update (should be exceedingly rare after protection fluxes) ###
    CALL self%apply_protections3d(q, ilow, ihi, jlow, jhi, klow, khi)

END SUBROUTINE update_states3d

!### the following routines are the CT algorithms for 2d and 3d ###

!### compute the zone centered electric field optionally from already computed fluxes ###
SUBROUTINE compute_reference_emf2d(self, dt, dx, q, fluxes, emf, ilow, ihi, jlow, jhi, halfdt)

    CLASS(MHDTVD) :: self
    REAL(WRD), INTENT(IN) :: dt, dx
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: q(:,:,:),fluxes(:,:,:,:,:)
    REAL(WRD), INTENT(OUT) :: emf(:,:,:)
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, jlow, jhi
    LOGICAL(WIS), INTENT(IN) :: halfdt
    REAL(WRD) :: den, momx, momy, dtdx, iden
    INTEGER(WIS) :: i, j
    
    !### if we are to first apply a half dt update we must work with the primary fluxes we have (flux exchanges will take care of the rest) ###
    IF (halfdt) THEN

        dtdx = 0.5_WRD * dt / dx

        DO j = jlow+1, jhi-1

            DO i = ilow+1, ihi-1

                !### compute the directonally unsplit estimate of the density anf X/Y momentum for use in the reference emf ###
                den  = MAX(self%min_density, q(i,j,1) - (dtdx * ((fluxes(i,j,1,1,1) - fluxes(i-1,j,1,1,1)) + (fluxes(i,j,1,1,2) - fluxes(i,j-1,1,1,2)))))
                momx = q(i,j,2) - (dtdx * ((fluxes(i,j,1,2,1) - fluxes(i-1,j,1,2,1)) + (fluxes(i,j,1,2,2) - fluxes(i,j-1,1,2,2))))
                momy = q(i,j,3) - (dtdx * ((fluxes(i,j,1,3,1) - fluxes(i-1,j,1,3,1)) + (fluxes(i,j,1,3,2) - fluxes(i,j-1,1,3,2))))
                iden = 1.0_WRD / den

                emf(i,j,3) = iden * (momy * q(i,j,5) - momx * q(i,j,6))

            END DO

        END DO

    ELSE

        DO j = jlow, jhi

            DO i = ilow, ihi

                iden       = 1.0_WRD / q(i,j,1)
                emf(i,j,3) = iden * (q(i,j,3) * q(i,j,5) - q(i,j,2) * q(i,j,6))

            END DO

        END DO

    END IF

END SUBROUTINE compute_reference_emf2d

!### this routine will interpolate reference and face centered emfs to cell corner ###
SUBROUTINE compute_corner_emf2d(self, q, emf, omega, ilow, ihi, jlow, jhi)

    CLASS(MHDTVD) :: self
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, jlow, jhi
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: emf(:,:,:)
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: q(:,:,:)
    REAL(WRD), CONTIGUOUS, INTENT(OUT) :: omega(:,:,:)
    REAL(WRD) :: idx, dtdx, dyomega_12_14, dyomega_12_34, dxomega_14_12, dxomega_34_12, &
         dyomegal_14, dxomegal_14, dyomegal_34, dxomegal_34, &
         dyomegar_14, dxomegar_14, dyomegar_34, dxomegar_34, &
         rhol, rhor, idenl, idenr, rhorl, rhorr, vx, vy, zero
    INTEGER(WIS) :: i, j

    zero = self%zero_value

    !### compute the zone corner EMF ###
    DO j = jlow, jhi-2

        DO i = ilow, ihi-2

            !### get the sign of the contact mode in both X and Y using the CG weighting used in the Reimann solver ###
            rhol  = q(i,j,1)
            rhor  = q(i+1,j,1)
            idenl = 1.0_WRD / rhol
            idenr = 1.0_WRD / rhor
            rhorl = SQRT(rhol)
            rhorr = SQRT(rhor)
            vx    = (rhorl * q(i,j,2) * idenl) + (rhorr * q(i+1,j,2) * idenr)
            rhor  = q(i,j+1,1)
            idenr = 1.0_WRD / rhor
            rhorr = SQRT(rhor)
            vy    = (rhorl * q(i,j,3) * idenl) + (rhorr * q(i,j+1,3) * idenr)

            !### compute the derivatives in both X and Y we'll need for the upwinding portion ###
            dxomegal_14 =  emf(i,j,1) - emf(i,j,3)
            dyomegal_14 =  emf(i,j,2) - emf(i,j,3)
            dxomegal_34 =  emf(i+1,j,3) - emf(i,j,1)
            dyomegal_34 =  emf(i,j+1,3) - emf(i,j,2)

            dxomegar_14 =  emf(i,j+1,1) - emf(i,j+1,3)
            dyomegar_14 =  emf(i+1,j,2) - emf(i+1,j,3)
            dxomegar_34 =  emf(i+1,j+1,3) - emf(i,j+1,1)
            dyomegar_34 =  emf(i+1,j+1,3) - emf(i+1,j,2)

            !### upwind the Y integrals (the integral is in Y, but the contribution depends on the velocity in X) ###
            IF (vx .GT. zero) THEN

                dyomega_12_14 = dyomegal_14
                dyomega_12_34 = dyomegal_34

            ELSE IF (vx .LT. -zero) THEN

                dyomega_12_14 = dyomegar_14
                dyomega_12_34 = dyomegar_34

            ELSE

                dyomega_12_14 = 0.5_WRD * (dyomegal_14 + dyomegar_14)
                dyomega_12_34 = 0.5_WRD * (dyomegal_34 + dyomegar_34)

            END IF

            !### upwind the X integrals (the integral is in X, but the contribution depends on the velocity in Y) ###
            IF (vy .GT. zero) THEN

                dxomega_14_12 = dxomegal_14
                dxomega_34_12 = dxomegal_34

            ELSE IF (vy .LT. -zero) THEN

                dxomega_14_12 = dxomegar_14
                dxomega_34_12 = dxomegar_34

            ELSE

                dxomega_14_12 = 0.5_WRD * (dxomegal_14 + dxomegar_14)
                dxomega_34_12 = 0.5_WRD * (dxomegal_34 + dxomegar_34)

            END IF

            !### compute the final cell corner electric field ###
            omega(i,j,1) = 0.25_WRD * ((emf(i+1,j,2) + emf(i,j,2)) + (emf(i,j+1,1) + emf(i,j,1))) + &
                 0.25_WRD * ((dyomega_12_14 - dyomega_12_34) + (dxomega_14_12 - dxomega_34_12))

        END DO

    END DO

END SUBROUTINE compute_corner_emf2d

!### this routine accepts cell corner emfs and updates the face cenetered magnetic field ###
SUBROUTINE applyct2d(self, dt, dx, q, bface, omega, ilow, ihi, jlow, jhi, halfdt)

    CLASS(MHDTVD) :: self
    REAL(WRD), INTENT(IN) :: dt, dx
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, jlow, jhi
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: q(:,:,:), bface(:,:,:)
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: omega(:,:,:)
    LOGICAL(WIS), INTENT(IN) :: halfdt
    REAL(WRD) :: idx, dtdx
    INTEGER(WIS) :: i, j

    idx  = 1.0_WRD / dx
    dtdx = dt * idx
    IF (halfdt) dtdx = 0.5_WRD * dtdx

    !### update the face centered magnetic fields  ###
    DO j = jlow+1, jhi-2

        DO i = ilow+1, ihi-2

            !### update the face-centered magnetic field with Eq. 16 and 17 in RMJF98 ###
            bface(i,j,1) = bface(i,j,1) - dtdx * (omega(i,j,1) - omega(i,j-1,1))
            bface(i,j,2) = bface(i,j,2) + dtdx * (omega(i,j,1) - omega(i-1,j,1))

        END DO

    END DO

    !### now update the zone-centered magnetic field by interpolating the face-centered field ###
    IF (.NOT. halfdt) THEN

        DO j = jlow+2, jhi-2

            DO i = ilow+2, ihi-2

                !### use Eq. 3 & 4 in RJMF98, which is just a simple average ###
                q(i,j,5) = 0.5_WRD * (bface(i,j,1) + bface(i-1,j,1))
                q(i,j,6) = 0.5_WRD * (bface(i,j,2) + bface(i,j-1,2))

            END DO

        END DO

        !### finally do a last check for unphysical values that came out of the update (should be exceedingly rare after protection fluxes) ###
        CALL self%apply_protections2d(q, ilow, ihi, jlow, jhi)

    END IF

END SUBROUTINE applyct2d

!### compute the zone centered electric field optionally from already computed fluxes ###
SUBROUTINE compute_reference_emf3d(self, dt, dx, q, bface, fluxes, emf, ilow, ihi, jlow, jhi, klow, khi, halfdt)

    CLASS(MHDTVD) :: self
    REAL(WRD), INTENT(IN) :: dt, dx
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: bface(:,:,:,:), fluxes(:,:,:,:,:)
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: q(:,:,:,:)
    REAL(WRD), INTENT(OUT) :: emf(:,:,:,:,:)
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, jlow, jhi, klow, khi
    LOGICAL(WIS), INTENT(IN) :: halfdt
    REAL(WRD) :: den, momx, momy, momz, dtdx, iden
    INTEGER(WIS) :: i, j, k
 
    !### if we are to first apply a half dt update we must work with the primary fluxes we have (flux exchanges will take care of the rest) ###
    IF (halfdt) THEN

        dtdx = 0.5_WRD * dt / dx

        DO k = klow+1, khi

            DO j = jlow+1, jhi

                DO i = ilow+1, ihi

                    !### compute the directonally unsplit estimate of the density anf X/Y momentum for use in the reference emf ###
                    den  = MAX(self%min_density, q(i,j,k,1) - (dtdx * ((fluxes(i,j,k,1,1) - fluxes(i-1,j,k,1,1)) + (fluxes(i,j,k,1,2) - fluxes(i,j-1,k,1,2)) + (fluxes(i,j,k,1,3) - fluxes(i,j,k-1,1,3)))))
                    momx = q(i,j,k,2) - (dtdx * ((fluxes(i,j,k,2,1) - fluxes(i-1,j,k,2,1)) + (fluxes(i,j,k,2,2) - fluxes(i,j-1,k,2,2)) + (fluxes(i,j,k,2,3) - fluxes(i,j,k-1,2,3))))
                    momy = q(i,j,k,3) - (dtdx * ((fluxes(i,j,k,3,1) - fluxes(i-1,j,k,3,1)) + (fluxes(i,j,k,3,2) - fluxes(i,j-1,k,3,2)) + (fluxes(i,j,k,3,3) - fluxes(i,j,k-1,3,3))))
                    momz = q(i,j,k,4) - (dtdx * ((fluxes(i,j,k,4,1) - fluxes(i-1,j,k,4,1)) + (fluxes(i,j,k,4,2) - fluxes(i,j-1,k,4,2)) + (fluxes(i,j,k,4,3) - fluxes(i,j,k-1,4,3))))
                    iden = 1.0_WRD / den

                    emf(i,j,k,3,3) = iden * (momy * q(i,j,k,5) - momx * q(i,j,k,6))
                    emf(i,j,k,2,3) = iden * (momx * q(i,j,k,7) - momz * q(i,j,k,5))
                    emf(i,j,k,1,3) = iden * (momz * q(i,j,k,6) - momy * q(i,j,k,7))

                END DO

            END DO

        END DO

    ELSE

        DO k = klow, khi

            DO j = jlow, jhi

                DO i = ilow, ihi

                    iden           = 1.0_WRD / q(i,j,k,1)
                    emf(i,j,k,3,3) = iden * (q(i,j,k,3) * q(i,j,k,5) - q(i,j,k,2) * q(i,j,k,6))
                    emf(i,j,k,2,3) = iden * (q(i,j,k,2) * q(i,j,k,7) - q(i,j,k,4) * q(i,j,k,5))
                    emf(i,j,k,1,3) = iden * (q(i,j,k,4) * q(i,j,k,6) - q(i,j,k,3) * q(i,j,k,7))

                END DO

            END DO

        END DO

    END IF

END SUBROUTINE compute_reference_emf3d

!### this routine will interpolate reference and face centered emfs to cell corner ###
SUBROUTINE compute_corner_emf3d(self, q, emf, omega, ilow, ihi, jlow, jhi, klow, khi)

    CLASS(MHDTVD) :: self
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, jlow, jhi, klow, khi
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: emf(:,:,:,:,:)
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: q(:,:,:,:)
    REAL(WRD), CONTIGUOUS, INTENT(OUT) :: omega(:,:,:,:)
    REAL(WRD) :: idx, dtdx, dyomega_12_14, dyomega_12_34, dxomega_14_12, dxomega_34_12, &
         dyomegal_14, dxomegal_14, dyomegal_34, dxomegal_34, &
         dyomegar_14, dxomegar_14, dyomegar_34, dxomegar_34, &
         rhol, rhor, idenl, idenr, rhorl, rhorr, vx, vy, zero
    INTEGER(WIS) :: i, j, k

    zero = self%zero_value

    !### compute the zone corner EMF ###
    DO k = klow, khi-2

        DO j = jlow, jhi-2

            !### compute the Z component of the electric field ###
            DO i = ilow, ihi-2

                !### get the sign of the contact mode in both X and Y using the CG weighting used in the Reimann solver ###
                rhol  = q(i,j,k,1)
                rhor  = q(i+1,j,k,1)
                idenl = 1.0_WRD / rhol
                idenr = 1.0_WRD / rhor
                rhorl = SQRT(rhol)
                rhorr = SQRT(rhor)
                vx    = (rhorl * q(i,j,k,2) * idenl) + (rhorr * q(i+1,j,k,2) * idenr)
                rhor  = q(i,j+1,k,1)
                idenr = 1.0_WRD / rhor
                rhorr = SQRT(rhor)
                vy    = (rhorl * q(i,j,k,3) * idenl) + (rhorr * q(i,j+1,k,3) * idenr)
                
                !### compute the derivatives in both X and Y we'll need for the upwinding portion ###
                dxomegal_14 = emf(i,j,k,1,1) - emf(i,j,k,3,3)
                dyomegal_14 = emf(i,j,k,2,1) - emf(i,j,k,3,3)
                dxomegal_34 = emf(i+1,j,k,3,3) - emf(i,j,k,1,1)
                dyomegal_34 = emf(i,j+1,k,3,3) - emf(i,j,k,2,1)
                
                dxomegar_14 = emf(i,j+1,k,1,1) - emf(i,j+1,k,3,3)
                dyomegar_14 = emf(i+1,j,k,2,1) - emf(i+1,j,k,3,3)
                dxomegar_34 = emf(i+1,j+1,k,3,3) - emf(i,j+1,k,1,1)
                dyomegar_34 = emf(i+1,j+1,k,3,3) - emf(i+1,j,k,2,1)
                
                !### upwind the Y integrals (the integral is in Y, but the contribution depends on the velocity in X) ###
                IF (vx .GT. zero) THEN
                    
                    dyomega_12_14 = dyomegal_14
                    dyomega_12_34 = dyomegal_34
                    
                ELSE IF (vx .LT. -zero) THEN
                    
                    dyomega_12_14 = dyomegar_14
                    dyomega_12_34 = dyomegar_34
                    
                ELSE
                    
                    dyomega_12_14 = 0.5_WRD * (dyomegal_14 + dyomegar_14)
                    dyomega_12_34 = 0.5_WRD * (dyomegal_34 + dyomegar_34)
                    
                END IF
                
                !### upwind the X integrals (the integral is in X, but the contribution depends on the velocity in Y) ###
                IF (vy .GT. zero) THEN
                    
                    dxomega_14_12 = dxomegal_14
                    dxomega_34_12 = dxomegal_34
                    
                ELSE IF (vy .LT. -zero) THEN
                    
                    dxomega_14_12 = dxomegar_14
                    dxomega_34_12 = dxomegar_34
                    
                ELSE
                    
                    dxomega_14_12 = 0.5_WRD * (dxomegal_14 + dxomegar_14)
                    dxomega_34_12 = 0.5_WRD * (dxomegal_34 + dxomegar_34)
                    
                END IF
                
                !### compute the final cell corner electric field ###
                omega(i,j,k,3) = 0.25_WRD * ((emf(i+1,j,k,2,1) + emf(i,j,k,2,1)) + (emf(i,j+1,k,1,1) + emf(i,j,k,1,1))) + &
                     0.25_WRD * ((dyomega_12_14 - dyomega_12_34) + (dxomega_14_12 - dxomega_34_12))

            END DO

            !### compute the Y component of the electric field (x = X, y = Z) ###
            DO i = ilow, ihi-2

                !### get the sign of the contact mode in both X and Y using the CG weighting used in the Reimann solver ###
                rhol  = q(i,j,k,1)
                rhor  = q(i+1,j,k,1)
                idenl = 1.0_WRD / rhol
                idenr = 1.0_WRD / rhor
                rhorl = SQRT(rhol)
                rhorr = SQRT(rhor)
                vx    = (rhorl * q(i,j,k,2) * idenl) + (rhorr * q(i+1,j,k,2) * idenr)
                rhor  = q(i,j,k+1,1)
                idenr = 1.0_WRD / rhor
                rhorr = SQRT(rhor)
                vy    = (rhorl * q(i,j,k,4) * idenl) + (rhorr * q(i,j,k+1,4) * idenr)
                
                !### compute the derivatives in both X and Y we'll need for the upwinding portion ###
                dxomegal_14 = emf(i,j,k,1,2) - emf(i,j,k,2,3)
                dyomegal_14 = emf(i,j,k,3,1) - emf(i,j,k,2,3)
                dxomegal_34 = emf(i+1,j,k,2,3) - emf(i,j,k,1,2)
                dyomegal_34 = emf(i,j,k+1,2,3) - emf(i,j,k,3,1)

                dxomegar_14 = emf(i,j,k+1,1,2) - emf(i,j,k+1,2,3)
                dyomegar_14 = emf(i+1,j,k,3,1) - emf(i+1,j,k,2,3)
                dxomegar_34 = emf(i+1,j,k+1,2,3) - emf(i,j,k+1,1,2)
                dyomegar_34 = emf(i+1,j,k+1,2,3) - emf(i+1,j,k,3,1)
                
                !### upwind the Y integrals (the integral is in Y, but the contribution depends on the velocity in X) ###
                IF (vx .GT. zero) THEN
                    
                    dyomega_12_14 = dyomegal_14
                    dyomega_12_34 = dyomegal_34
                    
                ELSE IF (vx .LT. -zero) THEN
                    
                    dyomega_12_14 = dyomegar_14
                    dyomega_12_34 = dyomegar_34
                    
                ELSE
                    
                    dyomega_12_14 = 0.5_WRD * (dyomegal_14 + dyomegar_14)
                    dyomega_12_34 = 0.5_WRD * (dyomegal_34 + dyomegar_34)
                    
                END IF
                
                !### upwind the X integrals (the integral is in X, but the contribution depends on the velocity in Y) ###
                IF (vy .GT. zero) THEN
                    
                    dxomega_14_12 = dxomegal_14
                    dxomega_34_12 = dxomegal_34
                    
                ELSE IF (vy .LT. -zero) THEN
                    
                    dxomega_14_12 = dxomegar_14
                    dxomega_34_12 = dxomegar_34
                    
                ELSE
                    
                    dxomega_14_12 = 0.5_WRD * (dxomegal_14 + dxomegar_14)
                    dxomega_34_12 = 0.5_WRD * (dxomegal_34 + dxomegar_34)
                    
                END IF
                
                !### compute the final cell corner electric field ###
                omega(i,j,k,2) = 0.25_WRD * ((emf(i,j,k+1,1,2) + emf(i,j,k,1,2)) + (emf(i+1,j,k,3,1) + emf(i,j,k,3,1))) + &
                     0.25_WRD * ((dyomega_12_14 - dyomega_12_34) + (dxomega_14_12 - dxomega_34_12))

            END DO

            !### compute the X component of the electric field (x = Y, y = Z) ###
            DO i = ilow, ihi-2

                !### get the sign of the contact mode in both X and Y using the CG weighting used in the Reimann solver ###
                rhol  = q(i,j,k,1)
                rhor  = q(i,j+1,k,1)
                idenl = 1.0_WRD / rhol
                idenr = 1.0_WRD / rhor
                rhorl = SQRT(rhol)
                rhorr = SQRT(rhor)
                vx    = (rhorl * q(i,j,k,3) * idenl) + (rhorr * q(i,j+1,k,3) * idenr)
                rhor  = q(i,j,k+1,1)
                idenr = 1.0_WRD / rhor
                rhorr = SQRT(rhor)
                vy    = (rhorl * q(i,j,k,4) * idenl) + (rhorr * q(i,j,k+1,4) * idenr)
                
                !### compute the derivatives in both X and Y we'll need for the upwinding portion ###
                dxomegal_14 = emf(i,j,k,2,2) - emf(i,j,k,1,3)
                dyomegal_14 = emf(i,j,k,3,2) - emf(i,j,k,1,3)
                dxomegal_34 = emf(i,j+1,k,1,3) - emf(i,j,k,2,2)
                dyomegal_34 = emf(i,j,k+1,1,3) - emf(i,j,k,3,2)
                
                dxomegar_14 = emf(i,j,k+1,2,2) - emf(i,j,k+1,1,3)
                dyomegar_14 = emf(i,j+1,k,3,2) - emf(i,j+1,k,1,3)
                dxomegar_34 = emf(i,j+1,k+1,1,3) - emf(i,j,k+1,2,2)
                dyomegar_34 = emf(i,j+1,k+1,1,3) - emf(i,j+1,k,3,2)
                
                !### upwind the Y integrals (the integral is in Y, but the contribution depends on the velocity in X) ###
                IF (vx .GT. zero) THEN
                    
                    dyomega_12_14 = dyomegal_14
                    dyomega_12_34 = dyomegal_34
                    
                ELSE IF (vx .LT. -zero) THEN
                    
                    dyomega_12_14 = dyomegar_14
                    dyomega_12_34 = dyomegar_34
                    
                ELSE
                    
                    dyomega_12_14 = 0.5_WRD * (dyomegal_14 + dyomegar_14)
                    dyomega_12_34 = 0.5_WRD * (dyomegal_34 + dyomegar_34)
                    
                END IF
                
                !### upwind the X integrals (the integral is in X, but the contribution depends on the velocity in Y) ###
                IF (vy .GT. zero) THEN
                    
                    dxomega_14_12 = dxomegal_14
                    dxomega_34_12 = dxomegal_34
                    
                ELSE IF (vy .LT. -zero) THEN
                    
                    dxomega_14_12 = dxomegar_14
                    dxomega_34_12 = dxomegar_34
                    
                ELSE
                    
                    dxomega_14_12 = 0.5_WRD * (dxomegal_14 + dxomegar_14)
                    dxomega_34_12 = 0.5_WRD * (dxomegal_34 + dxomegar_34)
                    
                END IF
                
                !### compute the final cell corner electric field ###
                omega(i,j,k,1) = 0.25_WRD * ((emf(i,j,k+1,2,2) + emf(i,j,k,2,2)) + (emf(i,j+1,k,3,2) + emf(i,j,k,3,2))) + &
                     0.25_WRD * ((dyomega_12_14 - dyomega_12_34) + (dxomega_14_12 - dxomega_34_12))

            END DO

        END DO

    END DO

END SUBROUTINE compute_corner_emf3d

!### this routine accepts cell corner emfs and updates the face cenetered magnetic field ###
SUBROUTINE applyct3d(self, dt, dx, q, bface, omega, ilow, ihi, jlow, jhi, klow, khi, halfdt)

    CLASS(MHDTVD) :: self
    REAL(WRD), INTENT(IN) :: dt, dx
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, jlow, jhi, klow, khi
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: q(:,:,:,:), bface(:,:,:,:)
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: omega(:,:,:,:)
    LOGICAL(WIS), INTENT(IN) :: halfdt
    REAL(WRD) :: idx, dtdx
    INTEGER(WIS) :: i, j, k

    idx  = 1.0_WRD / dx
    dtdx = dt * idx
    IF (halfdt) dtdx = 0.5_WRD * dtdx

    !### update the face centered magnetic fields ###
    DO k = klow+1, khi-2
        
        DO j = jlow+1, jhi-2
            
            DO i = ilow+1, ihi-2

                !### update the face-centered magnetic field with Eq. 16 and 17 in RMJF98 ###
                bface(i,j,k,1) = bface(i,j,k,1) - (dtdx * ((omega(i,j,k,3) - omega(i,j-1,k,3)) - (omega(i,j,k,2) - omega(i,j,k-1,2))))
                bface(i,j,k,2) = bface(i,j,k,2) + (dtdx * ((omega(i,j,k,3) - omega(i-1,j,k,3)) - (omega(i,j,k,1) - omega(i,j,k-1,1))))
                bface(i,j,k,3) = bface(i,j,k,3) - (dtdx * ((omega(i,j,k,2) - omega(i-1,j,k,2)) - (omega(i,j,k,1) - omega(i,j-1,k,1))))
                
            END DO
            
        END DO
        
    END DO

    !### now update the zone-centered magnetic field by interpolating the face-centered field ###
    IF (.NOT. halfdt) THEN

        DO k = klow+2, khi-2

            DO j = jlow+2, jhi-2
            
                DO i = ilow+2, ihi-2
                
                    !### use Eq. 3 & 4 in RJMF98, which is just a simple average ###
                    q(i,j,k,5) = 0.5_WRD * (bface(i,j,k,1) + bface(i-1,j,k,1))
                    q(i,j,k,6) = 0.5_WRD * (bface(i,j,k,2) + bface(i,j-1,k,2))
                    q(i,j,k,7) = 0.5_WRD * (bface(i,j,k,3) + bface(i,j,k-1,3))

                END DO
            
            END DO
        
        END DO

        !### finally do a last check for unphysical values that came out of the update (should be exceedingly rare after protection fluxes) ###
        CALL self%apply_protections3d(q, ilow, ihi, jlow, jhi, klow, khi)

    END IF

END SUBROUTINE applyct3d

!### compute the maximum wave speed from interface states ###
SUBROUTINE get_maxwave2d(self, workp, pool, lilow, lihi, ljlow, ljhi, ilow, ihi, jlow, jhi)

    CLASS(MHDTVD) :: self
    TYPE(Patch) :: workp
    TYPE(MHDPool) :: pool
    INTEGER(WIS), INTENT(IN) :: lilow, lihi, ljlow, ljhi, ilow, ihi, jlow, jhi
    INTEGER :: i, j

    !### get the wave speeds along X first ###
    CALL self%compute_Xfluxes2d(workp, pool, ljlow, ljhi, jlow, jhi, lilow, lihi, ilow, ihi, .FALSE., .TRUE.)

    DO j = ljlow, ljhi-1

        DO i = lilow, lihi-1

            pool%max_wave = MAX(pool%max_wave, MAXVAL(pool%waves2d(i,j,:)) + ABS(pool%qwork2d(i,j,2)) / pool%qwork2d(i,j,1))

        END DO

    END DO

    !### get the wave speeds along Y ###
    CALL self%compute_Yfluxes2d(workp, pool, lilow, lihi, ilow, ihi, ljlow, ljhi, jlow, jhi, .FALSE., .TRUE.)

    DO i = lilow, lihi      

        DO j = ljlow, ljhi-1

            pool%max_wave = MAX(pool%max_wave, MAXVAL(pool%waves2d(j,i,:)) + ABS(pool%qwork2d(j,i,2)) / pool%qwork2d(j,i,1))

        END DO

    END DO

END SUBROUTINE get_maxwave2d

!### compute the maximum wave speed from interface states ###
SUBROUTINE get_maxwave3d(self, workp, pool, koff, lilow, lihi, ljlow, ljhi, lklow, lkhi, ilow, ihi, jlow, jhi, klow, khi)

    CLASS(MHDTVD) :: self
    TYPE(Patch) :: workp
    TYPE(MHDPool) :: pool
    INTEGER(WIS), INTENT(IN) :: koff, lilow, lihi, ljlow, ljhi, lklow, lkhi, ilow, ihi, jlow, jhi, klow, khi

    !### get the wave speeds along X and Y ###
    CALL self%compute_Zplane_XYfluxes3d(workp, pool, lklow, lkhi, klow, khi, koff, ljlow, ljhi, jlow, jhi, lilow, lihi, ilow, ihi, .FALSE., .TRUE.)

    !### get the wave speeds along Z ###
    CALL self%compute_Zfluxes3d(workp, pool, ljlow, ljhi, jlow, jhi, koff, lklow, lkhi, klow, khi, lilow, lihi, ilow, ihi, .FALSE., .TRUE.)

END SUBROUTINE get_maxwave3d

!### initialize the time-averaged velocity arrays with the current velocity ###
SUBROUTINE init_vtavg1d(self, workp)

    CLASS(MHDTVD) :: self
    TYPE(Patch) :: workp
    INTEGER(WIS) :: ilow, ihi, i
    REAL(WRD) :: iden

    ilow = 1 - workp%nb
    ihi  = workp%nx + workp%nb
    DO i = ilow, ihi

        iden = 1.0_WRD / MAX(workp%grid1d(i,1), self%min_density)
        workp%vtavg1d(i,1) = workp%grid1d(i,2) * iden
        workp%vtavg1d(i,2) = workp%grid1d(i,3) * iden
        workp%vtavg1d(i,3) = workp%grid1d(i,4) * iden
        
    END DO

END SUBROUTINE init_vtavg1d

SUBROUTINE init_vtavg2d(self, workp)

    CLASS(MHDTVD) :: self
    TYPE(Patch) :: workp
    INTEGER(WIS) :: ilow, ihi, i, jlow, jhi, j
    REAL(WRD) :: iden

    ilow = 1 - workp%nb
    ihi  = workp%nx + workp%nb
    jlow = 1 - workp%nb
    jhi  = workp%ny + workp%nb
    DO j = jlow, jhi

        DO i = ilow, ihi

            iden = 1.0_WRD / MAX(workp%grid2d(i,j,1), self%min_density)
            workp%vtavg2d(i,j,1) = workp%grid2d(i,j,2) * iden
            workp%vtavg2d(i,j,2) = workp%grid2d(i,j,3) * iden
            workp%vtavg2d(i,j,3) = workp%grid2d(i,j,4) * iden
        
        END DO

    END DO

END SUBROUTINE init_vtavg2d

SUBROUTINE init_vtavg3d(self, workp)

    CLASS(MHDTVD) :: self
    TYPE(Patch) :: workp
    INTEGER(WIS) :: ilow, ihi, i, jlow, jhi, j, klow, khi, k
    REAL(WRD) :: iden

    ilow = 1 - workp%nb
    ihi  = workp%nx + workp%nb
    jlow = 1 - workp%nb
    jhi  = workp%ny + workp%nb
    klow = 1 - workp%nb
    khi  = workp%nz + workp%nb
    DO k = klow, khi

        DO j = jlow, jhi

            DO i = ilow, ihi

                iden = 1.0_WRD / MAX(workp%grid3d(i,j,k,1), self%min_density)
                workp%vtavg3d(i,j,k,1) = workp%grid3d(i,j,k,2) * iden
                workp%vtavg3d(i,j,k,2) = workp%grid3d(i,j,k,3) * iden
                workp%vtavg3d(i,j,k,3) = workp%grid3d(i,j,k,4) * iden

            END DO
        
        END DO

    END DO

END SUBROUTINE init_vtavg3d

!### finalize the time-averaged velocity ###
SUBROUTINE compute_vtavg1d(self, workp)

    CLASS(MHDTVD) :: self
    TYPE(Patch) :: workp
    INTEGER(WIS) :: ilow, ihi, i
    REAL(WRD) :: iden

    ilow = 1 - workp%nb
    ihi  = workp%nx + workp%nb
    DO i = ilow, ihi

        iden = 1.0_WRD / MAX(workp%grid1d(i,1), self%min_density)
        workp%vtavg1d(i,1) = 0.5_WRD * (workp%vtavg1d(i,1) + workp%grid1d(i,2) * iden)
        workp%vtavg1d(i,2) = 0.5_WRD * (workp%vtavg1d(i,2) + workp%grid1d(i,3) * iden)
        workp%vtavg1d(i,3) = 0.5_WRD * (workp%vtavg1d(i,3) + workp%grid1d(i,4) * iden)
        
    END DO

END SUBROUTINE compute_vtavg1d

SUBROUTINE compute_vtavg2d(self, workp)

    CLASS(MHDTVD) :: self
    TYPE(Patch) :: workp
    INTEGER(WIS) :: ilow, ihi, i, jlow, jhi, j
    REAL(WRD) :: iden

    ilow = 1 - workp%nb
    ihi  = workp%nx + workp%nb
    jlow = 1 - workp%nb
    jhi  = workp%ny + workp%nb
    DO j = jlow, jhi

        DO i = ilow, ihi

            iden = 1.0_WRD / MAX(workp%grid2d(i,j,1), self%min_density)
            workp%vtavg2d(i,j,1) = 0.5_WRD * (workp%vtavg2d(i,j,1) + workp%grid2d(i,j,2) * iden)
            workp%vtavg2d(i,j,2) = 0.5_WRD * (workp%vtavg2d(i,j,2) + workp%grid2d(i,j,3) * iden)
            workp%vtavg2d(i,j,3) = 0.5_WRD * (workp%vtavg2d(i,j,3) + workp%grid2d(i,j,4) * iden)
        
        END DO

    END DO

END SUBROUTINE compute_vtavg2d

SUBROUTINE compute_vtavg3d(self, workp)

    CLASS(MHDTVD) :: self
    TYPE(Patch) :: workp
    INTEGER(WIS) :: ilow, ihi, i, jlow, jhi, j, klow, khi, k
    REAL(WRD) :: iden

    ilow = 1 - workp%nb
    ihi  = workp%nx + workp%nb
    jlow = 1 - workp%nb
    jhi  = workp%ny + workp%nb
    klow = 1 - workp%nb
    khi  = workp%nz + workp%nb
    DO k = klow, khi

        DO j = jlow, jhi

            DO i = ilow, ihi

                iden = 1.0_WRD / MAX(workp%grid3d(i,j,k,1), self%min_density)
                workp%vtavg3d(i,j,k,1) = 0.5_WRD * (workp%vtavg3d(i,j,k,1) + workp%grid3d(i,j,k,2) * iden)
                workp%vtavg3d(i,j,k,2) = 0.5_WRD * (workp%vtavg3d(i,j,k,2) + workp%grid3d(i,j,k,3) * iden)
                workp%vtavg3d(i,j,k,3) = 0.5_WRD * (workp%vtavg3d(i,j,k,3) + workp%grid3d(i,j,k,4) * iden)

            END DO
        
        END DO

    END DO

END SUBROUTINE compute_vtavg3d

END MODULE Mod_MHDTVD
