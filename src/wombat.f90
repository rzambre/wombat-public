#include "config.h"

PROGRAM Wombat

!######################################################################
!#
!# FILENAME: wombat.f90
!#
!# DESCRIPTION: This is the main driver for Wombat, which assembles all
!#  solvers and initializes classes.  It also drives the time loop.
!#
!# DEPENDENCIES: The rest of Wombat
!#
!# HISTORY:
!#    7/29/2015  - Peter Mendygral - Adding this header
!#
!######################################################################

!### require the needed externals ###
USE OMP_LIB
USE MPI

!### pull in all of the required modules ###
USE Mod_Globals
USE Mod_Error
USE Mod_RankLocation
USE Mod_SimulationUnits
USE Mod_Domain
USE Mod_Problem
USE Mod_DomainSolver
USE Mod_Decomposition
USE Mod_IO

IMPLICIT NONE

TYPE(RankLocation) :: rankloc
TYPE(SimulationUnits) :: simunits
TYPE(Domain) :: rootd
TYPE(Problem) :: prob
TYPE(DomainSolver) :: dsolve
TYPE(Decomposition) :: decomp
TYPE(IO) :: ioo
REAL(WRD) :: timer1, timer2, timer3, DM_buff_fac, particle_mass
REAL(WRD), ALLOCATABLE :: solve_time(:), mgmt_time(:), writes_time(:)
REAL(WRD), ALLOCATABLE :: solve_comm_time(:), solve_comm_init_time(:), solve_comm_flush_time(:)
REAL(WRD) :: t_start
REAL(WRD) :: tot_solve_time, node_tot_solve_time
REAL(WRD) :: tot_mgmt_time, node_tot_mgmt_time
REAL(WRD) :: tot_writes_time, node_tot_writes_time
REAL(WRD) :: tot_solve_comm_time, node_tot_solve_comm_time
REAL(WRD) :: tot_solve_comm_init_time, node_tot_solve_comm_init_time
REAL(WRD) :: tot_solve_comm_sync_time, node_tot_solve_comm_sync_time
REAL(WRD) :: tot_solve_comm_flush_time, node_tot_solve_comm_flush_time
REAL(WRD) :: tot_barrier_time, node_tot_barrier_time
REAL(WRD) :: dt_mod_comp, dt_mod_movie, dtproposed, dtreduced
INTEGER(WIS) :: n, dtid, npass_vars, ncr_momentum_bins, max_steps, n_xpatches, n_ypatches, n_zpatches, patch_nx, patch_ny, patch_nz, patch_nb
INTEGER(WIS) :: ierr
INTEGER(WIS) :: tid, tid_index
INTEGER(WIS) :: npm_maxblockmgrs, npm_partcls_per_dm, npm_dms_per_blkmgr, npm_steps_per_defrag
LOGICAL(WIS) :: mhdOn, passOn, crsOn, static_gravityOn, variable_gravityOn, dark_matterOn, writeCompressed, writeMovie

NAMELIST /Grid/ npass_vars, ncr_momentum_bins, mhdOn, passOn, crsOn, static_gravityOn, variable_gravityOn, dark_matterOn, n_xpatches, &
     n_ypatches, n_zpatches, patch_nx, patch_ny, patch_nz, patch_nb, npm_maxblockmgrs, npm_partcls_per_dm, npm_dms_per_blkmgr, &
     npm_steps_per_defrag, DM_buff_fac, particle_mass

CALL InitDecomposition()
CALL InitMyLocation()
CALL InitClasses()

#ifdef WMBT_COMM_BENCHMARK
IF (decomp%myrank .EQ. 0) &
    PRINT '("COMM BENCHMARK ENABLED: Running for ", I8, " total iterations")', max_steps
#endif

IF (decomp%myrank .EQ. 0) &
    PRINT *, "t_end = ", simunits%t_end

!### allocate thread timers ###
ALLOCATE(solve_time(decomp%nthreads))
ALLOCATE(mgmt_time(decomp%nthreads))
ALLOCATE(writes_time(decomp%nthreads))

ALLOCATE(solve_comm_time(decomp%nthreads))
ALLOCATE(solve_comm_init_time(decomp%nthreads))
ALLOCATE(solve_comm_flush_time(decomp%nthreads))

!### initialize timers ###

!### START OF THREADED REGION ###
CALL OMP_SET_NUM_THREADS(decomp%nthreads)
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(tid_index, tid, t_start)

!### set the thread info ###
WTHREAD   = OMP_GET_THREAD_NUM()
WNTHREADS = OMP_GET_NUM_THREADS()
WRITE(WTHREADSTR, '(I10)') WTHREAD
WRITE(WNTHREADSTR, '(I10)') WNTHREADS

tid_index = WTHREAD + 1

!### domains can be built and paged in now that we are in the threaded region
CALL ConstructDomain()

!$OMP MASTER

dtid   = 0
n      = 0
timer1 = decomp%global_time()

IF (decomp%myrank .EQ. 0) &
    CALL Show_Summary()

!$OMP END MASTER

!### initialize per-thread timers ###
t_start = 0;
solve_time(tid_index) = 0
mgmt_time(tid_index) = 0
writes_time(tid_index) = 0

solve_comm_time(tid_index) = 0
solve_comm_init_time(tid_index) = 0
solve_comm_flush_time(tid_index) = 0

!$OMP BARRIER

!### open up the RMA locks to our neighbors ###
CALL decomp%rma_lock_neighbors()

!### start the loop ###
#ifdef WMBT_COMM_BENCHMARK
DO WHILE (n .LT. simunits%max_steps)
#else
DO WHILE (simunits%t .LT. simunits%t_end .AND. n .LT. simunits%max_steps)
#endif

    !$OMP SINGLE

    !### start the loop timer and init the maxsignal back to 0 ###
    timer3          = decomp%global_time()
    rootd%maxsignal = 0.0_WRD

    !$OMP END SINGLE

    IF (n .EQ. 0) THEN
        t_start = decomp%global_time()
        CALL DoWrites()
        writes_time(tid_index) = writes_time(tid_index) + (decomp%global_time() - t_start)
    END IF
    
    t_start = decomp%global_time()
    CALL DoSolves()
    solve_time(tid_index) = solve_time(tid_index) + (decomp%global_time() - t_start)

    t_start = decomp%global_time()
    CALL TimeStepManagement()
    mgmt_time(tid_index) = mgmt_time(tid_index) + (decomp%global_time() - t_start)
    
    t_start = decomp%global_time()
    CALL DoWrites()
    writes_time(tid_index) = writes_time(tid_index) + (decomp%global_time() - t_start)
 
    !$OMP SINGLE

    timer2 = decomp%global_time()

    IF (decomp%myrank .EQ. 0) THEN
        PRINT *, "T = ", simunits%t
        PRINT '("Step=", I8, " | T=", ES14.7, " | DT=", ES14.7, " | StepWall=", ES11.4, " sec | Elapsed=", ES11.4, " sec")', &
            n, simunits%t, simunits%dt, (timer2 - timer3), (timer2 - timer1)
    END IF

    !$OMP END SINGLE
    
END DO

!$OMP SINGLE
CALL decomp%end_allreduce(dtid)
!$OMP END SINGLE

CALL decomp%rma_unlock_neighbors()

CALL ioo%destroy()
CALL rootd%destroy()

!$OMP END PARALLEL
!### END OF THREADED REGION ###

node_tot_solve_time = 0
node_tot_mgmt_time = 0
node_tot_writes_time = 0

node_tot_solve_comm_time = 0
node_tot_solve_comm_init_time = 0
node_tot_solve_comm_sync_time = 0
node_tot_solve_comm_flush_time = 0

node_tot_barrier_time = 0

DO tid = 1, decomp%nthreads
    solve_comm_time(tid) = decomp%get_time(tid) + decomp%put_time(tid) + decomp%sync_time(tid) + &
        decomp%flush_time(tid) + decomp%flush_all_time(tid) + decomp%flush_local_all_time(tid)

    solve_comm_init_time(tid) = decomp%get_time(tid) + decomp%put_time(tid)
    solve_comm_flush_time(tid) = decomp%flush_time(tid) + decomp%flush_all_time(tid) + decomp%flush_local_all_time(tid)
    
    !PRINT *, "Rank ", decomp%myrank, " TID ", tid, " solve_time = ", solve_time(tid)
    node_tot_solve_time = node_tot_solve_time + solve_time(tid)
    node_tot_mgmt_time = node_tot_mgmt_time + mgmt_time(tid)
    node_tot_writes_time = node_tot_writes_time + writes_time(tid)

    node_tot_solve_comm_time = node_tot_solve_comm_time + solve_comm_time(tid)
    node_tot_solve_comm_init_time = node_tot_solve_comm_init_time + solve_comm_init_time(tid)
    node_tot_solve_comm_sync_time = node_tot_solve_comm_sync_time + decomp%sync_time(tid)
    node_tot_solve_comm_flush_time = node_tot_solve_comm_flush_time + solve_comm_flush_time(tid)

    node_tot_barrier_time = node_tot_barrier_time + decomp%barrier_time(tid)
END DO 

CALL MPI_REDUCE(node_tot_mgmt_time, tot_mgmt_time, 1, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
CALL MPI_REDUCE(node_tot_writes_time, tot_writes_time, 1, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
CALL MPI_REDUCE(node_tot_solve_time, tot_solve_time, 1, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
CALL MPI_REDUCE(node_tot_solve_comm_time, tot_solve_comm_time, 1, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
CALL MPI_REDUCE(node_tot_solve_comm_init_time, tot_solve_comm_init_time, 1, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
CALL MPI_REDUCE(node_tot_solve_comm_sync_time, tot_solve_comm_sync_time, 1, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
CALL MPI_REDUCE(node_tot_solve_comm_flush_time, tot_solve_comm_flush_time, 1, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
CALL MPI_REDUCE(node_tot_barrier_time, tot_barrier_time, 1, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, ierr)

IF (decomp%myrank .EQ. 0) THEN
    PRINT *, "Elapsed time = ", (timer2 - timer1)
    
    !PRINT *, "node_tot_solve_time = ", node_tot_solve_time, "tot_solve_time = ", tot_solve_time
    !PRINT *, "nranks = ", decomp%world_nranks, " nthreads = ", decomp%nthreads    

    PRINT *, "Mgmt time = ", (tot_mgmt_time / (decomp%world_nranks * decomp%nthreads))
    PRINT *, "Writes time = ", (tot_writes_time / (decomp%world_nranks * decomp%nthreads))
    PRINT *, "Solve time = ", (tot_solve_time / (decomp%world_nranks * decomp%nthreads))
    PRINT *, "Solve_comm time = ", (tot_solve_comm_time / (decomp%world_nranks * decomp%nthreads))
    PRINT *, "Solve_comm_init time = ", (tot_solve_comm_init_time / (decomp%world_nranks * decomp%nthreads))
    PRINT *, "Solve_comm_sync time = ", (tot_solve_comm_sync_time / (decomp%world_nranks * decomp%nthreads))
    PRINT *, "Solve_comm_comp time = ", (tot_solve_comm_flush_time / (decomp%world_nranks * decomp%nthreads))
    PRINT *, "Average barrier time = ", (tot_barrier_time / (decomp%world_nranks * decomp%nthreads))
END IF

CALL prob%destroy()
CALL dsolve%destroy()
CALL decomp%destroy()

!-----------------------------------------
!### INTERNAL ROUTINES ###
!-----------------------------------------

CONTAINS

!### get our inpur file and have the Decomposition object establish the parallel enviroment for MPI ###
SUBROUTINE InitDecomposition()

    CHARACTER(WNAMELIST_PATHLEN) :: setup_file

    CALL GET_COMMAND_ARGUMENT(1, setup_file)
    decomp = Decomposition(setup_file) !  (AIO servers will remain in this call until we're done)

    !### now that we have the namelist read in we can obtain the variables we need ###
    READ(decomp%namelist, NML=Grid)

    !### assert on incompatible solver combinations ###
    IF (npass_vars .LT. 1       ) passOn = .FALSE.
    IF (ncr_momentum_bins .LT. 1) crsOn  = .FALSE.
    RASSERT(.NOT. (.NOT. mhdOn .AND. passOn), "MHD must be enabled to solve for passive scalars")
    RASSERT(.NOT. (.NOT. mhdOn .AND. crsOn), "MHD must be enabled to solve for cosmic rays")
    RASSERT(.NOT. ((.NOT. static_gravityOn .AND. .NOT. variable_gravityOn) .AND. dark_matterOn), "Either static or time variable gravity must be enabled to solve for dark matter")
    RASSERT(.NOT. (static_gravityOn .AND. variable_gravityOn), "Static and time varying gravity cannot both be enabled")
    RASSERT(.NOT. (n_xpatches .LT. 1 .OR. n_ypatches .LT. 1 .OR. n_zpatches .LT. 1), "There must be at least 1 Patch in each dimension on the root domain")
    RASSERT(.NOT. (patch_nx .LT. 1 .OR. patch_ny .LT. 1 .OR. patch_nz .LT. 1), "There must be at least 1 zone in each dimension")
    RASSERT(.NOT. (dark_matterOn .AND. (npm_maxblockmgrs .LT. 1 .OR. npm_partcls_per_dm .LT. 1. .OR. npm_dms_per_blkmgr .LT. 1 .OR. npm_steps_per_defrag .LT. 1)), "npm_maxblockmgrs, npm_partcls_per_dm, npm_dms_per_blkmgr, and npm_steps_per_defrag must all be at least 1 when dark matter is enabled")

    CALL decomp%initRMA(patch_nx, patch_ny, patch_nz, patch_nb, MAX(8, npass_vars, ncr_momentum_bins))

END SUBROUTINE InitDecomposition

!### define the world location of this MPI rank ###
SUBROUTINE InitMyLocation()

    INTEGER(WIS) :: nranks(3), npatches(3), patch_size(3)
    LOGICAL(WIS) :: is_boundary(2,3)

    nranks     = (/decomp%nranks_x, decomp%nranks_y, decomp%nranks_z/)
    npatches   = (/n_xpatches, n_ypatches, n_zpatches/)
    patch_size = (/patch_nx, patch_ny, patch_nz/)

    is_boundary(:,1) = (/decomp%lx_bound, decomp%hx_bound/)
    is_boundary(:,2) = (/decomp%ly_bound, decomp%hy_bound/)
    is_boundary(:,3) = (/decomp%lz_bound, decomp%hz_bound/)

    rankloc = RankLocation(decomp%my_coords, is_boundary, nranks, npatches, patch_size)

END SUBROUTINE InitMyLocation

!### initialize the various classes we need ###
SUBROUTINE InitClasses()

    simunits = SimulationUnits(decomp%namelist, rankloc)
    CALL rankloc%calc_location(simunits%dx)

    prob   = Problem(decomp%namelist, decomp%nthreads, rankloc, simunits)
    rootd  = Domain(decomp%nthreads, decomp%ndims, n_xpatches, n_ypatches, n_zpatches, rankloc, simunits)
    dsolve = DomainSolver(decomp%namelist, decomp%nthreads, decomp%ndims, prob%gamma, npass_vars, npm_partcls_per_dm, particle_mass)

    ioo = IO(decomp%nthreads, decomp%iolead, decomp%naio, rankloc, simunits, decomp%namelist)

END SUBROUTINE InitClasses

!### build up our domains and patches ###
SUBROUTINE ConstructDomain()

    !### create the shells of the Patches within the root domain (arrays allocated in a separate step) ###
    IF (decomp%ndims .EQ. 1) THEN

        CALL rootd%build(decomp%myrank, decomp%domain_neighbors1d(0,:), 8, npass_vars, ncr_momentum_bins, .TRUE., passOn, crsOn, static_gravityOn, variable_gravityOn, dark_matterOn,  &
            patch_nx, patch_ny, patch_nz, patch_nb, npm_maxblockmgrs, npm_partcls_per_dm, npm_dms_per_blkmgr, npm_steps_per_defrag, DM_buff_fac)

    ELSE IF (decomp%ndims .EQ. 2) THEN

        CALL rootd%build(decomp%myrank, decomp%domain_neighbors2d(0,0,:), 8, npass_vars, ncr_momentum_bins, .TRUE., passOn, crsOn, static_gravityOn, variable_gravityOn, dark_matterOn,  &
            patch_nx, patch_ny, patch_nz, patch_nb, npm_maxblockmgrs, npm_partcls_per_dm, npm_dms_per_blkmgr, npm_steps_per_defrag, DM_buff_fac)

    ELSE IF (decomp%ndims .EQ. 3) THEN
        
        CALL rootd%build(decomp%myrank, decomp%domain_neighbors3d(0,0,0,:), 8, npass_vars, ncr_momentum_bins, .TRUE., passOn, crsOn, static_gravityOn, variable_gravityOn, dark_matterOn,  &
            patch_nx, patch_ny, patch_nz, patch_nb, npm_maxblockmgrs, npm_partcls_per_dm, npm_dms_per_blkmgr, npm_steps_per_defrag, DM_buff_fac)

    END IF

    !### actually construct the Patches now that they are prepared ###
    !### this step does first touches etc. and is separate as we will be dynamically building and un-building patches once we have AMR ###
    !### we are also indicating that the user Init routine (init the data on the grid) should be called on each Patch ###
    CALL rootd%build_all_patches(prob, rankloc, simunits, .TRUE.)

    !### initialize the RMA pipeline for solves of any type ###
    CALL decomp%initRMA_pipeline()

    !### have each thread create its own workpools ###
    CALL dsolve%init_workpools(mhdOn, passOn, crsOn, variable_gravityOn, dark_matterOn, patch_nx, patch_ny, patch_nz, patch_nb, npm_partcls_per_dm)

END SUBROUTINE ConstructDomain

!### call the various solvers ###
SUBROUTINE DoSolves()

    !### do not shuffle the list below.  leave as is since they are properly coordinated ###
    IF (mhdOn                          ) CALL dsolve%solve(rootd, prob, decomp, rankloc, simunits, 1)           ! MHD solver
    IF (mhdOn .AND. decomp%ndims .GT. 1) CALL dsolve%solve(rootd, prob, decomp, rankloc, simunits, 2)           ! CT algorithm (2 and 3d only)
    IF (passOn                         ) CALL dsolve%solve(rootd, prob, decomp, rankloc, simunits, 3)           ! VanLeer passive solver

END SUBROUTINE DoSolves

!### go through each dumpset type and perform the AIO write if it is time.  we also track adjustments to dt to get dumps at precisely the desired times ###
SUBROUTINE DoWrites()

    CALL ioo%time_to_write(1, simunits%t, simunits%dt, writeCompressed, dt_mod_comp)
    CALL ioo%time_to_write(2, simunits%t, simunits%dt, writeMovie, dt_mod_movie)

    !### set dt based on when our next write will occur ###
    !$OMP SINGLE
    simunits%dt = MIN(simunits%dt, dt_mod_comp, dt_mod_movie)
    CALL simunits%update_code2cgs()
    !$OMP END SINGLE

    !### inform all domains and Patches of the current time and time step ###
    CALL rootd%broadcast_t_and_dt(simunits%t, simunits%dt)

    IF (writeCompressed .OR. writeMovie) &
        CALL ioo%allocate_field_buffer_patch_flags(rootd)

    !### process dumps in order of likely smallest to largest to improve overlap opportunities ###
    IF (writeMovie) THEN

        CALL ioo%init_dumpset(2)

        !$OMP MASTER
        IF (decomp%myrank .EQ. 0) &
            PRINT '("-->> Movie dump ", I6, " write...")', ioo%movieNumber
        !$OMP END MASTER

        CALL ioo%write_dumpset(rootd, 2, prob%gamma)

    END IF

    IF (writeCompressed) THEN

        CALL ioo%init_dumpset(1)

        !$OMP MASTER
        IF (decomp%myrank .EQ. 0) &
            PRINT '("-->> Compressed dump ", I6, " write...")', ioo%compressedNumber
        !$OMP END MASTER

        CALL ioo%write_dumpset(rootd, 1, prob%gamma)

    END IF

    IF (writeCompressed .OR. writeMovie) &
        CALL ioo%deallocate_field_buffer_patch_flags(rootd)

END SUBROUTINE DoWrites

!### time step management including the global reduction ###
SUBROUTINE TimeStepManagement()

    !$OMP SINGLE

    !### increment time and the step counter ###
    simunits%t = simunits%t + simunits%dt
    n          = n + 1

    !### if we already had a time step reduction in-flight, wait for it and apply the new step ###
    IF (dtid .NE. 0) THEN

        CALL decomp%end_allreduce(dtid)
        simunits%dt = dtreduced
        IF ((simunits%t+simunits%dt) .GT. simunits%t_end) &
            simunits%dt = simunits%t_end - simunits%t

    END IF
    
    !### determine a locally proposed new time step size then start a reduction across ranks of that value ###
    dtproposed = simunits%propose_timestep(n, rootd%maxsignal)
    CALL decomp%start_allreduce_min(dtproposed, dtreduced, dtid)

    !$OMP END SINGLE

END SUBROUTINE TimeStepManagement

SUBROUTINE Show_Summary()

    CHARACTER(40) :: hostname, username
    INTEGER(WID) :: nzones

    CALL GET_ENVIRONMENT_VARIABLE("HOST", hostname)
    CALL GET_ENVIRONMENT_VARIABLE("USER", username)

    nzones = INT(decomp%nranks_x,WID) * INT(decomp%nranks_y,WID) * INT(decomp%nranks_z,WID) * &
         INT(n_xpatches,WID) * INT(patch_nx,WID) * INT(n_ypatches,WID) * INT(patch_ny,WID) * &
         INT(n_zpatches,WID) * INT(patch_nz,WID) 

    PRINT '("+-----------------------------------------------------------+")'
    PRINT '("| Wombat - Scalable astrophysical MHD                       |")'
    PRINT '("| Version: ", F3.1, "                                              |")', WVERSION
    PRINT '("|                                                           |")'
    PRINT '("|                   System Name: ", A24, "   |")', hostname
    PRINT '("|                     User Name: ", A24, "   |")', username
    PRINT '("|    Number of Worker MPI Ranks: ", I24, "   |")', decomp%nranks_x * decomp%nranks_y * decomp%nranks_z
    PRINT '("|    Number of Threads per Rank: ", I24, "   |")', decomp%nthreads
    PRINT '("|               Number of Cores: ", I24, "   |")', decomp%nranks_x * decomp%nranks_y * decomp%nranks_z * decomp%nthreads
    PRINT '("|         Number of AIO Servers: ", I24, "   |")', decomp%naio
    PRINT '("|                                                           |")'
    PRINT '("|              Simulation Title: ", A24, "   |")', ioo%simulationTitle
    PRINT '("|          Number of Dimensions: ", I24, "   |")', decomp%ndims
    PRINT '("|   Number of Root Domain Zones: ", I24, "   |")', nzones
    PRINT '("|                                                           |")'
    PRINT '("|            MHD Solver Enabled: ", L1, "                          |")', mhdOn
    PRINT '("|        Passive Solver Enabled: ", L1, "                          |")', passOn
    !PRINT '("|    Cosmic Ray Solver Enabled: ", L1, "                        |")', mhdOn
    PRINT '("|        Static Gravity Enabled: ", L1, "                          |")', static_gravityOn
    PRINT '("| Time-variable Gravity Enabled: ", L1, "                          |")', variable_gravityOn
    PRINT '("|    Dark Matter Solver Enabled: ", L1, "                          |")', dark_matterOn
    PRINT '("|                                                           |")'
    PRINT '("| UofM Computational Astrophysics                           |")'
    PRINT '("| Contact: pjm@cray.com                                     |")'
    PRINT '("+-----------------------------------------------------------+")'
    PRINT '("...Starting the simulation...")'

END SUBROUTINE Show_Summary

END PROGRAM Wombat
