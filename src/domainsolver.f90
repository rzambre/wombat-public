#include "config.h"

MODULE Mod_DomainSolver

!######################################################################
!#
!# FILENAME: mod_domainsolver.f90
!#
!# DESCRIPTION: 
!#
!# DEPENDENCIES: Mod_Globals for defining data types
!#
!# HISTORY:
!#    5/7/12  - Peter Mendygral
!#
!######################################################################

!### require the needed externals ###
USE OMP_LIB

!### require the needed modules ###
USE Mod_RankLocation
USE Mod_SimulationUnits
USE Mod_Patch
USE Mod_Domain
USE Mod_Globals
USE Mod_Error
USE Mod_MHDPool
USE Mod_MHDTVD
USE Mod_PassivePool
USE Mod_Passive
USE Mod_Decomposition
USE Mod_Problem

IMPLICIT NONE

!### default private but make the data type available ###
PRIVATE
PUBLIC :: DomainSolver

!### define a data type for this module ###
TYPE :: DomainSolver

   !### default everything as private ###
   PRIVATE
   
   TYPE(MHDTVD) :: mhd
   TYPE(Passive) :: pass
   TYPE(MHDPool), ALLOCATABLE :: mhdpool(:)
   TYPE(PassivePool), ALLOCATABLE :: passpool(:)
   
   INTEGER(WIS), PUBLIC :: nthreads
   INTEGER(WIS) :: ndim, n_updated, n_update_cmplt
   LOGICAL(WIS) :: is1d, is2d, is3d
   INTEGER(WIS), ALLOCATABLE :: myn_updated(:), myn_update_cmplt(:)
   LOGICAL(WIS), ALLOCATABLE :: myall_packed(:), myall_signals_recv(:)
   REAL(WRD), ALLOCATABLE :: mymaxsignal(:)
   
 CONTAINS
   
   !### default everything as private ###
   PRIVATE
   
   PROCEDURE :: initsolver
   PROCEDURE :: update_single_ready_patch
   PROCEDURE :: update_any_ready_patches
   PROCEDURE :: free_workpools
   PROCEDURE, PUBLIC :: solve
   PROCEDURE, PUBLIC :: init_workpools
   PROCEDURE, PUBLIC :: destroy
   
END TYPE DomainSolver

INTERFACE DomainSolver
   
   MODULE PROCEDURE constructor
   
END INTERFACE

!### object methods ###
CONTAINS
  
FUNCTION constructor(namelist, nthreads, ndim, gamma, npass, nslotspdm, ptcl_mass)
  
    TYPE(DomainSolver) :: constructor
    CHARACTER(WMAX_NAMELIST_LEN), INTENT(IN) :: namelist
    INTEGER(WIS), INTENT(IN) :: nthreads, ndim, npass, nslotspdm
    REAL(WRD), INTENT(IN) :: gamma, ptcl_mass
    
    CALL constructor%initsolver(namelist, nthreads, ndim, gamma, npass, nslotspdm, ptcl_mass)
    RETURN
  
END FUNCTION constructor

!### initsolver will prepare the object for use ###
SUBROUTINE initsolver(self, namelist, nthreads, ndim, gamma, npass, nslotspdm, ptcl_mass)

    CLASS(DomainSolver) :: self
    CHARACTER(WMAX_NAMELIST_LEN), INTENT(IN) :: namelist
    INTEGER(WIS), INTENT(IN) :: nthreads, ndim, npass, nslotspdm
    REAL(WRD), INTENT(IN) :: gamma, ptcl_mass
    
    self%nthreads = nthreads
    
    !### set our dimensionality ###
    self%is1d = .TRUE.
    self%is2d = .FALSE.
    self%is3d = .FALSE.
    IF (ndim .EQ. 2) THEN
        self%is1d = .FALSE.
        self%is2d = .TRUE.
    ELSE IF (ndim .EQ. 3) THEN
        self%is1d = .FALSE.
        self%is3d = .TRUE.
    END IF
    self%ndim = ndim
    
    !### create all of the solver objects ###
    self%mhd    = MHDTVD(namelist, gamma)
    self%pass   = Passive(namelist, 1, npass, .FALSE.)

    !### allocate space to our thread private progress counters ###
    ALLOCATE(self%myn_updated(nthreads), self%myn_update_cmplt(nthreads), self%myall_packed(nthreads), &
         self%myall_signals_recv(nthreads), self%mymaxsignal(nthreads))
  
END SUBROUTINE initsolver

!### desctructor for the class ###
SUBROUTINE destroy(self)

    CLASS(DomainSolver) :: self

    IF (ALLOCATED(self%myn_updated)) THEN

        DEALLOCATE(self%myn_updated)
        DEALLOCATE(self%myn_update_cmplt)
        DEALLOCATE(self%myall_packed)
        DEALLOCATE(self%myall_signals_recv)
        DEALLOCATE(self%mymaxsignal)
        
    END IF
    CALL self%free_workpools()

END SUBROUTINE destroy

!### allocate and touch all of the work pools for evey "physics modules" ###
!### NOTE* we pass all the on options here instead of init.  you may ask why? ###
!### the layout is suppose to be that each patch is a self-contained problem.  the values passed here ###
!### need to represent that larget or most true value of all the arguments amongst all patches we'll solve ###
SUBROUTINE init_workpools(self, mhdOn, passOn, crsOn, vargravOn, dmOn, nx, ny, nz, nb, nslotspdm)
  
    CLASS(DomainSolver) :: self
    LOGICAL(WIS), INTENT(IN) :: mhdOn, passOn, crsOn, vargravOn, dmOn
    INTEGER(WIS) :: nx, ny, nz, nb, tid, nslotspdm
    
    !$OMP SINGLE

    !### we always allocate a thread pool for the MHD solver ###

    DEBUGMSG('Allocating thread work pools')

    IF (mhdOn) THEN
        ALLOCATE(self%mhdpool(1:self%nthreads))
    END IF
    IF (passOn) THEN
        ALLOCATE(self%passpool(1:self%nthreads))
    END IF
    IF (crsOn) THEN
        ! once we have this
    END IF

    !$OMP END SINGLE

    tid = OMP_GET_THREAD_NUM() + 1

    !$OMP SINGLE
    DEBUGMSG('Assigning work pool(s)')
    !$OMP END SINGLE

    !$OMP CRITICAL
    IF (mhdOn ) self%mhdpool(tid)  = MHDPool(self%ndim, nx, ny, nz, nb)
    IF (passOn) self%passpool(tid) = PassivePool(self%ndim, nx, ny, nz, nb)
    !$OMP END CRITICAL

    !$OMP BARRIER

END SUBROUTINE init_workpools

!### cleanup of workpools ###
SUBROUTINE free_workpools(self)

    CLASS(DomainSolver) :: self
    INTEGER(WIS) :: t

    IF (ALLOCATED(self%mhdpool)) THEN

        DO t = 1, self%nthreads

            CALL self%mhdpool(t)%destroy()

        END DO
        DEALLOCATE(self%mhdpool)

    END IF

    IF (ALLOCATED(self%passpool)) THEN

        DO t = 1, self%nthreads

            CALL self%passpool(t)%destroy()

        END DO
        DEALLOCATE(self%passpool)

    END IF

END SUBROUTINE free_workpools

!### solve will choose a thread balancing scheme to apply a solve with ###
!### the solve is performed on a domain, and the method will update all active ###
!### patches within the domain ###
SUBROUTINE solve(self, workd, prob, decomp, rankloc, simunits, flavor)
  
    CLASS(DomainSolver) :: self
    TYPE(Domain) :: workd
    TYPE(Problem) :: prob
    TYPE(Decomposition) :: decomp
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits
    INTEGER(WIS), INTENT(IN) :: flavor
    INTEGER(WIS) :: n_unpacked, tid, t, bflavor, n_update_cmplt, n_updated
    LOGICAL(WIS) :: count_exchanges, local_unpacked, reset_pass_count, all_signals_recv, all_packed
    
    !### if the domain has no active patches this is a no-op ###
    IF (.NOT. workd%hasPatches) RETURN

    !### generally the boundary exchange flavor is the update flavor, but there are exceptions ###
    bflavor = flavor
    IF (flavor .EQ. 5) bflavor = 0  ! no exchange done for pmkd_solver
    IF (flavor .EQ. 7) bflavor = 1  ! gravity solve with FMG but uses grid array in a Patch
    IF (flavor .EQ. 8) bflavor = 5  ! grav potential exchange

    !### track the count of updates that have gone through all required passes ###
    n_update_cmplt   = 0
    tid              = OMP_GET_THREAD_NUM() + 1
    reset_pass_count = .TRUE.
    
    !### set the maximum signal speed for this domain solve to zero and we'll let the solvers determine the overall maximum ###
    self%mymaxsignal(tid)      = 0.0_WRD
    self%myn_update_cmplt(tid) = 0

    !### mark all Patch boundaries as unresolved and reset the pass counter ###
    CALL workd%mark_all_patch_bounds_unresolved(bflavor, reset_pass_count)
    reset_pass_count = .FALSE.

    !### we have an outer while loop since som updates require more than a single pass to complete ###
    DO WHILE (n_update_cmplt .LT. workd%nLivePatches)

        !### reset the counters for neighbor mailboxes ###
        CALL decomp%rma_reset_neighbors()

        !### track the number of Patches updated in this pass ###
        n_updated = 0

        !### init our local counters ###
        self%myn_updated(tid)        = 0
        self%myall_packed(tid)       = .FALSE.
        self%myall_signals_recv(tid) = .FALSE.
    
        count_exchanges = .TRUE.
        local_unpacked  = .FALSE.
        n_unpacked      = 0

        !### mark all Patch boundaries as unresolved but do not reset the pass counter ###
        CALL workd%mark_all_patch_bounds_unresolved(bflavor, reset_pass_count)

        !### start a loop that will get every Patch eventually updated ###
        DO WHILE (n_updated .LT. workd%nLivePatches)

            !### pack all boundaries that we can (both local and into RMA buffers) ###
            CALL workd%pack_some_patch_bounds(decomp, bflavor, self%myall_packed(tid), count_exchanges)
            IF (count_exchanges) count_exchanges = .FALSE.

            !### do any signalling necessary if we did not use all of the decomp resources we have ###
            CALL decomp%signal_empty_mailboxes()

            !### unpack all local Patch boundaries if we have not done so yet ###
            IF (.NOT. local_unpacked) THEN

                CALL workd%unpack_all_patch_bounds_local(decomp%myrank, bflavor)
                local_unpacked = .TRUE.

            END IF

            !### enter a polling loop where we check to see if we can start any boundary transfers from neighbors and start them if ready ###
            all_signals_recv = .FALSE.
            DO WHILE (.NOT. all_signals_recv)

                !### check for any transfers we can start.  this loop continues until all GETs have been issued ###
                CALL decomp%issue_signalled_gets(self%myall_signals_recv(tid))
                all_signals_recv = ALL(self%myall_signals_recv)

                !### if we're still going to poll, see if there's anything we can update while we wait ###
                IF (.NOT. all_signals_recv) THEN

                    CALL self%update_single_ready_patch(workd, prob, rankloc, simunits, flavor, bflavor)

                END IF 

            END DO

            !### update any Patches with resolved boundary "recvs". ###
            CALL self%update_any_ready_patches(workd, prob, rankloc, simunits, flavor, bflavor)

            !### now loop back through neighbors flushing each one and then copying out all messages from that neighbor ###
            IF (self%is1d) THEN
                CALL decomp%unpack_mailboxes(workd%domain1d, workd%npx, 1, 1, n_unpacked)
            ELSE IF (self%is2d) THEN
                CALL decomp%unpack_mailboxes(workd%domain2d, workd%npx, workd%npy, 1, n_unpacked)
            ELSE IF (self%is3d) THEN
                CALL decomp%unpack_mailboxes(workd%domain3d, workd%npx, workd%npy, workd%npz, n_unpacked)
            END IF

            !### now we poll waiting to hear back from neighbors that the buffer can be reused in the next loop ###
            all_signals_recv = .FALSE.
            DO WHILE (.NOT. all_signals_recv)

                !### check that our neighbors have all completed their GETs ###
                CALL decomp%check_completed_gets(self%myall_signals_recv(tid))
                all_signals_recv = ALL(self%myall_signals_recv)

                !### if we're still going to poll, see if there's anything we can update while we wait ###
                IF (.NOT. all_signals_recv) THEN

                    CALL self%update_single_ready_patch(workd, prob, rankloc, simunits, flavor, bflavor)

                END IF

            END DO

            !### reduce the number of updated Patches across all threads ###
            !$OMP BARRIER
            n_updated      = SUM(self%myn_updated)
            n_update_cmplt = SUM(self%myn_update_cmplt)
            all_packed     = ALL(self%myall_packed)

        END DO

    END DO

    !### set the maximum found signal speed ###
    !$OMP SINGLE
    workd%maxsignal = MAX(workd%maxsignal, MAXVAL(self%mymaxsignal))
    !$OMP END SINGLE

END SUBROUTINE solve

!### this routine will update no more than a single ready Patch per thread ###
SUBROUTINE update_single_ready_patch(self, workd, prob, rankloc, simunits, flavor, bflavor)
  
    CLASS(DomainSolver) :: self
    TYPE(Domain) :: workd
    TYPE(Problem) :: prob
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits
    INTEGER(WIS), INTENT(IN) :: flavor, bflavor
    INTEGER(WIS) :: tid, pi, pj, pk, ierr, p
    LOGICAL(WIS) :: update_done, update_cmplt
    
    tid         = OMP_GET_THREAD_NUM() + 1
    update_done = .FALSE.

    IF (self%is1d) THEN

        !$OMP DO SCHEDULE(GUIDED)
        DO pi = 1, workd%npx

            !## if the Patch has zones, was not already updated, and has all boundaries for this flavor resolved we can perform the update ###
            !### note that we can be compiled to turn off updates so that communication alone can be benchmarked ###
            IF (.NOT. update_done) THEN

                IF (workd%domain1d(pi)%hasZones .AND. .NOT. workd%domain1d(pi)%updated .AND. workd%domain1d(pi)%flavor_resolved(bflavor,1) &
                     .AND. workd%domain1d(pi)%flavor_resolved(bflavor,2)) THEN

#ifndef WMBT_COMM_BENCHMARK
                    !### we need to apply any problem specific boundary conditions ###
                    CALL workd%applyproblem_patch_bounds(prob, rankloc, simunits, pi, 1, 1, self%mymaxsignal(tid), ierr)

                    IF (flavor .EQ. 1) THEN
                        CALL self%mhd%mhd_solve(workd%domain1d(pi), self%mhdpool(tid), self%mymaxsignal(tid), update_cmplt)
                    ELSE IF (flavor .EQ. 3) THEN
                        CALL self%pass%passive_solve(workd%domain1d(pi), self%passpool(tid), self%mymaxsignal(tid), update_cmplt)
                    END IF
#else
                    update_cmplt = .TRUE.
#endif
                
                    !### perform only a single update ###
                    workd%domain1d(pi)%update_pass_count = workd%domain1d(pi)%update_pass_count + 1
                    workd%domain1d(pi)%updated           = .TRUE.
                    self%myn_updated(tid)                = self%myn_updated(tid) + 1
                    update_done                          = .TRUE.
                    IF (update_cmplt) self%myn_update_cmplt(tid) = self%myn_update_cmplt(tid) + 1

                END IF

            END IF

        END DO
        !$OMP END DO NOWAIT

    ELSE IF (self%is2d) THEN

        !$OMP DO SCHEDULE(GUIDED)
        DO p = 1, workd%npx*workd%npy

            !### obtain the x/y location of the Patch ###
            CALL collapse2d(p, workd%npx, workd%npy, pi, pj)

            !## if the Patch has zones, was not already updated, and has all boundaries for this flavor resolved we can perform the update ###
            !### note that we can be compiled to turn off updates so that communication alone can be benchmarked ###
            IF (.NOT. update_done) THEN

                IF (workd%domain2d(pi,pj)%hasZones .AND. .NOT. workd%domain2d(pi,pj)%updated .AND. workd%domain2d(pi,pj)%flavor_resolved(bflavor,1) &
                     .AND. workd%domain2d(pi,pj)%flavor_resolved(bflavor,2)) THEN
                        
#ifndef WMBT_COMM_BENCHMARK
                    !### we need to apply any problem specific boundary conditions ###
                    CALL workd%applyproblem_patch_bounds(prob, rankloc, simunits, pi, pj, 1, self%mymaxsignal(tid), ierr)

                    IF (flavor .EQ. 1) THEN
                        CALL self%mhd%mhd_solve(workd%domain2d(pi,pj), self%mhdpool(tid), self%mymaxsignal(tid), update_cmplt)
                    ELSE IF (flavor .EQ. 2) THEN
                        CALL self%mhd%ct_solve(workd%domain2d(pi,pj), self%mhdpool(tid), self%mymaxsignal(tid), update_cmplt)
                    ELSE IF (flavor .EQ. 3) THEN
                        CALL self%pass%passive_solve(workd%domain2d(pi,pj), self%passpool(tid), self%mymaxsignal(tid), update_cmplt)
                    END IF
#else
                    update_cmplt = .TRUE.
#endif
                    
                    !### perform only a single update ###
                    workd%domain2d(pi,pj)%update_pass_count = workd%domain2d(pi,pj)%update_pass_count + 1
                    workd%domain2d(pi,pj)%updated           = .TRUE.
                    self%myn_updated(tid)                   = self%myn_updated(tid) + 1
                    update_done                             = .TRUE.
                    IF (update_cmplt) self%myn_update_cmplt(tid) = self%myn_update_cmplt(tid) + 1
                    
                END IF
                    
            END IF

        END DO
        !$OMP END DO NOWAIT

    ELSE IF (self%is3d) THEN

        !$OMP DO SCHEDULE(GUIDED)
        DO p = 1, workd%npx*workd%npy*workd%npz

            !### obtain the x/y/z location of the Patch ###
            CALL collapse3d(p, workd%npx, workd%npy, workd%npz, pi, pj, pk)

            !## if the Patch has zones, was not already updated, and has all boundaries for this flavor resolved we can perform the update ###
            !### note that we can be compiled to turn off updates so that communication alone can be benchmarked ###
            IF (.NOT. update_done) THEN
                
                IF (workd%domain3d(pi,pj,pk)%hasZones .AND. .NOT. workd%domain3d(pi,pj,pk)%updated .AND. workd%domain3d(pi,pj,pk)%flavor_resolved(bflavor,1) &
                     .AND. workd%domain3d(pi,pj,pk)%flavor_resolved(bflavor,2)) THEN
                    
#ifndef WMBT_COMM_BENCHMARK
                    !### we need to apply any problem specific boundary conditions ###
                    CALL workd%applyproblem_patch_bounds(prob, rankloc, simunits, pi, pj, pk, self%mymaxsignal(tid), ierr)
                    
                    IF (flavor .EQ. 1) THEN
                        CALL self%mhd%mhd_solve(workd%domain3d(pi,pj,pk), self%mhdpool(tid), self%mymaxsignal(tid), update_cmplt)
                    ELSE IF (flavor .EQ. 2) THEN
                        CALL self%mhd%ct_solve(workd%domain3d(pi,pj,pk), self%mhdpool(tid), self%mymaxsignal(tid), update_cmplt)
                    ELSE IF (flavor .EQ. 3) THEN
                        CALL self%pass%passive_solve(workd%domain3d(pi,pj,pk), self%passpool(tid), self%mymaxsignal(tid), update_cmplt)
                    END IF
#else
                    update_cmplt = .TRUE.
#endif
                            
                    !### perform only a single update ###
                    workd%domain3d(pi,pj,pk)%update_pass_count = workd%domain3d(pi,pj,pk)%update_pass_count + 1
                    workd%domain3d(pi,pj,pk)%updated           = .TRUE.
                    self%myn_updated(tid)                      = self%myn_updated(tid) + 1
                    update_done                                = .TRUE.
                    IF (update_cmplt) self%myn_update_cmplt(tid) = self%myn_update_cmplt(tid) + 1
                        
                END IF
                    
            END IF

        END DO
        !$OMP END DO

    END IF

END SUBROUTINE update_single_ready_patch

!### this routine will update any ready patches leaving maybe 1-5 Patches per thread left do do later ###
SUBROUTINE update_any_ready_patches(self, workd, prob, rankloc, simunits, flavor, bflavor)
  
    CLASS(DomainSolver) :: self
    TYPE(Domain) :: workd
    TYPE(Problem) :: prob
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits
    INTEGER(WIS), INTENT(IN) :: flavor, bflavor
    INTEGER(WIS) :: tid, n_updated, pi, pj, pk, ierr, p, n_update_cmplt
    LOGICAL(WIS) :: update_cmplt
    
    tid            = OMP_GET_THREAD_NUM() + 1
    n_updated      = 0
    n_update_cmplt = 0

    IF (self%is1d) THEN
            
        !$OMP DO SCHEDULE(GUIDED)
        DO pi = 1, workd%npx

            IF (workd%domain1d(pi)%hasZones .AND. .NOT. workd%domain1d(pi)%updated .AND. workd%domain1d(pi)%flavor_resolved(bflavor,1) &
                 .AND. workd%domain1d(pi)%flavor_resolved(bflavor,2)) THEN

#ifndef WMBT_COMM_BENCHMARK
                !### we need to apply any problem specific boundary conditions ###
                CALL workd%applyproblem_patch_bounds(prob, rankloc, simunits, pi, 1, 1, self%mymaxsignal(tid), ierr)

                IF (flavor .EQ. 1) THEN
                    CALL self%mhd%mhd_solve(workd%domain1d(pi), self%mhdpool(tid), self%mymaxsignal(tid), update_cmplt)
                ELSE IF (flavor .EQ. 3) THEN
                    CALL self%pass%passive_solve(workd%domain1d(pi), self%passpool(tid), self%mymaxsignal(tid), update_cmplt)
                END IF
#else
                update_cmplt = .TRUE.
#endif
                workd%domain1d(pi)%update_pass_count = workd%domain1d(pi)%update_pass_count + 1
                workd%domain1d(pi)%updated           = .TRUE.
                n_updated                            = n_updated + 1
                IF (update_cmplt) n_update_cmplt     = n_update_cmplt + 1

            END IF

        END DO
        !$OMP END DO NOWAIT

    ELSE IF (self%is2d) THEN

        !$OMP DO SCHEDULE(GUIDED)
        DO p = 1, workd%npx*workd%npy

            !### obtain the x/y location of the Patch ###
            CALL collapse2d(p, workd%npx, workd%npy, pi, pj)
                        
            IF (workd%domain2d(pi,pj)%hasZones .AND. .NOT. workd%domain2d(pi,pj)%updated .AND. workd%domain2d(pi,pj)%flavor_resolved(bflavor,1) &
                 .AND. workd%domain2d(pi,pj)%flavor_resolved(bflavor,2)) THEN
                        
#ifndef WMBT_COMM_BENCHMARK
                !### we need to apply any problem specific boundary conditions ###
                CALL workd%applyproblem_patch_bounds(prob, rankloc, simunits, pi, pj, 1, self%mymaxsignal(tid), ierr)

                IF (flavor .EQ. 1) THEN
                    CALL self%mhd%mhd_solve(workd%domain2d(pi,pj), self%mhdpool(tid), self%mymaxsignal(tid), update_cmplt)
                ELSE IF (flavor .EQ. 2) THEN
                    CALL self%mhd%ct_solve(workd%domain2d(pi,pj), self%mhdpool(tid), self%mymaxsignal(tid), update_cmplt)
                ELSE IF (flavor .EQ. 3) THEN
                    CALL self%pass%passive_solve(workd%domain2d(pi,pj), self%passpool(tid), self%mymaxsignal(tid), update_cmplt)
                END IF
#else
                update_cmplt = .TRUE.
#endif
                workd%domain2d(pi,pj)%update_pass_count = workd%domain2d(pi,pj)%update_pass_count + 1
                workd%domain2d(pi,pj)%updated           = .TRUE.
                n_updated                               = n_updated + 1
                IF (update_cmplt) n_update_cmplt        = n_update_cmplt + 1

            END IF
            
        END DO
        !$OMP END DO NOWAIT
            
    ELSE IF (self%is3d) THEN

        !$OMP DO SCHEDULE(GUIDED)
        DO p = 1, workd%npx*workd%npy*workd%npz

            !### obtain the x/y/z location of the Patch ###
            CALL collapse3d(p, workd%npx, workd%npy, workd%npz, pi, pj, pk)
                    
            IF (workd%domain3d(pi,pj,pk)%hasZones .AND. .NOT. workd%domain3d(pi,pj,pk)%updated .AND. workd%domain3d(pi,pj,pk)%flavor_resolved(bflavor,1) &
                 .AND. workd%domain3d(pi,pj,pk)%flavor_resolved(bflavor,2)) THEN
                        
#ifndef WMBT_COMM_BENCHMARK
                !### we need to apply any problem specific boundary conditions ###
                CALL workd%applyproblem_patch_bounds(prob, rankloc, simunits, pi, pj, pk, self%mymaxsignal(tid), ierr)
                
                IF (flavor .EQ. 1) THEN
                    CALL self%mhd%mhd_solve(workd%domain3d(pi,pj,pk), self%mhdpool(tid), self%mymaxsignal(tid), update_cmplt)
                ELSE IF (flavor .EQ. 2) THEN
                    CALL self%mhd%ct_solve(workd%domain3d(pi,pj,pk), self%mhdpool(tid), self%mymaxsignal(tid), update_cmplt)
                ELSE IF (flavor .EQ. 3) THEN
                    CALL self%pass%passive_solve(workd%domain3d(pi,pj,pk), self%passpool(tid), self%mymaxsignal(tid), update_cmplt)
                END IF
#else
                update_cmplt = .TRUE.
#endif
                workd%domain3d(pi,pj,pk)%update_pass_count = workd%domain3d(pi,pj,pk)%update_pass_count + 1
                workd%domain3d(pi,pj,pk)%updated           = .TRUE.
                n_updated                                  = n_updated + 1
                IF (update_cmplt) n_update_cmplt           = n_update_cmplt + 1
                
            END IF

        END DO
        !$OMP END DO NOWAIT

    END IF

    !### increment our count of updated Patches ###
    self%myn_updated(tid)      = self%myn_updated(tid) + n_updated
    self%myn_update_cmplt(tid) = self%myn_update_cmplt(tid) + n_update_cmplt

END SUBROUTINE update_any_ready_patches

END MODULE Mod_DomainSolver
