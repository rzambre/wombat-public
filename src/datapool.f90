#include "config.h"

MODULE Mod_DataPool

!######################################################################
!#
!# FILENAME: mod_datapool.f90
!#
!# DESCRIPTION: This module defines an abstract class for managing solver
!#              specific work arrays.  Solvers can each have their own
!#              sub-class of this.
!#
!# DEPENDENCIES: Mod_Globals for defining data types
!#
!# HISTORY:
!#    5/7/12  - Peter Mendygral
!#
!######################################################################

!### require the needed modules ###
USE Mod_Globals

IMPLICIT NONE

!### default private but make the data type available ###
PRIVATE
PUBLIC :: DataPool

!### define a data type for this module ###
TYPE, ABSTRACT :: DataPool

    !### default everything as private ###
    PRIVATE

    !### we'll hold on to our own sizing parameters ###
    INTEGER(WIS), PUBLIC :: ndim, nx, ny, nz, nb

    CONTAINS

    !### default everything as private ###
    PRIVATE

    PROCEDURE, PUBLIC :: getDims

END TYPE DataPool

CONTAINS

!### getDims just returns a list of the number of dimensions and their size used to allocate the work arrays ###
SUBROUTINE getDims(self, vals)

    CLASS(DataPool) :: self
    INTEGER(WIS), INTENT(OUT) :: vals(4)

    vals = (/self%ndim, self%nx, self%ny, self%nz/)

END SUBROUTINE getDims

END MODULE Mod_DataPool
