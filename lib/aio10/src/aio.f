c
c  AIO:  Asynchronous Input/Ouput routines
c  Version   Date          Modification
c  aio02a
c  aio03     2008 May 17   aio_read implemented
c  aio03     2008          aio_server pings clients when write is done
c  aio03     2008 Nov  8   mylast... fixed in aio_team_init & aio_init
c  aio05     2008 Dec 15   added aio_buf* routines
c  aio06     2009 Apr  9   added compression in aio_server
c  aio07     2008 Apr 11   added precomprs handling in aio_server
c  aio07ddt  2008 Apr 11   timing info installed
c  aio07ddt  2008 Oct 16   timing measures fixed for OpenMPI
c  aio08     2011 Apr 16   format and write HV files
c  aio09     2012 Jan 14   Packadged as a library & aio_all_read
c  aio09a    2012 Jan 14   fixed init hang: removed mpi_com_create
c  aio10(a)  2014 Oct  1   Non-standard %LOC() ==> standqrd LOC()
c  aio10(b)  2014 Oct  1   deallocs in aio_server
c  aio10(b)  2014 Oct  1   varibble buffer out tested
c  aio10(b)  2014 Oct  1   added convenience routine: aio_write_buffer
c
c  Support large writes from many sources merged into one or a few:
c    bobs
c    cdumps & moment dumps
c    restarts
c    HV files (generated in memory by a single AIO server)
c
c TODO:
c
c >>>
c
c
c
c  * aio_init(...)
c    + re-implment interms of routines, above
c
c * aio_server(ifile)
c    + calls aio_set_data_extention(iFile)
c    + loops over iclients(1:nclients) to accumulate meta-data to one file
c * 
c
c***********************************************************************
c***********************************************************************
c****                                                               ****
c****                      Client API                               ****
c****                                                               ****
c***********************************************************************
c***********************************************************************
c
c routtines:
c     aio_init        initialize/set io teams
c     aio_team_init   initialize/set io teams PRW style teams
c     aio_wait        blocking wait on io a given buffer
c     aio_write       non-blocking write request from a buffer
c     aio_read        non-blocking read request to a buffer
c     aio_progress    non-blocking progress routine (called when there's only one user thread)
c     aio_status      non-blocking: returns current status of an aio slot
c     aio_set_data_extention_range   sets data file extention range
c     aio_set_meta_extention         set default meta-data file extention
c     aio_set_max_data_size          set maximum data buffer size
c     aio_gen_data_file_name         Generatge data file name with extention
c
c  Routines: aio_all_read
c
c=====================================================================72

c=====================================================================72

c=====================================================================72
      !!
      subroutine aio_set_data_extention_range(cExtLow, cExtHigh) !!
      implicit none
      include "aio_team_info.h"
      character*(*) cExtLow   !!! Lowest (1st) aio data file extention
      character*(*) cExtHigh  !!! Max possible aio data file extention
      integer nLo, nHi
      !!  Default: cExtLow="-000" ;   cExtHigh="-999"
      !!  Available range: 0 to Z = {0,...,9, a,...,z, A,...,Z}
      !!  Examples (n = number of fields used):
      !!    Decimal: "-000"    "-999"     up to 10^n servers
      !!    Alpha:   "A"       "Z"        up to 26^n servers 
      !!    Hex:     "-00.dat" "-ff.dat"  up to 16^n servers
      !!    Binary:  "--0000"  "--1111"   up to  2^n servers
      !!    Compact: "_00"     "_ZZ"      up to 62^n servers
      data cExtLo/"-000            "/
      data cExtHi/"-999            "/

      cExtLo = "                "
      cExtHi = "                "
      nLo = len(trim(cExtLow))
      nHi = len(trim(cExtHigh))
      cExtLo(1:nLo) = trim(cExtLow)
      cExtHi(1:nHi) = trim(cExtHigh)

      return
      end
c=====================================================================72
      !!
      subroutine aio_set_meta_extention(cExtMeta)  !!
      implicit none
      include "aio_team_info.h"
      character*(*) cExtMeta  !!! aio meta-data file extention
      integer n
      !! Default: cExtMeta=".meta"
      data cExtMe/".meta           "/

      cExtMe = "                "
      n = len(trim(cExtMe))
      cExtMe(1:n) = trim(cExtMeta)

      return
      end

c=====================================================================72
      !!
      subroutine aio_set_verbosity(verbosity)  !!
      implicit none
      include "aio_team_info.h"
      integer verbosity       !!! aio verosity level
      data iverbose/0/
      !! Controls diagnostic output written by AIO servers to STDOUT
      !!   verbosity  = 0   No diagnostic output (default)
      !!   verbosity >= 1   AIO servers report at start-up only
      !!   verbosity >= 2   AIO servers report IO timing info
      !!   verbosity >= 10  Detailed AIO debugging information
      iverbose = verbosity
      return
      end

c=====================================================================72
      !!
      subroutine aio_set_max_data_size(iMaxBuf)  !!
      implicit none
      include "aio_team_info.h"
      integer iMaxBuf        !!! Maximum aio data buffer size [bytes]
      !! Default:  iMaxBuf = 10000000   (10 MB)
      data maxb/10000000/
      maxb = iMaxBuf
      return
      end
c=====================================================================72
      !!
      subroutine aio_set_data_extention(iServerNumber) !!
      implicit none
      include "aio_team_info.h"
      integer iServerNumber  !!! Number of aio server (0,...,nservers-1)
      !!  Generates aio extention for files written by this aio server
      integer nc, i, is, ic, i0, i1, base
      character*1 cSet(100)
      call gen_cSet(cSet,nc)

      cExtDa = cExtLo
      i = iServerNumber
      do ic = len(trim(cExtLo)), 1, -1
        i0 = nc + 1
        i1 = 0
        do is = 1, nc
          if(cExtLo(ic:ic) .eq. cSet(is)) i0 = is
          if(cExtHi(ic:ic) .eq. cSet(is)) i1 = is
        enddo

        if(i1 .gt. i0) then
          base = 1 + i1 - i0
          cExtDa(ic:ic) = cSet(i0 + mod(i,base))
          i = i / base
        endif
      enddo

      return
      end
c=====================================================================72
      subroutine gen_cSet(cSet, nc)
      implicit none
      character*1 cSet(100)
      integer nc, i
      nc = 0
      do i = 0, 9
        nc = nc + 1
        cSet(nc) = char(ichar('0') + i)
      enddo
      do i = 0, 25
        nc = nc + 1
        cSet(nc) = char(ichar('a') + i)
      enddo
      do i = 0, 25
        nc = nc + 1
        cSet(nc) = char(ichar('A') + i)
      enddo
      return
      end
c=====================================================================72
      !!
      subroutine aio_gen_data_file_name(cRoot, cDataFile)  !!
      implicit none
      include "aio_team_info.h"
      character*(*) cRoot     !!! Root name
      character*(*) cDataFile !!! Full name for this server or client
      !!  Generage full (root+extention) file name written by aio server
      integer i, nRoot, nExt

      do i = 1, len(cDataFile)
        cDataFile(i:i) = ' '
      enddo
      nRoot = len(trim(cRoot))
      nExt  = len(trim(cExtDa))
      cDataFile(1:nRoot+nExt) = trim(cRoot) // trim(cExtDa)
      
      return
      end

c PJM added to allow passing an explicit list of clients/servers
      subroutine aio_set_explicit_clients(servers, nioservers,
     1  workers, nworkers, comm)

      implicit none
      include "mpif.h"
      include 'aio_client_data.h'
      include "aio_team_info.h"
      integer comm, ierr, i, nranks, w
      integer nioservers, nworkers
      integer servers(nioservers), workers(nworkers)
      logical amserver

      call MPI_COMM_SIZE (comm, nranks, ierr)
      call MPI_COMM_RANK (comm, myid, ierr)
      if(nioservers .le. 0) then
         write (6,*) 'PROBLEM in aio_set_explicit_clients: '
         write (6,*) '   nranks = ', nranks
         write (6,*) '   mynworkers = ', nworkers
         write (6,*) '   nioservers = ', nioservers
         stop
      endif

      amserver = .false.
      do i = 1, nioservers
         if (myid .eq. servers(i)) then

            amserver  = .true.
            nclients  = nworkers
            do w = 1, nworkers
               myclients(w) = workers(w)
            end do
            myserver  = -1
            ifile_aio = i-1

         endif
      enddo

      !### if we're not a server we need 
      !### to set who our server is ###
      if (.not. amserver) then

         do i = 1, nioservers

            if (i .eq. 1) then
               if (myid .lt. servers(i)) then
                  myserver  = servers(i)
                  ifile_aio = i-1
                  nclients  = 0
               endif
            else
               if (myid .gt. servers(i-1) .and. 
     1             myid .lt. servers(i)) then
                  myserver  = servers(i)
                  ifile_aio = i-1
                  nclients  = 0
               end if
            end if

         end do

      end if

      ! on all ranks:
      nservers = nioservers
      do i = 1, nioservers
         iservers(i) = servers(i)
      end do

      call aio_set_data_extention(ifile_aio)

      return
      end
c=====================================================================72
      !!
      subroutine aio_set_clients_to_low_ranks(nworkers, comm)  !!
      implicit none
      include "mpif.h"
      include 'aio_client_data.h'
      include "aio_team_info.h"
      integer nworkers     !!! Number of worker ranks
      integer comm         !!! MPI communicator
      integer nranks, ierr, nioservers, i
      integer maxclientsperserver
      integer myfirstclient, mylastclient
      !!!  Ranks 0,nworkers-1     will be aio clients
      !!!  Ranks nworkers,nranks  will be aio wervers

      call MPI_COMM_SIZE (comm, nranks, ierr)
      call MPI_COMM_RANK (comm, myid, ierr)
      nioservers = nranks - nworkers
      if(nioservers .le. 0) then
         write (6,*) 'PROBLEM in aio_set_clients_to_low_ranks: '
         write (6,*) '   nranks = ', nranks
         write (6,*) '   nworkers = ', nworkers
         write (6,*) '   nioservers = ', nioservers
         stop
      endif

      maxclientsperserver = (nworkers+nioservers-1) / nioservers
      if(maxclientsperserver .gt. 10000) then
         write (6,*) 'PROBLEM in aio_set_clients_to_low_ranks: '
         write (6,*) '   maxclientsperserver = ', maxclientsperserver
         write (6,*) '   exceeds limit of 10000'
         write (6,*) '   Stopping.'
         stop
      endif

      if(myid .lt. nworkers) then
        ! I am a client
        ! Set my aio server
        !
        myserver = nworkers + myid / maxclientsperserver
        ifile_aio = myserver - nworkers
        nclients = 0
      else
        ! I am a server
        ! Generate list of my aio clients
        !
       myfirstclient = (myid-nworkers)*maxclientsperserver
       mylastclient  = myfirstclient + maxclientsperserver - 1
       mylastclient  = min(nworkers-1, mylastclient)
       nclients      = 1 + mylastclient - myfirstclient
       do i=1, nclients
         myclients(i) = myfirstclient + i - 1
        enddo
         myserver = -1   ! flag that I am a server
        ifile_aio = myid - nworkers
      endif

      ! on all ranks:
      nservers = nioservers
      do i = 1, nservers
        iservers(i) = nworkers + i - 1  ! MPI rank of server
      enddo

      call aio_set_data_extention(ifile_aio)

      return
      end

c=====================================================================72
c >>>
c=====================================================================72
      !!
      subroutine aio_write_buffer(cFullPath, nbytes, buffer, handle) !!
      implicit none
      character*(*) cFullPath  !!! file name, or path to file
      integer nbytes           !!! NUmber of bytes in buffer
      byte buffer(*)           !!! Pointer to data buffer
      integer handle           !!! AIO handle for write request, range: [1, 100]
      integer i
      character*256 cHost, cDir, cName
      real sendinfo(64)
      !! Initiates an asyncronous write.
      !! Data in buffer will be written into file cFullPath with data from other AIO clients.
      !! use aio_wait(handle) to wait till "buffer" is free.

      do i = 1,64
        sendinfo(i) = 0.0
      enddo
      call get_HDF(cFullPath, cHost, cDir, cName) 
      call aio_write(cHost,cDir,cName,sendinfo, nbytes, buffer, handle)

      return
      end

c=====================================================================72
      subroutine aio_add_meta_to_buf(nbuf, nmeta, buf, meta)
      implicit none
      integer nbuf, nmeta, i
      byte buf(*), meta(*)
      do i = 1, nmeta
        buf(nbuf+i) = meta(i)
      enddo
      return
      end

c=====================================================================72
      !!
      subroutine aio_write_buf_meta(cFullPath,nbuf,nmeta,buf,ibuf,ic) !!
      implicit none
      character*(*) cFullPath  !!! file name, or path to file
      integer nbuf             !!! NUmber of bytes of real data in buf
      integer nmeta            !!! NUmber of bytes of meta-data in buf
      byte buf(*)              !!! Pointer to buffer with real and meta-data
      integer ibuf             !!! AIO handle for write request, range: [1, 100]
      integer ic               !!! Meta-data output control
      integer nbytes, i
      character*256 cHost, cDir, cName
      real sendinfo(64)
      !! Initiates an asyncronous write of real data with meta-data.
      !! Meta-data must all be at the end of the buf
      !! use aio_wait(handle) to wait till real data is written and "buf" is free.
      !! ic = 0: keep collecting meta-data
      !! ic = 1: collect meta-data in this call and write to file with detaul meta-data file extension
      !! ic = 2: write meta-data to file name provided in this cal

      nbytes = nbuf + nmeta
      do i = 1,64
        sendinfo(i) = 0.0
      enddo
      sendinfo(64) = float(nmeta) ! nmeta
      sendinfo(63) = float(ic)    ! metafin
      call get_HDF(cFullPath, cHost, cDir, cName) 
      call aio_write(cHost,cDir,cName,sendinfo, nbytes, buf, ibuf)

      return
      end

c=====================================================================72
      subroutine aio_all_read(cFullPath, maxread, ngot, buffer)
      include 'mpif.h'
      include 'aio_client_data.h'
      character*(*) cFullPath
      character*256 cHost, cDir, cName
      byte buffer(maxread)
      integer(kind=8) i64off

      integer sw_read
      integer myrank, nallread, comm
      common /aio_all_read_data/ myrank, nallread, comm
      data myrank/-1/ 
      data nallread/-1/  ! means aio_init has not been called yet & all ranks participate in bcast

      if(myrank .lt. 0) call mpi_comm_rank(MPI_COMM_WORLD,myrank,ierr)
      if(nallread .lt. 0) comm = MPI_COMM_WORLD

      if(myrank .eq. 0) then
        i64off = 0
        call get_HDF(cFullPath, cHost, cDir, cName) 
        ngot = sw_read(cHost,cDir,cName,i64off,maxread,buffer)
      endif

      call mpi_bcast(ngot, 1, MPI_INTEGER, 0, comm, ierr)
      if(ngot .gt. 0) then
        call mpi_bcast(buffer, ngot, MPI_BYTE, 0, comm, ierr)
      endif

      return
      end

c=====================================================================72
      subroutine get_HDF(cHostDirName, cHost, cDir, cName) 
      character*(*) cHostDirName
      character*256 cHost, cDir, cName

      ! Find the index in cHostDirName berween the directory and the file name 
      ! I should be the last character in the string
      islash = 0
      ilast = len(cHostDirName)
      do i=1, ilast
        if(cHostDirName(i:i) .eq. '/') islash = i
      enddo

      ! pad all names with spaces
      do i=1, 256
        cHost(i:i) = ' '
        cDir(i:i)  = ' '
        cName(i:i) = ' '
      enddo

      ! Default: the file is on a locally mounted disk
      cHost(1:9) = 'localdisk'

      if(islash .gt. 0) then
        ! cHostDirName contains path info to the file
        iPathLen = islash - 1
        iNameLen = ilast-islash
        cDir(1:iPathLen)  = cHostDirName(1:iPathLen)
        cName(1:iNameLen) = cHostDirName(islash+1:ilast)
      else
        ! cHostDirName is just the name of the file, assume it is in cwd
        cDir(1:1)      = '.'
        cName(1:ilast) = cHostDirName(1:ilast)
      endif

      return
      end

c=====================================================================72
c=====================================================================72
c  Sequential (seq) IO to a single file
c  Routines: aio_seq_open, aio_seq_close, aio_seq_write, aio_seq_read
c=====================================================================72
      subroutine aio_seq_open(islot, host, path, file)
      include 'mpif.h'
      include 'aio_client_data.h'
      character*256 host, path, file

      ! Make sure requested slot is in the range
      if(islot .lt. 1  .or.  islot .gt. nslots) then
        write (6,*) 'PROBLEM in aio_write:'
        write (6,*) '   islot = ', islot
        write (6,*) '   is out of range.'
        write (6,*) '   Stopping.'
        stop
      endif

      ! Make sure this slot is clear, then mark slot as open
      call aio_wait(islot)
      is_open(islot) = 1

      ! Save host, path, and file for later use
      hostslot(islot) = host
      pathslot(islot) = path
      fileslot(islot) = file

      ! Initialize offset into file
      i64fileoff(islot) = 0

      return
      end


c=====================================================================72
      subroutine aio_seq_write(islot, nbytes, buffer)
      include 'mpif.h'
      include 'aio_client_data.h'

      call aio_wait_all()
      call aio_check_open(islot)

      ! Save all write request data, including a pointer to the real data
      do i = 1,64
        rsendslot(i,islot) = 0.0
      enddo
      mbytes(islot) = nbytes
      ptr(islot) = loc(buffer)
      iprogress(islot) = 1

      ! Start and finish aio write here
      call aio_progress()
      call aio_wait_all()

      return
      end

c=====================================================================72
      subroutine aio_seq_read(islot, nbytes, buffer)
      include 'mpif.h'
      include 'aio_client_data.h'
      real buffer(10)

      call aio_wait_all()
      call aio_check_open(islot)

      ! Save all read request data, including a pointer to the receive buffer
      mbytes(islot) = -nbytes   ! Negative means read in bytes
      ptr(islot) = loc(buffer)
      iprogress(islot) = 1

      ! Start and finish aio read here
      call aio_progress()
      call aio_wait_all()

      return
      end

c=====================================================================72
      subroutine aio_seq_close(islot)
      include 'mpif.h'
      include 'aio_client_data.h'

      call aio_wait_all()
      call aio_check_open(islot)
      i64fileoff(islot) = 0
      is_open(islot)    = 0

      return
      end

c=====================================================================72
      subroutine aio_seq_offset64(islot, i64offset)
      include 'mpif.h'
      include 'aio_client_data.h'
      integer(kind=8) i64offset

      call aio_check_open(islot)

      ! Set offset into target file
      call aio_wait(islot)
      i64fileoff(islot) = i64offset

      return
      end

c=====================================================================72
      subroutine aio_check_open(islot)
      include 'mpif.h'
      include 'aio_client_data.h'

      if(is_open(islot) .ne. 1) then
        write (6,*) 'PROBLEM in aio_seq_offset64: slot not open'
        ! Attempt to exit cleanly
        call aio_finalize()
        call MPI_BARRIER(MPI_COMM_WORLD, ierr)
        call MPI_FINALIZE(ierr)
        stop
      endif

      return
      end
c  
c=====================================================================72
c=====================================================================72
c=====================================================================72
      subroutine aio_team_init(myid0,nworkers,nioservers,
     1                         ntxbricks,ntybricks,ntzbricks,
     1                         nxteams,nyteams,nzteams,
     1                         maxbytes,myservr)
      include 'aio_client_data.h'
      include "aio_team_info.h"
      integer myid0, myid1
      common /aio_debug/ myid1
      myid  = myid0
      myid1 = myid0

      do i = 1, nslots
        iprogress(i) = 0
        is_open(i)   = 0
      enddo

      maxb = maxbytes

      maxclientsperserver = (nworkers+nioservers-1) / nioservers
      if(maxclientsperserver .gt. 10000) then
         write (6,*) 'PROBLEM in aio_team_init: '
         write (6,*) '   maxclientsperserver = ', maxclientsperserver
         write (6,*) '   exceeds limit of 10000'
         write (6,*) '   Stopping.'
         myid0 = -1
         return
      endif

      nbricksperteam = ntxbricks*ntybricks*ntzbricks
      nxbricks = ntxbricks * nxteams
      nybricks = ntybricks * nyteams
      nzbricks = ntzbricks * nzteams

      if(myid .lt. nworkers) then
        ! I am a client
        ! Set my aio server
        !
        ixyzteam = myid / nbricksperteam
        ixteam   = mod(ixyzteam, nxteams)
        iyteam   = mod(ixyzteam/nxteams, nyteams)
        izteam   = mod(ixyzteam/(nxteams*nyteams), nzteams)

        itbrick  = mod(myid, nbricksperteam)
        itxbrick = mod(itbrick, ntxbricks)
        itybrick = mod(itbrick/ntxbricks, ntybricks)
        itzbrick = mod(itbrick/(ntxbricks*ntybricks), ntzbricks)

        izbrick  = itzbrick + ntzbricks*izteam
        iybrick  = itybrick + ntybricks*iyteam
        ixbrick  = itxbrick + ntxbricks*ixteam

        ibrick   = ixbrick + nxbricks*(iybrick + nybricks*izbrick)
        myserver = nworkers + ibrick / maxclientsperserver
        myservr  = myserver
        ifile_aio = myserver - nworkers

c        write (6,*) 'myid,myservr,ibrick=',myid,myservr,ibrick
      else
        ! I am a server
        ! Generate list of my aio clients
        !
        myserver = -1
        myfirstbrick = (myid-nworkers)*maxclientsperserver
        mylastbrick  = myfirstbrick + maxclientsperserver - 1
        mylastbrick  = min(nworkers-1, mylastbrick)
        nclients     = 1 + mylastbrick - myfirstbrick
        do i=1, nclients
          ibrick   = myfirstbrick+i-1
          ixbrick  = mod(ibrick, nxbricks)
          iybrick  = mod(ibrick/nxbricks, nybricks)
          izbrick  = mod(ibrick/(nxbricks*nybricks), nzbricks)

          ixteam   = ixbrick / ntxbricks
          iyteam   = iybrick / ntybricks
          izteam   = izbrick / ntzbricks

          itxbrick = mod(ixbrick, ntxbricks)
          itybrick = mod(iybrick, ntybricks)
          itzbrick = mod(izbrick, ntzbricks)

          itbrick  = itxbrick+ntxbricks*(itybrick+ntybricks*itzbrick)
          ixyzteam = ixteam  +  nxteams*(iyteam  +  nyteams*  izteam)
          irank    = itbrick + nbricksperteam * ixyzteam

          myclients(i) = irank
        enddo

        ifile_aio = myid - nworkers
c        write (6,*) 'myid,clients=',myid,(myclients(i),i=1,nclients)
      endif

      ! on all ranks:
      nservers = nioservers
      do i = 1, nservers
        iservers(i) = nworkers + i - 1  ! MPI rank of server
      enddo

      call aio_set_data_extention(ifile_aio)

      return
      end

c=====================================================================72
      subroutine aio_init(myid0,nworkers,nioservers,maxbytes,myservr)
      include 'aio_client_data.h'
      include "aio_team_info.h"
      integer myid0, myid1
      common /aio_debug/ myid1
      myid  = myid0
      myid1 = myid0

      do i = 1, nslots
        iprogress(i) = 0
        is_open(i)   = 0
      enddo

      maxb = maxbytes

      maxclientsperserver = (nworkers+nioservers-1) / nioservers
      if(maxclientsperserver .gt. 10000) then
         write (6,*) 'PROBLEM in aio_init: '
         write (6,*) '   maxclientsperserver = ', maxclientsperserver
         write (6,*) '   exceeds limit of 10000'
         write (6,*) '   Stopping.'
         myid = -1
         return
      endif

      if(myid .lt. nworkers) then
        ! I am a client
        ! Set my aio server
        !
        myserver = nworkers + myid / maxclientsperserver
        myservr  = myserver
        ifile_aio = myserver - nworkers
      else
        ! I am a server
        ! Generate list of my aio clients
        !
       myfirstclient = (myid-nworkers)*maxclientsperserver
       mylastclient  = myfirstclient + maxclientsperserver - 1
       mylastclient  = min(nworkers-1, mylastclient)
       nclients      = 1 + mylastclient - myfirstclient
       do i=1, nclients
         myclients(i) = myfirstclient + i - 1
         ifile_aio = myid - nworkers
        enddo
      endif

      ! on all ranks:
      nservers = nioservers
      do i = 1, nservers
        iservers(i) = nworkers + i - 1  ! MPI rank of server
      enddo

      call aio_set_data_extention(ifile_aio)

      return
      end

c=====================================================================72
      subroutine aio_start_servers()
      implicit none
      include "mpif.h"
      include 'aio_client_data.h'
      include "aio_team_info.h"
      integer i, ierr
      data ifile_aio/-1/

      if(ifile_aio .lt. 0) then
        write (6,*) "Problem in aio_start_servers:"
        write (6,*) "  aio lients and serervers have not been set"
        write (6,*) "  Stopping."
        call MPI_FINALIZE(ierr)
        stop
      endif

      do i = 1, nslots
        iprogress(i) = 0
        is_open(i)   = 0
      enddo
      call aio_set_data_extention(ifile_aio)

      if(myserver .lt. 0) then
        ! Only aio servers
        call aio_server(ifile_aio)
        call MPI_BARRIER(MPI_COMM_WORLD, ierr)
        call MPI_FINALIZE(ierr)
        stop
      endif

      return
      end

c=====================================================================72
      subroutine aio_status(islot, istatus)
      include 'aio_client_data.h'

      istatus = iprogress(islot)

      return
      end
c=====================================================================72
      subroutine aio_wait(islot)
      include 'mpif.h'
      include 'aio_client_data.h'
      include "aio_team_info.h"
      dimension iprobestatus(MPI_STATUS_SIZE)

10    continue
      if(iprogress(islot) .eq. 0) return
      call MPI_IPROBE(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD,
     1                iprobeflag, iprobestatus, iprobeerr)
      call aio_progress()
c      call sleep(10)
      go to 10
      return
      end

c=====================================================================72
      subroutine aio_wait_all()
      include 'mpif.h'
      include 'aio_client_data.h'
      do i = 1, nslots
        call aio_wait(i)
      enddo
      return
      end

c=====================================================================72
      subroutine aio_write(host,path,file,sendinfo,nbytes,buffer,islot)
      include 'mpif.h'
      character*256 host, path, file
      real sendinfo(64)
      include 'aio_client_data.h'
      include "aio_team_info.h"

      if(islot .lt. 1  .or.  islot .gt. nslots) then
        write (6,*) 'PROBLEM in aio_write:'
        write (6,*) '   islot = ', islot
        write (6,*) '   is out of range.'
        write (6,*) '   Stopping.'
        stop
      endif
      if(iprogress(islot) .ne. 0) then
        write (6,*) 'PROBLEM in aio_write:'
        write (6,*) '   islot = ', islot
        write (6,*) '   is still in use.'
        write (6,*) '   Stopping.'
        stop
      endif

      ! Save all write request data, including a pointer to the real data
      hostslot(islot) = host
      pathslot(islot) = path
      fileslot(islot) = file
      do i = 1,64
        rsendslot(i,islot) = sendinfo(i)
      enddo
      i64fileoff(islot) = 0
      mbytes(islot) = nbytes
      ptr(islot) = loc(buffer)
      iprogress(islot) = 1

      call aio_progress()

      return
      end

c=====================================================================72
      subroutine aio_read(host, path, file, nbytes, buffer, islot)
      include 'mpif.h'
      character*256 host, path, file
      include 'aio_client_data.h'
      include "aio_team_info.h"

      if(islot .lt. 1  .or.  islot .gt. nslots) then
        write (6,*) 'PROBLEM in aio_read:'
        write (6,*) '   islot = ', islot
        write (6,*) '   is out of range.'
        write (6,*) '   Stopping.'
        stop
      endif
      if(iprogress(islot) .ne. 0) then
        write (6,*) 'PROBLEM in aio_read:'
        write (6,*) '   islot = ', islot
        write (6,*) '   is still in use.'
        write (6,*) '   Stopping.'
        stop
      endif

      ! Save all read request data, including a pointer to the receive buffer
      hostslot(islot) = host
      pathslot(islot) = path
      fileslot(islot) = file
      i64fileoff(islot)    = 0
      mbytes(islot) = -nbytes   ! Negative is flag for read
      ptr(islot) = loc(buffer)
      iprogress(islot) = 1

      call aio_progress()

      return
      end


c=====================================================================72
      subroutine aio_progress()
      implicit none
      include 'mpif.h'
      include 'aio_client_data.h'
      integer  mstatus(MPI_STATUS_SIZE)

      character*1 buff(10)
      pointer (p,buff)
      integer islot, itag, ierr, i, imaxprog, nbytes
      LOGICAL incoming, issent, ialldone, ishere

      include "aio_team_info.h"
      integer myid1
      common /aio_debug/ myid1
      integer itagalldone, i_alldone_handle
      real irecvdone(10)
      common /aio_done/ i_alldone_handle, irecvdone


      ! Will only work on one slot at a time
      islot = -1
      imaxprog = 0
      do i = nslots, 1, -1
        if(iprogress(i) .gt. imaxprog) then
          islot = i
          imaxprog = iprogress(i)
        endif
      enddo
      if(islot .lt. 0) return    ! No active slots --> nothing to do

      if(mbytes(islot) .ge. 0) then
        !--------------------------------------------------------------------
        ! Start of code that services an aio_write request

        if(iprogress(islot) .eq. 1) then
          ! write request has been made, but nothing else has happened

          ! Post a recieve for servers ping, will indicate that server is ready for real data
          itag = islot
          call MPI_IRECV(irecv, 10, MPI_REAL4, myserver, itag,
     1                   MPI_COMM_WORLD, irecv_handle(islot), ierr)


          ! Post a recieve for server having finished all writes
          itagalldone = islot+1000
          call MPI_IRECV(irecvdone,10,MPI_REAL4,myserver,itagalldone,
     1                   MPI_COMM_WORLD, i_alldone_handle, ierr)

          ! send meta-data to aio server
          isend(1) = mbytes(islot)  ! >0 flags server to do a write
          isend(2) = islot
          isend(3) = int(0.1+rsendslot(64,islot))   ! used for nmeta
          isend(4) = int(0.1+rsendslot(63,islot))   ! used for metafin
          sendhost = hostslot(islot)
          sendpath = pathslot(islot)
          sendfile = fileslot(islot)
          i64sendoff = i64fileoff(islot)
          i64fileoff(islot) = i64fileoff(islot) + mbytes(islot)
          do i=1,64
            rsend(i) = rsendslot(i,islot)
          enddo
          itag = 10000
          call MPI_ISEND(isend, 320, MPI_REAL4, myserver, itag,
     1                   MPI_COMM_WORLD, isend_handle(islot), ierr)
          iprogress(islot) = 2
        endif

        if(iprogress(islot) .eq. 2) then
          ! Have sent control info: poll for recv ping

          itag = islot
          call MPI_Test(irecv_handle(islot),incoming,mstatus,ierr)

          if(incoming) then
            ! aio_server is now ready to recieve real data

            ! Free up handles
            call MPI_WAIT (isend_handle(islot), mstatus, ierr)
            !  do not need to do call MPI_WAIT (irecv_handle(islot), mstatus, ierr)

             ! Send real data
             p = ptr(islot)
             itag = islot
             call MPI_ISEND(buff,mbytes(islot),MPI_BYTE,myserver,itag,
     1                      MPI_COMM_WORLD, isend_handle(islot), ierr)
             iprogress(islot) = 3
          endif
        endif

        if(iprogress(islot) .eq. 3) then
          ! Have sent real data: poll for finish of isend
          call MPI_Test(isend_handle(islot),issent,mstatus,ierr)
          if(issent) then
            ! all the real data has been sent, and the buffer is now free

            ! do not need to do call MPI_WAIT (isend_handle(islot), mstatus, ierr)
            iprogress(islot) = 4
          endif
        endif

        if(iprogress(islot) .eq. 4) then
          ! poll for all writes done
          call MPI_Test(i_alldone_handle,ialldone,mstatus,ierr)
          if(ialldone) then
            ! aio_server has finished ALL writes
            iprogress(islot) = 0
          endif
        endif

        ! End of code that services an aio_write request
        !--------------------------------------------------------------------
      else
        !--------------------------------------------------------------------
        ! Beginning of code that services an aio_read request

        if(iprogress(islot) .eq. 1) then
          ! read request has been made, but nothing else has happened

          ! Post recieve for real data, which aio_server will read from "sendfile"
          p = ptr(islot)
          itag = islot
          nbytes = -mbytes(islot)
          call MPI_IRECV(buff,nbytes,MPI_BYTE,myserver,itag,
     1                   MPI_COMM_WORLD, irecv_handle(islot), ierr)

          ! send meta-data to aio server for read request
          isend(1) = mbytes(islot)  ! <0 flags server to read from "sendfile"
          isend(2) = islot
          sendhost = hostslot(islot)
          sendpath = pathslot(islot)
          sendfile = fileslot(islot)
          i64sendoff = i64fileoff(islot)
          i64fileoff(islot) = i64fileoff(islot) - mbytes(islot)
          do i=1,64
            rsend(i) = 0.0
          enddo
          itag = 10000
          call MPI_ISEND(isend, 320, MPI_REAL4, myserver, itag,
     1                   MPI_COMM_WORLD, isend_handle(islot), ierr)

          iprogress(islot) = 2
          iprogress(islot) = 3 ! skip iprogress=2, not needed for a read
        endif

        if(iprogress(islot) .eq. 3) then
          ! Have sent request for real data: poll for finish of irecv

          call MPI_Test(irecv_handle(islot),ishere,mstatus,ierr)
          if(ishere) then
            ! all the real data has been received, and the buffer is now filled
            ! Free up send handle
            call MPI_WAIT (isend_handle(islot), mstatus, ierr)
            iprogress(islot) = 0
          endif
        endif

        ! End of code that services an aio_read request
      endif

      return
      end

c=====================================================================72
      subroutine aio_finalize()
      include 'mpif.h'
      include "aio_team_info.h"
      include 'aio_client_data.h'
      integer  mstatus(MPI_STATUS_SIZE)

      do islot = 1, nslots
        call aio_wait(islot)
      enddo

      ! Flag server to exit
      isend(2) = -1
      itag = 10000
      call MPI_ISEND(isend, 320, MPI_REAL4, myserver, itag,
     1               MPI_COMM_WORLD, ifinish_handle, ierr)
      call MPI_WAIT (ifinish_handle, mstatus, ierr)

      return
      end

c=====================================================================72
      subroutine aio_HV_dims(nx,ny,nz, ntx,nty,ntz)
      implicit none
      integer nx,ny,nz, ntx,nty,ntz, isset, lenbob, i
      real    sendinfo(64)
      common /hv_dims_aio/ isset, lenbob, sendinfo
      data isset/0/

      do i = 1, 64
        sendinfo(i) = 0.0
      enddo
      sendinfo( 5) = 1.0
      sendinfo( 6) = float(nx)
      sendinfo( 7) = float(ny)
      sendinfo( 8) = float(nz)
      sendinfo( 9) = float(ntx)
      sendinfo(10) = float(nty)
      sendinfo(11) = float(ntz)
      lenbob       = nx*ny*nz

      ! Default XYZ ranges
      sendinfo(12) = -0.5
      sendinfo(13) =  0.5
      sendinfo(14) = -0.5 * float(ny)/float(nx)
      sendinfo(15) =  0.5 * float(ny)/float(nx)
      sendinfo(16) = -0.5 * float(nz)/float(nx)
      sendinfo(17) =  0.5 * float(nz)/float(nx)

      return
      end

c---------------------------------------------------------------------72
      subroutine aio_HV_ranges(xmin,xmax,ymin,ymax,zmin,zmax)
      implicit none
      integer isset, lenbob
      real    sendinfo(64), xmin,xmax,ymin,ymax,zmin,zmax
      common /hv_dims_aio/ isset, lenbob, sendinfo

      ! Set XYZ ranges
      sendinfo(12) = xmin
      sendinfo(13) = xmax
      sendinfo(14) = ymin
      sendinfo(15) = ymax
      sendinfo(16) = zmin
      sendinfo(17) = zmax

      return
      end
c---------------------------------------------------------------------72

      subroutine aio_HV_write(host, path, name, bob, ibobslot)
      implicit none
      character*256 host, path, name
      byte          bob(10)
      integer       ibobslot
      real          sendinfo(64)
      integer       isset, lenbob
      common /hv_dims_aio/ isset, lenbob, sendinfo

c        call aio_HV_write(host, path, cHVrootname,

c        call aio_HV_dims(nx,ny,nz, ntx,nty,ntz)
cc       call aio_HV_ranges(xmin,xmax,ymin,ymax,zmin,zmax)
c        call aio_HV_write(host, path, cHVrootname,
c     1                    bobs(1,ifield), ibobslot)
        call aio_write(host,path,name,sendinfo,lenbob,bob,ibobslot)

      return
      end
c=====================================================================72


c
c***********************************************************************
c***********************************************************************
c****                                                               ****
c****                  Server Routine                               ****
c****                                                               ****
c***********************************************************************
c***********************************************************************
c
      subroutine aio_server(ifile)
      include 'mpif.h'
      include "aio_team_info.h"

      integer   mstatus(MPI_STATUS_SIZE), ifile

      integer(kind=8) i64off, i64recvoff, i64fileoff
      character*256 host, path, file, cstr
      real sdata(10)

      integer irecv(320,10000)
      real    rrecv(64)
      character*256 recvhost,recvpath,recvfile
      equivalence (i64recvoff, irecv( 61,1))
      equivalence (rrecv(1)  , irecv( 65,1))
      equivalence (recvhost  , irecv(129,1))
      equivalence (recvpath  , irecv(193,1))
      equivalence (recvfile  , irecv(257,1))

      integer irecv_handle, isend_handle, idata_handle
      character*256 cchost, cctype
      character*256 chost,cpath,cfile, cmetafile
      character*4 ctail
      byte   data
      byte   compressed
      integer nbytes, nmeta, metafin, islot, length, i, iisum, nbmax
      parameter(MAXMETA = 10000000)
      byte bufmeta(MAXMETA)

      real*8 t00, twait, twrite, t0, t1, t2, t3, tstart, tend
      real*8 tinit, topen, tirs, tclose, fmbps, fbytes
      integer sw_read, sw_write, sw_compress2, isalloc_comp, icompress
      integer my_header_size, mysize

      ! inmem HVs
      integer idoreformat, idohv, nxfull,nyfull,nzfull
      integer nxlast, nylast, nzlast, lastHVsize
      integer nxsub,nysub,nzsub, ntx,nty,ntz
      byte    bob, bufferHV
      allocatable bob(:,:,:)
      allocatable bufferHV(:)
      ! inmem HVs

      allocatable irecv_handle(:), isend_handle(:), idata_handle(:)
      allocatable data(:,:)
      allocatable nbytes(:), islot(:)
      allocatable nmeta(:), metafin(:)
      allocatable compressed(:)
      allocatable mysize(:)

      common /cntrl_record/ idoit

      idoit = iverbose

      call aio_set_data_extention(ifile)

      call aio_record(ifile, "AIO Server start", 0)
      isalloc_comp = 0
      nxlast = -1
      nylast = -1
      nzlast = -1
      lastHVsize = -1
      nmetatot = 0

      t00 = MPI_WTIME()
      if(iverbose .ge. 1) write (6,901) t00, ifile, maxb
901   format("# AIO STARTUP: MPI_WTIME,ifile,maxb:", f15.3, i4, i20)

      allocate (irecv_handle(nclients))
      allocate (isend_handle(nclients))
      allocate (idata_handle(nclients))
      allocate (data(maxb, 2))
      allocate (nbytes(nclients))
      allocate (islot(nclients))
      allocate (nmeta(nclients))
      allocate (metafin(nclients))
      allocate (mysize(nclients))

      itag = 10000
      do i = 1, nclients
      call MPI_IRECV(irecv(1,i), 320, MPI_REAL4, myclients(i), itag,
     1               MPI_COMM_WORLD, irecv_handle(i), ierr)
      enddo

10    continue

      do i = 1, nclients
        call MPI_WAIT (irecv_handle(i), mstatus, ierr)
      enddo
       
      nbmax      = 0
      idohv      = 0
      do i = 1, nclients
        nbytes(i)  = irecv(1,i)
        islot(i)   = irecv(2,i)
        nmeta(i)   = irecv(3,i)
        metafin(i) = irecv(4,i)
        nbmax = max(nbmax, iabs(nbytes(i)))

        if(i .eq. 1) then
          ndump = int(0.1 + rrecv(1))
          time  = rrecv(2)
          istep = int(0.1 + rrecv(3))
          dtime = rrecv(4)
c inmem HVs
          idoreformat = int(0.1 + rrecv(5))
c         write (6,*) 'idoreformat = ', idoreformat
          if(idoreformat .eq. 1) then
            idohv = 1
            nxsub = int(0.1 + rrecv( 6))
            nysub = int(0.1 + rrecv( 7))
            nzsub = int(0.1 + rrecv( 8))
            ntx   = int(0.1 + rrecv( 9))
            nty   = int(0.1 + rrecv(10))
            ntz   = int(0.1 + rrecv(11))
            nxfull = ntx * nxsub
            nyfull = nty * nysub
            nzfull = ntz * nzsub

            if(iverbose .ge. 4) write (6,*) 'nXYZsub=',nxsub,nysub,nzsub
            if(iverbose .ge. 4) write (6,*) 'ntXYZ  =',ntx,nty,ntz
          endif
c inmem HVs

          chost = recvhost
          cpath = recvpath
          cfile = recvfile
          i64fileoff = 0
          do j = 1, nclients
            i64fileoff = i64fileoff + i64recvoff
          enddo

c          write (6,*) 'chost=',chost(1:20)
c          write (6,*) 'cpath=',cpath(1:20)
c          write (6,*) 'cfile=',cfile(1:20)
c          write (6,*) 'nbytes,fileoff=',nbytes(i),i64fileoff
        endif
        if(iabs(nbytes(i)) .gt. maxb) then
          write (6,*) 'AIO_SERVER PROBLEM: nbytes,i = ', nbytes(i),i
          write (6,*) 'AIO_SERVER PROBLEM: maxb   = ', maxb
          write (6,*) 'AIO_SERVER STOPPING'
          stop
        endif
c       if(i .eq. 64) write (6,*) 'I,N,F=',i,nbytes(i),cfile(1:18)
        if(islot(i) .gt. 0) then
          itag = 10000
          call MPI_IRECV(irecv(1,i), 320, MPI_REAL4, myclients(i), itag,
     1                   MPI_COMM_WORLD, irecv_handle(i), ierr)
        endif
      enddo     ! end loop i=1,nclients

      if(islot(1) .lt. 0) then
        deallocate (irecv_handle)
        deallocate (isend_handle)
        deallocate (idata_handle)
        deallocate (data)
        deallocate (nbytes)
        deallocate (islot)
        deallocate (nmeta)
        deallocate (metafin)
        deallocate (mysize)
        return
      endif

      if(idohv .eq. 0) call aio_gen_data_file_name(recvfile, cfile)
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      if(idohv .eq. 0) then
c      ctail(1:1) = "-"
c      ctail(2:2) = char(ichar("0") +     ifile/100     )
c      ctail(3:3) = char(ichar("0") + mod(ifile/ 10, 10))
c      ctail(4:4) = char(ichar("0") + mod(ifile    , 10))
c      do i=256,1,-1
c      if(cfile(i:i) .eq. " ") itail = i
c      enddo
c      cfile(itail:itail+3) = ctail   ! dhp was here
c      endif
c was already commented      if(idohv .eq. 1) cfile(itail:itail+3) = ".hv "
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc


      if(nbytes(1) .ge. 0) then
      !--------------------------------------------------------------------
      ! Begin of code for doing an aio_write
      ! <> <> <> <> <> <> <> <> <> <> <> <> <> <> <> <> <> <> <> <> <>

      call aio_record(ifile, "----------------", 0)
      call aio_record(ifile, "Write file      ", 0)
      call aio_recordc(ifile, cfile)

      t0 = MPI_WTIME()
      if(i64fileoff .eq. 0) then
        tstart = t0 - t00
        tinit  = 0.0
        topen  = 0.0
        twrite = 0.0
        twait  = 0.0
        tirs   = 0.0
      endif
 
      i64off = i64fileoff

      ! Switches at beginning of chost:
      !   "compress-"    Server to compress data
      !   "precompr-"    Clients are sending pre-compressed data
      icompress    = 0
      if(chost(1:9) .eq. "compress-") icompress = 1
      if(chost(1:9) .eq. "precompr-") icompress = 2
      if(icompress .gt. 0) then
        my_header_size = max(16*1024, 80+16*nclients)
        
        if(isalloc_comp .eq. 0) then
          ! If not allocated: allocate compress-ed array
          ! Only needed for header info if writting pre-compressed data
          isalloc_comp = maxb
          if(icompress .eq. 2) isalloc_comp = my_header_size
          allocate (compressed(isalloc_comp))
        endif

        ! Parse for compression type and host from format
        !      "compress-<type of compression>-<host>"
        idash = 9
        do i=256,10,-1
          if(chost(i:i) .eq. '-') idash = i
        enddo
        do i = 1, 256
          cctype(i:i) = ' '
          cchost(i:i) = ' '
        enddo
        length = 256-idash
        cchost(1:length)= chost(idash+1:256)
        cctype(1:5)  = "Nzlib"
        length = idash - 10
        if(length .gt. 0) cctype(1:length) = chost(10:idash-1)

        ! Add extension to compressed file name
        if(cctype(1:5) .eq. "Nzlib") cfile(itail+4:itail+9) = ".nzlib"


        ! Pad begnning of file with enough <charage returns=10>
        do i = 1, my_header_size
           compressed(i) = 10
        enddo
        i64off = 0
        call aio_record(ifile, "Start Nzlib pad ", 0)
        ierr = sw_write(cchost,cpath,cfile,i64off,my_header_size,
     1                  compressed)
        call aio_record(ifile, "End   Nzlib pad ", 0)
        i64off = my_header_size
      endif
      tinit = tinit + MPI_WTIME() - t0


      if(idohv .eq. 1) then
        ! Allocate memory for full bob data
        if(nxfull.ne.nxlast .or. nyfull.ne.nylast
     1                      .or. nzfull.ne.nzlast) then
          if(nxlast .gt. 0) deallocate(bob)
          allocate (bob(nxfull,nyfull,nzfull))
          nxlast = nxfull
          nylast = nyfull
          nzlast = nzfull
        endif
      endif

      t0 = MPI_WTIME()
      ! A write of 0 bytes toggles the file between open & closed
      ! toggle file open if this is a local write
      if(chost.eq.'localdisk' .and. idohv.eq.0 .and. nbmax.gt.0) then
        call aio_record(ifile, "Start open file ", 0)
        ierr = sw_write(chost,cpath,cfile,i64off, 0,sdata)
        call aio_record(ifile, "End   open file ", 0)
      endif
      topen = topen + MPI_WTIME() - t0

      do i = 1, nclients+1
        if(i .le. nclients) then
          t0 = MPI_WTIME()
          itag = islot(i)
          ibuf = 1 + mod(i,2)
          call MPI_IRECV(data(1,ibuf),nbytes(i),MPI_BYTE,myclients(i),
     1                   itag,MPI_COMM_WORLD, idata_handle(i), ierr)
          call MPI_ISEND(sdata,10,MPI_REAL4,myclients(i),
     1                   itag,MPI_COMM_WORLD, isend_handle(i), ierr)
          tirs = tirs + MPI_WTIME() - t0
        endif


        if(i .gt. 1) then
          t0 = MPI_WTIME()
          i1 = i - 1
          ibuf1 = 1 + mod(i1,2)
          call MPI_WAIT(isend_handle(i1), mstatus, ierr)
          call MPI_WAIT(idata_handle(i1), mstatus, ierr)
          t1 = MPI_WTIME()
          twait = twait + (t1 - t0)
          nn = nbytes(i1) - nmeta(i1)

          if(nmeta(i1) .gt. 0) then
            do ii = 1, nmeta(i1)
              nmetatot = nmetatot + 1
              bufmeta(nmetatot) = data(nn+ii, ibuf1)
            enddo
          endif

          if(icompress .eq. 0) then    ! Uncompressed data
            if(idohv .eq. 0) then
              call aio_record(ifile, "Start write     ", i1)
            if(nn .gt. 0)
     1       ierr = sw_write(chost,cpath,cfile,i64off,nn,data(1,ibuf1))
              call aio_record(ifile, "End   write     ", i1)
              i64off = i64off + nn
            else
              call copy_into_bob(i1,ntx,nty,ntz,nxsub,nysub,nzsub,
     1                           nxfull,nyfull,nzfull,data(1,ibuf1),bob)
            endif
          endif
          if(icompress .eq. 1) then    ! Server compresses data
            mm = maxb
            ierr = sw_compress2(nn,data(1,ibuf1),mm,compressed,cctype)
            call aio_record(ifile, "Start write     ", i1)
            ierr = sw_write(cchost,cpath,cfile,i64off,mm,compressed)
            call aio_record(ifile, "End   write     ", i1)
            i64off     = i64off + mm
            mysize(i1) = mm
          endif
          if(icompress .eq. 2) then    ! Pre-compressed data
            call getintatend(data(1,ibuf1), nn, mm)
            call aio_record(ifile, "Start write     ", i1)
            ierr = sw_write(cchost,cpath,cfile,i64off,mm,data(1,ibuf1))
            call aio_record(ifile, "End   write     ", i1)
            i64off     = i64off + mm
            mysize(i1) = mm
          endif

          twrite = twrite + (MPI_WTIME() - t1)
          fbytes = dble(i64off)
        endif
      enddo
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      ! Toggle file closed if this is a local write
      t0 = MPI_WTIME()
      if(chost.eq.'localdisk' .and. idohv.eq.0 .and. nbmax.gt.0) then
        call aio_record(ifile, "Start close     ", 0)
        ierr = sw_write(chost,cpath,cfile,i64off,0,sdata)
        call aio_record(ifile, "End   close     ", 0)
      endif
      tclose = MPI_WTIME() - t0

      ! metafin == 0:   keep collecting meta-data (no not write out)
      ! metafin == 1:   meta-data collection done, write out, add extention to file name if needed
      ! metafin == 2:   meta-data collection done, write out, use recvfile name verbatim
      if(iverbose .ge. 10) write (6,*) "metafin(1) = ", metafin(1)
      if(metafin(1) .gt. 0) then      ! Write out meta-data
        call aio_write_meta(chost, cpath, recvfile,
     1                      metafin(1), nmetatot, bufmeta)
      endif

      ! If comprssed data:
      ! Write compression type, header size, number of compress-ed blocks,
      ! and block sizes at beginning of file
      !
      if(icompress .gt. 0) then
        do i = 1, my_header_size
          compressed(i) = 10
        enddo
        write (cstr,891) cctype, my_header_size, nclients, nbytes(1)
891     format("multiple-compressed-blocks    ", a20, 3i10)

        ipos = 1
        call str2array(cstr, compressed, ipos, 80)
        do i = 1, nclients
          write (cstr, 890) mysize(i)
          call str2array(cstr, compressed, ipos, 15)
        enddo
890     format(i15)

        i64off = 0
        call aio_record(ifile, "Start Nzlib head", 0)
        ierr = sw_write(cchost,cpath,cfile,i64off,my_header_size,
     1                  compressed)
        call aio_record(ifile, "Start Nzlib head", 0)

         if(iverbose .ge. 10  .and.  ifile .eq. 0) 
     1      write (6,*) '# AIO COMPRESSED WRITE DONE'
      endif

      if(idohv .eq. 1) then
        t0 = MPI_WTIME()
        call inmem_set_hv_dims(nxfull, nyfull, nzfull, iHVsize)
        if(iHVsize .ne. lastHVsize) then
          if(lastHVsize .gt. 0) deallocate(bufferHV)
          allocate (bufferHV(iHVsize))
          do i = 1, iHVsize
            bufferHV(i) = 0
          enddo
          lastHVsize = iHVsize
        endif
        t1 = MPI_WTIME()
        call inmem_HV_format(cfile,bob,bufferHV,ierror)
        t2 = MPI_WTIME()
        i64off = 0
        ierr = sw_write(chost,cpath,cfile,i64off,iHVsize,bufferHV)
        t3 = MPI_WTIME()
        if(iverbose .ge. 2)
     1  write (6,991) cfile,iHVsize,(t3-t2),1.e-6*float(iHVsize)/(t3-t2)
991   format("#Wrote: ", a16, "   Size=",i9,"   sec=",f6.3,
     1       "   MB/s=",f6.2)

      endif


      ! Send a ping to all clients telling them that all writes are now done
      call aio_record(ifile, "Start ping done ", 0)
      do i = 1, nclients
        itagalldone = islot(i) + 1000
        call MPI_ISEND(sdata,10,MPI_REAL4,myclients(i),
     1                 itagalldone,MPI_COMM_WORLD,isend_handle(i),ierr)
        call MPI_WAIT(isend_handle(i), mstatus, ierr)
      enddo
      call aio_record(ifile, "End   ping done ", 0)


      if(cfile(1:3) .ne. "res"  .or.  nbytes(1) .lt. 10240) then
      tend = MPI_WTIME() - t00
      if(ifile .eq. 0 .and. iverbose .ge. 2) write (6,994)
994   format("#                    AIO Times:     Start    Del   Init",
     1       "   Open   Tirs   Wait  Write  Close   MB/S")

      fmbps  = fbytes / ((tend-tstart) * 1024.0**2)
      if(iverbose .ge. 2)
     1  write (6,998) cfile, tstart, tend-tstart, tinit, topen, tirs,
     1              twait, twrite, tclose, fmbps
998   format("#Wrote: ", a16, " Times: ", f9.2, 8f7.2)
      endif

cccccccccccccccc
c      if(ifile .eq. 0) then
c        write (6,996) cfile(1:26), i64fileoff, i64off
c996     format("AIO wrote:", a26,"  fileoff=",i10,"  fulloff=",i10)
c      endif
cccccccccccccccc


      ! <> <> <> <> <> <> <> <> <> <> <> <> <> <> <> <> <> <> <> <> <>
      ! End of code for doing an aio_write
      !--------------------------------------------------------------------
      else
      !--------------------------------------------------------------------
      ! Begin of code for doing an aio_read

      call aio_record(ifile, "----------------", 0)
      call aio_record(ifile, "Read file       ", 0)
      call aio_recordc(ifile, cfile)

      i64off = i64fileoff

      ! A read of 0 bytes toggles the file between open & closed
      ! toggle file open if this is a local read
      if(chost .eq. 'localdisk') then
      call aio_record(ifile, "Start open      ", 0)
        ierr = sw_read(chost,cpath,cfile,i64off,0,sdata)
      call aio_record(ifile, "End   open      ", 0)
      endif

      do i = 1, nclients
        ibuf = 1 + mod(i,2)
        mbytes = -nbytes(i)
        call aio_record(ifile, "Start read      ", i)
        ierr = sw_read(chost,cpath,cfile,i64off,mbytes,data(1,ibuf))
        call aio_record(ifile, "End   read      ", i)
        itag = islot(i)
        call MPI_ISEND(data(1,ibuf),mbytes,MPI_BYTE,myclients(i),
     1                 itag,MPI_COMM_WORLD, idata_handle(i), ierr)
        i64off = i64off + mbytes

        if(i .gt. 1) then
          call MPI_WAIT(idata_handle(i-1), mstatus, ierr)
        endif
        if(i .eq. nclients) then
          call MPI_WAIT(idata_handle(i), mstatus, ierr)
        endif
      enddo

      ! Toggle file closed if this is a local write
      if(chost .eq. 'localdisk') then
        call aio_record(ifile, "Start close     ", 0)
        ierr = sw_read(chost,cpath,cfile,i64off,0,sdata)
        call aio_record(ifile, "Start close     ", 0)
      endif

c      if(ifile .eq. 0) then
c        write (6,995) cfile(1:26), i64fileoff, i64off
c995     format("AIO read:", a26,"  fileoff=",i10,"  fulloff=",i10)
c      endif

      ! End of code for doing an aio_read
      !--------------------------------------------------------------------
      endif

      go to 10


      end

c=====================================================================72
      subroutine aio_write_meta(chost, cpath, recvfile,
     1                          metafin, nmetatot, bufmeta)
      implicit none
      include "mpif.h"
      include "aio_team_info.h"
      character*256 chost,cpath,recvfile,cmetafile
      integer metafin, nmetatot, ierr, sw_write, nmeta, i
      integer(kind=8) i64
      integer mstatus(MPI_STATUS_SIZE)
      byte bufmeta(*)


      ! collect all meta-data to 1st aio server
      if(myid .eq. iservers(1)) then
        do i = 2, nservers
          call MPI_RECV(nmeta,1,MPI_INTEGER,iservers(i),
     1                   10321,MPI_COMM_WORLD, mstatus, ierr)
          call MPI_RECV(bufmeta(nmetatot+1),nmeta,MPI_BYTE,iservers(i),
     1                   10322,MPI_COMM_WORLD, mstatus, ierr)
          nmetatot = nmetatot + nmeta
        enddo
      else
          call MPI_SEND(nmetatot,1,MPI_INTEGER,iservers(1),
     1                   10321,MPI_COMM_WORLD, ierr)
          call MPI_SEND(bufmeta,nmetatot,MPI_BYTE,iservers(1),
     1                   10322,MPI_COMM_WORLD, ierr)
      endif

      ! Write meta-data to file named in cmetafile
      if(myid .eq. iservers(1)) then
        cmetafile = recvfile
        if(metafin.eq.1) call aio_add_meta_extention(cmetafile)
        if(iverbose .ge. 10)
     1     write (6,*) "aio writing: ", trim(cmetafile),nmetatot
        i64 = 0
        ierr = sw_write(chost,cpath,cmetafile,i64,0,bufmeta)
        ierr = sw_write(chost,cpath,cmetafile,i64,nmetatot,bufmeta)
        ierr = sw_write(chost,cpath,cmetafile,i64,0,bufmeta)
      endif

      ! Reset meta-data buffer to empty on all aio servers
      nmetatot = 0

      return
      end
c=====================================================================72
      subroutine aio_add_meta_extention(cname)
      implicit none
      include "aio_team_info.h"
      character*(*) cname
      integer n, nExt
      n = len(trim(cname))
      nExt = len(trim(cExtMe))
      cname(n+1:n+nExt) = trim(cExtMe)
      return
      end

c=====================================================================72
      subroutine copy_into_bob(it, ntx,nty,ntz, nxs,nys,nzs,
     1                             nxf,nyf,nzf, buff,bob)
      byte buff(nxs*nys*nzs), bob(nxf,nyf,nzf)

      itx = mod(it          , ntx)
      ity = mod(it/ntx      , nty)
      itz = mod(it/(ntx*nty), ntz)
      ioffx = nxs * itx
      ioffy = nys * ity
      ioffz = nzs * itz
      isrc = 0
      do izs=1,nzs
      izdst = ioffz + izs
      do iys=1,nys
      iydst = ioffy + iys
      do ixs=1,nxs
      ixdst = ioffx + ixs
      isrc = isrc + 1
      bob(ixdst,iydst,izdst) = buff(isrc)
      enddo
      enddo
      enddo

      return
      end
      
c=====================================================================72

      subroutine str2array(cstr, barray, ipos, nchar)
      character*256 cstr
      byte barray(16*1024)
      do i = 1, nchar
        barray(ipos) = ichar(cstr(i:i))
        ipos = ipos + 1
      enddo
      barray(ipos) = 10
      ipos = ipos + 1
      return
      end
c=====================================================================72
      subroutine getintatend(ibuf, nbytes, lastint)
      integer ibuf(nbytes/4)
      lastint = ibuf(nbytes/4)
      return
      end

c=====================================================================72
      subroutine aio_record(ifile, cLine, ivalue)
      character*16 cLine
      character*14 cDebugFile
      common /cntrl_record/ idoit
      if(idoit .lt. 20) return

      cDebugFile = "aio_record_000"
      cDebugFile(12:12) = char(ichar('0') +     ifile/100    )
      cDebugFile(13:13) = char(ichar('0') + mod(ifile/ 10,10))
      cDebugFile(14:14) = char(ichar('0') + mod(ifile    ,10))
      open(unit=23,file=cDebugFile,form="formatted",position="append")
      write (23,999) cLine, iValue
999   format(a16, i10)
      close(23)

      return
      end

c=====================================================================72
      subroutine aio_recordc(ifile, cLine)
      character*256 cLine
      character*14  cDebugFile
      common /cntrl_record/ idoit
      if(idoit .lt. 20) return

      cDebugFile = "aio_record_000"
      cDebugFile(12:12) = char(ichar('0') +     ifile/100    )
      cDebugFile(13:13) = char(ichar('0') + mod(ifile/ 10,10))
      cDebugFile(14:14) = char(ichar('0') + mod(ifile    ,10))
      open(unit=23,file=cDebugFile,form="formatted",position="append")
      write (23,*) trim(cLine)
      close(23)

      return
      end
       
      
c=====================================================================72
