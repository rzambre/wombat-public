
      integer nslots
      parameter (nslots = 100)

      integer*8     ptr(nslots)
      integer       mbytes(nslots), iprogress(nslots)
      integer       isend_handle(nslots),irecv_handle(nslots)
      integer       is_open(nslots)
      integer(kind=8) i64fileoff(nslots)
      character*256 hostslot(nslots)
      character*256 pathslot(nslots)
      character*256 fileslot(nslots)
      real          rsendslot(64, nslots)

      integer(kind=8) i64sendoff
      integer irecv(10)
      integer isend(320)
      real    rsend(64)
      character*256 sendhost, sendpath, sendfile
      equivalence (i64sendoff, isend(61))
      equivalence (rsend(1)  , isend(65))
      equivalence (sendhost  , isend(129))
      equivalence (sendpath  , isend(193))
      equivalence (sendfile  , isend(257))

      common /aio_client_data/ ptr, i64fileoff, mbytes, iprogress
      common /aio_client_data/ hostslot,pathslot,fileslot,rsendslot
      common /aio_client_data/ irecv,isend,isend_handle,irecv_handle
      common /aio_client_data/ is_open

