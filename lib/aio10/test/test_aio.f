      include 'mpif.h'

      ! Declare output buffer
      parameter (nx = 128)
      parameter (ny = 128)
      real*4 buffer(nx,ny), fData(1024)

      ! Host name, directory path, and file name of output target
      character*256 cfile
      parameter(maxread=1024)
      character*(maxread) cBuffer, cMeta
      character*20 cRoot, cFileName

      ! Meta-data used be some variants of output (not generally needed)



cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c                 INITIALIZE MPI and AIO                               c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      ! Initialize MPI
      call MPI_INIT (ierr)
      call MPI_COMM_SIZE (MPI_COMM_WORLD, nprocs, ierr)
      call MPI_COMM_RANK (MPI_COMM_WORLD, myrank, ierr)


      ! Test aio_all_read before aio_servers are started
      call aio_all_read("input_file_all_ranks", maxread, ngot, cBuffer)
      if(ngot .gt. 0) then
        write (6,999) myrank, cBuffer(1:ngot)
999     format("All ranks: Rank", i3, " got: ", a)
      else
        if(myrank .eq. 0) write (6,*) 'ngot = ', ngot, ' bytes'
      endif

      nioservers = 2                       ! Number of IO servers
      nworkers   = nprocs - nioservers     ! All remaining ranks are IO clients (workers)
      maxbytes   = 4*nx*ny                 ! Maximum size [bytes] in any one send

      ! Initialize/set AIO parameters
      call aio_init(myrank,nworkers,nioservers,maxbytes,myserver)

      ! Start aio servers, set file extensions for aio reads & writes
      ! Only workers will return from this call
      call aio_start_servers()

      ! Start AIO server ranks
      ! These ranks will not return till all AIO clients call aio_finalize
      if(myrank .ge. nworkers) then
        ifileno = myrank - nworkers    ! file number assciated with AIO server
        call aio_server(ifileno)
        call MPI_BARRIER(MPI_COMM_WORLD, ierr)
        call MPI_FINALIZE(ierr)
        stop
      endif

c      write (6,*) "AT -B-, rank:", myrank
c      call sleep(10)

      ! Test aio_all_read after aio_servers are started
      call aio_all_read("input_just_workers", maxread, ngot, cBuffer)
      if(ngot .gt. 0) then
        write (6,998) myrank, cBuffer(1:ngot)
998     format("Clients only: Rank", i3, " got: ", a)
      else
        if(myrank .eq. 0) write (6,*) 'ngot = ', ngot, ' bytes'
      endif

c      write (6,*) "AT 00000, rank:", myrank
c      call sleep(10)

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c                 FILL OUTPUT BUFFER ON EACH WORKER RANK               c
c                 (same buffer lengh on each worker rank)              c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      ! Find 2D array of workers which is most square
      nmax = int(0.1 + sqrt(float(nworkers)))
      do i = 1, nmax
        if(mod(nworkers,i) .eq. 0) ntilex = i
      enddo
      ntiley = nworkers / ntilex

      ! This rank's X & Y tile coordinates (start counting from 0)
      mytilex = mod(myrank,ntilex)
      mytiley =     myrank/ntilex

      ! Fill output buffer with sin(2*pi*(x/Lx+y/Ly))
      pi = 4.0 * atan(1.0)
      facx = 2.0 * pi / float(nx*ntilex)
      facy = 2.0 * pi / float(ny*ntiley)
      do iy=1,ny
      do ix=1,nx
        buffer(ix,iy) = sin(facx*float(ix-1) + facy*float(iy-1))
      enddo
      enddo

c      write (6,*) "AT AAAAA, rank:", myrank
c      call sleep(10)

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c                 WRITE OUTPUT BUFFER WITH AIO                         c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      do i = 1, 256
        cfile(i:i) = ' '
      enddo
      cfile(1:10) = "output    "

      nbytes = 4 * nx * ny
      ibuf = 1
      call aio_write_buffer(cfile,  nbytes, buffer, ibuf)

      ! Wait for writes to be finished and output buffer to cleared
      call aio_wait(ibuf)

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c            FILL OUTPUT BUFFER ON EACH WORKER RANK                    c
c            (different buffer lengh on each worker rank)              c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      !                123456789 123456789 123456789 123456789 
      cBuffer(1:40) = "Test variable buffer sizes,           : "
      nc = 1 + mod(7+107*myrank,31)
      write (cBuffer(29:38),997) myrank, nc
997   format(2i5)
      ic = 41
      do i = 1, nc
        cBuffer(ic:ic) = 'x'
        ic = ic + 1
      enddo
      cBuffer(ic:ic) = char(10)
      if(mod(myrank,10) .eq. 2) ic = 0
      call aio_write_buffer("text_out", ic, cBuffer, ibuf)
      call aio_wait(ibuf)

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c          Now write buffer+metadata
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      do i=1,4
        buffer(i,1) = float(myrank)
      enddo
      nbuf = 4*4
      call aio_add_meta_to_buf(nbuf, ic, buffer, cBuffer)
      call aio_write_buf_meta("rootname",nbuf,ic,buffer,ibuf,1)
      call aio_wait(ibuf)

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c          Now do several sets of aio writes,                          c
c          collecting meta-data along the way, and                     c
c          write meta-data to an arbitrary file name                   c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      do iwriteset = 0, 2
      cRoot = "mine-0123-00        "
      cRoot(12:12) = char(ichar('0') + iwriteset)
      call aio_gen_data_file_name(cRoot, cFileName)

      irank = nprocs*iwriteset + myrank
      nbufs = mod(23*irank,7)   ! different numbers of buffers for each rank and write set
      write (6,*) "rank,set,bufs:",myrank,iwriteset,nbufs
      idata = 0
      nmeta = 0
      do ibuf = 1, nbufs
        ! add real data
        do i=1,4
          iData = iData + 1
          fData(iData) = float(myrank) + 0.01 * float(ibuf-1)
        enddo

        ! add meta-data (descriging the real data)
        write (cBuffer,899) myrank, ibuf, cFileName
899     format("Rank=", i2, "     ibuf=", i2, "   file=",a)
        n = len(trim(cBuffer))
        cMeta(nMeta+1:nMeta+n) = cBuffer(1:n)
        nmeta = nmeta + n + 1
        cMeta(nMeta:nMeta) = char(10)
      enddo
      nData = 4 * iData
      call aio_add_meta_to_buf(nData, nMeta, fData, cMeta)
      
      call aio_write_buf_meta(cRoot,nData,nMeta,fData,ibuf,0)
      call aio_wait(ibuf)
      enddo

      call aio_write_buf_meta("mine-0123.manifest",0,0,fData,ibuf,2)
      call aio_wait(ibuf)



cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c                 FINALIZE AIO and MPI                                 c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      call aio_finalize()
      call MPI_BARRIER(MPI_COMM_WORLD, ierr)
      call MPI_FINALIZE(ierr)


      stop
      end
