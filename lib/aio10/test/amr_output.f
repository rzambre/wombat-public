
      include "amr_output.h"
      integer jlevel, nbuftot
      byte buf
      character*15 cMetaFile
      allocatable buf(:)


      call init_mpi()
      call set_defaults()
      call get_inputs()
      call init_aio()
      call gen_patch_list()

      ! Output buffer for all levels
      ! Different levels are offset in this buffer and written is
      ! separate aio writes.
      allocate (buf(maxbuf))

      do jlevel = 1, maxlevels
        ! Do al the patches on each level, one level at a time
        ! - generate patch content
        ! - generate patch meta-data
        ! - copy content and metat-data into output buffer
        ! - start aio write of output buffer to file(s) for this level
        call do_patches_on_one_level(jlevel, nbuftot, buf)
      enddo

      write (cMetaFile,999) cname(1:5), idump
999   format(a5, "-", i4.4, "-000")
      call aio_write_buf_meta(cMetaFile, 0, 0, buf, 1, 2)
      call aio_wait(1)

      deallocate(buf)

      call my_exit("amr_output completed sucesfully")

      stop
      end

C=====================================================================72
      subroutine do_patches_on_one_level(ilev, nbuftot, buf)
      implicit none
      include "amr_output.h"
      integer ilev, nbuftot
      byte buf(maxbuf)
      character*20000 cmeta
      character*256 cstr, cRoot, cFileName
      integer ipatch, n, j, nData, nMeta, nbytesdata, ihandle
      real flds
      allocatable flds(:,:,:,:)
      allocate (flds(1-nb:nx+nb,1-nb:ny+nb,1-nb:nz+nb,5))

      ! generate & store data in buffer for all patches on this level
      nData = 0
      do ipatch = 1, npatches
        if(level(ipatch) .eq. ilev) then
          call gen_one_patch(ipatch,flds,nbytesdata)
          call copy_bytes(flds,buf(nData+1),nbytesdata)
          nData = nData + nbytesdata
          endif
        enddo

      ! Generate file name for real data at this level
      call set_blank(croot)
      write (cRoot(1:16),998) cname(1:5), idump, ilev
998   format(a5, "-", i4.4, "-lev", i2.2)
      call aio_gen_data_file_name(cRoot, cFileName)
      if(nData .gt. 0) write (6,*) trim(cRoot),"   ",trim(cFileName)

      ! generate e meta-data in for all patches on this level
      nMeta = 0
      if(myrank .eq. 0  .and.  ilev .eq. 1)
     1    call gen_global_meta_data(nMeta, cMeta)
      do ipatch = 1, npatches
        if(level(ipatch) .eq. ilev) then
          call set_blank(cstr)
          write (cstr,999) nrefine(ipatch), (ixyzoff(j,ipatch),j=1,3),
     1                     trim(cFileName)
999       format("boxb    ",4i5, "  ", a)
          n =  len(trim(cstr))
          cmeta(nMeta+1:nMeta+n+1) = cstr(1:n) // char(10)
          nMeta = nMeta+n+1
        endif
      enddo

      nbuftot = nData + nMeta

      deallocate(flds)

      ! Copy meta-data to end of buffer & submit aio write
      call aio_add_meta_to_buf(nData, nMeta, buf, cMeta)
       ihandle = ilev
      call aio_write_buf_meta(cRoot,nData,nMeta,buf,ihandle,0)
      call aio_wait(ihandle)

      return
      end

C=====================================================================72
      subroutine gen_global_meta_data(nMeta, cMeta)
      implicit none
      include "amr_output.h"
      integer nMeta, i, n
      character*20000 cmeta
      character*256 cLine(256), cComment
      character*16 cBndry
      cBndry = "continuation    "

      do i = 1, 256
        call set_blank(cLine(i))
      enddo
      call set_blank(cComment)
      cComment(1:33) = "This is an example of an amr dump"

900   format(10a)
901   format(a, 10i10)
902   format(a, 1p5e15.6)
      write (cLine( 1),900) "comment   ", trim(cComment)
      write (cLine( 2),902) "time      ", 123.0
      write (cLine( 3),902) "dtime     ", 0.01
      write (cLine( 4),901) "step      ", 12300
      write (cLine( 5),901) "ncube     ", 0
      write (cLine( 6),901) "i2max     ", 65501
      write (cLine( 7),901) "i2off     ", 32750
      write (cLine( 8),901) "ncubesxyz ", nx, ny, nz
      write (cLine( 9),901) "nbrickxyz ", nbx, nby, nbz
      write (cLine(10),901) "boundary  ", nb
      write (cLine(11),902) "xlimits   ", -xsize/2, xsize/2
      write (cLine(12),902) "ylimits   ", -ysize/2, ysize/2
      write (cLine(13),902) "zlimits   ", -zsize/2, zsize/2
      write (cLine(14),900) "lobndry   ", cBndry, cBndry, cBndry
      write (cLine(15),900) "hibndry   ", cBndry, cBndry, cBndry

      write (cLine(16),900) "#"
      write (cLine(17),900) "field3d   ", "q    Charge    real4 0 0"
      write (cLine(18),900) "field3d   ", "phi  Potential real4 0 0"
      write (cLine(19),900) "field3d   ", "Ex   X-Efield  real4 0 0"
      write (cLine(20),900) "field3d   ", "Ey   Y-Efield  real4 0 0"
      write (cLine(21),900) "field3d   ", "Ez   Z-Efield  real4 0 0"
      write (cLine(22),900) "#"
      write (cLine(23),900) "# Patch refinement 3D offsets and file"

      do i = 1, 23
        n = len(trim(cLine(i)))
        cMeta(nMeta+1:nMeta+n+1) = trim(cLine(i)) // char(10)
        nMeta = nMeta + n + 1
      enddo

      return
      end

c  time         0.000000E+00
c  dtime        1.000000E+00
c  step                 0
c  ncube                0
c  i2max            65501
c  i2off            32750
c  ncubesxyz      640     640     512
c  nbrickxyz        1       1       1
c  ndvar               14
c  nbpf                 1
c  xlimits     -5.000000E+00   5.000000E+00
c  ylimits     -5.000000E+00   5.000000E+00
c  zlimits      0.000000E+00   2.000000E+01
c  expandxl           0   1.000000E+00
c  expandxr           0   1.000000E+00
c  expandyl           0   1.000000E+00
c  expandyr           0   1.000000E+00
c  expandzl           0   1.000000E+00
c  expandzr           0   1.000000E+00
c  lobndry   continuation    continuation    continuation    
c  hibndry   continuation    continuation    continuation    
c  field3d   Vx      X-Velocity    real4     0.000000E+00   0.000000E+00
c  field3d   Vy      Y-Velocity    real4     0.000000E+00   0.000000E+00
c  field3d   Vz      Z-Velocity    real4     0.000000E+00   0.000000E+00
c  field3d   rho     Mass_Density  real4     0.000000E+00   0.000000E+00
c  field3d   energy  Total_Energy  real4     0.000000E+00   0.000000E+00


C=====================================================================72
      subroutine copy_bytes(a,b,n)
      implicit none
      byte a(n), b(n)
      integer n, i
      do i = 1, n
        b(i) = a(i)
      enddo
      return
      end

C=====================================================================72
      subroutine gen_one_patch(ipatch, flds, nbytesdata)
      implicit none
      include "amr_output.h"
      real flds(1-nb:nx+nb,1-nb:ny+nb,1-nb:nz+nb,5)
      integer i, ix, iy, iz, iv, ipatch, nbytesdata, isrc
      real dx,dy,dz, xoff,yoff,zoff, x,y,z,phi
      real pi, q0, r0, r02, p0, f0, qden, r2, r, qr3

      do iv = 1, 5
      do iz = 1-nb,nz+nb
      do iy = 1-nb,ny+nb
      do ix = 1-nb,nx+nb
        flds(ix,iy,iz,iv) = 0.0
      enddo
      enddo
      enddo
      enddo

      dx = (xyz1(1,ipatch) - xyz0(1,ipatch)) / float(nx)
      dy = (xyz1(2,ipatch) - xyz0(2,ipatch)) / float(ny)
      dz = (xyz1(3,ipatch) - xyz0(3,ipatch)) / float(nz)
      xoff = xyz0(1,ipatch) - 0.5 * dx
      yoff = xyz0(2,ipatch) - 0.5 * dy
      zoff = xyz0(3,ipatch) - 0.5 * dz
      pi = 4.0 * atan(1.0)

      do isrc = 1, nsrc
      q0 = qrxyz(1,isrc)
      r0 = qrxyz(2,isrc)
      r02 = r0 ** 2
      p0 = q0 / (2.0 * r0)
      f0 = q0 / r0**3
      qden = q0 / ((4.0/3.0)*pi * r0**3)

      do iz = 1-nb,nz+nb
      z = zoff + dz*float(iz) - qrxyz(5,isrc)
      do iy = 1-nb,ny+nb
      y = yoff + dy*float(iy) - qrxyz(4,isrc)
      do ix = 1-nb,nx+nb
      x = xoff + dx*float(ix) - qrxyz(3,isrc)

        r2 = (x**2 + y**2 + z**2)
        r = sqrt(r2)
        qr3 =  q0 / (r * r2)
        if(r .lt. r0) then
          flds(ix,iy,iz,1) = flds(ix,iy,iz,1) + qden
          flds(ix,iy,iz,2) = flds(ix,iy,iz,2) + p0 * (3.0 - r2/r02)
          flds(ix,iy,iz,3) = flds(ix,iy,iz,3) + f0 * x
          flds(ix,iy,iz,4) = flds(ix,iy,iz,4) + f0 * y
          flds(ix,iy,iz,5) = flds(ix,iy,iz,5) + f0 * z
        else
          flds(ix,iy,iz,2) = flds(ix,iy,iz,2) + q0 / r
          flds(ix,iy,iz,3) = flds(ix,iy,iz,3) + qr3 * x
          flds(ix,iy,iz,4) = flds(ix,iy,iz,4) + qr3 * x
          flds(ix,iy,iz,5) = flds(ix,iy,iz,5) + qr3 * x
        endif
      enddo
      enddo
      enddo
      enddo

      nbytesdata = 20 * (nx+2*nb)*(ny+2*nb)*(nz+2*nb)

      return
      end

C=====================================================================72
      subroutine gen_patch_list()
      implicit none
      include "amr_output.h"
      integer ibx0, ibx1, iby0, iby1, ibz0, ibz1, ibx,iby,ibz
      integer npatches0, level0, nrefine0, i, j

      call gen_offset1d(myrank,      1,npx,nbx,ibx0,ibx1)
      call gen_offset1d(myrank,    npx,npy,nby,iby0,iby1)
      call gen_offset1d(myrank,npx*npy,npz,nbz,ibz0,ibz1)

      level0    = 1
      nrefine0  = 1
      npatches0 = 0
      npatches  = npatches0
      do ibz = ibz0, ibz1
      do iby = iby0, iby1
      do ibx = ibx0, ibx1
        npatches = npatches + 1
        ixyzoff(1,npatches) = ibx
        ixyzoff(2,npatches) = iby
        ixyzoff(3,npatches) = ibz
        level(npatches)     = level0
        nrefine(npatches)   = nrefine0
        call gen_patch_limits(npatches)
      enddo
      enddo
      enddo

      do while(npatches0 .lt. npatches  .and.  level0 .le. 3)
        call refine_patches(level0, nrefine0)
         level0   =  level0 + 1
         nrefine0 =  nrefine0 * nrep_fac
         npatches0 = npatches
      enddo

c      if(myrank .eq. 1) then
c      do i = 1, npatches
c      write (6,999) "lro:", level(i),nrefine(i),(ixyzoff(j,i),j=1,3)
c      enddo
c      endif

      write (6,999) "myrank, io[XYZ]0, npatches :",
     1                 myrank,ibx0,iby0,ibz0, npatches
c      write (6,999) "myrank,ib1XYZ :", myrank,ibx1,iby1,ibz1
c      write (6,*) " "
999   format(a,20i4)


      return
      end

C=====================================================================72
      subroutine refine_patches(level0, nrefine0)
      implicit none
      include "amr_output.h"
      integer level0, nrefine0, n, level1, nrefine1
      integer i, irx, iry, irz, i_need_to_refine


      n = npatches
      do i = 1, n
        if(level(i) .eq. level0) then
           if(i_need_to_refine(i) .eq. 1) then
             ! Reifine patch into 8 patches
             do irz = 0, nrep_fac-1
             do iry = 0, nrep_fac-1
             do irx = 0, nrep_fac-1
               npatches = npatches + 1
               ixyzoff(1,npatches) = irx + nrep_fac*ixyzoff(1,i)
               ixyzoff(2,npatches) = iry + nrep_fac*ixyzoff(2,i)
               ixyzoff(3,npatches) = irz + nrep_fac*ixyzoff(3,i)
               level(npatches)   = level0 + 1
               nrefine(npatches) = nrep_fac * nrefine0
               call gen_patch_limits(npatches)
             enddo
             enddo
             enddo
           endif 
        endif
      enddo

      return
      end

C=====================================================================72
      subroutine gen_patch_limits(i)
      implicit none
      include "amr_output.h"
      integer i, j
      real psize(3), poff(3)

      psize(1) = xsize / float(nbx*nrefine(i))
      psize(2) = ysize / float(nby*nrefine(i))
      psize(3) = zsize / float(nbz*nrefine(i))
      poff(1) = -0.5 * xsize
      poff(2) = -0.5 * ysize
      poff(3) = -0.5 * zsize
      do j = 1, 3
        xyz0(j,i) =  poff(j) + psize(j) * float(ixyzoff(j,i)  )
        xyz1(j,i) =  poff(j) + psize(j) * float(ixyzoff(j,i)+1)
      enddo

      return
      end
      
C=====================================================================72
      integer function i_need_to_refine(i)
      implicit none
      include "amr_output.h"
      integer isrc, i, j, need
      real d(3), delmax, D2MAX, p, r, r2, d2, c2

      need = 0

      delmax = max((xyz1(1,i) - xyz0(1,i)) / float(nx),
     1             (xyz1(2,i) - xyz0(2,i)) / float(ny),
     1             (xyz1(3,i) - xyz0(3,i)) / float(nz) )
       d2max = (crit_fac * delmax) ** 2

      do isrc = 1, nsrc
        d2 = 0.0
        do j = 1, 3
          p = qrxyz(2+j,isrc)
          if(p .lt. xyz0(j,i)) d2 = d2 + (xyz0(j,i)-p)**2
          if(p .gt. xyz1(j,i)) d2 = d2 + (p-xyz1(j,i))**2
        enddo
        r2 = qrxyz(2,isrc) ** 2
        c2 = d2max
        if(d2.gt.r2) c2 = d2max * r2 / d2
        ! src in patch         mesh to big
        if(r2 .le. c2) need = 1

c          write (6,998) "lor2d2 c2,xyz0", level(i),
c     1       (ixyzoff(j,i),j=1,3),
c     1     r2,d2,c2,(xyz0(j,i),j=1,3)
c998       format(a,4i5,6f10.5)

      enddo

      if(need .ge. 1) then
          write (6,999) "REFINING: lo ", level(i), (ixyzoff(j,i),j=1,3)
999       format(a,10i5)
      endif


      i_need_to_refine = need
      return
      end
C=====================================================================72
      subroutine gen_offset1d(iproc,mp,np,nbox,ibox0,ibox1)
      implicit none
      integer iproc,mp,np,nbox,ibox0,ibox1, nbperp, i
      nbperp = nbox / np
      i = mod(iproc/mp,np)
      ibox0 =     nbperp* i
      ibox1 = min(nbperp*(i+1),nbox) - 1
      return
      end

C=====================================================================72
      subroutine init_aio()
      implicit none
      include "mpif.h"
      include "amr_output.h"
      integer nfields,ncells,maxdata,maxmeta,nworkers

      ! set max aio buffer size to accomodate both real and meta data
      nfields = 5
      ncells = (nx+2*nb) * (ny+2*nb) * (nz+2*nb)
      maxdata = 4 * nfields * maxpatches * ncells
      maxmeta = 200 * maxpatches
      maxbuf = maxdata + maxmeta
      call aio_set_max_data_size(maxbuf)

       ! set extentions for real and meta data files
      call aio_set_data_extention_range("-A", "-Z") 
      call aio_set_meta_extention(".meta")  

      ! Assign aio client & servers to ranks
      nworkers = npx*  npy * npz
      call aio_set_clients_to_low_ranks(nworkers, MPI_COMM_WORLD)  

      ! Initialize aio internal variables & start aio servers
       ! All ranks must call this, but only woker ranks retrun
      call aio_start_servers()

      return
      end
C=====================================================================72
c      call aio_gen_data_file_name(cRoot, cDataFile)  
C=====================================================================72
      subroutine get_inputs()
      implicit none
      include "mpif.h"
      integer ierr
      include "amr_output.h"
      integer i0, n, next_line, j
      character*(256) cLine, cVal
      character*8 cKey
      integer i1, i2, i3

      call set_blank(cBuffer)
      call aio_all_read("amr_output.in", maxread, nread, cBuffer)
      idump = 123

      nsrc = 0
      i0 = 1
      do while(next_line(i0,nread,cBuffer, cLine) .gt. 0)
        call line_to_key_val(cLine, cKey, Cval)
        if(cKey .eq. "runname ") read(cVal,*) cName
        if(cKey .eq. "meshxyz ") read(cVal,*) nx,ny,nz
        if(cKey .eq. "box_xyz ") read(cVal,*) nbx,nby,nbz
        if(cKey .eq. "procxyz ") read(cVal,*) npx,npy,npz
        if(cKey .eq. "nbndry  ") read(cVal,*) nb
        if(cKey .eq. "sizexyz ") read(cVal,*) xsize,ysize,zsize
        if(cKey .eq. "crit_fac") read(cVal,*) crit_fac
        if(cKey .eq. "nrep_fac") read(cVal,*) nrep_fac
        if(cKey .eq. "source  ") then
          nsrc = nsrc + 1
          read(cVal,*) (qrxyz(j,nsrc),j=1,5)
        endif
      enddo

      return
      end

C=====================================================================72
      subroutine set_defaults()
      implicit none
      include "amr_output.h"

      nrep_fac = 2      ! refinment factore between consecutive levels
      crit_fac = 4.00   ! mesh must resolve src radius ith brit_vac cells
      nx = 32           ! mesh res of each patch
      ny = 32
      nz = 32
      nb = 2            ! boundary depth

      return
      end
C=====================================================================72
      subroutine init_mpi()
      implicit none
      include "mpif.h"
      include "amr_output.h"
      integer ierr

      call MPI_INIT (ierr)
      call MPI_COMM_SIZE (MPI_COMM_WORLD, nprocs, ierr)
      call MPI_COMM_RANK (MPI_COMM_WORLD, myrank, ierr)

      return
      end
C=====================================================================72
      subroutine my_exit(cComment)
      implicit none
      include "mpif.h"
      include "amr_output.h"
      integer ierr
      character*(*) cComment

      call aio_finalize()
      call MPI_BARRIER(MPI_COMM_WORLD, ierr)
      call MPI_FINALIZE(ierr)

      if(myrank .eq. 0) write (6,*) trim(cComment)

      stop
      end
C=====================================================================72
      subroutine set_blank(str)
      implicit none
      character*(*) str
      integer i
      do i = 1, len(str)
        str(i:i) = ' '
      enddo
      return
      end
C=====================================================================72
      subroutine line_to_key_val(cLine, cKey, Cval)
      implicit none
      character*(*) cLine, cKey, Cval
      integer nkey, nlin
      nkey = len(cKey)
      nlin = len(trim(cLine)) - 1    ! remove charage return
      call set_blank(cVal)
      ckey              = cLine(1:nkey)
      cVal(1:nlin-nKey) = cLine(1+nkey:nlin)

      return
      end

C=====================================================================72
      integer function next_line(i0,nBuf,cBuf, cLine)
      implicit none
      integer i0, nBuf
      character*(*) cBuf, cLine
      integer i, n

      call set_blank(cLine)
      if(i0 .gt. nBuf) then
        next_line = 0
        return
      endif

      i = i0
      do while(i.le.nBuf .and. cBuf(i:i).ne.char(10))
         i = i + 1
      enddo
      n = 1 + i - i0
      cLine(1:n) = cBuf(i0:i)
      i0 = i0 + n
      next_line = n

      return
      end

C=====================================================================72
