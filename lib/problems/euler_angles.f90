program rott

implicit none

real*8 :: vector(3), uvector(3), phi, theta, psi, Rot(3,3)
real*8 :: vectory(3), vectorz(3), vvector(3), wvector(3)

vector = (/1.d0, 0.d0, 0.d0/)
vectory = (/ 0.d0, 1.d0, 0.d0 /)
vectorz = (/ 0.d0, 0.d0, 1.d0 /)

! rotate to X
phi   = 0.d0
theta = 90.d0
psi   = 0.d0
uvector = vector
CALL GetRotate(Rot, phi, theta, psi)
CALL Rotate(Rot, uvector)
print '("X", 6(ES13.5))', phi, theta, psi, uvector

! rotate to Y
phi   = -90.d0
theta = 0.d0
psi   = 0.d0
uvector = vector
CALL GetRotate(Rot, phi, theta, psi)
CALL Rotate(Rot, uvector)
print '("Y", 6(ES13.5))', phi, theta, psi, uvector

! rotate to 45 in XY 
phi   = 0.d0
theta = 90.d0
psi   = -45.d0
uvector = vector
CALL GetRotate(Rot, phi, theta, psi)
CALL Rotate(Rot, uvector)
print '("XY 45", 6(ES13.5))', phi, theta, psi, uvector

! rotate to 45 in XZ
phi   = 45.d0
theta = 90.d0
psi   = 0.d0
uvector = vector
CALL GetRotate(Rot, phi, theta, psi)
CALL Rotate(Rot, uvector)
print '("XZ 45", 6(ES13.5))', phi, theta, psi, uvector

! rotate to 45 in YZ
phi   = -90.d0
theta = -45.d0
psi   = 0.d0
uvector = vector
CALL GetRotate(Rot, phi, theta, psi)
CALL Rotate(Rot, uvector)
print '("YZ +45", 6(ES13.5))', phi, theta, psi, uvector

! rotate to 45 in XYZ
phi   = ATAN(SQRT(0.5d0)) * 180.d0 / (2.d0 * DASIN(1.d0))!37.d0
theta = 90.d0
psi   = -45.d0
uvector = vector
CALL GetRotate(Rot, phi, theta, psi)
CALL Rotate(Rot, uvector)
print '("XYZ +45", 6(ES13.5))', phi, theta, psi, uvector

do while (.true.)

    print '("PHI, THETA, PSI: ",$)'
    read *, phi, theta, psi
    uvector = vector
    CALL GetRotate(Rot, phi, theta, psi)
    CALL Rotate(Rot, uvector)
    CALL Rotate(Rot, vectory)
    CALL Rotate(Rot, vectorz)

    print '("X, Y, Z", 6(ES13.5))', phi, theta, psi, uvector
    print '("vectory", 3(ES13.5))', vectory
    print '("vectorz", 3(ES13.5))', vectorz




end do

contains

!### this is a 3-d Euler angle rotation matrix ###
SUBROUTINE GetRotate (A, phi, theta, psi)

    REAL*8, DIMENSION(3,3), INTENT(INOUT) :: A
    REAL*8 :: phi, theta, psi, pi

    pi    = 2.d0 * DASIN(1.d0)
    phi   = phi * pi / 180.d0
    theta = theta * pi / 180.d0
    psi   = psi * pi / 180.d0

    !### calculate the rotation given the normal Euler angle rotation ###
    !### this is literally as given in Goldstein pp 153 ###
    A(1,1) = DCOS(psi)*DCOS(phi) - DCOS(theta)*DSIN(phi)*DSIN(psi)
    A(1,2) = DCOS(psi)*DSIN(phi) + DCOS(theta)*DCOS(phi)*DSIN(psi)
    A(1,3) = DSIN(psi)*DSIN(theta)

    A(2,1) = -DSIN(psi)*DCOS(phi) - DCOS(theta)*DSIN(phi)*DCOS(psi)
    A(2,2) = -DSIN(psi)*DSIN(phi) + DCOS(theta)*DCOS(phi)*DCOS(psi)
    A(2,3) = DCOS(psi)*DSIN(theta)

    A(3,1) = DSIN(theta)*DSIN(phi)
    A(3,2) = -DSIN(theta)*DCOS(phi)
    A(3,3) = DCOS(theta)

    phi   = phi * 180.d0 / pi
    theta = theta * 180.d0 / pi
    psi   = psi * 180.d0 / pi

END SUBROUTINE GetRotate

!### Rotate takes a rotation matrix and some other matrix and multiplies them ###
SUBROUTINE Rotate (A, mat)

    REAL*8, DIMENSION(3,3) :: A
    REAL*8, DIMENSION(3), INTENT(INOUT) :: mat
    REAL*8, DIMENSION(3) :: work
    INTEGER :: i, j

    !### init work as 0 ###
    work = 0.d0

    !### do the matrix multiplication ###
    DO i = 1, 3

        DO j = 1, 3

            work(i) = work(i) + mat(j) * A(i,j)

        END DO

    END DO

    !### update the input array ###
    mat = work

END SUBROUTINE Rotate

end program
