###!/bin/bash
###Run all RJ95 tests in 2D at 0, 30, 45, and 60 degrees:

gfortran -o brick brick.f90 

### Test names
tests[0]='1a'
tests[1]='1b'
tests[2]='2a'
tests[3]='2b'
tests[4]='3a'
tests[5]='3b'
tests[6]='4a'
tests[7]='4b'
tests[8]='4c'
tests[9]='4d'
tests[10]='5a'
tests[11]='5b'

### Which output for each test to automatically generate bof files for (can do any other output files later as well)
tests_out[0]='0008'
tests_out[1]='0003'
tests_out[2]='0020'
tests_out[3]='0007'
tests_out[4]='0001'
tests_out[5]='0010'
tests_out[6]='0015'
tests_out[7]='0015'
tests_out[8]='0015'
tests_out[9]='0016'
tests_out[10]='0010'
tests_out[11]='0016'

### angles for file paths
angles[0]='0d'
angles[1]='22p5d'
angles[2]='30d'
angles[3]='45d'
angles[4]='60d'

### angles for brick.f90 program
phi[0]='0'
phi[1]='22.5'
phi[2]='30'
phi[3]='45'
phi[4]='60'

### variables tracked
var[0]='RHO'
var[1]='PRS'
var[2]='Vx'
var[3]='Vy'
var[4]='Vz'
var[5]='Bx'
var[6]='By'
var[7]='Bz'
var[8]='Ptot'
var[9]='Bmag'
var[10]='Vmag'
var[11]='E'

### Make file paths if not already present
mkdir -p RJ95Tests/2Dtest

fileroot='RJ95Tests/2Dtest/test'

for (( i = 0 ; i < 12; i++ ))
do
	for (( j = 0 ; j < 5; j++ ))
	do
		### Make more file paths
		direc=$fileroot
		direc+=${tests[i]}
		direc+='/'
		direc+=${angles[j]}
		direc+='/'
		mkdir -p $direc
		### Remove old data files if present
		removedata=$direc
		removedata+='RJ*'
		rm -f $removedata
		### Get the wombat namelist file for this test
		getwombatfile='namelists/wombat.'
		getwombatfile+=${tests[i]}
		getwombatfile+=${angles[j]}
		wombatfile=$direc
		wombatfile+='wombat.'
		wombatfile+=${tests[i]}
		wombatfile+=${angles[j]}
		cp $getwombatfile $wombatfile
		echo $wombatfile
		### Run Wombat
		mpirun -np 17 ../../src/wombat $wombatfile
		### Get the formulas file for e3d
		form=$direc
		form+='formulas.e3d'
		cp formulas.e3d $form
		# Go into the current test folder
		cd $direc
		### Choose the output file to run e3d on
		data='RJ952-'
		data+=${tests_out[i]}
		data+='-000'
		### Run e3d to make brick of float files for each variable
		for (( k = 0 ; k < 12; k++ ))
		do
			ee $data ${var[k]} bof
		done
		### Return to the wombat parent folder
		cd ../../../..
		### Create the data files for the line cut along the center of the interface normal
		for (( k = 0 ; k < 12; k++ ))
		do
			boffile=$direc
			boffile+='RJ952-'
			boffile+=${tests_out[i]}
			boffile+='-'
			boffile+=${var[k]}
			boffile+='.bof'
			datfile=$direc
			datfile+=${var[k]}
			datfile+='.dat'
			./brick $boffile 256 128 1 ${phi[j]} 0 0 $datfile
		done
		plotfilename=$direc
		plotfilename+='plotfile'
		cp plotfile $plotfilename
		cd $direc
		rm -f *.png
		gnuplot plotfile 2>/dev/null
		cd ../../../..
	done
done
