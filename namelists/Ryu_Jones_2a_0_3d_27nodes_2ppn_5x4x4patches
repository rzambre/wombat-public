&RankDecomposition
!### Domain decomposition and communication setup ###

!--- BASIC DOMAIN/RESOURCE INFORMATION ---
ndims				= 3,	
nranks_x			= 6,	
nranks_y			= 3,	
nranks_z			= 3,	
naio				= 0,		! number of AIO server ranks (total ranks = naio + nranks_x*ranks_y*nranks_z)
nthreads			= 20,		! number of OpenMP threads per rank

!--- LOAD BALANCING/COMMUNICATION PARAMETERS ---
neighborhood_Xlength		= 3,		! number of MPI ranks in the neighborhood along X
neighborhood_Ylength		= 3,		! number of MPI ranks in the neighborhood along Y
neighborhood_Zlength		= 3,		! number of MPI ranks in the neighborhood along X
patch_mailboxes_pslot		= 20,		! number of Patches that can be communicated at once for each neighbor
max_inflightcomms		= 0,		! maximum number of GETs ops for large data motion in flight at one time (0 = unlimited)
nspins				= 1,		! number of polling spins for ready messages

/

&Simulation
!### propeties and units of the simulation ###

!--- SIMULATION PROPERTIES ---
xboxsize			= 1.0d0,	! length of world grid along X in CGS units
dt0				= 1.d-8,	! initial time step size in CGS units
t0				= 0.d0,		! initial time in CGS units
t_end				= 0.205d0,	
max_steps			= 10000000,	! maximum number of time steps to take
cfl				= 0.4d0,	

!--- PHYSICAL SCALING ---
mass_cgs			= 1.0d0,	! unit of mass in CGS unit
length_cgs			= 1.0d0,	! unit of length in CGS units
velocity_cgs			= 1.0d0,	! unit of velocity in CGS units

/

&Grid
!### root domain properties ###

!--- SOLVER COMPONENT CONTROLS ---
npass_vars			= 0,		! number of passive scalar variables to advect
ncr_momentum_bins		= 0,		! number of cosmic ray momentum bins
mhdOn				= .TRUE.,	! enable the MHD solver?
passOn				= .FALSE.,	! enable the passive advection solver?
crsOn				= .FALSE.,	! enable the cosmic ray solver (not yet implemented)
static_gravityOn		= .FALSE.,	! enable static gravity?
variable_gravityOn		= .FALSE.,	! enable time-variable gravity?
dark_matterOn			= .FALSE.,	! enable the dark matter solver?

!--- DARK MATTER MANAGEMENT ---
npm_maxblockmgrs		= 0,		! number of dark matter block managers (first level in DM tree)
npm_dms_per_blkmgr		= 0,		! number of dark matter segments per block manager (second level in DM tree)
npm_partcls_per_dm		= 0,		! number of dark matter particle slots per DM segment
npm_steps_per_defrag		= 0,		! number of time steps between DM tree defragmentation operations
DM_buff_fac			= 0.d0,		! ? (ask Brian)
particle_mass			= 0.d0,		! dark matter particle mass in code units

!--- ROOT DOMAIN DECOMPOSITION PER MPI RANK ---
n_xpatches			= 5,		! number of Patches per MPI rank along X
n_ypatches			= 4,		! number of Patches per MPI rank along Y
n_zpatches			= 4,		! number of Patches per MPI rank along Z
patch_nx			= 32,	
patch_ny			= 32,	
patch_nz			= 32,	
patch_nb               		= 5,		! number of boundary zones

/

&MHD
!### MHD solver settings ###

eps1				= 0.15d0,	! fast mode numerical dissipation (< 0.5)
eps2				= 0.1d0,	! alfven mode numerical dissipation (< 0.5)
eps3				= 0.15d0,	! slow mode numerical dissipation (< 0.5)
eps4				= 0.1d0,	! contact mode numerical dissipation (< 0.5)
zero_value			= 1.d-15,	! floating point tolerance for zero
min_pressure			= 1.d-16,	! pressure floor in code units
min_density			= 1.d-16,	! density floor in code units
prot_min_pressure		= 1.d-9,	! protection flux pressure threshold
prot_min_density		= 1.d-9,	! protection flux density threshold
/

&MG_Grid
!### settings for the multigrid solver ###

nlevels				= 3,		! total number of levels including base
coarse_factor_patches(1)	= 2,		! coarsening factor for the number of Patches from level (N) to (N+1)
coarse_factor_patches(2)	= 1,
coarse_factor_grid(1)		= 2,		! coarsening factor for the number of zones from level (N) to (N+1)
coarse_factor_grid(2)		= 2,
/

&MG
!### settings for the full multigrid solver ###

npresmooth_iters		= 2,		! number of smoothing iterations going down a V cycle
npostsmooth_iters		= 2,		! number of smoothing operations going up a V cycle
nmaxsmooth_iters		= 1000000,	! maximum number of smoothing operations at coarsest level
nsmooth_block			= 4,		! number of full multigrid cycles
nvcycles			= 1,		! number of V cycles
convergence_criteria		= 1.d-14,	! maximum error tolerance for convergence
/

&MGGrav
!### settings for the gravity smoothing operator for MG ###

grav_const			= 1.d0,		! gravitational constant in code units
/

&ProblemSetup
!### Problem setup information ###

bounds_routine				= "ContinuousBounds",
gamma				= 1.66666666666667d0,
init_routine				= "Test_RJ95_2a",
phi				= 0.0d0,
psi				= 0.0d0,
theta				= 0.0d0,
/

&IOOptions
!### IO Options ###

!### memory tuning - Set the size (in MBytes) that an IO buffer should be.  Setting this lower will lessen the memory overhead for IO on each rank but also ###
!### result in more files per dump and possibly lower IO performance.  If unsure or not constrained by node memory these should be left as their defaults by setting them to 0. ###
restart_buffer_size		= 0,
compressed_buffer_size		= 0,
movie_buffer_size		= 0,

!### Fill out some information about this run that would be helpful to store into the output files ###
!### The title can be up to 256 characaters and the description up to 2048 characters. ###
simulationTitle			= "Ryu_Jones_2a_0_3d",	
simulationDescription		= "Ryu and Jones 95 1d shock tube test 2a 0 degree rotation in 3d",	

!### RESTARTS ###
!### restart files dump all data needed to resume the simulation at full precision ###
!### the number indicates the number to start at.  if it is greater than 0 the existing file with that number will be used to resume the simulation ###
restartOn			= .FALSE.,
restartNumber			= 0,
restartFileRoot			= 'SomeFileName',
restartPath			= 'somedir',
restartHost			= 'localhost',
restartInterval			= 10.d0,

!### COMPRESSED ###
!### compressed dumps are "analysis" files where the variables and their precision can be user defined ###
compressedOn			= .FALSE.,
compressedNumber		= 0,
compressedFileRoot		= 'CPAlf',
compressedPath			= './',
compressedHost			= 'localhost',
compressedInterval		= 2.5d-1,

!### MOVIE ###
!### movie dumps are intended to be frequently created files where the variables and their precision can be user defined ###
movieOn				= .FALSE.,
movieNumber			= 0,
movieFileRoot			= 'SomeFileName',
moviePath			= 'somedir',
movieHost			= 'localhost',
movieInterval			= 0.1d0,

!### COMPRESSED AND MOVIE FILE CONTENTS ###
!### here we define two arrays, compressed and movie, that set the variables and their mapping/precision. ###
!### the following tables defines the available options. ###
! Precision Options:
!
! byte1	- convert value into a 8 bit integer with the selected mapping
! byte2	- convert value into a 16 bit integer with the selected mapping
! real4	- convert value into a 32 bit floating point value
! real8	- convert (if necessary) value into a 64 bit floating point value
!
! Mapping Options (value will be clipped to the range, log mapping will only use value absolute magnitudes):
!
! linear	- map according to B * (value - varmin) / (varmax - varmin), where B = 255 for byte1 and 65535 for byte2
! log_e		- map according to B * ln(value/varmin) / ln(varmax/varmin), where B = 255 for byte1 and 65535 for byte2
! natural	- (real4 and real8 only) do not map the value to any range
! 
! Fields (use the number given here in the format specifiers below to get the described value):
!
! 1-8		- state variables; 1 = density, 2-4 = x,y,z, momentum, 5-7 = x,y,z magnetic field, 8 = energy density
! 10		- pressure
! 11-80		- raw passive field
! 81		- magnitude of velocity
! 82		- magnitude of magnetic field
! 83		- kinetic energy density
! 84		- magentic energy density
! 85		- thermal energy density
! 86		- gas temperature
! 90-93		- gx, gy, gz, phi for gravity
! 94		- Mach numbers from shock detector
! 95		- dark matter particles
! 96		- gravitating mass density (gas and DM)
! 101-103	- velocity components vx, vy, vz

!### COMPRESSED FORMAT ###
! NOTE: the full length of the format string should be 60 characters with exactly 6 space delimited columns
!                          Num  Sym  Name              Prec.     Map     varmin  varmax
compressedFormat(1) 	= "  1  RHO  Mass_density      real4     natural 1.e-3      1e3",
compressedFormat(2)	= "101  Vx   X-Velocity        real4     linear   -10.      10.",
compressedFormat(3)	= "102  Vy   Y-Velocity        real4     linear   -10.      10.",
compressedFormat(4)	= "103  Vz   Z-Velocity        real4     linear   -10.      10.",
compressedFormat(5)	= "  5  Bx   X-MagneticField   real4     linear   -10.      10.",
compressedFormat(6)	= "  6  By   Y-MagneticField   real4     linear   -10.      10.",
compressedFormat(7)	= "  7  Bz   Z-MagneticField   real4     linear   -10.      10.",
compressedFormat(8)	= " 82  Bma  B-Magnitude       real4     log_e   1.e-3      1e3",
compressedFormat(9)	= " 10  PRS  Pressure          real4     natural 1.e-3      1e3",

!### MOVIE FORMAT ###
! NOTE: the full length of the format string should be 60 characters with exactly 6 space delimited columns
!                          Num  Sym  Name              Prec.     Map     varmin  varmax
movieFormat(1)		= "  1  RHO  Mass_density      byte1     log_e   1.e-3      1e3",
movieFormat(2)  	= "101  Vx   X-Velocity        byte1     linear   -10.      10.",
movieFormat(3)  	= "102  Vy   Y-Velocity        byte1     linear   -10.      10.",
movieFormat(4)  	= "103  Vz   Z-Velocity        byte1     linear   -10.      10.",
movieFormat(5)  	= "  5  Bx   X-MagneticField   byte1     linear   -10.      10.",
movieFormat(6) 		= "  6  By   Y-MagneticField   byte1     linear   -10.      10.",
movieFormat(7)  	= "  7  Bz   Z-MagneticField   byte1     linear   -10.      10.",
movieFormat(8)  	= " 10  PRS  Gas_Pressure      byte1     log_e   1.e-3      1e3",
movieFormat(9)  	= " 11  PS0  Color             byte2     linear     0.       1.",

/
