c     Program p03d
c     Solve geometric (0-point) 3D Poisson's problem
c
c ----------------------------------------------------------------------
c     Collect mg3d data to subset of ranks when mesh is to small
cDONE * mg_init_vars_3d0p: set hierarchy of collected levels
cDONE   + have a minimimum res allowed
cDONE   + nclct(ilev): mesh on level ilev is
cDONE          nclct(ilev)*nxfull / nrefine**(ilev-1) >= minres
cDONE          nclct(ilev)*nxfull >=  minres * (nrefine**(ilev-1))
cDONE * gen_level_offsets_3d
cDONE   + independent set of nieghbors for each level
cDONE   + map of ranks (from/to) for collecting & splitting
cDONE * MG_blend_fld: use nx/nrefine BEFORE mg_blend_params
cDONE * MG_refine_fld: use nx*nrefine AFTER mg_refine_params
cDONE * mg_refine_apram & mg_blend_param:
cDONE   + copy nieghbors for each level
c     * mg_refine_fld & mg_blend_fld: gather & scatter field(s)
c     * ranks with no mesh on a given level skip the work
C=======================================================================

      implicit none
      include 'mpif.h'
#include "p3d.h"
      real*dsp v
      allocatable v(:)
      integer iter, ierr


      call MPI_INIT (ierr)
      call MPI_COMM_SIZE (MPI_COMM_WORLD, nranks, ierr)
      call MPI_COMM_RANK (MPI_COMM_WORLD, myrank, ierr)

      call stopwatch_start("total               ")
      call stopwatch_start("init                ")
      call read_inputs()
      call set_mesh()

      call mg_init_vars_3d0p()
      call mg_set_comm(MPI_COMM_WORLD, neighbor, ntx, nty, ntz)
c      call mg_set_trace_verbosity(0)   ! default is 0
      call mg_set_cycle("V", nlevels, nsteps(1))
      call gen_level_offsets_3d(nlevels,nx,ny,nz,nb,maxvsize)
      allocate(v(maxvsize))

      call mg_set_sor_factor(sorfac1)
      call mg_set_npencil(npen)
      call mg_1st_touch_3d(v)
      call set_flds(v)    ! calls: mg_set_source_3d(src, v)
      call stopwatch_stop("init                ")

      call MPI_BARRIER(MPI_COMM_WORLD, ierr)
      call stopwatch_start("mg_trace            ")
      call mg_trace(v)
      call MPI_BARRIER(MPI_COMM_WORLD, ierr)
      call stopwatch_stop("mg_trace            ")
      call stopwatch_start("dump_fields         ")
      call dump_fields(v)
      call stopwatch_stop("dump_fields         ")

      call MPI_BARRIER(MPI_COMM_WORLD, ierr)
      do iter = 1, niterate
        call stopwatch_start("mg_run_cycle        ")
        call mg_run_cycle(v)
        call stopwatch_stop("mg_run_cycle        ")

        if(iverbose_trace .gt. 0) then
          call stopwatch_start("mg_trace            ")
          call mg_trace(v)
          call stopwatch_stop("mg_trace            ")
        endif
      enddo

        if(iverbose_trace .le. 0) then
        call stopwatch_start("mg_trace            ")
        call mg_trace(v)
        call stopwatch_stop("mg_trace            ")
      endif

      call stopwatch_start("dump_fields         ")
      call dump_fields(v)
      call stopwatch_stop("dump_fields         ")
      call stopwatch_stop("total               ")

c      if(myrank .eq. 0) then
c      call stopwatch_write("timings             ")
c      endif
      call stopwatch_write_ranks("timings_ranks       ")
      call stopwatch_write_rec()

      call myexit("# Succesfull completion of p3d")

      stop
      end

c=====================================================================72
      subroutine myexit(cstr)
      include 'mpif.h'
#include "p3d.h"
      character*(*) cStr
      integer ierr
      call MPI_BARRIER(MPI_COMM_WORLD, ierr)
      if(myrank .eq. 0) write (6,*) cStr
      call MPI_FINALIZE(ierr)
      stop
      end

c=====================================================================72
      subroutine set_mesh()
      implicit none
#include "p3d.h"
      real*dsp qsum

      dx = xsize / dble(nx*ntx)
      dy = dx
      dz = dx

      ysize = dy * dble(ny*nty)
      zsize = dz * dble(nz*ntz)

      xyzmin(1) = -0.5d0 * xsize
      xyzmin(2) = -0.5d0 * ysize
      xyzmin(3) = -0.5d0 * zsize

      return
      end


ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      real*dsp dx, dy, dz, xyzmin(3), xsize, ysize, zsize
c      real*dsp eps0, sorfac1
c      real*dsp qsrc(100),xsrc(100),ysrc(100),zsrc(100),rsrc(100)
c      integer nx, ny, nz, nsrcs
c      integer ncycles, nlevels, nstages,nsteps(10000),ilevel(10000)
c      integer nfinish, maxvsize, nperdump
c      character*8 cRunName, cOutSymb(100), cOutName(100)
c
c      common /p3dcom/ dx, dy, dz, xyzmin, xsize, ysize, zsize
cc      common /p3dcom/ eps0, sorfac1
c      common /p3dcom/ qsr, xsrc, ysrc, zsrc, rsrc
c      common /p3dcom/ nx, ny, nz, nsrcs
c      common /p3dcom/ ncycles, nlevels, nstages, nsteps, ilevel
c      common /p3dcom/ nfinish
c      common /p3dcom/ maxvsize, nperdump
c      common /p3dcom/ cRunName, cOutSymb, cOutName
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

c=====================================================================72
      subroutine p3d_report_src(src, srcfac)
      implicit none
#include "p3d.h"
      integer i,j,k
      real*dsp src(nx,ny,nz), s2, srcfac

      s2 = 0.0d0
      do k = 1, nz
      do j = 1, ny
      do i = 1, nx
        s2 = s2 + src(i,j,k)**2
      enddo
      enddo
      enddo

      write (6,999) myrank, nx, ny, nz, s2, srcfac
999   format("p3d src: rank=",i1,"   Nxyz=",3i4,"    src2=",1p2e15.6)

      return
      end
c=====================================================================72
      subroutine set_flds(v)
      implicit none
#include "p3d.h"
      real*dsp v(maxvsize), qsum, qsums(100), src, qfac, qmax, srcfac
      integer ixoff, iyoff, izoff, i,j,k
      allocatable src(:,:,:)
      allocate(src(nx,ny,nz))

      do k = 1, nz
      do j = 1, ny
      do i = 1, nx
        src(i,j,k) = 0.0d0
      enddo
      enddo
      enddo

      if(ampk .gt. 0.0d0) call wave_src(src)

      do i = 1, nsrcs
        qsums(i) = qsum(i)
        qfac = qsrc(i) / qsums(i)
        do izoff = -1, 1
        do iyoff = -1, 1
        do ixoff = -1, 1
          call fill_src(i,ixoff,iyoff,izoff,src,qfac)
        enddo
        enddo
        enddo
      enddo

      call stopwatch_start("mg_set_source       ")
      srcfac = dx**2 / eps0
      call mg_set_source_3d(srcfac, src, v)
      call stopwatch_stop("mg_set_source       ")
      return
      end

c      k = nz / 2
c      do j = 1, ny
c      do i = 1, nx
c       qmax = max(qmax, src(i,j,k))
c      enddo
c      enddo
c      do j = 1, ny
c        write (6,999) (int(99.999*src(i,j,k)/qmax),i=1,nx)
c999     format(100i3)
c      enddo

c=====================================================================72
      subroutine fill_src(isrc,ixoff,iyoff,izoff,src,qfac)
      implicit none
#include "p3d.h"
      integer isrc,ixoff,iyoff,izoff, imin,imax,jmin,jmax,kmin,kmax
      integer i,j,k
      real*dsp src(nx,ny,nz), xs,ys,zs,qfacs, x,y,z,r2,q, qfac, rad2inv
      real*dsp x0, y0, z0

      xs = xsrc(isrc) + xsize * dble(ixoff)
      ys = ysrc(isrc) + ysize * dble(iyoff)
      zs = zsrc(isrc) + zsize * dble(izoff)
      x0 = xyzmin(1) + dx*float(nx*(itx-1))
      y0 = xyzmin(2) + dy*float(ny*(ity-1))
      z0 = xyzmin(3) + dz*float(nz*(itz-1))
      call iminmax(xs,rsrc(isrc),x0,dx,nx,imin,imax,1)
      call iminmax(ys,rsrc(isrc),y0,dy,ny,jmin,jmax,1)
      call iminmax(zs,rsrc(isrc),z0,dz,nz,kmin,kmax,1)
      if(imin .gt. imax .or. jmin .gt. jmax .or. kmin .gt. kmax) return

      rad2inv = 1.0d0 / rsrc(isrc)**2
      do k = kmin, kmax
      do j = jmin, jmax
      do i = imin, imax
        x = x0 + dx*(0.5d0 + dble(i))
        y = y0 + dy*(0.5d0 + dble(j))
        z = z0 + dz*(0.5d0 + dble(k))
        r2 = (x-xs)**2 + (y-ys)**2 + (z-zs)**2
        q = max(0.0d0, 1.0d0 - rad2inv * r2)
        src(i,j,k) = src(i,j,k) + q * qfac
      enddo
      enddo
      enddo

      return
      end




c=====================================================================72
      subroutine wave_src(src)
      implicit none
#include "p3d.h"
      real*dsp src(nx,ny,nz), x0,y0,z0, pi2,kx,ky,kz, x,y,z, amp
      integer i,j,k

      x0 = xyzmin(1) + dx*dble(nx*(itx-1))
      y0 = xyzmin(2) + dy*dble(ny*(ity-1))
      z0 = xyzmin(3) + dz*dble(nz*(itz-1))
      pi2 = 8.0d0 * atan(1.0d0)
      kx = dble(ksrc(1)) / (dx*dble(nx*ntx))
      ky = dble(ksrc(2)) / (dy*dble(ny*nty))
      kz = dble(ksrc(3)) / (dz*dble(nz*ntz))

      do k = 1, nz
      do j = 1, ny
      do i = 1, nx
        x = x0 + dx*(0.5d0 + dble(i-1))
        y = y0 + dy*(0.5d0 + dble(j-1))
        z = z0 + dz*(0.5d0 + dble(k-1))
        amp = ampk * sin(pi2*(x*kx+y*ky+z*kz+phasek))
        src(i,j,k) = src(i,j,k) + amp
      enddo
      enddo
      enddo

      return
      end

c=====================================================================72
      subroutine iminmax(x,r,x0,d,n, imin, imax, ilim)
#include "p3d.h"
      real*dsp x,r,x0,d, f
      integer n, imin, imax, ilim
      f = dble(n)
      imin = int(f+(x-r-x0)/d) - n - 1
      imax = int(f+(x+r-x0)/d) - n + 2

      if(ilim .ne. 1) return
      imin = max(imin, 1)
      imax = min(imax, n)
      
      return
      end
      
c=====================================================================72
      real*dsp function qsum(isrc)
      implicit none
#include "p3d.h"
      integer isrc, imin, imax, jmin, jmax, kmin, kmax, i,j,k
      real*dsp f, q, rad2inv, r2, x, y, z, qq(-1000:1000)

c      f = dble(nx)
c      imin = int(f+(xsrc(isrc)-rsrc(isrc)-xyzmin(1))/dx) - nx - 1
c      imax = int(f+(xsrc(isrc)+rsrc(isrc)-xyzmin(1))/dx) - nx + 2
c      f = dble(ny)
c      jmin = int(f+(ysrc(isrc)-rsrc(isrc)-xyzmin(2))/dy) - ny - 1
c      jmax = int(f+(ysrc(isrc)+rsrc(isrc)-xyzmin(2))/dy) - ny + 2
c      f = dble(nz)
c      kmin = int(f+(zsrc(isrc)-rsrc(isrc)-xyzmin(3))/dz) - nz - 1
c      kmax = int(f+(zsrc(isrc)+rsrc(isrc)-xyzmin(3))/dz) - nz + 2

      call iminmax(xsrc(isrc),rsrc(isrc),xyzmin(1),dx,nx,imin,imax,0)
      call iminmax(ysrc(isrc),rsrc(isrc),xyzmin(2),dy,ny,jmin,jmax,0)
      call iminmax(zsrc(isrc),rsrc(isrc),xyzmin(3),dz,nz,kmin,kmax,0)


c      do i = 1, nx
c        qq(i) = 0.0d0
c      enddo

      rad2inv = 1.0d0 / rsrc(isrc)**2
      qsum = 0.0d0
      do k = kmin, kmax
      do j = jmin, jmax
      do i = imin, imax
        x = xyzmin(1) + dx*(0.5d0 + dble(i))
        y = xyzmin(2) + dy*(0.5d0 + dble(j))
        z = xyzmin(3) + dz*(0.5d0 + dble(k))
        r2 = (x-xsrc(isrc))**2 + (y-ysrc(isrc))**2 + (z-zsrc(isrc))**2
        q = max(0.0d0, 1.0d0 - rad2inv * r2)
        qsum = qsum + q
c        qq(i) = q
      enddo
c      if(k .eq. nz/2) then
c      write (6,999) j, (int(9.999*qq(i)),i=1,nx)
c999   format("j=", i2, "   ", 128i2)
c      endif
      enddo
      enddo
      qsum = qsum * dx*dy*dz

      return
      end

c=====================================================================72
      subroutine set_defaults()
      implicit none
#include "p3d.h"
      cRunName = "ttt00000"
      eps0 = 1.0
      xsize = 1.0
      nx = 32
      ny = 32
      nz = 32
      nb = 1
      nlevels = 1
      nsteps(1) = 5
      niterate = 10
      nsrcs = 0
      ampk = 0.0d0
      sorfac1 = 0.95d0
      npen = 1000000
      ntx = 1
      nty = 1
      ntz = 1
      iverbose_trace = 0
      return
      end

c=====================================================================72
      subroutine read_inputs()
      implicit none
#include "p3d.h"
      character*64 ckey, ckey1
      character*256 cline, clines(1000)
      integer i, j, iline, nlines

      call set_defaults()
      call get_lines("p3d.in", nlines, clines)

      do iline = 1, nlines
      cline = clines(iline)
      ! write (6,*) "cline = ", trim(cline)
      if(len(trim(cline)) .ne. 0) then
      read (cline,*) ckey

      if(trim(ckey) .eq. "runname") read (cline,*) ckey1, cRunName
      if(trim(ckey) .eq. "eps0") read (cline,*) ckey1, eps0
      if(trim(ckey) .eq. "nxyz") read (cline,*) ckey1,nx,ny,nz
      if(trim(ckey) .eq. "ntxyz") read (cline,*) ckey1,ntx,nty,ntz
      if(trim(ckey) .eq. "nlevels") read (cline,*) ckey1,nlevels
      if(trim(ckey) .eq. "sorfac") read (cline,*) ckey1,sorfac1
      if(trim(ckey) .eq. "nsteps") read (cline,*) ckey1,nsteps(1)
      if(trim(ckey) .eq. "iterate") read (cline,*) ckey1,niterate
      if(trim(ckey) .eq. "xsize") read (cline,*) ckey1,xsize
      if(trim(ckey) .eq. "npencil") read (cline,*) ckey1,npen
      if(trim(ckey) .eq. "trace") read (cline,*) ckey1,iverbose_trace

      if(trim(ckey) .eq. "wavesrc") read (cline,*) ckey1,
     1   ampk, phasek, ksrc(1),ksrc(2),ksrc(3)

      if(trim(ckey) .eq. "source") then
        i = nsrcs + 1
        nsrcs = i
        read (cline,*) ckey1,qsrc(i),xsrc(i),ysrc(i),zsrc(i),rsrc(i)
      endif

      endif  ! end of if(len(trim(cline)) ...) block
      enddo  ! end of do iline ... loop

      if(nlevels .le. 0) call auto_set_nlevels()

      call set_3d_decomp()

      return
      end

c=====================================================================72
      subroutine auto_set_nlevels()
      implicit none
#include "p3d.h"
      integer nmaxdim

      nmaxdim = max(ntx*nx, nty*ny, ntz*nz)
      nlevels = int(log(dble(1+nmaxdim))/log(2.0d0)) - 1
      write (6,999) nlevels
999   format("# Auto set nlevels =",i3)

      return
      end
      
c=====================================================================72
      subroutine set_3d_decomp()
      implicit none
#include "p3d.h"
      integer i,j,k, itxn, ityn, itzn

      if(ntx*nty*ntz .ne. nranks) call myexit("ntxyz != nranks")

      itx = 1 + mod(myrank    ,ntx)
      ity = 1 + mod(myrank/ntx,nty)
      itz = 1 +     myrank/(ntx*nty)

      do k = -1, 1
      do j = -1, 1
      do i = -1, 1
        itxn = mod(itx+i+ntx-1,ntx)
        ityn = mod(ity+j+nty-1,nty)
        itzn = mod(itz+k+ntz-1,ntz)
        neighbor(i,j,k) = itxn + ntx*(ityn + nty*itzn)
      enddo
      enddo
      enddo

      return
      end

c=====================================================================72
      subroutine get_lines(cFile, nlines, clines)
      implicit none
      character*(*) cFile
      character*256 cline, clines(1000)
      integer i, j, nlines, nbytes

      j = 0
      open(unit=17,file=cFile,form="formatted",status="old",err=900)
10    j = j + 1
      do i = 1, 256
        clines(j)(i:i) = ' '
      enddo
      read (17,990,end=1000) clines(j)
990   format(a256)
      go to 10
900   continue
      write (6,*) 'error opening file:', trim(cFile)
1000  continue
      close(17)
      nlines = j - 1

      return
      end

c=====================================================================72
      subroutine dump_fields(v)
      implicit none
      include 'mpif.h'
#include "p3d.h"
      real*dsp v(maxvsize)
      integer ix,iy,iz,iter, irank, ierr
      real*4 vout
      allocatable vout(:,:,:)
      integer NHEAD
      parameter (NHEAD = 64*1024)
      real time4, dtime4
      real xyzranges(6)
      integer istep, i, nbytes, nvar
      integer*2 abuf(NHEAD)
      character*14 cDumpFile
      integer ndump
      common /dump_com/ ndump
      data ndump/0/

      if(crunName(1:4) .eq. "none") return

      cDumpFile        = cRunName(1:5) // "-0000-000"
      cDumpFile( 7: 7) = char(ichar('0') +     ndump/1000   )
      cDumpFile( 8: 8) = char(ichar('0') + mod(ndump/100,10))
      cDumpFile( 9: 9) = char(ichar('0') + mod(ndump/10 ,10))
      cDumpFile(10:10) = char(ichar('0') + mod(ndump    ,10))

      call calc_residual_par(v,1)

      call mg_get_iter(iter)
      call mg_get_nvar(nvar)

      do i = 1, nvar
        call mg_get_var_name(i, cOutSymb(i))
        cOutName(i) = cOutSymb(i)
      enddo

      time4  = float(iter)
      dtime4 = 1.0
      istep  = iter
      xyzranges(1) = xyzmin(1)
      xyzranges(2) = xyzmin(2)
      xyzranges(3) = xyzmin(3)
      xyzranges(4) = xyzmin(1) + xsize
      xyzranges(5) = xyzmin(2) + ysize
      xyzranges(6) = xyzmin(3) + zsize

      ! Set dump paramters & list of variables
      call set_dump_parameters(ntx,nty,ntz)
      do i = 1, nvar
        call set_dump_var(i,cOutSymb(i),cOutName(i),"real4",0.0,0.0)
      enddo

      ! Format meta-data (header info) into a buffer (abuf).
      nbytes = 2*NHEAD
      call format_adump_header(abuf,ndump,time4,dtime4,istep,xyzranges,
     1             nbytes,nvar, nx,ny,nz, itx,ity,itz, ntx,nty,ntz)

      ! Write header buffer to a file
      allocate(vout(nx,ny,nz))

      do irank = 0, nranks-1
        if(irank .eq. myrank) then
          open(unit=12,file=cDumpFile,form="binary",access="append")
          if(myrank .eq. 0) rewind(12)
          write (12) abuf
          do i = 1, nvar
            call mg_get_var_array_3d(i, vout, v)
            write (12) vout
          enddo
          flush(12)
          close(12)
        endif   ! (irank .eq. myrank)
        call MPI_BARRIER(MPI_COMM_WORLD, ierr)
      enddo   ! irank

      deallocate(vout)
      ndump = ndump + 1

      return
      end

c=====================================================================72
