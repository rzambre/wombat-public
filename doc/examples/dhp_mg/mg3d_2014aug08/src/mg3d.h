      real*dsp sorfac, sorfacs(100), bc_values(6), fcycle
      real*dsp epsc, epss(100)
      integer nx0, ny0, nz0, nrefine, maxlevels, nx,ny,nz, iter
      integer nb, is(100), maxv
      integer mycomm, myrank, neighbor(-1:1,-1:1,-1:1)
      integer nijkwork(1000), ijkminmax(2,3,7,1000)
      integer nfld, nvar, ifld, isrc, ires, nfieldsize(100)
      integer iisrc(20), iires(20), iter_type, ibcs(6,100)
      integer nstages0,ilevel0(10000),nsteps0(10000)
      integer iwgt, iverbose_trace, lev1_iter, matrix_type
      integer ixmax, iymax, ixmaxs(100), iymaxs(100)
      integer minres
      integer nxs(100),nys(100),nzs(100), npen, ntx,nty,ntz
      integer mtx(100), mty(100), mtz(100),nbr(-1:1,-1:1,-1:1,100)
      character*(8) cVarSymb(100)

      common /state/ sorfac, sorfacs, fcycle, epsc, epss
      common /state/ bc_values
      common /state/ nx0, ny0, nz0, nrefine, maxlevels, nx, ny,nz, iter
      common /state/ nb, is, maxv, ixmax, iymax, ixmaxs,iymaxs,minres
      common /state/ nxs,nys,nzs, npen, mycomm, myrank, neighbor
      common /state/ nijkwork, ijkminmax, ntx,nty,ntz,mtx,mty,mtz,nbr
      common /state/ nfld, nvar, ifld, isrc, ires,nfieldsize
      common /state/ iisrc, iires, iter_type, ibcs,iwgt,iverbose_trace
      common /state/ nstages0,ilevel0,nsteps0, lev1_iter, matrix_type
      common /state/ cVarSymb

      integer maxbuf
      parameter(maxbuf = 512*512)
      real*dsp bxpr(maxbuf), bxps(maxbuf), bxmr(maxbuf), bxms(maxbuf)
      real*dsp bypr(maxbuf), byps(maxbuf), bymr(maxbuf), byms(maxbuf)
      real*dsp bzpr(maxbuf), bzps(maxbuf), bzmr(maxbuf), bzms(maxbuf)
      integer hxmr,hxms,hxpr,hxps, hymr,hyms,hypr,hyps
      integer hzmr,hzms,hzpr,hzps
      integer nbytesx,nbytesy,nbytesz
      common /bdy_exchange/ bxpr, bxps, bxmr, bxms
      common /bdy_exchange/ bypr, byps, bymr, byms
      common /bdy_exchange/ bzpr, bzps, bzmr, bzms
      common /bdy_exchange/ hxmr,hxms,hxpr,hxps, hymr,hyms,hypr,hyps
      common /bdy_exchange/ hzmr,hzms,hzpr,hzps
      common /bdy_exchange/ nbytesx,nbytesy,nbytesz

      integer max1psize, maxnclct
      parameter(max1psize = 128*128*128)
      parameter(maxnclct  = 64)
      real*dsp b1p(max1psize), bnp(max1psize,maxnclct)
      integer iclctw(100),iclctp(64,100), nclctp(100), nclct(100)
      integer hclctp(maxnclct), hclctw
      common /collect/ b1p, bnp
      common /collect/ iclctw, iclctp, nclctp, nclct, hclctp, hclctw


