C=====================================================================72
      integer function iget_cval_from_key_file(cFile, cKey, cVal)
      character*(*) cFile, cKey, cVal
      character*64 ckey1
      character*256 cline

      n = len(cKey)
      nfill = 0
      open(unit=17, file=cFile, form="formatted", err=900)
10    continue
      do i = 1, 256
        cline(i:i) = ' '
      enddo
      read (17,990,end=1000) cline
990   format(a256)
      if(cline(1:n) .eq. ckey) then
        read (cline,*) ckey1, cVal
        nfill = 1
      endif
      go to 10

900   continue
      write (6,*) 'error opening file:', trim(cFile)
      iget_cval_from_key_file = -1
      return

1000  continue
      close(17)
      iget_cval_from_key_file = nfill

      return
      end


      integer function iget_ival_from_key_file(cFile, cKey, iVal)
      character*(*) cFile, cKey
      nfill = iget_fval_from_key_file(cFile, cKey, fVal)
      if(nfill .gt. 0) then
        if(fval .ge. 0.0) then
          iVal = int(fVal + 0.5)
        else
          iVal = int(fVal - 0.5)
        endif
      endif
      iget_ival_from_key_file = nfill
      return
      end
      
      integer function iget_dval_from_key_file(cFile, cKey, dVal)
      character*(*) cFile, cKey
      real*8 dVal
      nfill = iget_fval_from_key_file(cFile, cKey, fVal)
      if(nfill .gt. 0) dVal = fVal
      iget_dval_from_key_file = nfill
      return
      end

      integer function iget_fval_from_key_file(cFile, cKey, fVal)
      character*(*) cFile, cKey
      character*64 ckey1
      character*256 cline

      n = len(cKey)
      nfill = 0
      open(unit=17, file=cFile, form="formatted", err=900)
10    continue
      do i = 1, 256
        cline(i:i) = ' '
      enddo
      read (17,990,end=1000) cline
990   format(a256)
      if(cline(1:n) .eq. ckey) then
        read (cline,*) ckey1, fVal
        nfill = 1
      endif
      go to 10

900   continue
      write (6,*) 'error opening file:', trim(cFile)
      iget_fval_from_key_file = -1
      return

1000  continue
      close(17)
      iget_fval_from_key_file = nfill
      return
      end
