#include "config.h"

PROGRAM os

USE MPI
USE OMP_LIB
USE ISO_C_BINDING
IMPLICIT NONE

!### define a type for the slots/mailboxes ###
TYPE :: MsgSlots

    INTEGER(8) :: segment_start(26), segment_end(26), signal(26)
    LOGICAL :: ready_for_update, locked, inflight(26)

END TYPE MsgSlots

!### define a type for managing our neighbors ###
TYPE :: Neighbor

    INTEGER(4) :: lock
    INTEGER :: rank, myslot, realn, myloc(3), loc(3)
    LOGICAL :: locked

END TYPE

TYPE(c_ptr) :: cbuffer
REAL(8), POINTER, CONTIGUOUS :: buffer(:)
REAL(8), ALLOCATABLE :: patch(:,:)
REAL(8) :: t, fresult, fsignal, tend, t0, t1
INTEGER(8) :: totB
INTEGER(MPI_ADDRESS_KIND) :: offset, length
INTEGER :: nranks, myrank, mywrank, ierr, window, grid_comm, sizeof, bsize, i, j, k, nn, snn, sn, n, m, provided, &
     dims(3), maxdims, npx, npy, npz, ns, nstot, nsme, mycoords(3), rcoords(3), noffset(3), myslot, myneighbor, tid, nthreads, msglen
INTEGER(4) :: mylock, winlock
INTEGER, ALLOCATABLE :: shuf_neighbors(:)
TYPE(MsgSlots), ALLOCATABLE :: slots(:,:)
TYPE(Neighbor), ALLOCATABLE :: neighbors(:)
LOGICAL :: keepgoing, fflag, alld, periodic(3), mylocked, lib_thread_safe, two_sided_backend
CHARACTER(6) :: snpx, snpy, snpz, sns

PARAMETER(lib_thread_safe = .FALSE., two_sided_backend = .FALSE.)

CALL MPI_INIT_THREAD(MPI_THREAD_MULTIPLE, provided, ierr)
CALL MPI_COMM_SIZE(MPI_COMM_WORLD, nranks, ierr)
CALL MPI_COMM_RANK(MPI_COMM_WORLD, mywrank, ierr)

!### get the cart dimensions and neighborhood size ###
CALL GETARG(1, snpx)
CALL GETARG(2, snpy)
CALL GETARG(3, snpz)
CALL GETARG(4, sns)
READ(snpx,*) npx
READ(snpy,*) npy
READ(snpz,*) npz
READ(sns,*) ns

!### get a list of neighbors in the neighborhood ###
periodic = .TRUE.
dims     = (/npx, npy, npz/)
CALL MPI_CART_CREATE(MPI_COMM_WORLD, 3, dims, periodic, .TRUE., grid_comm, ierr)
IF (ierr .NE. MPI_SUCCESS) THEN

    IF (mywrank .EQ. 0) PRINT '("ERROR: failed to create Cartesian domain communicator")'
    CALL MPI_FINALIZE(ierr)

END IF

!### determine this rank's origin in world coordinates (note we mean the zone center at 1,1,1) ###
maxdims = 3
CALL MPI_COMM_RANK(grid_comm, myrank, ierr)
CALL MPI_CART_GET(grid_comm, maxdims, dims, periodic, mycoords, ierr)

!### determine all of the neighbors in the neighborhood ###
nstot = (2*ns + 1)**3
nsme  = 0
ALLOCATE(neighbors(nstot), shuf_neighbors(nstot))
n = 1
DO k = -ns, ns
    DO j = -ns, ns
        DO i = -ns, ns
            noffset = (/i, j, k/)
            noffset = noffset + mycoords
            CALL MPI_CART_RANK(grid_comm, noffset, shuf_neighbors(n), ierr)

            neighbors(n)%rank   = shuf_neighbors(n)
            neighbors(n)%locked = .FALSE.
            neighbors(n)%realn  = n
            neighbors(n)%loc    = noffset - mycoords
            CALL OMP_INIT_LOCK(neighbors(n)%lock)
            IF (neighbors(n)%rank .EQ. myrank) nsme = nsme + 1
            n = n + 1
        END DO
    END DO
END DO
mylocked = .FALSE.
CALL OMP_INIT_LOCK(mylock)
CALL OMP_INIT_LOCK(winlock)

!### for each neighbor, determine which of their slots we occupy ###
DO nn = 1, nstot
    
    !#### check if we already have a neighbor opening that tracks this rank ###
    IF (nn .GT. 1) THEN
        DO snn = 1, nn-1
            
            IF (neighbors(snn)%rank .EQ. neighbors(nn)%rank) THEN
                
                neighbors(nn)%realn = snn
                EXIT
                
            END IF

        END DO
    END IF

    CALL MPI_CART_COORDS(grid_comm, shuf_neighbors(nn), maxdims, rcoords, ierr)
    n = 1
    DO k = -ns, ns
        DO j = -ns, ns
            DO i = -ns, ns
                noffset = (/i, j, k/)
                noffset = noffset + rcoords
                CALL MPI_CART_RANK(grid_comm, noffset, myneighbor, ierr)
                IF (myneighbor .EQ. myrank) THEN
                    noffset = noffset - rcoords
                    IF (-noffset(1) .EQ. neighbors(nn)%loc(1) .AND. -noffset(2) .EQ. neighbors(nn)%loc(2) .AND. -noffset(3) .EQ. neighbors(nn)%loc(3)) THEN
                        neighbors(nn)%myslot = n
                        neighbors(n)%myloc   = noffset
                        !print *, "RANK", myrank, "is in slot", n, "for rank", neighbors(nn)%rank
                    END IF
                END IF
                n = n + 1
            END DO
        END DO
    END DO

END DO

!### get a shuffled list of neighbor slots (ideally we'd have a single seed and everyone computes a shuffled list that way, but this is simple for now) to reduce network contention ###
DO nn = 1, nstot
    shuf_neighbors(nn) = nn
END DO
CALL shuffle(shuf_neighbors(1:nstot), nstot, myrank)

!### 26 messages each of 512, 8 byte elements, send and recv (basically treat like a single Patch in this test code) ###
!### in the real code we'll know how many messages a particular Patch expects from which rank.  this is a trivialized version of that to demonstrate the idea ###
ALLOCATE(slots(nstot,2))
msglen = 8192
length = 2 * nstot * 26 * (1+msglen)

!### i'm told for Cray MPICH this is not necessary. but I think that is ONLY with CCE since we use tc_malloc.  For safety sake... ###
!ALLOCATE(buffer(length))
CALL MPI_ALLOC_MEM(length*INT(STORAGE_SIZE(t)/8,MPI_ADDRESS_KIND), MPI_INFO_NULL, cbuffer, ierr)
CALL C_F_POINTER(cbuffer, buffer, (/length/))

!$OMP PARALLEL
!$OMP MASTER
nthreads = OMP_GET_NUM_THREADS()
!$OMP END MASTER
!$OMP END PARALLEL
ALLOCATE(patch(26*msglen,nthreads))

!### set the rank and message segments for our slots ###
offset = 1
DO n = 1, nstot

    DO m = 1, 26

        slots(n,1)%signal(m) = offset
        offset = offset + 1
        slots(n,1)%segment_start(m) = offset
        slots(n,1)%segment_end(m)   = offset + msglen - 1
        offset = offset + msglen
        slots(n,1)%inflight(m) = .FALSE.

        slots(n,2)%signal(m) = offset
        offset = offset + 1
        slots(n,2)%segment_start(m) = offset
        slots(n,2)%segment_end(m)   = offset + msglen - 1
        offset = offset + msglen
        slots(n,2)%inflight(m) = .FALSE.

    END DO

    !### flag that the data in here can be moved to a Patch (in the real code buffers will be unpacked into ###
    slots(n,1)%ready_for_update = .FALSE.
    slots(n,1)%locked           = .FALSE.
    slots(n,2)%ready_for_update = .FALSE.
    slots(n,2)%locked           = .FALSE.

END DO

!### init the buffer ###
buffer = 0.0_8

! create the window
CALL MPI_WIN_CREATE(buffer, length*INT(STORAGE_SIZE(t)/8,MPI_ADDRESS_KIND), STORAGE_SIZE(t)/8, MPI_INFO_NULL, grid_comm, window, ierr)
CALL MPI_WIN_FENCE(0, window, ierr)

!### time loop ###
totB = 0
t    = 0.0_8
tend = 1.0_8
t0   = MPI_WTIME()

!$OMP PARALLEL PRIVATE(n, tid, ierr)
tid = OMP_GET_THREAD_NUM() + 1
DO WHILE (t .LE. tend)

    patch(:,tid) = myrank

    !### open shared locks to all of our neighbors ###
    !$OMP DO PRIVATE(nn, sn, snn, myneighbor, myslot)
    DO nn = 1, nstot

        sn         = shuf_neighbors(nn)
        snn        = neighbors(sn)%realn
        myneighbor = neighbors(sn)%rank
        IF (myneighbor .EQ. myrank) CYCLE
        myslot = neighbors(sn)%myslot

        CALL OMP_SET_LOCK(neighbors(snn)%lock)
        IF (.NOT. neighbors(snn)%locked) THEN
            IF (.NOT. lib_thread_safe) CALL OMP_SET_LOCK(winlock)
            CALL MPI_WIN_LOCK(MPI_LOCK_SHARED, myneighbor, 0, window, ierr)
            IF (.NOT. lib_thread_safe) CALL OMP_UNSET_LOCK(winlock)
            neighbors(snn)%locked  = .TRUE.
        END IF
        CALL OMP_UNSET_LOCK(neighbors(snn)%lock)
        slots(myslot,2)%locked = .TRUE.

    END DO
    !$OMP END DO

    !### just imagine we need to do the following loop many times to get all of the necessary messages exchanged in a time step ###
    !### we won't really be making use of this (will be a DO WHILE loop) since we expect only 26 messages.  this will be needed for when there are TONS of messages to work through ###
    !### basically each loop does a batch of messages and a batch of Patch updates for a domain ###
    DO n = 1, 24

        !### for each neighbor, send a signal that our send buffer is ready (will be a dynamic OpenMP loop) ###
        !$OMP DO PRIVATE(nn, sn, snn, myneighbor, myslot, fsignal, fresult, m, offset)
        DO nn = 1, nstot

            sn         = shuf_neighbors(nn)
            snn        = neighbors(sn)%realn
            myneighbor = neighbors(sn)%rank
            IF (myneighbor .EQ. myrank) CYCLE
            myslot = neighbors(sn)%myslot

            !### fill the send buffer with "boundary" data and signal that we're ready ###
            fsignal = 1.0_8
            DO m = 1, 26
            
                !### imagine we're packing in boundary data from a Patch with non-local neighbors here (and mark the send of the bound as resolved) ###
                buffer(slots(myslot,2)%segment_start(m):slots(myslot,2)%segment_end(m)) = patch((m-1)*msglen+1:(m-1)*msglen+msglen,tid)

                offset = slots(myslot,2)%signal(m) - 1
                IF (.NOT. lib_thread_safe) CALL OMP_SET_LOCK(winlock)
                CALL MPI_FETCH_AND_OP(fsignal, fresult, MPI_REAL8, myneighbor, offset, MPI_SUM, window, ierr)
                IF (.NOT. lib_thread_safe) CALL OMP_UNSET_LOCK(winlock)
                slots(myslot,2)%inflight(m) = .TRUE.

            END DO
        
        END DO
        !$OMP END DO

        !### ensure our neighbor signals have completed by flushing our comms to neighbors ###
        !$OMP DO PRIVATE(nn, sn, snn, myneighbor, myslot, m)
        DO nn = 1, nstot

            sn         = shuf_neighbors(nn)
            snn        = neighbors(sn)%realn
            myneighbor = neighbors(sn)%rank
            IF (myneighbor .EQ. myrank) CYCLE
            myslot = neighbors(sn)%myslot

            CALL OMP_SET_LOCK(neighbors(snn)%lock)
            IF (neighbors(snn)%locked) THEN
                IF (.NOT. lib_thread_safe) CALL OMP_SET_LOCK(winlock)
                CALL MPI_WIN_FLUSH(myneighbor, window, ierr)
                IF (.NOT. lib_thread_safe) CALL OMP_UNSET_LOCK(winlock)
                neighbors(snn)%locked  = .FALSE.
            END IF
            CALL OMP_UNSET_LOCK(neighbors(snn)%lock)
            slots(myslot,2)%locked = .FALSE.

            DO m = 1, 26
                slots(myslot,2)%inflight(m) = .FALSE.
            END DO

        END DO
        !$OMP END DO

        IF (two_sided_backend)  THEN
            !$OMP BARRIER
            !$OMP MASTER
            CALL MPI_WIN_FLUSH_ALL(window, ierr)
            !$OMP END MASTER
            !$OMP BARRIER
        END IF

        !### now we enter a polling loop where we look for ready signals and get the data when it's ready ###
        !### we'll always have at least as many messages in flight as we have neighbors (by the design below), but we can go up from there. ###
        !$OMP MASTER
        keepgoing = .TRUE.
        j         = 0
        !$OMP END MASTER
        !$OMP BARRIER
        DO WHILE (keepgoing)

            !### do an initial check for ready buffers on the remote side (will be a dynamic nowait OpenMP loop) ###
            !$OMP DO PRIVATE(nn, sn, snn, myneighbor, myslot, m, bsize, offset)
            DO nn = 1, nstot

                sn         = shuf_neighbors(nn)
                snn        = neighbors(sn)%realn
                myneighbor = neighbors(sn)%rank
                IF (myneighbor .EQ. myrank) CYCLE
                myslot = sn

                !### we must lock our own window so that we can read from the buffer ###
                CALL OMP_SET_LOCK(mylock)
                IF (.NOT. mylocked) THEN
                    IF (.NOT. lib_thread_safe) CALL OMP_SET_LOCK(winlock)
                    CALL MPI_WIN_LOCK(MPI_LOCK_SHARED, myrank, 0, window, ierr)
                    IF (.NOT. lib_thread_safe) CALL OMP_UNSET_LOCK(winlock)
                    mylocked = .TRUE.
                END IF
                CALL OMP_UNSET_LOCK(mylock)

                !### check if we got the ready signal for a boundary ###
                DO m = 1, 26

                    IF (buffer(slots(myslot,2)%signal(m)) .EQ. 1.0_8) THEN

                        !### flag that we have in-flight comms to the neighbor by "locking" it ###
                        IF (.NOT. slots(myslot,2)%locked) THEN
                            CALL OMP_SET_LOCK(neighbors(snn)%lock)
                            IF (.NOT. neighbors(snn)%locked) THEN
                                neighbors(snn)%locked = .TRUE.
                            END IF
                            CALL OMP_UNSET_LOCK(neighbors(snn)%lock)
                            slots(myslot,2)%locked = .TRUE.
                        END IF

                        !### initiate the GET if this message is not already in flight ###
                        IF (.NOT. slots(myslot,1)%inflight(m)) THEN

                            bsize  = slots(myslot,1)%segment_end(m) - slots(myslot,1)%segment_start(m) + 1
                            offset = slots(myslot,2)%segment_start(m) - 1
                            IF (.NOT. lib_thread_safe) CALL OMP_SET_LOCK(winlock)
                            CALL MPI_GET(buffer(slots(myslot,1)%segment_start(m)), bsize, MPI_REAL8, myneighbor, offset, bsize, MPI_REAL8, window, ierr)
                            IF (.NOT. lib_thread_safe) CALL OMP_UNSET_LOCK(winlock)
                            slots(myslot,1)%inflight(m) = .TRUE.

                            !$OMP CRITICAL
                            totB = totB + bsize * 8
                            j = j + 1
                            IF (j .GE. (nstot-nsme)*26) keepgoing = .FALSE.
                            !$OMP END CRITICAL

                        END IF
                              
                    END IF
                    
                END DO

                !### unlock our window ###
                CALL OMP_SET_LOCK(mylock)
                IF (mylocked) THEN
                    IF (.NOT. lib_thread_safe) CALL OMP_SET_LOCK(winlock)
                    CALL MPI_WIN_UNLOCK(myrank, window, ierr)
                    IF (.NOT. lib_thread_safe) CALL OMP_UNSET_LOCK(winlock)
                    mylocked = .FALSE.
                END IF
                CALL OMP_UNSET_LOCK(mylock)

                !if (myrank .eq. 0) print *, myrank, "POLL 1", j, (nstot-nsme)*26, myneighbor, myslot

            END DO
            !$OMP END DO
            
            !### do some of our local Patch boundary exchanging here ###

        END DO

        !### now we can loop back through neighbors completing the windows and signalling back that we're done with the GETs ###
        !### do an initial check for ready buffers on the remote side (will be a dynamic OpenMP loop) ###
        !$OMP DO PRIVATE(nn, sn, snn, myneighbor, myslot, fsignal, fresult, m, offset)
        DO nn = 1, nstot

            sn         = shuf_neighbors(nn)
            snn        = neighbors(sn)%realn
            myneighbor = neighbors(sn)%rank
            IF (myneighbor .EQ. myrank) CYCLE
            myslot = sn
            
            !### if we have an open window for the neighbor we'll try to close it now ###
            IF (slots(myslot,2)%locked) THEN
                
                CALL OMP_SET_LOCK(neighbors(snn)%lock)
                IF (neighbors(snn)%locked) THEN
                    IF (.NOT. lib_thread_safe) CALL OMP_SET_LOCK(winlock)
                    CALL MPI_WIN_FLUSH(myneighbor, window, ierr)
                    IF (.NOT. lib_thread_safe) CALL OMP_UNSET_LOCK(winlock)
                    neighbors(snn)%locked = .FALSE.
                END IF
                CALL OMP_UNSET_LOCK(neighbors(snn)%lock)
                slots(myslot,2)%locked = .FALSE.
                
                !### now we can mark the GETs from this rank as completed and copy their data into a Patch ###
                DO m = 1, 26
                    patch((m-1)*msglen+1:(m-1)*msglen+msglen,tid) = buffer(slots(myslot,1)%segment_start(m):slots(myslot,1)%segment_end(m))
                    slots(myslot,1)%inflight(m)          = .FALSE.
                    buffer(slots(myslot,2)%signal(m))    = 0.0_8  ! reset flag for that slot (ready for next GET)
                END DO

                myslot = neighbors(sn)%myslot
                
                !### flag that we have new in-flight comms to the neighbor by "locking" it ###
                CALL OMP_SET_LOCK(neighbors(snn)%lock)
                IF (.NOT. neighbors(snn)%locked) THEN
                    neighbors(snn)%locked = .TRUE.
                END IF
                CALL OMP_UNSET_LOCK(neighbors(snn)%lock)
                slots(myslot,2)%locked = .TRUE.
                
                !### signal back that we are done with our GETs ###
                fsignal = 1.0_8
                DO m = 1, 26
                    offset = slots(myslot,1)%signal(m) - 1
                    IF (.NOT. lib_thread_safe) CALL OMP_SET_LOCK(winlock)
                    CALL MPI_FETCH_AND_OP(fsignal, fresult, MPI_REAL8, myneighbor, offset, MPI_SUM, window, ierr)
                    IF (.NOT. lib_thread_safe) CALL OMP_UNSET_LOCK(winlock)
                    slots(myslot,1)%inflight(m) = .TRUE.
                END DO
                
            END IF
            
        END DO
        !$OMP END DO

        !### ensure our neighbor signals have completed by flushing the neighbors ###
        !$OMP DO PRIVATE(nn, sn, snn, myneighbor, myslot, m)
        DO nn = 1, nstot
            
            sn         = shuf_neighbors(nn)
            snn        = neighbors(sn)%realn
            myneighbor = neighbors(sn)%rank
            IF (myneighbor .EQ. myrank) CYCLE
            myslot = neighbors(sn)%myslot
            
            CALL OMP_SET_LOCK(neighbors(snn)%lock)
            IF (neighbors(snn)%locked) THEN
                IF (.NOT. lib_thread_safe) CALL OMP_SET_LOCK(winlock)
                CALL MPI_WIN_FLUSH(myneighbor, window, ierr)
                IF (.NOT. lib_thread_safe) CALL OMP_UNSET_LOCK(winlock)
                neighbors(snn)%locked = .FALSE.
            END IF
            CALL OMP_UNSET_LOCK(neighbors(snn)%lock)
            
            DO m = 1, 26
                slots(myslot,1)%inflight(m) = .FALSE.
            END DO
            
        END DO
        !$OMP END DO

        !### now wait until we have been signalled that all of our neighbors' GETs are done (means at the next loop we can go ahead an pack buffers again) ###
        !$OMP MASTER
        keepgoing = .TRUE.
        j         = 0
        !$OMP END MASTER
        !$OMP BARRIER
        DO WHILE (keepgoing)

            !### check to see if we've been signalled that the neighbor has completed their GET ###
            CALL OMP_SET_LOCK(mylock)
            IF (.NOT. mylocked) THEN
                IF (.NOT. lib_thread_safe) CALL OMP_SET_LOCK(winlock)
                CALL MPI_WIN_LOCK(MPI_LOCK_SHARED, myrank, 0, window, ierr)
                IF (.NOT. lib_thread_safe) CALL OMP_UNSET_LOCK(winlock)
                mylocked = .TRUE.
            END IF
            CALL OMP_UNSET_LOCK(mylock)

            !### do a check for ready buffers on the remote side (will be a dynamic nowait OpenMP loop) ###
            !$OMP DO PRIVATE(nn, sn, snn, myneighbor, myslot, m)
            DO nn = 1, nstot
            
                sn         = shuf_neighbors(nn)
                myneighbor = neighbors(sn)%rank
                IF (myneighbor .EQ. myrank) CYCLE
                myslot = sn

                !### check if we got the ready signal for a boundary ###
                DO m = 1, 26
                    IF (buffer(slots(myslot,1)%signal(m)) .EQ. 1.0_8) THEN
                        buffer(slots(myslot,1)%signal(m)) = 0.0_8
                        !$OMP CRITICAL
                        j = j + 1
                        !$OMP END CRITICAL
                    END IF
                END DO

            END DO
            !$OMP END DO

            !### unlock our window ###
            CALL OMP_SET_LOCK(mylock)
            IF (mylocked) THEN
                IF (.NOT. lib_thread_safe) CALL OMP_SET_LOCK(winlock)
                CALL MPI_WIN_UNLOCK(myrank, window, ierr)
                IF (.NOT. lib_thread_safe) CALL OMP_UNSET_LOCK(winlock)
                mylocked = .FALSE.
            END IF
            CALL OMP_UNSET_LOCK(mylock)

            !### we need to start a new comm loop once all of our sends
            !$OMP CRITICAL
            IF (j .GE. (nstot-nsme)*26) keepgoing = .FALSE.
            !$OMP END CRITICAL
            !$OMP BARRIER

            !if (myrank .eq. 0) print *, myrank, "POLL 2", j, (nstot-nsme)*26, myneighbor, myslot

        END DO

    END DO

    !### our neighbor window locks are still open, but comms are done.  set the flags that they are open so that we can close them properly ###
    !$OMP DO PRIVATE(nn, sn, snn, myneighbor, myslot)
    DO nn = 1, nstot

        sn         = shuf_neighbors(nn)
        snn        = neighbors(sn)%realn
        myneighbor = neighbors(sn)%rank
        IF (myneighbor .EQ. myrank) CYCLE
        myslot = neighbors(sn)%myslot
    
        !### flag that we have new in-flight comms to the neighbor by "locking" it ###
        CALL OMP_SET_LOCK(neighbors(snn)%lock)
        IF (.NOT. neighbors(snn)%locked) THEN
            neighbors(snn)%locked = .TRUE.
        END IF
        CALL OMP_UNSET_LOCK(neighbors(snn)%lock)
        slots(myslot,2)%locked = .TRUE.

    END DOunpack_all_patch_bounds_local
    !$OMP END DO

    ! openclose shared locks to all of our neighbors
    !$OMP DO PRIVATE(nn, sn, snn, myneighbor, myslot)
    DO nn = 1, nstot

        sn         = shuf_neighbors(nn)
        snn        = neighbors(sn)%realn
        myneighbor = neighbors(sn)%rank
        IF (myneighbor .EQ. myrank) CYCLE
        myslot = neighbors(sn)%myslot

        CALL OMP_SET_LOCK(neighbors(snn)%lock)
        IF (neighbors(snn)%locked) THEN
            IF (.NOT. lib_thread_safe) CALL OMP_SET_LOCK(winlock)
            CALL MPI_WIN_UNLOCK(myneighbor, window, ierr)
            IF (.NOT. lib_thread_safe) CALL OMP_UNSET_LOCK(winlock)
            neighbors(snn)%locked  = .FALSE.
        END IF
        CALL OMP_UNSET_LOCK(neighbors(snn)%lock)
        slots(myslot,2)%locked = .FALSE.

    END DO
    !$OMP END DO

    !$OMP MASTER
    IF (myrank .EQ. 0) PRINT *, "TIME ", t, "FINISHED"
    t = t + 0.1_8
    !$OMP END MASTER
    !$OMP BARRIER
    
END DO
!$OMP END PARALLEL

CALL MPI_WIN_FENCE(0, window, ierr)
t1 = MPI_WTIME()
CALL MPI_WIN_FREE(window, ierr)

CALL MPI_FREE_MEM(buffer, ierr)
DEALLOCATE(slots)

IF (myrank .EQ. 0) print *, (totB / (1024**3*(t1-t0))), "GB/s"
CALL MPI_FINALIZE(ierr)

CONTAINS

!### take an integer list and shuffle the contents ###
SUBROUTINE shuffle(list, n, seed)

    INTEGER, CONTIGUOUS, INTENT(INOUT) :: list(:)
    INTEGER, INTENT(IN) :: n, seed
    INTEGER :: i, j, tmp, ns
    INTEGER, ALLOCATABLE :: rseed(:)
    REAL :: vv

    CALL RANDOM_SEED(SIZE = ns)
    ALLOCATE(rseed(ns))

    rseed = (/ (seed, i = 1, ns) /)

    CALL RANDOM_SEED(PUT = rseed)
    DO i = n-1, 1, -1

        CALL RANDOM_NUMBER(vv)
        j         = MOD(INT(vv*n), i)
        tmp       = list(j+1)
        list(j+1) = list(i+1)
        list(i+1) = tmp

    END DO
    DEALLOCATE(rseed)

END SUBROUTINE shuffle

END PROGRAM os

